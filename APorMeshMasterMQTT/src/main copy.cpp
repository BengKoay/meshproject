// // Code explanation

// // 1. read preferences for node, ssid, pass, isMesh
// // 2. read AP or STA mode, isMESH true = mesh, isMESH false = AP
// // 3. if STA mode, attempt connect Mesh Network
// // 3.1. for 60 tries, will attempt connect Mesh Network, start counter "meshNotConnectedCounter"
// // 3.1.1. if after 60 tries still unable to connect, will write isMESH false,
// // 3.2. if between 60 tries , Mesh Network is connected, counter "meshNotConnectedCounter" resets
// // 4. if AP mode, launch AP
// // 4.1. if not connected for 60 seconds, will only set isMESH = true after reboot
// // 4.2. will only set isMESH = true after reboot

// // to work on:
// // Done - set AP mode on purpose and not failover, during isMesh
// // Done - set timeout after AP?
// // progmem
// // Done - asynctcp
// //

// // to do

// // Done 1. LED light up when start
// // Done 2. name change when updated, version-ing
// // Done 3. name of sensor
// // Done 4. name of dryer
// // Done 5. AP mode for 3 min at start?, after 3 min reboot to Mesh

// // naming - version_sensorname_dryername_MAC

// // version
// // 1.4 - using ref to change to int for backend
// // 1.5 - setRoot
// // 1.6 - broadcast mesh msg, every 5 sec

// // // 3.2.2 - stable code
// // // 3.3.0 - reboot mesh if not connected to rootID
// // // 3.3.2 - extend time reboot 5 minutes mesh 3.3.0
// // // 3.3.3 - bug fix AP
// // // 3.3.4 - setRoot code
// // // 3.3.5 - reboot mesh if no detect setRoot
// #include <Arduino.h>

// // #include <ESP8266WiFi.h>
// // #include <ESP8266HTTPClient.h>
// // #include <ESP8266WebServer.h>

// #include <WiFi.h>
// // #include <WebServer.h>
// // #include <EEPROM.h>
// #include <AsyncTCP.h>
// #include <ESPAsyncWebServer.h>
// #include <AsyncElegantOTA.h>
// // #include <painlessMesh.h>
// // #include "namedMesh.h"
// // #include "SHT31.h"

// #include <Preferences.h>

// // define

// // some gpio pin that is connected to an LED...
// // on my rig, this is 5, change to the right number of your LED.
// #define LED 2 // GPIO number of connected LED, ON ESP-12 IS GPIO2
// // #define SHT31_ADDRESS 0x44 // define SHT address

// #define BLINK_PERIOD 3000  // milliseconds until cycle repeat
// #define BLINK_DURATION 100 // milliseconds LED is on for
// // #define MESH_PORT 5555

// // Variables
// Preferences preferences;
// int i = 0;
// int statusCode;
// // const char *ssid = "";       // not used?
// // const char *passphrase = ""; // not used?
// // String st;
// // String content;
// // String enodeName;
// // String esid;
// // String epass = "";

// // variable for init
// bool isMesh = false; // isMESH true = mesh, isMESH false = AP
// // bool isMesh;
// int meshNotConnectedCounter = 0;
// int apNotConnectedCounter = 0;
// int APcounter = 0;
// // // unsigned long rootID = 3046393153; // A13
// // unsigned long rootID = 3046322341; // A13.
// // unsigned long rootID = 3046321129; // A11
// // unsigned long rootID = 3046391301; // A11.

// // variable for AP

// String versionName = "M"; // version-ing
// String meshSSID;
// String meshPass;
// String nodeName = "Master"; // Name needs to be unique          //--------------------- root will recognize slave by its nodeName
// int ref;
// String uniq;

// const char *PARAM_NODENAME = "param_nodename";
// const char *PARAM_SSID = "param_ssid";
// const char *PARAM_PASS = "param_pass";

// const char *PARAM_INPUT_1 = "output";
// const char *PARAM_INPUT_2 = "state";

// // variable for mesh
// // Scheduler userScheduler; // to control your personal task
// // namedMesh mesh;
// bool calc_delay = false;
// // SimpleList<uint32_t> nodes;

// // variable for sensor

// // SHT31 sht; // declare sht variable

// // maybe use blink without delay concept

// const int ledPin = 2;
// int ledState = LOW;

// // declare currentMillis huge value to make sure wifi can connect
// // unsigned long currentMillis = 10000;

// // constants won't change:
// const long interval = 1000; // interval at which to blink (milliseconds)

// const long intervalCheckMesh = 1000; // interval at which to blink (milliseconds)
// const long intervalCheckAP = 60000;  // interval at which to blink (milliseconds)
// const long intervalOneMinute = 60000;

// unsigned long previous = 0;

// // Function Declaration

// void setupMesh();
// // bool testWifi(void);
// // void launchWeb(void);
// // void setupAP(void);

// // void createWebServer();

// // prototype

// String processor(const String &var);
// String outputState(int output);
// void APmode();

// void checkAPConnection();

// // Establishing Local server at port 80 whenever required
// //  ESP8266WebServer server(80);
// //  WebServer server(80);
// AsyncWebServer server(80);

// // Task to blink the number of nodes
// bool onFlag = false;

// // HTML

// const char index_html[] PROGMEM = R"rawliteral(
// <!DOCTYPE HTML><html>
// <head>
//   <title>ESP Web Server</title>
//   <meta name="viewport" content="width=device-width, initial-scale=1">
//   <link rel="icon" href="data:,">
//   <style>
//     html {font-family: Arial; display: inline-block; text-align: center;}
//     h2 {font-size: 3.0rem;}
//     p {font-size: 3.0rem;}
//     body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
//     .switch {position: relative; display: inline-block; width: 120px; height: 68px}
//     .switch input {display: none}
//     .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
//     .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
//     input:checked+.slider {background-color: #b30000}
//     input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
//   </style>
// </head>
// <body>
//   <h2>ESP Web Server</h2>
//   %BUTTONPLACEHOLDER%
// <script>function toggleCheckbox(element) {
//   var xhr = new XMLHttpRequest();
//   if(element.checked){ xhr.open("GET", "/blink?output="+element.id+"&state=1", true); }
//   else { xhr.open("GET", "/blink?output="+element.id+"&state=0", true); }
//   xhr.send();
// }
// </script>
// </body>
// <br>
// <button onclick="location.href = '/update';" id="myButton" class="float-left submit-button" >update</button>
// <br>
// <button onclick="location.href = '/rebootEsp';" id="myButton" class="float-left submit-button" >rebootEsp</button>
// <br>
// <form action="/setMeshDataNameSSIDPass">
//     param_nodename: <input type="text" name="param_nodename">

//   <br>
//     param_ssid: <input type="text" name="param_ssid">

//   <br>
//     param_pass: <input type="text" name="param_pass">
//   <br>
//     <input type="submit" value="Submit">
//   </form>
// </html>
// )rawliteral";

// void setup()
// {
//   // 1. read EEPROM for node, ssid, pass
//   // 2. read AP or STA mode
//   // 3. if STA mode, attempt connect wifi
//   // 4. if AP mode, launch AP

//   // read EEPROM
//   Serial.begin(115200); // Initialising if(DEBUG)Serial Monitor
//   Serial.println();

//   //---------------------------------------- Read preferences for ssid and pass
//   preferences.begin("credentials", false);

//   // ref = preferences.getInt("nodeName", 0);

//   meshSSID = preferences.getString("meshSSID", "");
//   meshPass = preferences.getString("meshPass", "");
//   isMesh = preferences.getBool("isMesh", "");

//   if (meshSSID == "" || meshPass == "")
//   {
//     Serial.println("No values saved for ssid or password");
//   }
//   else
//   {
//     Serial.println("Print  ssid or password");
//     Serial.println(meshSSID);
//     Serial.println(meshPass);
//     // Serial.println(ref);
//   }

//   //---------------------------------------- End Read preferences for ssid and pass

//   //---------------------------------------- Init wifi
//   if (isMesh)
//   {
//     // sht.begin(SHT31_ADDRESS);
//     // sht.requestData();
//     // pinMode(LED, OUTPUT);
//     // setupMesh();
//     // Serial.print("setupMesh mesh");
//   }
//   else
//   {
//     APmode();
//   }
// }

// void loop()
// {

//   if (isMesh)
//   {
//     // mesh.update();
//     // digitalWrite(LED, !onFlag);

//     // getRootIDLoop();
//   }
//   else
//   {
//     checkAPConnection();

//     AsyncElegantOTA.loop();
//   }
// }

// void APmode()
// {
//   //---------------------------------------- Set preferences for AP

//   pinMode(ledPin, OUTPUT);
//   digitalWrite(ledPin, HIGH); // light up when start
//   byte mac[6];
//   WiFi.macAddress(mac);
//   uniq += versionName;
//   uniq += "_";
//   uniq += ref;
//   uniq += "_";
//   uniq += meshSSID;
//   uniq += "_";
//   uniq += String(mac[0], HEX);
//   uniq += String(mac[1], HEX);
//   uniq += String(mac[2], HEX);
//   uniq += String(mac[3], HEX);
//   uniq += String(mac[4], HEX);
//   uniq += String(mac[5], HEX);
//   // WiFi.mode(WIFI_STA);
//   // WiFi.begin(ssid, password);
//   WiFi.softAP(uniq.c_str(), "");
//   Serial.println("");

//   //---------------------------------------- End Set preferences for AP

//   // Wait for connection
//   // while (WiFi.status() != WL_CONNECTED)
//   Serial.println("");
//   Serial.print("Connected to ");
//   Serial.println(meshSSID);
//   Serial.print("IP address: ");
//   Serial.println(WiFi.localIP());

//   server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
//             { request->send_P(200, "text/html", index_html, processor); });

//   // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
//   server.on("/blink", HTTP_GET, [](AsyncWebServerRequest *request)
//             {
//               String inputMessage1;
//               String inputMessage2;
//               // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
//               if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
//               {
//                 inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
//                 inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
//                 digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
//               }
//               else
//               {
//                 inputMessage1 = "No message sent";
//                 inputMessage2 = "No message sent";
//               }
//               Serial.print("GPIO: ");
//               Serial.print(inputMessage1);
//               Serial.print(" - Set to: ");
//               Serial.println(inputMessage2);
//               request->send(200, "text/plain", "OK"); });

//   // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
//   server.on("/setMeshDataNameSSIDPass", HTTP_GET, [](AsyncWebServerRequest *request)
//             {
//               String inputMessage;
//               String inputParam;
//               // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
//               if (request->hasParam(PARAM_NODENAME))
//               {
//                 inputMessage = request->getParam(PARAM_NODENAME)->value();
//                 inputParam = PARAM_NODENAME;
//               }
//               // GET input2 value on <ESP_IP>/get?input2=<inputMessage>
//               else if (request->hasParam(PARAM_SSID))
//               {
//                 inputMessage = request->getParam(PARAM_SSID)->value();
//                 inputParam = PARAM_SSID;
//               }
//               // GET input3 value on <ESP_IP>/get?input3=<inputMessage>
//               else if (request->hasParam(PARAM_PASS))
//               {
//                 inputMessage = request->getParam(PARAM_PASS)->value();
//                 inputParam = PARAM_PASS;
//               }
//               else
//               {
//                 inputMessage = "No message sent";
//                 inputParam = "none";
//               }

//               if (request->hasParam(PARAM_NODENAME))
//               {
//                 // int refInt = ;
//                 preferences.putInt("nodeName", request->getParam(PARAM_NODENAME)->value().toInt());
//               }
//               if (request->hasParam(PARAM_SSID))
//                 preferences.putString("meshSSID", request->getParam(PARAM_SSID)->value());
//               if (request->hasParam(PARAM_PASS))
//                 preferences.putString("meshPass", request->getParam(PARAM_PASS)->value());

//               // preferences.end();
//               Serial.println(inputMessage);
//               request->send(200, "text/plain", "OK"); });
//   // request->send(200, "text/html", "HTTP GET request sent to your ESP on input field (" + inputParam + ") with value: " + inputMessage + "<br><a href=\"/\">Return to Home Page</a>"); });
//   server.on("/redirect/internal", HTTP_GET, [](AsyncWebServerRequest *request)
//             { request->redirect("/"); });
//   server.on("/rebootEsp", HTTP_GET, [](AsyncWebServerRequest *request)
//             { ESP.restart(); });

//   AsyncElegantOTA.begin(&server); // Start ElegantOTA
//   server.begin();
//   Serial.println("HTTP server started");
// }

// void checkAPConnection()
// {
//   unsigned long previousMillis = 0;
//   while ((WiFi.softAPgetStationNum() == 0))
//   {
//     // delay(500);

//     // blink without delay code
//     unsigned long currentMillis = millis();
//     if (currentMillis - previousMillis > interval)
//     {
//       previousMillis = currentMillis;
//       Serial.print(".");
//       // if (ledState == LOW)
//       //   // ledState = HIGH;

//       //   Serial.print(".");
//       // else
//       //   // ledState = LOW;

//       //   Serial.print("_");
//       // digitalWrite(ledPin, ledState);
//     }
//     if (currentMillis > intervalOneMinute)
//     {
//       preferences.putBool("isMesh", true);
//       ESP.restart();
//     }
//   }
// }

// String processor(const String &var)
// {
//   // Serial.println(var);
//   if (var == "BUTTONPLACEHOLDER")
//   {
//     String buttons = "";
//     buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
//     return buttons;
//   }
//   return String();
// }

// String outputState(int output)
// {
//   if (digitalRead(output))
//   {
//     return "checked";
//   }
//   else
//   {
//     return "";
//   }
// }