// Code explanation

// 1. read preferences for node, ssid, pass, isMesh
// 2. read AP or STA mode, isMESH true = mesh, isMESH false = AP
// 3. if STA mode, attempt connect Mesh Network
// 3.1. for 60 tries, will attempt connect Mesh Network, start counter "meshNotConnectedCounter"
// 3.1.1. if after 60 tries still unable to connect, will write isMESH false,
// 3.2. if between 60 tries , Mesh Network is connected, counter "meshNotConnectedCounter" resets
// 4. if AP mode, launch AP
// 4.1. if not connected for 60 seconds, will only set isMESH = true after reboot
// 4.2. will only set isMESH = true after reboot

// to work on:
// Done - set AP mode on purpose and not failover, during isMesh
// Done - set timeout after AP?
// progmem
// Done - asynctcp
//

// to do

// Done 1. LED light up when start
// Done 2. name change when updated, version-ing
// Done 3. name of sensor
// Done 4. name of dryer
// Done 5. AP mode for 3 min at start?, after 3 min reboot to Mesh

// naming - version_sensorname_dryername_MAC

// version
// 1.4 - using ref to change to int for backend
// 1.5 - setRoot
// 1.6 - broadcast mesh msg, every 5 sec

// // 3.2.2 - stable code
// // 3.3.0 - reboot mesh if not connected to rootID
// // 3.3.2 - extend time reboot 5 minutes mesh 3.3.0
// // 3.3.3 - bug fix AP
// // 3.3.4 - setRoot code
// // 3.3.5 - reboot mesh if no detect setRoot
#include <Arduino.h>
#include <pgmspace.h>

// #include <SPI.h>
// #include <Ethernet.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

// #include <ESP8266WiFi.h>
// #include <ESP8266HTTPClient.h>
// #include <ESP8266WebServer.h>

#include <WiFi.h>
// #include <WebServer.h>
// #include <EEPROM.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
// #include <painlessMesh.h>
// #include "namedMesh.h"
// #include "SHT31.h"

#include <Preferences.h>

// define

// some gpio pin that is connected to an LED...
// on my rig, this is 5, change to the right number of your LED.
#define LED 2 // GPIO number of connected LED, ON ESP-12 IS GPIO2
// #define SHT31_ADDRESS 0x44 // define SHT address

#define BLINK_PERIOD 3000  // milliseconds until cycle repeat
#define BLINK_DURATION 100 // milliseconds LED is on for
// #define MESH_PORT 5555

// Variables
Preferences preferences;
int i = 0;
int statusCode;
// const char *ssid = "";       // not used?
// const char *passphrase = ""; // not used?
// String st;
// String content;
// String enodeName;
// String esid;
// String epass = "";

// variable for init
bool isMesh = false; // isMESH true = mesh, isMESH false = AP
// bool isMesh;
int meshNotConnectedCounter = 0;
int apNotConnectedCounter = 0;
int APcounter = 0;
// // unsigned long rootID = 3046393153; // A13
// unsigned long rootID = 3046322341; // A13.
// unsigned long rootID = 3046321129; // A11
// unsigned long rootID = 3046391301; // A11.

// variable for AP

String versionName = "M"; // version-ing
String meshSSID;
String meshPass;
String nodeName = "Master"; // Name needs to be unique          //--------------------- root will recognize slave by its nodeName
int ref;
String uniq;

const char *PARAM_NODENAME = "param_nodename";
const char *PARAM_SSID = "param_ssid";
const char *PARAM_PASS = "param_pass";

const char *PARAM_INPUT_1 = "output";
const char *PARAM_INPUT_2 = "state";

// variable for mesh
// Scheduler userScheduler; // to control your personal task
// namedMesh mesh;
bool calc_delay = false;
// SimpleList<uint32_t> nodes;

// variable for sensor

// SHT31 sht; // declare sht variable

// maybe use blink without delay concept

const int ledPin = 2;
int ledState = LOW;

// declare currentMillis huge value to make sure wifi can connect
// unsigned long currentMillis = 10000;

// constants won't change:
// const long interval = 1000; // interval at which to blink (milliseconds)

const long intervalCheckMesh = 1000; // interval at which to blink (milliseconds)
const long intervalCheckAP = 60000;  // interval at which to blink (milliseconds)
const long intervalOneMinute = 60000;

unsigned long previous = 0;

// Function Declaration

void setupMesh();
// bool testWifi(void);
// void launchWeb(void);
// void setupAP(void);

// void createWebServer();

// prototype

String processor(const String &var);
String outputState(int output);
void APmode();

void checkAPConnection();

// Establishing Local server at port 80 whenever required
//  ESP8266WebServer server(80);
//  WebServer server(80);
AsyncWebServer server(80);

// Task to blink the number of nodes
bool onFlag = false;

// HTML

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px}
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.slider {background-color: #b30000}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>ESP Web Server</h2>
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/blink?output="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/blink?output="+element.id+"&state=0", true); }
  xhr.send();
}
</script>
</body>
<br>
<button onclick="location.href = '/update';" id="myButton" class="float-left submit-button" >update</button>
<br>
<button onclick="location.href = '/rebootEsp';" id="myButton" class="float-left submit-button" >rebootEsp</button>
<br>
<form action="/setMeshDataNameSSIDPass">
    param_nodename: <input type="text" name="param_nodename">

  <br>
    param_ssid: <input type="text" name="param_ssid">

  <br>
    param_pass: <input type="text" name="param_pass">
  <br>
    <input type="submit" value="Submit">
  </form>
</html>
)rawliteral";
////////////////////////////////////////////////////////// MQTT code

// AWS MQTT cert

// CA cert
static const char cacert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF
ADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6
b24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL
MAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv
b3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj
ca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM
9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw
IFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6
VOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L
93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm
jgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA
A4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI
U5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs
N+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv
o/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU
5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy
rqXRfboQnoZsG4q5WTP468SQvvG5
-----END CERTIFICATE-----
)EOF";

// client cert

static const char client_cert0[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUFr5Rn5BmM4zj07aulrvMaIj7pGwwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIxMTIxNDE0Mjky
MFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL9lGqKidlCk42gMKQGB
c9dZfS9XV0w91XVJzAi3NTncKNgPS5PVpfEEDCP5mGykVLWAdpuxHmqKJgajr082
t79M94031SHy2QlmcsyN/WvHEHYpwVWaT07dSwbiYUPtlr+fZfLmB604xMhMQJ5b
xKK69TdTJ3SPApZMjU3llWSOAwICTUo4Ss+/YYsKDyRdk95G5Ak28XYnjdl7+oJu
r2SZCRAD68PIelGxxqYq+MjoJBB2OWZpMcIVwwgoHGkauuElhJwwvzHjkU7XQZLv
k94N541dO+cwAtWkOHXSnKJp12SxagTowmPoTxEWh1GWX6p96YXdKPxuRHXGMw+D
GuUCAwEAAaNgMF4wHwYDVR0jBBgwFoAU2EBtyb8wQKlhiZXHgKLzgPB+C0UwHQYD
VR0OBBYEFAOWO+kTJy/rxZMwaDuj+tn/ELdJMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQAtov44sC3l2UcNGlJqIxY62K4o
ZKieWL4QGSZTb2BLp0G2tC2v/CO4wfvMPUpNIZM2mGzPdgZNU3coDTPHngtfmSyb
GS3aABWcAirgYztMCbNfhP7d4EyEfcq9tly0PzmBPXD/GVEYq7lfCskj3yTvr3KA
WBCyKSy7s+O6t3pw2S+OP4adpAM4quUG5DPZ8+w6apvQ/ElkqEY2VjKI3SSFI9Tm
uFjQyiIL4NM+RRU3E8KJAqAH3px1fcKNmijIGW0C2laGG3r3hQhGvw9Q0DI6hLU/
MQbmx5RRxygDRmWDNIW25isRLlrq2p/rlAZvsqJIwSXS4bgCNUIYHo0l4Utw
-----END CERTIFICATE-----
)KEY";

static const char client_cert1[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAMNdvqNAePmAgHZgkWaJr4cpMFeaMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMTEyMTYwOTU5
MjdaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDLgBZz+bCUwWeuGAUE
290+WAGXx//DYrVVP1Brs6rqKT/De3IkYzzGh2Y8z/zUM5aFQVufla7aM4A3DCIO
/f44/t37HCu6dAjfTaw/1Wj3cRVXBEUe8k43P6dg7ZytZ10uhHCEHvUjNSxfrbi4
m0AtxCSt7pqccLdevpgGMAHoWt1iUfry3qxSY3qpXmq3RX6tR6iIGbZthctswEEc
vk6nS2ascDZ2I3DaKW/3N6zkXIi8t4xPFyxIx0R8o4L5utfBcpzCt0h7q3JJulXL
TKDbImtUx8YiOUCvTG14Ybq4WuIzZYU4kniUUkMTXAc0c2mDpIM3i8NBUL0+tr4o
adYhAgMBAAGjYDBeMB8GA1UdIwQYMBaAFNhtM3y4ojeycvRJo+lSyE9f5jSeMB0G
A1UdDgQWBBReLnsWzUeRQLdYSCfutGnKEr+zgjAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAszGsU6zJkHn8MZZtQJuRswd+
dxbm9PZNNS9V/qIPQRyxh3ikzrPd8c4ISGgHcvVodhLsXZbK0qxS7G88EhY2wVOp
m7IDx9kRtfiCGfo5+Q4QUnTeRVYBmGaGBc1ZQKYM58jjKTkbZA2j/QtWuFwJtXcJ
Z+oOLD+6qE3StKgw0F+5KbROpx0GPvt7w2G3rmNk8ZvPLS/JqHeb7MgksnCfZUyR
Olu+4i4HHixkEcgpcamP0pmMokIkP/qVCQORfhsci5Egb72wmTdwRU0yben46GrQ
blYwa84jBzQk3pDNm5JXjhfXEl7KqZmSsky1C2UVmECDY0i4Fv9gyHL7cnmWGA==
-----END CERTIFICATE-----
)KEY";

static const char client_cert2[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAP2eEfzH3EWroGEMZLtZStFw4pJkMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMTEyMTYwOTU5
NTNaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC3MSWTB8fqM8KL8pvV
l1zbiCXhjmoffQIa5ONU2SrYUmCZBe4xuFPnEF27OQOCY9jgUi3UY23T/AJ66Bth
aI/hVU/NEyF3m8CjFyncEcC569X8hWJ92uPdcGQgv9QY5oyKjMXPHASR4wuoCE1j
BiG7q/dRnYUhHSIql3vPI2xyMktxJb632Jb6IgApI2Ei3OV8n01Z68LYpSmyQ8gX
9kqwWVjPOrWx3tH8+M6wRGG0ojxaGKosMIlkp+/rqfKdOABx4Jkkad58YXkcDmbt
YM/h6eul1mbBVbhldu09kqaamXVSo8N+AtVTpybBn/Bpcovzs6xOLZJTB2Cr5fXX
NPNjAgMBAAGjYDBeMB8GA1UdIwQYMBaAFD5YbblD/dee6YS/VVilqXMuZ3jsMB0G
A1UdDgQWBBTgpAcQ7IY7pbWeaPEx1xusNLT1vjAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAI+EZMev7zhO65hoVQynEi9TX
ILvzP+FCn8IhdGvcJ7Xi1ar/jqI49umfup53QsJRjt3JvSO6c/A7620LzdNN9Yap
YPd8wWG6aj5ugwmysI1/g+UwvhWLbNSyULwBtadrYfJXkukxx9+uIIbhMCzQ8BIs
/BN9BpWqMftHVNO9uyi4uY5+pY40tL053GY5CrIr6r1Mf+sX0sl6Z9j4S0J9W1o9
+8KMKynHdXHaLwixh0RaoK6/0aiS1nRq33K0bCLRtavbcWCiE+h38FARbjmCdmX2
Tz+WZ8xKSY5TY4v4bC4X8GDcKve3/LDuIlR0Ex35XmHOiRo7JShNp9ZtWMx6MQ==
-----END CERTIFICATE-----
)KEY";

static const char client_cert3[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUJDHCew9RMJxgUtG3dQ+V/x8HwnkwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIxMTIxNjEwMDAw
NVoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKSjMCHSTDzqTFxykC2f
WPjeqbE4swPiQyB/G38mJQXcq7dFvP6yp3rt1lxnCOHCSTV1eEA73TWTRiz7hGxe
KSIQGzAM3Be3ONgZdC+WUT9cw7DmVnpnB3TlVYK+/VXAiJcGmp26cUNM/wCR7LsY
kP7zE0wFhFNjKhpXm9fG5i32XVYBxO7rVUae98dx+PAMfM4QxTfgCSoDKQ1ET+pw
MNVo03Sqtw0809WiSwHyCJ0JMowujflnJJeq6yVdkSAB3wSoRlPiSqEef7bg8NeV
v5zpIVCCmahuTwehQjkNEmSmzhdT8lbM8kIrvfpZAVyv4bptJv7GAL/6zD7ORhng
sVUCAwEAAaNgMF4wHwYDVR0jBBgwFoAUNbDMBI5sE8Y1vyFjTmojLKqZdqAwHQYD
VR0OBBYEFC96jzoqqiV4KgkdWH1Mol9kV8mPMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQA0h8QisYXK78BfuB30STTsBDPd
G7I4MKytrYmbDXyKzmP+apZvZY1JlAjPNhwh30SBOnvEMrZI44hhHaEnmumqXxtw
W4AEtfXToF8hqVXfd215+InpufJ2hqXvf9zD3bmOdCWHSSv6i6FANZ8XXYIF3Rx7
1zviTLqkXUsDBXoP2u97HUX0T6r4NGliPI1ISIqpljV5fzPfwde1WRnOrHWl4VCL
WP9QIrpLWdOuow96iqOwvQZRtG2cGYHYtQpLAxE+fYVJmgE1cKYMcA4CaFUD8xYC
UHwhkNKpCifj6Q/benpbEmGwxrrXY35oWIDVONWY4ACBG6PCSGhMeKPLmeWg
-----END CERTIFICATE-----
)KEY";

static const char client_cert4[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAMgWoL96/cZwo0mIljvvPJrtIEE9MA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMTEyMTcwODQ2
NDVaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC8I614wmE4TPDzqXfn
x0Ui9t7roRAlWEROolawtwa4brRLP2qZTUk2qM6bVdFbWxuHthM8H+g1Odf6YxJU
DDsFd4NI6LuOUHmuu1EK+/FQorOGHkqX3i3ETT31anzTRC390vSowQUKSbGjlw/I
oH6D5laPl8VTHOwpsHNsA2DctCye4zPVxEU7tZ1BlMr7xcLPLRrdDwrSfDKLR23N
7CEouK8vjy7vo3M7iZKi8vBK16V47KOitd58ynzToqQMTPzeTY6dEUu7hnBO9qCB
wcBK9fdhcqBp2T1efZmxlXOpJ3RGTVyw2Dx9A/PmMFqMlFHdzWCcuKOKtr+SQCwv
TKuTAgMBAAGjYDBeMB8GA1UdIwQYMBaAFDWwzASObBPGNb8hY05qIyyqmXagMB0G
A1UdDgQWBBRxEAiTv6XXHicPQNWU62tno6gjozAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAHQp6++pJ80KuvYOwJ4Zpq10J
CMfSMQxkcklcWnqi1QEHcMmfDOIXVlgPoKE7OSYoP/4Xl2yRPm7t9c5AcuT5vAYY
zvGOOr9eB4KrUe11Hai40rXDC6+F2CLlrPf3mFgP2Seo85lcFee1BBp8OR3981vi
keY5U3c6a6asr6fzWYjhJDbfHnYjq15YeqYiofl813I5um6YUqPdfHvc4lDyIzNC
MWMIRvi1VmmfSS+0WPGxweQ//smdsH1mEllmwEU3bJq/FmSv6/h9jNTCCXE7je2A
Tm9uForog5ehFAgzd0Q2CvZWWVb5hNQW/JNsGYJEjXZpTvx/V1pY7w78sm/FqA==
-----END CERTIFICATE-----
)KEY";

static const char client_cert5[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUL6L2bji+Uf84q+HmaZKn9HwWGUIwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIxMTIxNzA4NDY1
OVoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALtaJRRKKdh13AtXSHoP
l1UAsj1GAXKsRgLuZd9pzuOIYf4DuE5tEfOtXXRuXn4aUydeJWMFbW1b27o4qWJX
S3X40r6M+9ANgiWH4kJsbAwQ8PL329GtOzodnB9Ya184cIewxLB9MD+kLjOmiok2
3AsHUk3zlKgQmh/lCWlIUPnqfLi2LFQ2hjyThodjeZ1KGg0X0VzWomwxUDyV38Od
xCNusOXlYV7WDNIDJUTrAPGWTJ7LRndSkAzMTrZRVkRdHqmSnZwDpxVCB5+Pmuz2
JDDr/YHteWZ61ndQqVrRMRV3nnUbATUvAWbvX2HNB7GFNDqNXSB46YaCHm7NZAKa
xssCAwEAAaNgMF4wHwYDVR0jBBgwFoAUPlhtuUP9157phL9VWKWpcy5neOwwHQYD
VR0OBBYEFPjZOgRCdV7Rd2a+2K+gAn2lIL4NMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQDGY70okWlgrOPTo2O2rHOJ8vwg
9sjdBrW6Xy3S/S7wsHPx//AG0hX+76VsO2h1CExynNREStsjomnVmDYczVzWriFN
V37o3k4cyLJqHWlsUnAaf4qfLYu+9KYxrsTDSxnAyu4B9bxjLZSggmMYWk4A9RUh
/BQbowOYw54xHJxm3cBeMyFkgBB0hBUirNjoP9QmGHkUIms2tIoaPTOzYjbpPjD6
fHtsxxCp67sqhZsdyfOXbE5xZ09s9RzCxmO7Uh7cCVZtjKZ7tev4fP2fAahrFMx+
Me+zlnnwl/JjeS4ZmetpJui7Bh0hymz8afsvsK/V93R7Uc9o2wdDrp3LesZe
-----END CERTIFICATE-----
)KEY";

static const char client_cert6[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUOhzDgN3jO8T7CaObzk+FzgbHJa0wDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIxMTIyMDAyMzIx
OVoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANk1PVI4z7gI2SOK2RLg
ZrMNSZLg73eqD5lMqG1ESs/fXz4QRWGV3AK5+fDVWpccEaBL9eJmPPAfwSQFJ7Sz
7r45Ccx/zr4p/mh+XKvSQy5XLnA1A2ozgLs9hqH6N73ZUdjXA+zdOdUiVx2yW1P8
mtSm+l2zcE0JaNVvEq2YaFh2E2hEpT08dv6pLgRxAnd/j5CxKD+OfxiMwq8E2WQD
Mvif2ndxgpT46h/5aY1YmtGaK6jsQqL7/u23EBOgX3rNsOx33RIpC2Qx2Emk7gek
TxUaZzegRIlNg0nBt7j2jnzw8chbCfOfToh4NIchtN8/nhDpQGdEq04wa7GS3ZPh
kocCAwEAAaNgMF4wHwYDVR0jBBgwFoAUi78mSDjT269LpwlyvKbOHEH6SDkwHQYD
VR0OBBYEFEvEWO1Sb1edNCAeBx/XnHRL50qKMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQAoLutAPOsQWwva5qd9ecs0/4JD
Oq35AK2EpIAH3FuY/BpS2m4MqMrowrKZ2iZe7Uv4fonpE0dQdu7mx8v2PRyYHXiH
qj3NILdZq1Jh1uVyQQ1tnxrvvoul7ao5rRiczU/HxFW3Ad9YCTS8IGYJPg2JA1AO
Nym0JdoH5YFahgLnalaMwsd0zHtRhh/biBt0wfW7LIVku81AXs54zP3p7fFTZaBi
7xVafJTeOzLRFolc/p6+9uJ64FSwoxV1OgoSiPJdSo+o98mo87JwxrAvuDGxZdxl
+auJq9+YGiizAR4AgYtVwVGZnhYVqW0FHFDLeAT69rv2gHFrEEySWhHI0+Mp
-----END CERTIFICATE-----
)KEY";

static const char client_cert7[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUeSMz2+YOCaNIEOAohveLwWkqw1YwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIxMTIyMDAyMzIz
MFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMaeCksC07zsdTOtflrB
VESt7fA/g3YjbAhZLJmqRt54s8adVnraYiR7qVBp0lQo2LQ4YKu/sFFdLVoRgy9B
bSX+SqYCfIZrIN1l5FpISv3B711AxAib4/E5Co5JPG7o1e7fU9KwoF92YFYQn05U
R9K+RPv6QQkSCfrx2k+5o8nB0UiFdfQIle9mXhdpKgSs5FHE2SvJ8eOa8A0GSlP1
6ULPDMzc/cU2Ge9xuEB9mej8132Aa1n/hH3sYCwXOUEdytQHGtCJrJqX3tvaLL0z
YkTo2gK7my7r+lbRXbRvvtoulueG+7PblI3GVqp2mCFjHaYri1wRFeB35JIGahzD
xysCAwEAAaNgMF4wHwYDVR0jBBgwFoAU9BMy88BgWQjOL9zhAlSedwQPq90wHQYD
VR0OBBYEFOmz0Tw281UgroWUdJqWJc9SwxGtMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQCgpcVU9++viJkHAhbZP/kP2wO2
aSVMYg4cy2y1Zos5OuN04kQehe+S4qz+G1Dtg4dbCsYXZZ9HjQ1xJB2O96wGlPLy
VOyzQY2YCHfPUZVmmzDLXKFMLewsrozoXvEtu+iI5fwmUezPn6yk67Js6qQ1MIXi
mead/pdG7aJ/EdDDvsVYM92+bdVkVAPPUcxu+CiGeKMKolVOX83WR4ZuKl/hB/7+
mcld3cjaDvBR2Yh1RdcXnrGl0AGMB3jD+tw5eqJG5SroR1nDbGZ9c/5dBEqGrBGj
wMgM6aPknQZa/6khOFDXlFVFks2XJPGH2dPqEOLOQ6VgxFICO4/m+1qAxc6C
-----END CERTIFICATE-----
)KEY";
static const char client_cert8[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUd5VRqadhIzQ5h5QUiitMpdFfEzowDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIxMTIyMDA2NDE0
NFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK7eJpuO2qiPfaQp3Ud4
+BPdqg1gDBVqP7HW9xkz5B2qIvrVtYelOyZBV+1bitXORvCtB4LeyCYErEqasoCk
rR1Ih0j8ZDNLTOxA+GS8kdCnQ5ZfkwP38qUd9xfqbuYgzjKIEi5MrVgvqg+8kUXp
YHyeHWLvaYXTyOPxX0EMm0MPVaaPFZbIcCuWAxbjDuSKEUwSo2PfET1Ys2LFQ+pr
mXbc752GCTzmLcqZ3+ZC3t8yCNQjQyi5OjBV4BJaKcrX+z7wtc/BWBIdK4Ltf+fg
TNKTW5NxZuj5tcoaPY9Jsxw174sR3epiyfnP6NKG2z7gDKlCnWdov8i/Wx7URzWY
hl8CAwEAAaNgMF4wHwYDVR0jBBgwFoAUi78mSDjT269LpwlyvKbOHEH6SDkwHQYD
VR0OBBYEFDnB4nLjGiDaCEaw5URR3aX5rKiwMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQDJalR+bgO9UNPepsw4JNjVGOoJ
/TsH0ohMyPMLPZ/WebXP8TAUZMeTGvqNSS1F3npwFXH+EY1KqnZVCKKLCCVmKAHu
QvV3N67U1l+xfBJVBc2YPvcYnDus3VaJtdrcb5GKlcyvB6S6Qyc+cCz3WuXnorks
KWcST1uV1ZM5DK35Q6vIEcIpa/qw5AGGw9bortcRCt0tKDSAdCCC7L8Snt07kdfA
ACe5Hio3dLNXMmx1nrSc55woNxNNT36yEey06A8Xy6jNJHsqrNvioror4w1C2f2Z
QUyEuAi8wc4GYqnqBR3/xLSh703mHt1KxgrgUyrLMY0iRm4UQB29ACYG3qrz
-----END CERTIFICATE-----
)KEY";
static const char client_cert9[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUQciZV2cc+qtj2dty+k8ohoGmeUMwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIxMTIyMDA2NDIw
MFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK8cvVMfrqYA05y0uZLJ
/OBDdyHbO6pKe3Bxg4MOxpNxKrCBCYBTNyinM0xk370ZgsvAzWdiWFAdm9jMtp8w
LIUi7sLgQIHDs0PiNWjNDtSt2v6on56KPU5EQPoEj/4KlvGTQfdIcq/G9p9HTVQv
0QZ3d2XlauM5K5aRjdb1mG2uCvwQE6T9SxnDR6CQ8NAvbejYiojjPZFC7xt9OYVz
KJ4l0BWo49kubyPf2vprJjlYanc/gvDB3vcrH0ojvKAi78QnUlVEV19NLPC9XpX/
PcRaOLS2znovCkpZQS3XJccl586Df/n1h1bL6X28qDiyGTqxEhcvIUFZK61AZZdh
IzsCAwEAAaNgMF4wHwYDVR0jBBgwFoAU2G0zfLiiN7Jy9Emj6VLIT1/mNJ4wHQYD
VR0OBBYEFGL7/FCPGxnpDFsft+1VdURQjMW/MAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQAcRq0laHnealkvqlPL/sWb2jub
F7QD0J0qYIXtVgPcdscuwMFejFwHvVDJzJQROwu1TJn4Fzta2uSOVac+QFgIcc4y
GeifJ5kqUP8yMLyZlUPvB+EVth1rjDo6vb3vJ0AG8rQo0Vwg3E2EOAkVJbInWJfr
yXiTK8+7Id366KxZT6e9yI1VcI1g3CbUG69Iy0skKMRkRwSirZfsiUVD0zg41dNU
TBbTgXtNNLTRMtk/FEiy1F0s/UEZflNI4jRXL1dnkqh6N2c1SngwJbGagbSlZv8j
Ru7AdL4SDsr7TwauGBgv6g7BvV73qYu/HRuHo8xKizWjCxvFYLQaTIK8kFqF
-----END CERTIFICATE-----
)KEY";
static const char client_cert10[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAO4gznerK5XLRrb6PU7USlFJd2ghMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAxMTIxNDU1
MTdaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDShREqrmqUc2WCX8Ts
HQbj6ZHmYTbPpMvhExdEMVeFLs0BZgKfLp2FEE9RlgG8wTQ10pGocaN7zYapKvUe
yN0/gYcfabT9TCG6zHEcNBxwdzsUHzuSSS9h/ogI7+7a9E+AxfsDAJtzmOOWgP8+
lP88Kpd2pnHrTwX5p07e41ZyLiB955C+EaZAOai47BJE1Mz64dIJiq652RJWAFh8
AJU7UYPsWfMAaoa7UgddFFiXIbEP+g3PJ1Sc668Eg832RenrTKo7++IHgmFnLwQx
T674gimE5mtCxWPq6BIFJgc/j4B4LP7Avifs8no+sUORfUJOZwOnx4SLTnA3/JaC
m2JZAgMBAAGjYDBeMB8GA1UdIwQYMBaAFMpO5mSgPyWUwFQ5s8H5CKSCgIT1MB0G
A1UdDgQWBBTlFdq/5n2vXt0ItwhsOXp4Dd5/sDAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAeZhOpj3EYPqGk+for1qar19A
GJj/6+TyYF7Zl6FoMWr6rsN2sRKSKiVAe5qGww8ONfvqHm6tnRwdf/oQca0e+1fN
hvoEh6DVLcWCF1iyLWKUo59I0/hf+tyBHq3zEFWJ3g9MnALAJAov0tjUduuiciz/
HpO0HcLxg9BVcO3yND9esw+MrriLpbGQ8HzN4QNMhQIhDOjvUNTD4nyAoQpKgO8/
wDIjEjL/LCibVxSM+xKjw37UJ/CvXRtgn4jEpx45a3oryMtC3YjYI1iM8vBt7poq
er+nbx85V0ejbK+dFySz3znHwke9oW904TZJfSjMQW/MJuvH9jL1Y2qj6gRaJw==
-----END CERTIFICATE-----
)KEY";
static const char client_cert11[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUEifZdbwFw7cJNwv1kpgoyhQub+wwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDExMjE0NTYz
NloXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMqIb+Y8mxNLvSU77ZjB
kjYVt/uThhb0sku+Gtxqmud5xihkwas6DiyY8P37O1vGjHPJv9mUV9XXJJd928dL
f5tomUxmpbH7M/L4OnClzvgbuJSWDgaS3OAz/FxMWO6qep8/OawyghwtP5q6LzKS
ZnIF+vKiwsaD62A3uCyvxnRRT/6Msa2gxA5z7QKJTEA7jw5G7v6zS310kaGEzL94
UqUm0nfOsf9kBd8d2meBChDHp2Nq+jbeGULvcvmInhbMIdJ2ggxo1tDry/hBCHtJ
JnVwYgqH3Us0MF1DBzYkrxodxfJmB8wqVMjcg45VpBI5fOW/ti6w5bD3Qjc549em
RMECAwEAAaNgMF4wHwYDVR0jBBgwFoAU9BMy88BgWQjOL9zhAlSedwQPq90wHQYD
VR0OBBYEFKjLHu6jpU7E1yBYVfjuG622vEfJMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQCwrpt2mY5JptyNeRqiRH4FWUkh
BqYVPOzye6+csUQ2trFFk8QqfgZEfaCj0zmlZQuQdbZDWISig9XiaM/3jI7MOC5v
faX8uo/+qfemfAMHeBxs2dgeIstgRurwpVowlFhm8rtxzkaugPf5+PkjCKIKs5+y
H7Lcdm7JdzZMHOMAOtnDH3ZUYzynYOAgR7KUdbSMjZ1fHrzE7yrPM/jDgZ9IASjW
k1HEqSw98hloNgflWP5TUHdfvgeSc4iKd9fFz3yUVK2gby9Zqk/MfWdUF8afdR4I
C1cvR7gTHKjjp5Ifdz8RiuIWU8RwVuEtz7HlKmT7ylZdzts/C9B7S4wq18af
-----END CERTIFICATE-----
)KEY";
// private key
static const char privkey0[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAv2UaoqJ2UKTjaAwpAYFz11l9L1dXTD3VdUnMCLc1Odwo2A9L
k9Wl8QQMI/mYbKRUtYB2m7EeaoomBqOvTza3v0z3jTfVIfLZCWZyzI39a8cQdinB
VZpPTt1LBuJhQ+2Wv59l8uYHrTjEyExAnlvEorr1N1MndI8ClkyNTeWVZI4DAgJN
SjhKz79hiwoPJF2T3kbkCTbxdieN2Xv6gm6vZJkJEAPrw8h6UbHGpir4yOgkEHY5
ZmkxwhXDCCgcaRq64SWEnDC/MeORTtdBku+T3g3njV075zAC1aQ4ddKcomnXZLFq
BOjCY+hPERaHUZZfqn3phd0o/G5EdcYzD4Ma5QIDAQABAoIBAQC3kLijEz52yi5x
NHh/wTDhcirmpNZNgWChRU4zbcPW4pkp4EGUYGzvY/pg5pqSWGoGSld3q8CIXkdq
RsN1Lf29tIwZHVuFW+1dXqzsE6LjI+Fd815CQ+WcthGnYKf5Bn52+uJEvOrP8IqL
sNapC0rk6HsXvOEyoZbRoEpGU9OcxAzbPOQ7whYU+idTrnOtZvTs6mhzNy7mUxtZ
LKUWfri+uOAAE3iE54sismblk4rZ3DydGsvz3nwH02/c6k5X4I/YV0xA9VLV5F2x
saX4euL48QF1jY5d8zFHs5ybrL8krohCaC5v1K0hBfUyu+qa0vdjpG3BmpreUqRD
t6LQlpsBAoGBAPR6q480Jtwxua/3Wzl1hwKQqbyLIObwvXv/hcKxP+ovBLUDU7VT
boXcabYKavrY+8KjPeQSzi1vAGDP1jPIbWIqwEgUxL9qpkLBhVNBpfUEh5eDgPea
jupaNGZ4ffN0oIHFsZL0LawtnZwc5OfN4HZLT8RIgeNWExuLPq++IzHpAoGBAMhq
CkYVIUrQIg4vVpJRxaXYzwQeIeR5nIAgHJtw++E1PzpvmCoWrzC5+apEYveozLd4
qKxTudcbZwD+HnKjScab+YASP8T0NNp4A4n81JpezZfbJCkDebTmBsoj3sQmub1s
wWikvXHw4PGbZviqQ/Y/XUECBcHyrJr2FIkd2yedAoGBAJxXThrX6PnrOqnS7x2B
kQMM697Mwg+taB9BupJUs2DZDJONOxy76/Pijh+B7gPYerdvmQafGAZp8731ay/W
Pk0GtcvirCcU1pneE5q4Yp6bdPJpgDHXPwmTAet9pl3n+TTD7djiGcr/qMjKmX+7
Yo2Df9Ev5RAyXSnCFCZi2zLxAoGAQTZoA2fqvlo9SppStCfRhWan7fIUGzELIJHS
XS4Fd8GIwm3/eGqLNPK+741zGp0dh/HGCNTxmrH68VK++5RwdCTaqbbGc0L3U8NL
e4H+eACzK1+OQ54y4w8aVHoZiYAEXkPeJzu7vTChMVzcw7s7hAwYPwf2caaSTZYX
ForVfe0CgYEAtIs5Yo1E544fLgwO/PaexinKMjcvJ/YrUzmw4mr9hKV0jP964NcM
+y7nr7cOQrsMkndC9vT0BCdyX4lvryd3fWxf/9/46cJ3LwKWDqi/FINvu9DSER6/
Eiq6bKNi2+yaaj2G7ZgvWAvCBT2mgeXKFyh0Oj9TzFU3EKb+KKglslk=
-----END RSA PRIVATE KEY-----
)KEY";

static const char privkey1[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAy4AWc/mwlMFnrhgFBNvdPlgBl8f/w2K1VT9Qa7Oq6ik/w3ty
JGM8xodmPM/81DOWhUFbn5Wu2jOANwwiDv3+OP7d+xwrunQI302sP9Vo93EVVwRF
HvJONz+nYO2crWddLoRwhB71IzUsX624uJtALcQkre6anHC3Xr6YBjAB6FrdYlH6
8t6sUmN6qV5qt0V+rUeoiBm2bYXLbMBBHL5Op0tmrHA2diNw2ilv9zes5FyIvLeM
TxcsSMdEfKOC+brXwXKcwrdIe6tySbpVy0yg2yJrVMfGIjlAr0xteGG6uFriM2WF
OJJ4lFJDE1wHNHNpg6SDN4vDQVC9Pra+KGnWIQIDAQABAoIBADKV5X7wy38+0GoL
WHwLt2R+9e5n7eEA0XieY3xSTiMlG8+ZCReYf4wOLlDhlsSdCkaXp5H04GEnOjD0
2CaH5sD+Qeng59HSdD4mDbJUd2JC/v4BdrkpnF3Jee7zQeYs8pHUpC+DjoYw+pXS
Fj/1f6+guUlvLYnDzyFDGYE4P9ElC7XPHSXhu/qvmd9egwPCpvu42SAfF3g/ShoK
uQnMhskul0+OU9SBKwBKUCQA7Ipr1lfYahCu/dd1V5oaRCs62ImYcObssgBYepik
X4pgxPqZejWXAYi0OTJ1IFWmvjIU8P94XYo9VFQ5UhMyKy2Dp/0tJr2vW7FuEs3G
QfS75kECgYEA5rhm4ktMMFilPGegkYAcLUNsjx1Fj9HbYhnAQhAASAPRiKp3rFLn
W0IhpPyblrFLg4Uvshg3EwDxfe8U4TY/oCc56VE8VSFj+yUW00MF2w6eQVqz6/aM
B9yZHAQSmJVPbEkhYJZFTfzWA11yjFhzW11jCsz02fpyIUlm0veQgPkCgYEA4cwt
lXDrifBaRsI2/7oI0zj8wvFKrHjUwXW/MI6YdbYLV23StZ8YEz7I2credHapvBKD
B5Bh79nVMXcik6kArnkwyfS7SSLE4Z7hQ4m7pjUiXJ9nOtVMrOyvySciric09eaZ
XGz0Kv3aZ1VsS3RiJq6S/zcfupPbamHhb7l4cGkCgYAx3u4uQARp9t9QnT2x9bUh
e499p+AfZEc9fz91iCvnZRvnpKFVmF319ed72DZxbkVaWnGi12UgMHYCuKkUvTsn
jRMRgcO+ywbnhG7Yn4YmgZRnoAmAGbzO/joCOkciwDN4vp3+WL0GTH6eH5bhC0V0
HE2KSh8q9t2woEMfmu+bUQKBgQCepBJ+xXhnULxMrJVxtGYQ6SAFYvwhEz//sFek
qPsK8N0lDx8UxfNnShxZkIQc9WYpoiP3Q4TbvINiHp7nebzl2HXj3pNbPhdvoW4J
QpLZoslCpCgmGMinUh/rRxjBnQBDqEGlw1MFhZdWNJlCpkFzhgH3V+Zt8wYcxypZ
kcjByQKBgA87UbdbZEXKqRnxac4k2R1xGgTqegSEtEAsIEJYmsTIYIXFxCBNpxrR
usrLvGv0/Q6+on0/LD5Stl4GRXYpFeph/YytFmOqt+5Hk4DdOBWUOm+E3HBFkqp4
TBDPUhE1f6DudvovsRGrAT9OiExdOt+mx8cDF5NZY7Za29nsQMsB
-----END RSA PRIVATE KEY-----
)KEY";
static const char privkey2[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAtzElkwfH6jPCi/Kb1Zdc24gl4Y5qH30CGuTjVNkq2FJgmQXu
MbhT5xBduzkDgmPY4FIt1GNt0/wCeugbYWiP4VVPzRMhd5vAoxcp3BHAuevV/IVi
fdrj3XBkIL/UGOaMiozFzxwEkeMLqAhNYwYhu6v3UZ2FIR0iKpd7zyNscjJLcSW+
t9iW+iIAKSNhItzlfJ9NWevC2KUpskPIF/ZKsFlYzzq1sd7R/PjOsERhtKI8Whiq
LDCJZKfv66nynTgAceCZJGnefGF5HA5m7WDP4enrpdZmwVW4ZXbtPZKmmpl1UqPD
fgLVU6cmwZ/waXKL87OsTi2SUwdgq+X11zTzYwIDAQABAoIBAQCeGhN0pqV50nwX
7iViHMUuTn1rAk9Cp8/WSC6OvvKESexGhODDFnOLGwvaFmWZ9fRp/WLCR6G0721w
h7uBwQ1UCx3ZPqaGpOyHlOkynju3+frvemq7ddts9J/lx8kUSH2I4d+iVGr5QXuA
5NFtIhCg+NR1Ir6QCqW/HofjCI6Cqr8qJRbpeyxe2temt51xUYv/ll26OoHjs5aI
CZI74M4c7cdDFpLWb4PnAW0UakwX+TwzHGbRmxDANRm7pmxOhEN7rsIfVybFj5JC
u1tKooDYPgIM1/GkbSORstmL0ttYKWPDU5u3Y5xX9OIHWNVL1IRXuhu/httfbxPy
mGk4q7FJAoGBAPNc6qVC73fjZ7dO4j32brLj5+ZWjObnYMI5GZ8U6NykfPhPj/f1
40Pq3QDIKb3EBvFyfmp+jIPRoQw1/a7I/91FLZA2gHcGEgfAaklqjYJ4aOsnjCOF
JTMxd0BvWqsvVR5kuna+zf5vJmfvNXptkVN8YqaRbv7nXCiQaSivl9YdAoGBAMC0
XNdWRxaUYUNh7DWMvbldXTt14SWEf6d11UENbjvd50ixLI3e8xgcCGe2fe5u+l3m
LJGCiOeFFlUwV72QGV567DDxq/LMjajBgu4OGBHQUt2/Sgw23isOytq/LmLTkTqk
gERo7JO+2pjrYLs9NKPdT1BddDjFUvRH6fivr7d/AoGANuIxmOurjeIfNdv4cfjt
pjJ3zh6e0ERGMgcmnBXfHkOKW4W19jnh8yM4jECYCU/4MOOdVu8LSX8xTsuuVojG
C2Tajbd86VrZHh5q2fvJxi7Zmo7kr+4LIax6NHHohMq764rT8ou9cTAn/TCZYu7q
42Ud6mrKGXqb8hn/XUmceJECgYEAuUz2NY7ePsUhnfTMeVXDnjo8C5A3lqcJRVMW
ZD5w8JpZ61mLOUTYxyc1od0mzR7kG2TZGHbIBLDCH9qjlEG7U1GqSwooyQmPcmXZ
6+WbmY2UEWz+3C3qhsbro1rRVPCSJyzFtoKacxvEAeXcl0+sjaGiwemLd4evU/zO
7Har5isCgYAWHqYSyCFQnGpEkS1Y6LYr5ZyiRs7yKqOpfCZRI4BUPVgHpI55tKUd
b9etbl5YwIPoPT1E6QMy7/nzJCFCkDDZNlQCxviuP4w069K8jun83k9RA/Mq8iGx
l6n91d4aAJCmjnVVemQIoCeNG/Egas8ZRqkyBI1w2c62pocA0SKMgA==
-----END RSA PRIVATE KEY-----
)KEY";

static const char privkey3[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEApKMwIdJMPOpMXHKQLZ9Y+N6psTizA+JDIH8bfyYlBdyrt0W8
/rKneu3WXGcI4cJJNXV4QDvdNZNGLPuEbF4pIhAbMAzcF7c42Bl0L5ZRP1zDsOZW
emcHdOVVgr79VcCIlwaanbpxQ0z/AJHsuxiQ/vMTTAWEU2MqGleb18bmLfZdVgHE
7utVRp73x3H48Ax8zhDFN+AJKgMpDURP6nAw1WjTdKq3DTzT1aJLAfIInQkyjC6N
+Wckl6rrJV2RIAHfBKhGU+JKoR5/tuDw15W/nOkhUIKZqG5PB6FCOQ0SZKbOF1Py
VszyQiu9+lkBXK/hum0m/sYAv/rMPs5GGeCxVQIDAQABAoIBAQCEwEDTRKrMg2/H
K2kuIEqxgXj6uWk/PHY8AcG+KJe5vlGHmefmx09cQ+KQ51O2Fd7KCEpKIPshc/sl
0jjY2KP6H1qMyBG/3bEsLnoM1Pv2R2pTfadFU0Yj77efkTjcqD566IxLmkYxqigD
4aYsxoZDzORGbDxisJcGXpX+yxZXAACXBcKbWiklrqoBGWyFkPG+L6BMDLI0MPYs
ToT9YI51fHa2ctKdKIAwLm7nda28Q2/eVcOBZ1XgtnvRP1Ey7lVm99LloRRbUqXa
CZR8Ci69GU0IGR5faMuiL13gPwPB80U4G2YvSFIpFcZUyE6dsM8GVONPBsPLbLft
kOK6QhiRAoGBANdJD2x0oZEYcHdA0HC/CNAK8m4cTPPcFMsVVsBtqc4qPKXs6Gc7
f5mQcUHapbK8tqt9ZJDi9823Dp0QQ/u9Y4Tf8P5X2+GBFo8ChuTbuTByTLvd12Eu
iO1AWBDzehWRUDNrKLLwYZjVVe2ip0XR7Rt4u+0vM/h7qlVzWSm0iRb/AoGBAMPG
CC0urBsv/BUipPQ6QB4WdKz45yIW5vkBCDzIksh9Mb0CPRWW0MrsFGtP7jsqDJes
4m86E4fnKJQJnPP+/+X3ni2LzvEY3bXZz9AUSLZt6Oiq5IwqkT+K97paJilyqs4B
37inPsrz6wkMiM9CSmpfDNETA7VH0beQSwpSDqurAoGAH9dJdbQGVWx07d5jNdm7
VvHXi1uaMhMh7Ct6yjkt/TUYBT513Rbat366kY/iI/5vUgvYvOkk14M0oRxPwcLw
Hi30+dlKxx6NlJc+Fkk9KaIuITcdbN0yGU0D+XXZX6k/YGt/+H8/IBilfeEptlTn
KcIAoYLyv9Sii8LktBtkj0cCgYBylXe3qjeFeiJa5HoTj/xzH8WOb+goJatuVlJR
8TsoKU5xR9wkxK7T9jVFUfUnn8bbB49Idn9z7bHsY2v/y2JPYQytr8ShjGJHy4I8
UYL1c4L8mvlFqKx2uomgAp+PZtPd227sDsdL1lPcaf+w/yW6+ILx4+Upty1h+QxS
sBByHQKBgQCc0jJOdqrh/NwFpgHz6C9f/0g0vzEXsxvyGlirXN1RjpCZv2lHbKV/
I62StOT5Cp81d4Y/0TyyoZCEQAY4Z9e5gbUXR6GJfqt6/2ic0U4D9VzmJr8R6cjJ
CavilbyzsgQIDeCOU0HO6qtsAcU4fwucR6pSGLD4qOAprEn0sjwdow==
-----END RSA PRIVATE KEY-----
)KEY";

static const char privkey4[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAvCOteMJhOEzw86l358dFIvbe66EQJVhETqJWsLcGuG60Sz9q
mU1JNqjOm1XRW1sbh7YTPB/oNTnX+mMSVAw7BXeDSOi7jlB5rrtRCvvxUKKzhh5K
l94txE099Wp800Qt/dL0qMEFCkmxo5cPyKB+g+ZWj5fFUxzsKbBzbANg3LQsnuMz
1cRFO7WdQZTK+8XCzy0a3Q8K0nwyi0dtzewhKLivL48u76NzO4mSovLwSteleOyj
orXefMp806KkDEz83k2OnRFLu4ZwTvaggcHASvX3YXKgadk9Xn2ZsZVzqSd0Rk1c
sNg8fQPz5jBajJRR3c1gnLijira/kkAsL0yrkwIDAQABAoIBAQCmu3eg7aL7IjTn
u+hnoFHFCsPETpjgf7kJdxUcxxdNm85sYWc3NScNKczwHL3NGJDtVI73Wc+QXBLr
86Dqm2wzDBdtJdQR63/rOlY4Rc82hvpSgcXZkS0+KrYeKKE3GdUofSwV4fl1hJnJ
QLbcNl/oxNkxqG9XmOoyiA3CS7wr2nnLpf3FVerrJIprQX4csg3A0RH/gdMJJoLT
KsKtYQXQDiyYSK7tc+OOgIarAiozgsMT+uP9thQQrdtRdG/RHyVya0cQ6PqUzjfl
wNs8Q2Vv4T6t/RllbQlyv4TTceOzFA/xe1yUYq6YKAq/6dxSKHa+R5u4pnS49BBo
NEFnzRsBAoGBAPVFtlMUI3LEq58MA9MeEPWyNMrICvXu/B2tqKcabBYH3YmeIe6h
eZMHmv06drxnq2AiRFbeec30KPZ8f9UDhnwH4E2viFcL/5MN1akop2kvy9ZMVzbG
bYwQScZCTAhAXawZybpPdGLdk9xZS6LxgyYOvpdPz2A5ofrZC3Cg0oXfAoGBAMRe
QQGSAja+n2hZDr2fJriUBMh2iHFd9Rj74qoacrNSapssM9A3cU/Cp0L8Z4QwRLaj
0al3iAKubEwXUDhfK9EEDV7YuvoOIWVGzXZNiwd4ghQJ+6UCCOTt+A8nrQmkWKYo
O6NwfDzxSUqTAjZspD5ls/qGTsH6uMPAqTHph4jNAoGAffj8F9d0DXIto1aMcFY0
57xLmQr9dir0mlmjEZBkizwm8JOjYejVhEqXtA/fBA1K2+k9qFBDxx/sGgz/quxQ
mkuBTOUm5W5oKaMlZehXc5L04m/7K0rm4NVMzuMmIebNNaNZFLPSVyTu2HGUg2//
qABSq2x2Wjm/lBL96S7wcYkCgYEAqWkXdB9fQZObOw76JKWVZlBs//UjKYsB1Rga
KN7AbeyAhHbSDKSIYWNxodzYAbcS0O7sBuRUttt8GvlSABA7YQu57UXVqb1fGlzH
DXVAqKVSs68ZJvZmbfrPs2NraZIb2N2E9jApPvp7XJcPpZx85kgfydbtQ3TmyhsA
3Uq331ECgYEA7xyjOAkXUK05S9Vu8qIcxlW7VaulVEOgqlQrN/yoAZx+77mADx5v
LOGbz+0qt18xRtGtTKbIfHPMFIPSO6Hbxr0eq7lt/KJ7GqbBR8cReG/pybFRtTZE
K0IkpcddmRAgXZooc1Ckp9SJRU1Wkg7YWFHut9/eZveWfeftf/uHlLM=
-----END RSA PRIVATE KEY-----
)KEY";

static const char privkey5[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAu1olFEop2HXcC1dIeg+XVQCyPUYBcqxGAu5l32nO44hh/gO4
Tm0R861ddG5efhpTJ14lYwVtbVvbujipYldLdfjSvoz70A2CJYfiQmxsDBDw8vfb
0a07Oh2cH1hrXzhwh7DEsH0wP6QuM6aKiTbcCwdSTfOUqBCaH+UJaUhQ+ep8uLYs
VDaGPJOGh2N5nUoaDRfRXNaibDFQPJXfw53EI26w5eVhXtYM0gMlROsA8ZZMnstG
d1KQDMxOtlFWRF0eqZKdnAOnFUIHn4+a7PYkMOv9ge15ZnrWd1CpWtExFXeedRsB
NS8BZu9fYc0HsYU0Oo1dIHjphoIebs1kAprGywIDAQABAoIBADmFNqchEfHkVZeh
x526FbNCUHbhfGZRn/kcamiKWruezr8nMznY4M8Ypf2uORKsUGVg0UeyjeMqda6Z
4N9Rf9iLeWCJdnJtA000h/Huycl2lhNyp6mbbviryJgTFHwanun6CplJBjwz0v+G
nTf9gNdd+xX1t+lVl4w9TINncX0Yy40i1L/xSkSCfxB6tMjIKQdmjpzAxsVnFfe7
tiStblGfLHb+Ot01+0uYpi5wkGVSNQKUMAFebw9iJ+t38LTD6EENtrjTVm0l7SO9
VsfRPglGa5/nBpRaZGoAIqu/gIN4PVRIXp5HtLTOzC4LPhGOBGe4ow6QgVyZovSM
kWJTdekCgYEA3qc4a7dMAnqMXMC2fQXpCm1vF7AT/G8rqkK5xZrp/SjNhwgJLuUo
o+sd10saPnN84v5eIz64n38XPGxvwjiSbU9YiHW3bxsou1lfXbWDRESqCmwtyHSa
Eu7hEb5W5tQoHw+pcbPY/QzjQBQsXFBeuUT63cu2hRgCF3A1GKPuL8cCgYEA12lw
zUM0nGRqOubN5dYat8OYG94BmGKawesHX/CZYIUTQzVzB8vySSnRzZLtdwilNYDO
nS1MNZDIla8evFS2XHPDLRMi1xW90NOwFhDcCElVXbzry7jUCzJe0JLentnTGC4w
+tDQB+7f+Tmo6ZxCqY3njbsiZrUtMX/xeJA4ON0CgYEAt8G6xuuYR9NsVICYG8bs
9jYrIZmoZQY7O2GmgHfB9bXfKjpvmcx3JhG9pxnMpw4ZXB4jA2OYa8owGyZDdj/K
WXzKzJSat4/qpfS0yFot5AHndIKjpnTVVBrilgpJHmiqVDhQ2A5eXzprs07y7Z1H
EBDmGncuQ9n0n/NzXsqsobECfzGlG1ozIuzoN5NbnhhaP84aRfQmNjfogcIvaIOz
76VPBiF3MAub9TrnaHL4acIFPA4YoYWX6vhtZc0ZjvvYii7EfzbkMiNf319BalOr
jwpZ3FK+N5m7/yKVeUSYlLYHfZ8VBjTH6yno47WMlGFCohpxmewQNN124/OEiSJA
GrUCgYEAuL2i+tHNZ2Z/xFQDQ/j1ajmCfcrS1XFf1lILabMb/bUQl5MR6ca5lqc/
6bWWkikNW2pK9sypdQsPEdltbTcH1dq7SQ3T5mGKDI6dbqx8kQUNqH00fPhe2T4t
6crmpgwZ4ux+/l9TPH9+X6u+KRjy2kIgv8f4XUSVYa33C/zZmsg=
-----END RSA PRIVATE KEY-----
)KEY";
static const char privkey6[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA2TU9UjjPuAjZI4rZEuBmsw1JkuDvd6oPmUyobURKz99fPhBF
YZXcArn58NValxwRoEv14mY88B/BJAUntLPuvjkJzH/Ovin+aH5cq9JDLlcucDUD
ajOAuz2Gofo3vdlR2NcD7N051SJXHbJbU/ya1Kb6XbNwTQlo1W8SrZhoWHYTaESl
PTx2/qkuBHECd3+PkLEoP45/GIzCrwTZZAMy+J/ad3GClPjqH/lpjVia0ZorqOxC
ovv+7bcQE6Bfes2w7HfdEikLZDHYSaTuB6RPFRpnN6BEiU2DScG3uPaOfPDxyFsJ
859OiHg0hyG03z+eEOlAZ0SrTjBrsZLdk+GShwIDAQABAoIBAEcdUDtoUFHvy7AA
WLv+boF2+zy0mpKyQjMtzwMfVeMjPhqi1yRxHTxjSXrZT7Sjwv2ZgSKj2JXBv8mc
Sni2ZJcxM40Z10WZmCr8HDlBOS+l6ZVhNgICZMZpCXhk5PUqfsqg8y8M609l0+Hs
MEyaKtMu7E/bXQxL5V/j4tilSpyTSYM1vVku8tDYf1yHiPYjBFlL5Hx3X+OBK47s
pEuFfq2nSS0D8ZvQzP94OVSFpTeOn+toNGKavdnCkUWsXb2okIg12Z4b01Yiv+mq
jHmWgszeKPtjAWOOBoIGY+/kDcVFxUz6NFsCQifYG+DpKtKC2cDT2IcIanrj/Ps7
X77vtdECgYEA99oSwhGkLtW0mZRKLrdn+Qn+8yZ5VVjjvXpyzRnpgGbyhukDyLkS
5VsnfOQBmrGzmk1nw8Nvv47+I7JQcMSEYuszhcWH1FQ9mI79IhB4agF+QM/tCRVS
UA4fxwTn7t5M0sPxYnPmLA16UxmB2Kr2lgoTlUiTJxUcm/baL184uRkCgYEA4FlE
RCIVGjFjAltJLp6aP62HMweX/g8z0q1YD79Y5I63+IIvp4Mg0Ob5sTysj0S1jNvz
mRl4dfWkeTiGZ1s9DKmVKZ3cjcOhqcDm1m1e1bqhV3tl6UE8BQYyELLRATjUK/cX
hLfNf6i4LxX30UeIm44rwdNi+0oAbORFtAlC/J8CgYBySqhtv47TXg6652vR4Rdb
+uULIQnAdo4GWQnJVJKdeFuMJLVvAREanI84AzhOKnWkyN75ogzM69Z7hYWcirJ1
nvoSTMVW/0WsLPvDko7Ea03Z+0fV8XQUoH682qFNQEtywZJ1EPbqB4kJ+vAqhqPR
CnJ/W4kn849OyGVShPl2OQKBgDBgj2vxvYMsayEy9hDrem+Ix8KlPI04UgB4CKk8
pbqDXABmckg46nahl+40GzG5qzx4oYq+B0/IbHMHUVQwRHXai6OxusGHEfE8+4n5
g52q5OesYElwG7UppNoc9RzCWF4ypgukZtfEVhMxNYMXXcnNWnotTwbI6laQ0scD
yEIZAoGAIlh9xtwaZrE/aQV/tAQnD0t66U7quViL8zIuy+Lk/wNiDo8jiXY3JjNc
kWz9FsXZWu2E6mladdnmJ01xWyg09M1P2U/ScugsEZH2z19CvYAEewGdUdAnFakE
LKG55LNMUcoeZxXsipRBO2zlTgUzkklCM35AGBGUa29yQqLzBu8=
-----END RSA PRIVATE KEY-----
)KEY";
static const char privkey7[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAxp4KSwLTvOx1M61+WsFURK3t8D+DdiNsCFksmapG3nizxp1W
etpiJHupUGnSVCjYtDhgq7+wUV0tWhGDL0FtJf5KpgJ8hmsg3WXkWkhK/cHvXUDE
CJvj8TkKjkk8bujV7t9T0rCgX3ZgVhCfTlRH0r5E+/pBCRIJ+vHaT7mjycHRSIV1
9AiV72ZeF2kqBKzkUcTZK8nx45rwDQZKU/XpQs8MzNz9xTYZ73G4QH2Z6PzXfYBr
Wf+EfexgLBc5QR3K1Aca0Imsmpfe29osvTNiROjaArubLuv6VtFdtG++2i6W54b7
s9uUjcZWqnaYIWMdpiuLXBEV4HfkkgZqHMPHKwIDAQABAoIBAEKEEvZkD/0pQ2b3
J2XFcS8/AHyBCMm7uyZglusvd2RyD+LoNloWYN/O+jkP93xv4YMZjmZ2X9of1lxf
mQtuYdtjeiR2mLV7kxoSUZNauqKZHEyisAQiEQhsDfjw1ehe2aaR7ESXGl5eCo1I
K9HTr+oIBppMiRM8k1MTRAWHKi8avlyXg+gYllRdAdq6NfRxeblklrvWZP1EHgqY
WUrYfDllJ4Qcb4nuZ1Wh3z8dNdBeNWSX86lWzVLkrwH+4rCpYF87waQnsyK9ts4H
dzDeHPesFt7rWFHIR1Cuz6ps6usm8bxiKpMDbZ+Lma4m7iJV13nFafG31WhPxVMZ
wqZJ02ECgYEA8L3mIbydGwtZr4qNu8MjXd800fut2u+mdev1Wfb2pjtr1UABeoo6
bvbQ1X2obKgI4bQ2F9iJd2sjqfXenp/oYNt9PORHLTE+257GQMsl0KXoS2WdvbFJ
hWj1A5/LBs5E3Q6w0EKcAseh9xaE0euuRylmlAKt/bNbCMAJ9rjttd0CgYEA0zSp
HvCvGkF2SNgEzm3d5m56AU3o2h4qX10hgMWr/gDLA+8pYpcm8xm9bcic2jwZuw+J
xmdaEr7fpOr9b8IllenXKkyihgIWsmMfVozLe6AcDebyZBRTXMwCsuYS/1XvFL1/
WwR4b3KFT0ZNWyqyKEeGW4d2U6kcI8bMp6TSdKcCgYALZCDohcE1T2MuADNWqBQb
U/1u1ibSzjYlLEYOvLXNBdDDZWTshG/hYkJdZ7W5dnX9IAbTOAbBJMuqNh0TimN3
ectCP1by13qu76NX24Tti3wjXSqKmEHvUxkM09nKd6Ygg5pwMEarhwCt7V3ZhPpZ
Gadl536OG5d6IYQXLYbp7QKBgE62CXEmcZtvWg27s87a0x/VfFKQHn9rTo9Lvfin
snlvAjntH5dGjZV7ukT3xrzuOQilucrfKYpi6CYmDV1hFXDRPQbemJMopBzHJB61
SgBAozPrJdp2WLr0rOzPIbVW6UrpdFUPTXL7UwNASAP53Xt1bHUf/tEe7QXISO0R
V/1xAoGASnGEqBj5kcbE2Rsg/XfU3COfq++dM2zaNlGxS7O2yIaekkHZnXhuvpPb
esdvRoqiHqjJlvYXLR+wr0BNY6nTfUSsrw4yiKyUV6bCPA+qygEcWR4J7DMXHLjd
x8iVozV3TrVbSlVNJByfVS44+JmpDMcynyovnjftPZTSe+GSZjU=
-----END RSA PRIVATE KEY-----
)KEY";
static const char privkey8[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEArt4mm47aqI99pCndR3j4E92qDWAMFWo/sdb3GTPkHaoi+tW1
h6U7JkFX7VuK1c5G8K0Hgt7IJgSsSpqygKStHUiHSPxkM0tM7ED4ZLyR0KdDll+T
A/fypR33F+pu5iDOMogSLkytWC+qD7yRRelgfJ4dYu9phdPI4/FfQQybQw9Vpo8V
lshwK5YDFuMO5IoRTBKjY98RPVizYsVD6muZdtzvnYYJPOYtypnf5kLe3zII1CND
KLk6MFXgElopytf7PvC1z8FYEh0rgu1/5+BM0pNbk3Fm6Pm1yho9j0mzHDXvixHd
6mLJ+c/o0obbPuAMqUKdZ2i/yL9bHtRHNZiGXwIDAQABAoIBAQCeA+aVSPhzuv5I
2v+29ywNH4/r+C/TJiiadmrhXk+zMqvbSER6Z6fUgNWIieEiJP1bvK995fysxio+
XB4TOzunq4N8Q330EAZZsrgCp5eIGa1sc77wphygcjx3YwOFdn19khIDvowhlSiy
ga4wo5vp4CMj5L3RDbiJqfJuYIrog7Megrct2KbDopZIsd9tuqmegd52rAxuK+cO
ZbpPQw9JJi7Hh+1qmQoKMVcSSMar5tKEy4UYoq/MuK+LTJJovcV12MXwk0uxv9+Q
m87A2uDXo76Bduk9p3NLjK2chF9RG806HLRI+vX4NYh2LiWbyQIsawL3OhAnwGA6
H8qzXfUpAoGBANz0RLmirbdutNCCkZA2jOOmeXJl45uMrfGBaYs6VHLOWKQBV6ie
+UMh5sXgNJsFJGQX9gVEW2m+2HYb13VqVnPRk7YZ9pEe6a/1rX4NsHuJEhPDOccX
qF2ay8LjDPGV3PJVKcLzD6tPN8sZu4whudc4NX/bJnkBkwj5VE8Gz8hlAoGBAMqa
kVUFjC3lrTmaSZVm5nk3mB+vhpWAJYYMGUFmhglwixLkWNuSdILdCusCtwFglcZn
EceQSgOTatnH11m1m6RiBgnH3Vr3bAlNeWqqBP9NDkj2WeRvaGpZYn0bDnSL+bIw
XF6ROT+DuLPll8uVqEgeWMenKwtNMdQFx3cBDO1zAoGAJf4mlxj+7+ysULIy325R
2Y81kNMuSP/upPlQsa0M+lS67zB/5zG2wLFioiHESeefc0qfdhZj9ZHoXJ669Kid
RvsBgC2EtT1wh14iP+uQrh1Kq3VFSROrUrUcVl1+S0gunsjADpKG3jkX6ed51UWz
U+uJX3vKaMI5izB8K76LGfUCgYEAgJ9cQIPDGKhFLtqaeHTZQH9g3kzKvkKaj91C
c1ipI3kFcbryBoOV4DaAZe5VTFSLAWgRy4X41whRv2y1dMhsxRFOEAuIpeWJ+uXq
fSLvBT1gIkXzj6iCu3F/qzjwFmF+5fb5zTeZBo+TRPIvShn6ub46UpX4J7acOaRp
m01Zba0CgYBcDZOBlrCHr4HezW6eLHXxH8/PJava/6Q6GCS5JJnFpol/TnHreEl3
MsolTVazPlSrsJOnBX9W8kR0OdfBuloHWct0dUR9uenKVt6GGeXZsa+eODae0xPl
bF1hbscmnX0ImuMoHtjXwA+obdovWZtofODt/ddopVCSlGfrMEG1lA==
-----END RSA PRIVATE KEY-----
)KEY";
static const char privkey9[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEArxy9Ux+upgDTnLS5ksn84EN3Ids7qkp7cHGDgw7Gk3EqsIEJ
gFM3KKczTGTfvRmCy8DNZ2JYUB2b2My2nzAshSLuwuBAgcOzQ+I1aM0O1K3a/qif
noo9TkRA+gSP/gqW8ZNB90hyr8b2n0dNVC/RBnd3ZeVq4zkrlpGN1vWYba4K/BAT
pP1LGcNHoJDw0C9t6NiKiOM9kULvG305hXMoniXQFajj2S5vI9/a+msmOVhqdz+C
8MHe9ysfSiO8oCLvxCdSVURXX00s8L1elf89xFo4tLbOei8KSllBLdclxyXnzoN/
+fWHVsvpfbyoOLIZOrESFy8hQVkrrUBll2EjOwIDAQABAoIBAQCEiCreO69SInQG
OV3uNAO4tDb5H9UjiGWub6tKSKeQNelKVw8a2WB42zrfCcWweBtfHJgZM/yE/jPS
vPVGvA4l4Y91XIZV8AjASveDTxd3bfG+JV9RfWzyzIOk9kkIy3j8cyhNAL8UPHCx
kiR7oAkCbO6n5x+jw8QynA0O8SghcYOOhu9ZwlHKGCvH3ATD5v8G+ox5qsZsvEN+
TjM+i84ad3Q06VP06eVefkfPqPGJJIuYYyZAiSrpusPnHTqFCWCy4b7gJ8HJSgqo
MAgPiX1w6pBR24UbeleyS/5eFBwk8Ogy4H9V3gGas9UQV9yP47nnC9sInWqBRi99
+9z9zGBZAoGBAOUg0MNQtgpuja5MdRoEWL17zuz5vBf3sx/Y5HIivNPp2T9Uulbh
j8jEM26h2DIm1b0oajTEqrdo2VZiN0lZA1emPzUQ1GXoMvKTu/P6jBiUZekVn4yv
EVQFCpyVnVWJs7PSQiwioAaQ4l5Yabg5kxye3/PSaAH8RNd75JT/hFe1AoGBAMOm
MBLMk7PSsqO/syq+TAIp7HZ2S5w02RTwOdC6iONwzvwyhDclU/SsDPI3lxTTIsXu
q9uUfR8rZt7A/fwYJuiydCN4uiar8t/MfR+l/HAmxcbL1YDwx7MuWMASic+IXZlQ
6RiO0Uv7F0SHj/gQlcMf4e7XEG0FAT/KLfaE9IUvAoGAckzVd6mqtjeCPCHuBMWI
UzYQ+sx2FWq+k7OJ+pnzoq80bdBhiHfLV3bwxU69X/9ZfpxLpvk4xHfse5hRRii+
bxoCzAESQ6f6ymICT+p7usNEt2rPBcpLlaNSMNy9+O7Rk3HfCs9XDw91rWTjkEBA
b/WdeK9MrTjgSrKGTAHPTkUCgYBFVk5PO0VVQ2Sf975NdaeGVMlCqmXddDVC4Gf8
+z6sLDyO0HBfAtGgMqFb7iuS68i1EQLDDPrS4xTP+OGKSBzx9tyF/bErJ2Epw1TM
tIykSnlpHLALxElhzRkS6P8CF7fkL/KMC4FNRi1W0hxJs8b/ofr2JC3kFRP+QW7M
oQ3vbQKBgQDN+d2FeI6zfY2Ouz8yo8Wibrm8g0zuM6jEtgN1E/pitFgr84HpZ7tx
cSrq5kj0vDX/HNM9pR0ddWjnWFHnhctvsgnfr39nFmnyvexun/uD+dE/Pw44GvJa
WBDLQZ5R0u4EHOCxnoi05b7ZonUIAZ6f6eD7fBvo27cQtcK2iHH3bA==
-----END RSA PRIVATE KEY-----
)KEY";
static const char privkey10[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA0oURKq5qlHNlgl/E7B0G4+mR5mE2z6TL4RMXRDFXhS7NAWYC
ny6dhRBPUZYBvME0NdKRqHGje82GqSr1HsjdP4GHH2m0/UwhusxxHDQccHc7FB87
kkkvYf6ICO/u2vRPgMX7AwCbc5jjloD/PpT/PCqXdqZx608F+adO3uNWci4gfeeQ
vhGmQDmouOwSRNTM+uHSCYquudkSVgBYfACVO1GD7FnzAGqGu1IHXRRYlyGxD/oN
zydUnOuvBIPN9kXp60yqO/viB4JhZy8EMU+u+IIphOZrQsVj6ugSBSYHP4+AeCz+
wL4n7PJ6PrFDkX1CTmcDp8eEi05wN/yWgptiWQIDAQABAoIBAQCMXV8J10uG+b44
BIT3bY83jXfTZQQ3xMiPpLwBouiZC5BzG0IqQtmSbEKGwn8cDLI0JNcrucFofawI
xXS0dvdWLZYbss6TiRyAQWo1hOVlc4o3q2975UIrk5u+YcQfx3/u+5KW+OOtWsZ8
hjwpzQy1YCynVulkHMd+IzjqUihEbhldoarJlsdM2eg/ZzY7fJPx6vlx4ua2+mcE
pIKn6Rgx6+hgiNjhNlg7vmOHGKAWiQokK4oJrw21vXK+w3GeZNnMXjyl+ad8RPZJ
pMeBlWUjP067sRYW6n8p1McD4419nU2Fmxn56HPznXTw4Q/fP4Qv8nnxE685OZw7
WO94Y0T1AoGBAO4Fd1S7AXK9Y19DpDT4ADExZJfwvkbmhpatuW4yIlJt47/EkCnA
n2WDK2cBajhf24wi2N/CnBFZvaGgrYm1NCjKhYmwnL4NIu+PmRYktOAXZBiVT342
fJaHSyNuiDLvh+2k2C1tQgra7i5nS7dxRrWY2MeH5EYjXYupW0PgU1XXAoGBAOJr
0CdS8mNInqLf/zcK89Z7EUIPNj3/Di8yi/3HA33DN324vRIZgDbUt2IUP4vlCYEp
ZGw95stGcTGrrFE81LUa5Urme04960RRXc9oMo6qiWx7MYQjrI4ReJGrELf61IuD
vVYTC1x3D+8TVdGVK6B2IJfcJ1Ft3uwJ6n8xJKNPAoGBAJfFmoLHv7OnOb8cBY/U
92JmyWrtEpdpFTbtTYkLpMEE2y2DrB1DeyAilMFYsJ+D2B2ydrGviQ6zN2zhZOaj
ScAc6yuSF1BEVhuFaCsrer5NjAD07Dxhh+4AFRRhva0k+Xq6GzJUMcF0Ol4SdcBi
FaFj9g69Av0KPfL5KX3Ng7X/AoGAFJdJbsoUUQMhqh5Ez3SniDHY9/D6NdkkKRan
tISMxLskKMLMLwEyr8nYg4rj3I0PuDdTcqSOgJ3/XU0HdrvZfD7d61najNsEsknU
BjCrqF3J8ZuifSnhX2onVNgBxTmCas2oQlOpDNenE07m1xCBzy3u+OCEktszErQ0
Jy0I7/ECgYEAoUZ4/fJjDrLQ0dS0SGJNvydVeS4vL0onT55DSalofIzXvc576h7H
iAsSL/JBxWWeQjEMR9QvfThZACb0hA9Fq91bWszOtgu2yQ1f3OAv0qFT/EIilup7
6eWwvqjUG+OOfnsZ4uzIRNl31iJWlezCxNEWAkZMeBgOKWAy2W8W0Zk=
-----END RSA PRIVATE KEY-----
)KEY";
static const char privkey11[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAyohv5jybE0u9JTvtmMGSNhW3+5OGFvSyS74a3Gqa53nGKGTB
qzoOLJjw/fs7W8aMc8m/2ZRX1dckl33bx0t/m2iZTGalsfsz8vg6cKXO+Bu4lJYO
BpLc4DP8XExY7qp6nz85rDKCHC0/mrovMpJmcgX68qLCxoPrYDe4LK/GdFFP/oyx
raDEDnPtAolMQDuPDkbu/rNLfXSRoYTMv3hSpSbSd86x/2QF3x3aZ4EKEMenY2r6
Nt4ZQu9y+YieFswh0naCDGjW0OvL+EEIe0kmdXBiCofdSzQwXUMHNiSvGh3F8mYH
zCpUyNyDjlWkEjl85b+2LrDlsPdCNznj16ZEwQIDAQABAoIBAE2FjbdFyWHIYrym
xJnstcdH5uU/oVqWDMzbFjL6BxLE1KlPb6XXkedWHyBIEn9dbxM+zNU+Z6RtNg9a
WE5fQpe8oLjSrWeDrG26v7uwa5kiGzOcf//aJPsYSucVPhEUZd0geTXFG3Ini6nw
y/ICgX3xdg17aSxNAEIS8ccA/Og2LSKut4jzNtFkuKP7l462BH1wNLCebRBbR7uK
+IWpVU6hveaFIiAFaviwAUUMFrZXg2gZxJ13QewhdzBFXdd0sANK6cwhO3S/Wb0L
3JVHyQPR3ktncFlN8xJ/BsRcrKPoZ8w2Zlo5NSHC20luzHqYhurLAEz2soTozPZO
d4cy5rUCgYEA5gWZqmo0tlbCSgwsPk4bJ/tMox2FFXMqwWhhFC4SaRQkGaSYb3h2
ZO3Pr1Rv+WInzHeHgv1EK2bGSGZ8hvatUERFpysP+UCM7A99pKUjgvLrbILhAq8l
d06DXgFg5GYv1Bz2IU1DctTwe+NKEgpFGP0XOdG1hooKFbXuzXfYAbcCgYEA4WgT
jOpXskWX1lQjPGWyHKRoCF+Sbi6It8sjvZ+TIePORYChW6FqaLM6CzkjlH1KKe9Q
1CG2Q+rXn2hKozHu9gy/XwIDlYEoDpgtEdXgv/n5P1JXjRKEgjWziMzhavNlUa+S
y/c8sgTSdHlgB8k4/C3KKY7IqBfmp2qnUHyUjUcCgYABfsPS8cHJ9aJprM6U0Rko
F2AWJ1j4HLmIbE9FPE9uU20wpXtALj5Gxaz17MyViGfN0nEVDuRki9R6IABrOCtE
PE1fms4NxrejyS395fznGwEgwXEI/W526hG8iLHCik+BR3bqHIB1vKCbjYyrtVlh
ojOGcrCFYVxTa81TlJXY3QKBgQCrwzdCpkB1ZHqzpCHFoGj9UnrEBq/6rhMYmv99
O+BObt18MOzEelgvuIN+kef++wpMskSRoa7WSlpM6pgVi7NgY45gw5QflPAFTwjp
EmXVZJL+Iaz9nyLCZlGM6TLM4PRQLs5aBuiFbw9AGk2HIVr/L5Vh3aAiFzR4Vtlw
h75beQKBgAeq3iyUmYhLdyEzif/4UhFAbiMNZMCV53cDYIoYI9NSejMgED0nzIcD
p6xwhuM841Rp5uL0Oxk38nivyGP9/O8eSWMST04dje+WLQP85fc48JUT2AYFeRHU
jnw74FmGPl1KdS+in0DTRWkPySmXJZwPrbGyYjj6yCdPCymHBV82
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert12[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUffgmCetL0aBAVoV6hAmixjJK4h8wDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDA1
NFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOPi1kGA/6SsRTMRa4G7
4K7woZWz1bOD2BbWlb5iqsOHtgHw12v52+sAcV8tvtfbqA44XDTQnj0rA41PXewD
QP1m0n48NijiEe8G0mQQPZOY1l5Ytlq+17rfP8A2S4+IWjUtVLsN3ACDLiM4/31Z
mN/VeUJgEC0wVetDapanjKk5Mldf60as54nptSD12uqJLkd5GPdagyvTkjHot391
DE6BgDShEKtN7trTc73cbTncbVk3BUh7+rBXuUdZBLU/pO2ajCkFWxY9ABzUAYBm
dfCQcyzj1sPQy9Ra697twLBRgbVn7w2/UYZ+l0Z99FpNokenkf4/u7EsFYJqQqng
C88CAwEAAaNgMF4wHwYDVR0jBBgwFoAUcZZ9vZLLmYDWoBqNlLM1LJmKj1MwHQYD
VR0OBBYEFMOC05VTrOzytqJcz2nIcMFWccWfMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQCPzncQ7LfMTZfb+/aX2qpMigry
iEW5rROyRA3HHaUQ13hv61lcC9pgGjIRYzwyt1LpVANCpUfLFjc7aVOwxG06vPCf
acYqOkHz4xF/JLGvYSnRIu4T+caDls+U1cl8jNSjyU4wmS8uthEW2UbgSLt4E7y7
CQLUWdN8amS35Fw3gNmPlZ2VvEcc6C/AWPSXT/JrI13gtcIX7w+ClDVTbgYsIX6F
0Pn6wc24PmQx7e7Gm/QhdxdBvgriYxOVZIpgky5Fsqt713tEVja+0VKBUWZWqsUi
4ajzSRVsc9GjHX1ybHc0GHd/q+JNbnkF0O6k1PRlW5s/+lpwGq2PbQ3S0A+3
-----END CERTIFICATE-----
)KEY";
static const char privkey12[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA4+LWQYD/pKxFMxFrgbvgrvChlbPVs4PYFtaVvmKqw4e2AfDX
a/nb6wBxXy2+19uoDjhcNNCePSsDjU9d7ANA/WbSfjw2KOIR7wbSZBA9k5jWXli2
Wr7Xut8/wDZLj4haNS1Uuw3cAIMuIzj/fVmY39V5QmAQLTBV60NqlqeMqTkyV1/r
Rqzniem1IPXa6okuR3kY91qDK9OSMei3f3UMToGANKEQq03u2tNzvdxtOdxtWTcF
SHv6sFe5R1kEtT+k7ZqMKQVbFj0AHNQBgGZ18JBzLOPWw9DL1Frr3u3AsFGBtWfv
Db9Rhn6XRn30Wk2iR6eR/j+7sSwVgmpCqeALzwIDAQABAoIBAFaxoRjiXl7jOqAm
7GM6ekuTN6Wf+JiGe9qUX/tBxf0Ez/UFDq8BKuzICllPKd8srU5bmphtFQXoKz3D
B9QVmM/OIh3sAh6jgncQfVGb7BEOf978mMME/0UUUUhqX2yHTuoUCLvwldWMEi1M
LcYeI85jg/IAYacPUGBkEVSnARgO9ovV3fUA3ZYRBcTFFBN4hHx7Yqc19GHMlv6f
r39QEslqnwFsyNzniwwjhJg3n1+xAqJwYwtxffU6X0r0oOYJrA8a7xXePRlPrGz0
jRJshq3/9S6w+mTQLn1Mo1RybozZ/56nuJM56tpiCPDqwPyelb267uX1EVyBMIc3
gfn1KiECgYEA9ndsL0pDCN+mfxXyXtETGWn9LvnhwUwYB1R/sZmzAefR0r4ptix4
Vx6NKHKF/CzeKPYRJl+aK30N+g7hVs3OG4DmrmZfVwM2DSgniyprRFLf1FBigy9d
9ODDKMF++DC8HVCvkhnllrH0H7D9F0QkYrrUlUQEFlE4bRPt2Roa8yUCgYEA7LNt
FTSSF+4WEzIgL2m1lC1ergoynGtIx6i6kdbP8T/7c1iG7/AlpFkE2/4e4DKm7L70
CReTOYxUAnD7pOxN/P4Q1ILPOLEar8wLMaInIRRHKCMLUPlV2aWlzV8rpChCbCY3
PdS7zulF4mGXhAIp4ZMkbQyqDcNRs5lSy2EfCuMCgYEAkTdwALUKpjHQUdTGh2M8
k57FVGZaw59oEPmSezwKIrdKA5eT0AiHpx0TIp/Hnft5sjy5ghiWJ8V49LrLvm9o
XwAl6t/akNQNhtb/5ZlKtmj2BIyQXqcL/vpnQp4vh2B/le4yZCzp/04+fmWz2X+G
yL03//KhTdzxGCPD4kTlWBkCgYBys2fy1KzO59LOroWmNCWHjozI7awk3GBCUGNI
dL4DfCvPc2DAOLEk+PY/3N79ac41/zqmhUQUqQm0vSdUfiY0qLFjKZhdMoVqFwh9
nsQzZW4t5v1+2bReacT0xl+RhujZG5O9ZK81zxjMYNpZX+sHopQOPArT7IhU7cGP
SJsQXQKBgCNUUTVN5RIHbsfjpm5MZmu2GvmSAt/kfN8sxPYcucLsI5QVRkl0Pjpy
U8mA5lF0qA4LOUN7ubgnRdeRgYiEL2WeISnCmSjA9dxy4StWyZuAHRCsho3meOWW
Fxq60FRVbCpe4duBBygFXyKIWw6e7ZeCq87IK7wzTzAU4BDDO/wH
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert13[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAOeeECEPxwCtA8L6MiwycmTeWylYMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODAx
MjNaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDSGxW3mtoED49qs7gK
E/7Mq8noKad5moW43vo3sOrRIxNWjfQWQeXdZ3ptJpRUfCQu7ZlLbADUxwk72W8Z
DjtRCCvX8Xp6ay97cH2Zvunmbe8XiDWtyuU1zY8Gbi/7L3l1ErzmBH5EqaGDKwZx
PE3aFVFk9bbaWHE+/U3P5NpQAclRN18jrdRkaWWMd7ukb1ascqYMy2Arv25rzhIE
oNLYWuRXhB1jTWKh24yCKcoZp/HHvxRuMEOatHyvO43mtX6lD1brUiucArEmJih2
wty0P0Iv2N7dZQ4T2sUNkyjqq1la3wcsLsh3mH+ZvtoJSxYEWgbQEm2doaK8d8IP
4WbrAgMBAAGjYDBeMB8GA1UdIwQYMBaAFFSV8ty6yXL4RJnoefL9Ia7dK5mMMB0G
A1UdDgQWBBQujnb1cpQmGQ+nIR8XhBUWKOIFxzAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEATlVuYrjpl13CkFXK2mVRr1Ph
Pc/fAamaonI4ndNsA9vAKmDGC2w/YS/lrf3MceZEd0+Meon13egAuvP0YIfhGd4M
i+EqQALjWJGoIYdsEm6grfv3hewf3qnXeLUCZIyArOEAHkmAFQfish5zB21mNsel
rUwyUeeJk2/Vs5MiNdqyGF2gp1yBQN3NsFWrh16NTi6pbRbVbc8rx+q40qDbDwdd
tO4MsfPuqmOyCkuHJPRIZCw87lAlQ5ydT5KI+PlKDNXNUPOvrRy5IMiuoi/9ze+K
d2NcU6oUHFulhIFB/mVHUgeXBbisRdF1GKkIUW1o03yYOd/1aZysDva7q0GLBw==
-----END CERTIFICATE-----
)KEY";
static const char privkey13[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA0hsVt5raBA+ParO4ChP+zKvJ6CmneZqFuN76N7Dq0SMTVo30
FkHl3Wd6bSaUVHwkLu2ZS2wA1McJO9lvGQ47UQgr1/F6emsve3B9mb7p5m3vF4g1
rcrlNc2PBm4v+y95dRK85gR+RKmhgysGcTxN2hVRZPW22lhxPv1Nz+TaUAHJUTdf
I63UZGlljHe7pG9WrHKmDMtgK79ua84SBKDS2FrkV4QdY01ioduMginKGafxx78U
bjBDmrR8rzuN5rV+pQ9W61IrnAKxJiYodsLctD9CL9je3WUOE9rFDZMo6qtZWt8H
LC7Id5h/mb7aCUsWBFoG0BJtnaGivHfCD+Fm6wIDAQABAoIBAGqbLZDEtJxc3NMy
L06FyMJG5CK1h/GOsw0Trng1f1/wYS6IxnHIq/6EM71QVvDrMFzTV/XJ7j4IY6oH
cHSRIQ3DjKN8Nj4JRsVmVYR+NyYhX7Bb/4iYK2b6TvjfTd8wvnrlsA0x+HOUcowg
d5tbEtF2AZ+tgUMWnxdr5WB36UGwSgTIoZer7BNBAW+fxK6khoe8WAwUV5yzLhRW
uWl3vQ9p//kLl/i9VNaCUGPg4c0k/SAVJO0T+qw9HIqvILMSE5Z2fSg5xAuO+m1x
vuOABNx/h1IvLIDMi+jpwG0Wnu3lNQomLpZXGMcDAAnlUvRqMn434sPNJieMcjfG
eSpYUnkCgYEA/hp1+aFwXhMmZyXKkQug1yXr4hI/nJmZ9U3GDkpVQmtNzhTzB8VS
qX9qeaymrRqrxtDkpgYTqih1BeGO+EZBmZHP5ViYLzk4VqQ0osT8tgloDaf+Yjc4
+klYqiDo1F1XROVOa8dIDFtVeQY68N9mk4LnfJ6O7d5kZPwZORke4R8CgYEA06yN
wHEVcHkODZ3EBWL6DCsZcKdr8r1WPMmWXtS3J7ON066+ESRFR3tlthaMJWkMs8ZT
wqpnmcKfWeuGJIShwfskn/ORNF4GXScTel4zAXyvUAwqKWG7Ta494rvlCBgAXckD
9tNqnimFO5zEIh6a+VJp/psLzV+2AKV1H37ARLUCgYEAhaQdECFcG94ugGlb1FUt
TnpfSRMX6aDPXDh/RdzKYphU32IdP6UECKk+sqefMcOpPHN5Deh8Ry3n/iPXMbSh
o8cQdCrYP130x3ytQj4gOw1PcH8+RyGMEfapbzDHw4kj8NQ5Hm488mLG3VPtg+m6
5TIf1Wb+/RPN8V61VVZk6zkCgYAZ98lMGpNuwOsgcIJjxsi2RXNRhriPhvrVakWw
TJhLnpN/rKhUogZ9xn2r4QhAbqrIQ0RzVURwcJfsCCDkygCuM4lCtjbSHdbaBrk1
Aelv/UPkphlmCj9YQFAHSNXIMGmbF9YMqdMipCVcMtc8+FQ60gl+2raP83X4jlPY
EUy1IQKBgAfZ0JmRNucAuOVIJd0g2dUM1PD0mx923/6uIiO3Rv77F4/CSEXjMP0z
Bwqf1ScXUIvyu6y24jN8eII3+T3ny1HmdizcTaAo8tuC89hOYOZZsKPgCc/x8UAY
DWx9R+q+ZZoyUrPffqjgDCcc0EUM1Kr5eityGnOXzenXYud31SwA
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert14[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUXw9p3PlPo8+a+CPK2unoQ6QG4TIwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDMx
MloXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL0d65pwmc2VlFYz9BN3
paVO/4jN6bcxtE++EOhVUt1EjPYcbnQIBKVM9UKDraC6NICMRugcV6PiZfjITDsk
jhP9e4AfklY+G+pbuvP97mzy0x01rFg4I6VMwfsuwT5oeSlM3TAeBPMKJsj7eJJb
bkc4ZSTBB6gdqJ1sq3Qf19jGYppcvfROj2fGCkbLJ6PZ9DR8PD30pk2mqST1Ysoe
84ESo2YiQ0TTDyRwGQGhsJkD+L3FjGiXm73W2lLyF6SsGaxuamclmfeqnA4LoY4R
mcDBRT/ldK29K/RIkAQQv0tknPDA5X3sK2FlY4eYzmRXCOzFeQ100igElu1oRbLB
EGMCAwEAAaNgMF4wHwYDVR0jBBgwFoAUcZZ9vZLLmYDWoBqNlLM1LJmKj1MwHQYD
VR0OBBYEFMM6mGckyPdbSS9Z+R1zEkkaL3UtMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBVx6SYfhc6RBKc9CuKPCrP6b8U
VkJ/weaurLehsJrWLlkEJ7QxuCsezKYkBcm+IheD9U0MQOdOA653XLL1upoFnZSS
Mn8NrGgea/TYrclZcNkugct+ZHsKf8jO76VMbfQMeFXQiApbxeTtYNytjambb9Zx
1tPhpMFt9CYIAC2gIAcjU+CwcWkTGbBlgfAkLWLwvl/iujLrExKM1qrlerS/41mZ
nkE2EDjsWqEAl6aINGgPJ358376oeJ4wgrhogqIze4lBq13XuI48d1NYRkSjtqPS
zLyALChUYOfnrcIvxAiumab4D3MoiLbr4tabyRDdVnhk94uvsE3PxMsG8EEO
-----END CERTIFICATE-----
)KEY";
static const char privkey14[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAvR3rmnCZzZWUVjP0E3elpU7/iM3ptzG0T74Q6FVS3USM9hxu
dAgEpUz1QoOtoLo0gIxG6BxXo+Jl+MhMOySOE/17gB+SVj4b6lu68/3ubPLTHTWs
WDgjpUzB+y7BPmh5KUzdMB4E8womyPt4kltuRzhlJMEHqB2onWyrdB/X2MZimly9
9E6PZ8YKRssno9n0NHw8PfSmTaapJPViyh7zgRKjZiJDRNMPJHAZAaGwmQP4vcWM
aJebvdbaUvIXpKwZrG5qZyWZ96qcDguhjhGZwMFFP+V0rb0r9EiQBBC/S2Sc8MDl
fewrYWVjh5jOZFcI7MV5DXTSKASW7WhFssEQYwIDAQABAoIBAA8qAgVNOBfzAYTz
/4M8Gc0qi3TlxfLJioLGY8ataMqTP5ZQa07kgbLbe+MMefSkWadhGCJue1yJv9F2
Bh7L7JSPw8JfRbWin0xodJIx71dPFTyHXD1qqvkLMZa5hgdCC00Te7zq+0ydqMR+
3Ykmya3uSLXswHdrgWS2HP1fvEgGZPSP59zXC60fGpKJmY2+3NZ8xvy61rrizHx1
bKHQQKHnT+tmyv/CMGb9IjmaEMX45COYQ8HVjyD2x1VVFUWhPrClrze1yWuWN0Ne
dVOe/GRSbE8XIevrkWD8KBQmkb1Pzn9gWcFl48xKFr3qzo6m9A7TEJ87JMRQPkwR
57KwLRkCgYEA6IVlcHrTJ1RlhaL/a1mA+dp3es6Jmfv10g5Yd17fN4Hzing+gH/i
ta206NCjdHCYM/RiGL8GqaOo7mlcTOrqfWv+LrPPDfcgWCUYZmaRvMl2Jre/7z9M
LVID3f2NH5hJEAzJH4TpUjJ9AQQbZrU3fdV9++fcE224Bp/IyWQXww0CgYEA0DaJ
rAsx0iuAfYt6eT21wKfJiwtEfeNr/K8lRRe1HfSy9SMmVdz+wuyYvGhR3HqSWJDb
3AGUaARb/e6IqHGh1Q8rNbzvT/dmOR2HGPO/lRh6SzpHFetGgAtF3IoDs8a2nwD2
A13SH5GDcdvVQ8IlZz9EyaodQFZiEPITonipBS8CgYEA4msrSSlBPjYPmd8MCzOf
xSz4VYyTL2/Lgp1Xhf3LLNkNFxRaAHUHhVQ+Kssnr7YIb+cUMvao/T2RvX65U2kZ
p8ImRMHw6K9xPoP4y5YragNfWvppGwavHh6UB3HkHxUCaG+a1WducA0b7oZaTFxA
Sfc/c7HmesDHhyPGuF0QSm0CgYEAj3V0SBDyg8+rWVUXzp+BMjupNt9Nkrt2dQSs
Vj2ucfRetV5BlJmPAvkHh5ENDi951AIDKkQAX+bZB04p5H1etrEb4YvLC7r3ONUq
dzvCvdS+tkRyJB0WVzTtLrX9NQnuXAHKf6lOFKgq+/EusN/863WBTL2kJ+cc7HYO
pzwhoMsCgYEAwBUOnqyZQfjmBacb8UVXuqHNg6jMxCTRc5f3y9uXqnBBEjt+sFVf
b5fe6WLoJBCt3Cd+ybePh2OlnEFj7XTI9gwnPgHjOhLl3lTbdrdFcOHUpDbRhyOi
P4ozWNuE/V4ve7w/ZT1X/36OVNaqc9KNs5qP+PP28B6Ph7+iTlQeLHY=
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert15[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVALDpQ/1uIFfOOus9NrbAf4otNmmlMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODAz
NDZaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDG0Fy8xnysVJbJ/+wQ
0H1+YxQMwmQKaghtiY9RFg+xLDLgd8omvbAfLdIXqQihLjw/vE76vtjNtJIBCAaF
ytiftVS2w00xYfkJiaIlzZJQfOQIqIfz0YGmwGCL4wtfHwPsVcvu0meAOrI4eOdd
jzMVlMrBn2qohVMdn2i7mXa41goUfPDUlCaS7cFtsNyY8EtIFNYEzMPx6B8g5+QX
cYbQsHfZX7Ix6MIVBicJr+5LnIGqX8EQWpqolrWP9VENJDXWn4pHkD9U/jB4JtJa
ub0upFtNlGyCySiqf3Tmu5UHGuDDVyZfmJMYoaL6AUFWiQ2iAfYt6+A03XFTXIxJ
vAlDAgMBAAGjYDBeMB8GA1UdIwQYMBaAFCKlIou2sg5Chv1ozoEAG0aM7iFOMB0G
A1UdDgQWBBRZW2qi9p/aV3TdCVBxOszR731ipzAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEADOZq8sU7+CHD09sAwusNIsWu
vq3gx4foXSILMGVA56Ix7t/xbPz+W6tz5tshUZRPzCToT8qA7ynwBF7ImaGuh3kW
AiYuWP6bTGLqwYZ418lm+/PpJXhYm3D+AvdRAyDAiFisy4mxWNiDPouvQTg/1pSA
wV2cIlQhI3DPiCGqmxo1946Al3Aware+x46z8P0neB0a6/l3yxRnRTF96Efj9NzA
0eFJhlvTOMqToaLPBl+dQP6+tRlSyS+4OyuSxRqmDJFdU9EkAVIoc8+o4eOfMlgM
EpY83bvFTKcxglxMCx0PLvzvbAcTpd/LF07yosXtrU4zYmpiSAKqTqayQ19Oqw==
-----END CERTIFICATE-----
)KEY";
static const char privkey15[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAxtBcvMZ8rFSWyf/sENB9fmMUDMJkCmoIbYmPURYPsSwy4HfK
Jr2wHy3SF6kIoS48P7xO+r7YzbSSAQgGhcrYn7VUtsNNMWH5CYmiJc2SUHzkCKiH
89GBpsBgi+MLXx8D7FXL7tJngDqyOHjnXY8zFZTKwZ9qqIVTHZ9ou5l2uNYKFHzw
1JQmku3BbbDcmPBLSBTWBMzD8egfIOfkF3GG0LB32V+yMejCFQYnCa/uS5yBql/B
EFqaqJa1j/VRDSQ11p+KR5A/VP4weCbSWrm9LqRbTZRsgskoqn905ruVBxrgw1cm
X5iTGKGi+gFBVokNogH2LevgNN1xU1yMSbwJQwIDAQABAoIBAQCV6kQjrOAR6m8R
vsM5Ip2ekgNqY6KgH7hfMJ07HZGTnap3jJyIffyyZhDH+pQhRgX0YYCEnyhIZOK7
mFuiw4R5WXiZn8czd5ccVAZKzyqHOsWQqdRqTA6huOqUUQ4gGfkRWti+KzYUgS4m
WzTLXdG9ckc5nYYFsRMtgSiuXVgiox6sfTTJ6Dp+uga+35/j1PKmGJxWdX5tGpF1
RdJ8TEuD4jqy4bZMTEr+TZneSfqUsUNhnIzluokjYuaEx/INB57gCQYu0GLhpsp2
jfJ+4PtRCh68s8QXDYnNWQPE7cJBkpCpTseDxhTeXRfFt6MI3xhEA3DCp/FE5U+O
HD0p+2oBAoGBAO0EvVVY/r1PuKQFR7ktGidNF/3uUOhUlXgqfiuQuwk4ILtW8qPG
p9EAqj0mT79SFFkRCxZuFL/DyTyBHAPZDC6uIgM9VnzZ5Zv17DIQ9KN+k4eurcrL
wQHKbwm+oWYeZtUwg8iiepOqEARdCIEWo9LM1f4SLTVw2lXQmBPaNAiBAoGBANa8
XfO9KTP5GG3abVnmazIrRZq0w3c6RTjR4p6vr1pmauF1wLuV/5nhs/erGQW8b60V
8sboEJY0EkSKsm9GQffY1AbpSU1apfh4nLxMh08Iy/tMKz7uy2SUEJ64vRj1Vsck
d6utu9k4P5gSSax39LkvT3s1/AsqrBtw+BHqmg/DAoGBAKLt/iT8X2jNs5R02MAP
pAneMh7pww5ijrkjJMZX4GrO/ZkqhPNWbSf8OAp/GpTfHIO5dpC9rV/Be8Uuz45J
ltlD9LebY6fBtdgHLo2gIDt/mHTN/EQK2qubd8cQFyz2uyPUDNNP9ckqP4ZwWZc7
c/RFwNi9r/wrjrstAE2tUtWBAoGAKqDGPealUU33zYb5Akm9qf3xGV8dFoQYzcjM
phfWRanjOdK4agTC8vddVKNAmmTn/oJG6bmT4V2A6t5lwbU+z0baAG07QgHmxMac
xryid8wVQlPCmgGpmtPR4NBnqZa6nVnd79TTX7HM3hFzUquVh1We/6Sd3lGgTOpG
NLhP0SUCgYBMeOMaz/c+l+4J+6eXt21SVHXHLjbpyMekNIrQ7hxKZmnFD+xpg31W
sNJ3ZC3ORNwIMJHYIBdVSKnTJ4wCHFVNwm5uWUFOcbJlHn6JkR7NJk4UoQGzUlz0
P0CvHtQnDHLZ5hqpoN8L0JAE4I8Z/NuOqH+rvHMPNWIOMAoJsSvN8A==
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert16[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVALdTSOqt5WJFPNiUPq4Cmm56NpMFMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODA0
MjBaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDg3WWwMzDnACnqaoUV
sQ0aRGVq3INPPnMgOo7VOdaVyP6JEiapi16qvL6B3T6X/E5cVdcj1teuhgCqu2Yz
P7fFVbzZHbczwPXN71H5qF57WlsOxQxuohFCDlg9/X7N6xpu5zNONVCSsgjoN6G3
wTnRtFOdeoImbhr8xXpcHTYGuQ+CRlYCKx+R6nO/jpp0l/5hrPVRGqOOKS5TPCbK
7clJVAqeBIOnZlwOJSHjaIBsCZ5a80AG6zLzPtvo8YQGte8uNgyYvyzz7maJWITN
yZtrIiv7QtLAKCG7C3FPqSjwFKWQcMBZuXzR3erppjCjiiLCuKM4iXII2ThKXUqt
nIAHAgMBAAGjYDBeMB8GA1UdIwQYMBaAFI3ebFMOu/NTE+b9BeU89a/DeYfCMB0G
A1UdDgQWBBQAVltWihEgaQyj8/sSN3C30+U85zAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAnZn8mCHfDtaMhrOuqX9zcG5H
L88KmxXcrjP9S+6b4xv0+jD0PJ8f8j24H90wJuwYCg1mMzIs/G9WvmJ185WZV9Ty
TNBowaC8uNqCiUo5g7VXPNcUzxNN+8tHjO9jOkhqx69iQZDPnQocSvYsf1LMJ5M/
bgtCoP3zeuNfdxZdGrC1yqGcu4Q+Ovf9N5gYzLV8mFtSFQO7+8sflQevj+xgcgHk
pHNZNnZ8NkWoH229azpyFqZqp2wZFoDsMp0+W6I9WfJEJVj7atij4oPUNIGSssVo
i2Yv0gwaMmdv7LQVYLsGJ5WV/Clj+nsTC1akB1fU2Wq7EEgyrsT1GctLALkuZQ==
-----END CERTIFICATE-----
)KEY";
static const char privkey16[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA4N1lsDMw5wAp6mqFFbENGkRlatyDTz5zIDqO1TnWlcj+iRIm
qYteqry+gd0+l/xOXFXXI9bXroYAqrtmMz+3xVW82R23M8D1ze9R+ahee1pbDsUM
bqIRQg5YPf1+zesabuczTjVQkrII6Deht8E50bRTnXqCJm4a/MV6XB02BrkPgkZW
Aisfkepzv46adJf+Yaz1URqjjikuUzwmyu3JSVQKngSDp2ZcDiUh42iAbAmeWvNA
Busy8z7b6PGEBrXvLjYMmL8s8+5miViEzcmbayIr+0LSwCghuwtxT6ko8BSlkHDA
Wbl80d3q6aYwo4oiwrijOIlyCNk4Sl1KrZyABwIDAQABAoIBAAEsZplyyvCJqa5V
e/hPAjh+3uCd4fmJ4+PpKlGmiZ4ifGGOvLDpz0ao0X2v/+GfKXGF6SeAuYNpo5tA
exoNXRIkjmW7p/cXLLE0bIMXA9c084CyOoxhQCHOHVvFdOHyNiMaBcSEtrTtROqh
T7C7y2dyv3T07LTyoiVrimXsqRmD3zW8P2Es73a5ycPexJ9ZJ4zJ7b2bxhpm7o3l
psVBl6I/nSW7jix6VFi59yynSIMShdWFCNlOD6/ABNALLFbwhqEtUCVA8aEWtXGK
ymTaXK4lMAw2lLm2qO3lOD9Tzdhy6CR7nTo6xsbAivsTqmvOdQ8JQUWtOUWxR/xN
/cElFpECgYEA8xIeDAJjfU7AB80iPw7sUXdRI/LKkBFoc7yboWEDHdr2nj2sFDtU
aslP6zai/eJGVhEyTrsX/BXQdwULub0VJ8ZmQOqoIBdR/gLPyxO7+i+m9ovcUAVz
XLAlE8GiYEARYJA1WKkQ162oAgp5jdCg1uLXEiapET7gQagzf8PTl8MCgYEA7NNe
2Kc3b52M3HvBUlPfZpho5S1Fz/y6BlGbh7p8t4bhnkTfPf5oJ97MTiou7g+5nxYz
8QrE/51HDIQYZ2VstCp5eOqnmw9CL3Lll4+RuM5LKGjxCT7or6r5uHeIJtBio8YH
OWXNUah2e96wwSkVj59Fo9WfSyThQEzY6AEbdm0CgYAlkFc0Y+ocI19p1TnFpPjA
XkAb3hmPyiDqQeuLNQLWSlByg/Vj6wdQuWuwXfEyaxWTl3S+1gsO2hAtVIpLWf06
+F41Cg6RZwk0yBp0t8gFEfgrgdG6TB1X5aDVPMdrOHt3ANI9sWL48c1VA2llkShK
DSRPmcnm8GCXxPaweALrIQKBgHuakk0ncTKLG0LUQIEUj2u/rje22lIEiJ7VpwQ/
G+q5tjXholZ72qa8jQtazuWnRW03rGJpCYqT7sokg4wrjoG2DXJDnri6TQPwxs3E
u/ylhuqPYwY9VDqjVPSnRmNjrafmqkyVyoSNFVHvV3dpVvx/11v8GcihdgMWWLs2
ILw9AoGAEg279YJO65JEhGhJQ5+QPm+BNsN4URZS7qf9uzTMyZBpjb6Z6EmtIZM0
svPvDFHcsgZDsM2vMnFR9lfbKtlhU84B+fEwYaLSNQi2IqatsqRLJNLVC3UiBzuY
cZpGJ9zYdXrmU8UJSU+9wSmvRSLUQF4FkZF3jxNk8QLWMBJEGy0=
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert17[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUEI7s3H9J1Mj5d7AqMFnl2y2uiwIwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDQ0
NFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANPuCqaQ7sTq/cbdGzqe
ebZZuu+ij5Ga3Yo2hqauOAkjOCW4U3RKfCTv+Xv+M7NXAZrLh1+0K8DMdOFywYja
D0T61PMbt4JjhJ7bvMcCH2rNDF8llm9K5t3f01EzuLBXRmbyvxR8Yr4O8v/OZWzl
ngHb28LAUoVDaC/AWDJDld75tw0/P9tfktOWR1QJVRUJPRY1UrWXsjHui0O8uU1a
koujTGdZFMLENrDYY7NMmbLD/9Q+ItB+Y//jlhE4xP3mNjUcHsBv6kgDw4p5Ka6H
uxYRkTQEyCAjc3jsDJSHCC3vkhLjaY1j/kb2/g/rvS16COscc5/yolP0gsvtThJR
XKcCAwEAAaNgMF4wHwYDVR0jBBgwFoAUVJXy3LrJcvhEmeh58v0hrt0rmYwwHQYD
VR0OBBYEFOCxP/pWNc9ZkJrFj4cxWX4SKCP0MAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQAfvcLFcdxO9K3+726wtTfgF1SQ
U1imSgHvJFPGk31RnOslyOQ+ShzoDoiYTv51+gDoSaXEufxGLhJjmtOz2PgmpiZw
BnzVv5yLI6QuRFLWjyE9RQ6JgG1TLm6At0Xwv2Y9sYF/hXpqCRk8mRu8fvZV8gcg
CXSHsBbAViin//tH/5thbTpFZaDb53ESojrupdKtPVmwcDOInOkYlEjH1I2ZY6el
OLHuoCnY3LfN+R/Eqw8rMwkFM1H1BzSWDxRqkceBNClJgkk+hsNu73ztjzLccXkE
JrSbL0O1kXfzBnjV2Iwst8yCkEdwAL2poTNkW8z+k87FA3h8LYHvuwqvrx8m
-----END CERTIFICATE-----
)KEY";
static const char privkey17[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA0+4KppDuxOr9xt0bOp55tlm676KPkZrdijaGpq44CSM4JbhT
dEp8JO/5e/4zs1cBmsuHX7QrwMx04XLBiNoPRPrU8xu3gmOEntu8xwIfas0MXyWW
b0rm3d/TUTO4sFdGZvK/FHxivg7y/85lbOWeAdvbwsBShUNoL8BYMkOV3vm3DT8/
21+S05ZHVAlVFQk9FjVStZeyMe6LQ7y5TVqSi6NMZ1kUwsQ2sNhjs0yZssP/1D4i
0H5j/+OWETjE/eY2NRwewG/qSAPDinkproe7FhGRNATIICNzeOwMlIcILe+SEuNp
jWP+Rvb+D+u9LXoI6xxzn/KiU/SCy+1OElFcpwIDAQABAoIBAQCU2joS2fNKzj6n
Ab/RQWP9hzK8g7JRM7PtEvUqd22WRW13WOsM4BAoJYFW7KnFihPvdIrF1pdwmGpz
uxViGL7m1ULi6hHPfDS3G2GRadVFP3Y0EgOCcdqL9lzHSQusl/Az+MXrDp6QHZHh
yEcG4JRFe/Xz6P9M6JZyC6gmzb+jVvwwzHn66Fa0SOKCgNRk4x7isInpPSsGfUkz
AMoMr0FfIWptTr2IEdG18prlpejkZ7I2tEvOkK5cj9768nXf3DyrZLBdE2wgyA/i
UEcIoSXME2AhsxhxmE2JRPrPmQB/NVi5Dqnv1DkXwXgPUem3CAj/aOjVGNLuNwIv
boiPsqKpAoGBAPC+pLj3YZ1lT6y0Mhp8K6iS9/MBjE/tPrOrWuwyNSNsQ/LkOiKT
s47y/W4qgr+hRXseelAUyk2rOAep0d8R553R2eizv+QNqffdb4tSZrdXllx7Ualf
SFhHE5xqH0lKluzuhKLDuYEXiJX1lErAG34eHwRN8Wr2AehDs7wHGIgjAoGBAOFb
9tCvACWDAuCoZeAnDBYeQf35C5DgFW9tZFZBOr7MsPd4eEDEulH5U/G2UkFGvfRj
PbMvC65HpNXCMjABIKJSk+qA/YABnJGHV04JPWR2W/MHa8hJC+sj8zXqy1FTcbG/
Z7gp9kDaxX6md/FEmXzwZ+tAiFOTFRDbAkixPH+tAoGBANYHJzqp88aSgzrvGHwj
WE0OHBGWlr+YE953wOWr3UIYxm/yJx0GYuP7cwO2F3d2UOccOXFoilJyneVyPHss
19We8zPGWhrNawZJfxlFsLveynZ9zCPf6dlRl0W/swDdp0AM7S4nA4yY930aTzQF
hmuhK8IP6UlGXHu5dNj9FfoxAoGAAelx0ptpeOf/okBLfPdX1ugyzyDJxFTiWaun
6kNraCr8ZV0USBhjLEbdO3O8huk+u1AMOfTwOG5LK7UWD+oyiK4Cqz6k5+0bkvB/
AR568ts+NHWfqHdWLoyDh7WSPAJlOIjTLHDOsgYpHsXyxo06cCDqAMjvXyYkS5xQ
DKXUSZECgYAX2kLlIbXJlfFimrIx8oeMf7d5rQZrqkNubPN+XX0l5hVTBVxcRWr9
RUkeqCdGCTWHX52cKfmI/8h2XqC3gyI8PdmuTgcZkM0aqAGXUP+fnvu8H62/q9uD
dkVAaycqQvBp4l05AaOfKnNBr8FJC1OXvHg4ua5GsZWLqssKv5VGNQ==
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert18[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUFO5R6D9h65E+t+HBO2ayOXcRHXgwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDUw
NFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALIYmxO+IClwKKKR8ZcH
xpa03NjwBziLnmp/v9HSs8imvorqjAH4g9DsgbgxMbnrRiNfmPutGMX63zUUIJyz
+gECgbi08xCROGcRxa27274TBoGoh6jFyvERCaaWIYrWbCX4DSGQA5mwlQrZiQms
lGToWj9rZ8QzDJZkXg4JFOqjp80wakVZORQBPHCXB8jzyCrYpyfFQrum/eXP/qNZ
wx7wbhOo/p4nYcLWvg1RuzX0eBd1MoKtBmHoI8gkQ3voOUq5K/FdE4FjEzKthJSQ
UidPKdiik7RIIvNhgQ70v6LlY7SFgBsakS1pc8l0OEYRDx1UIBzMPRUDmvQuctof
wNcCAwEAAaNgMF4wHwYDVR0jBBgwFoAUkGsumVm8hQQDvLYK1Io9OoLvubkwHQYD
VR0OBBYEFOBAvTsrVO0vmAARy9ZldYWB0MxuMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQA6gFev/7G385zBKp+JyepdbPxN
ut5BRVq3eVu5gy62mNiOqfC58tv9XARmUyEpPVaKX4ShLYsNj7Ew675omWRYCKWV
vrRxahlxsRAXfuAepSUbEhZprYj7ssjp1zlGU4MAvAvzqJjEcmQXltyWb0/wfg6R
PfkD1HZODxDIIDwJbNxpyd2FM3HS7mffSKHZDj/ycxEkeHSaIzvDqQ/+yv/dkO4y
NyqRhR/+4/hjhW6ipLJuYBWODhqR3n/Pgd4mBDXaYyTqK1IWlkft5lx0/JPA1p1b
xnaSuttLwhJzi0ebIiTuqcguYAF0r1e9ZmxkDkZc31HZIUKy2AEelBP1MLSY
-----END CERTIFICATE-----
)KEY";
static const char privkey18[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAshibE74gKXAoopHxlwfGlrTc2PAHOIuean+/0dKzyKa+iuqM
AfiD0OyBuDExuetGI1+Y+60YxfrfNRQgnLP6AQKBuLTzEJE4ZxHFrbvbvhMGgaiH
qMXK8REJppYhitZsJfgNIZADmbCVCtmJCayUZOhaP2tnxDMMlmReDgkU6qOnzTBq
RVk5FAE8cJcHyPPIKtinJ8VCu6b95c/+o1nDHvBuE6j+nidhwta+DVG7NfR4F3Uy
gq0GYegjyCRDe+g5Srkr8V0TgWMTMq2ElJBSJ08p2KKTtEgi82GBDvS/ouVjtIWA
GxqRLWlzyXQ4RhEPHVQgHMw9FQOa9C5y2h/A1wIDAQABAoIBAQCBwpfK7K32Qzf4
KynBBvNUfvaiQVChjGih4+5Y7JH9UjBmeroD/ZPdN1TUZVN7yMZTo6WNSfNQ3Jag
z8drJV60OaVWdNDcc1KLFlHXYZabiLUm5yngeCyDNAy6CYzq4Yvupiq7F3joE9ak
DxdfqxR76SdIKfNy6lbzhLFavJc21UuDzeC4w+6+50IUFDPpyaMm27tJZVGY55lZ
BQzm03mBmSNndrLzWtgvM312ZLMxij3tgejJQENp2c0CTWWI3+sKPI8JEhCySnab
vA9yWE8xJqkybRkp4D9pcSBlXFed3a9zxhKppZfW3aCgXiFj4aYdieRj4lAwZsU+
Om6GNyRRAoGBANqD4zoXSMsUDMQ+3WlQ0xEgA2BoW/gKH0nCDVF2EAOIHR/4EfzN
rwHS5fZP5K8fK1TUUtNB4AXmG2Ws7AUULKbSR6lD+oxZOoI7SGuOajEctgzxOL9r
kBpGHrxZmGc3yhl+OqJTiYCRUY40WAa7RiJsa0Bu928iBbfjBS5TM5/dAoGBANCl
th2rX+sVjNIH8BxiPtyXA7b8I+7oVlB994G4HwR0QKic/9QB6a6Y7dl0Q/B3bkRt
pmAYD3IcBRA4AenmTd7eGFBTi7bhgGk3i0u1CNDydmb634hPUP6OcQE2hSknBbsG
y1SWJS1+ZRSGkPwD61l27ti4qECGsHCvRDLXlvJDAoGAPp+ZmWZe6ERgvEU/8xTE
yJ2iGhkOcnGEMgE5C+WIBVsW7biMfyT7SMyGNycCZOXL503KRt0UNNyTcXozUelM
Oqr8+peO0HYp/HS89oYkDNsrmV3wY3uSGtWJrXUH5lzowJGagDbsqzn6Y4TAzX0S
Rn9H5Xln1COt+9tF+tbBbskCgYB/voWTAsL2TrpqOXv0FN7B9h/LH/LsKgn2vDkW
jeFHu9l+oqkrZ4RoZq9xbPLPvn+fm7a6rOCAD9G1zWS2jU/t/PLL82reLJCPWveJ
Aa0FKP3/LOhBL7a6NAHSKUw8Fu02RgGlp0vZ38IJYB5Sfh/RJbU0sVIqN4y9YUGT
E1YSuwKBgA8F9HnIyAbUBGzJ94tw1maj5H1Hb7J6G4OtMr0MrSKHOpcU1CATF8DC
A+Xbb3qxm3FVhfa/c1hN9Xy7h9j9K+tvN+vm9vZgAOYvBqNlfURpxeRcC5ri4RJ/
sda00ucXsr3iB+9h+dGDmMTFa94Ys2xjVX4+FSb6jMNPGzRpA+FS
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert19[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUB0e3BslRBY5xbFcdAkSpVh2PPHkwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDUy
OFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANjVCSch6UvenKmaRJh7
oFrSLOBtij/rUrelPWYunLU0X5eRzhoGJziwu6977Bkar2zL2M65BF8S9a5zpkeZ
g3tYQxgX3mrCuOHBmrOWkDj7PiM4r6Q1BWWOav4RcVn9U/Jz9kgYVniPF6HT9Ajn
SMDfktoMR6JhELNku497AaHxPhMGfJnuFhzAtV4o5daZ4rq87iC2BABH2hUpX5Fb
6KZUQVfEAPEZu4u8SMCiSoPhd1HXpEjWYnCUmtESZ/KsoQnnirbBup/5ap9wln03
bWGk23bNh3tMFPSgYdkmSEVKANVmcqILIZ/8WmQcVSFg+6xtEWVqJ1afvEdPxAGD
tg8CAwEAAaNgMF4wHwYDVR0jBBgwFoAU3eKOHSGG0yupToxwaIhFWug7AKAwHQYD
VR0OBBYEFGrMznQ7MOyEilEJu7hpKRdRA6PnMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQAN3YPBfq6wkTCtE+nO//rj1/P8
Ad4PAg7qrtUVtO3VKOqO0EaVo9dV6qbAJSwAyWZfXhoF0223hIK306Y9SommwYlj
9SvrFLnaTFgd13lnC8fEkCiMDlOG004/AC7nuAK3cmPb7dhE7FqzOZqfSK4QApyo
GmNDeqdcNqLDzjpL4iQmGLiYmw8F2Us1phXni7SMDyWJ1eGtKzv4pogWNvR4w8Es
9nPskZVl6Xjpqd7lewmt/UrIaXFfODJmGzLzrvZ1MHYj+2Bc/JEO/1zIlzhhZaco
FquEExzkogn/cJBzh7ecb50kU6JblhoWa4xy7hGHt97oF54GBJpHb25dTWqJ
-----END CERTIFICATE-----
)KEY";
static const char privkey19[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA2NUJJyHpS96cqZpEmHugWtIs4G2KP+tSt6U9Zi6ctTRfl5HO
GgYnOLC7r3vsGRqvbMvYzrkEXxL1rnOmR5mDe1hDGBfeasK44cGas5aQOPs+Iziv
pDUFZY5q/hFxWf1T8nP2SBhWeI8XodP0COdIwN+S2gxHomEQs2S7j3sBofE+EwZ8
me4WHMC1Xijl1pniurzuILYEAEfaFSlfkVvoplRBV8QA8Rm7i7xIwKJKg+F3Udek
SNZicJSa0RJn8qyhCeeKtsG6n/lqn3CWfTdtYaTbds2He0wU9KBh2SZIRUoA1WZy
ogshn/xaZBxVIWD7rG0RZWonVp+8R0/EAYO2DwIDAQABAoIBAQDSG/Tnj4uycf8a
tZr8qubiusCzpFGPyKb5v0u0xTwitqJei/HL8+RzoYXs8McCOIJ+iTUIc+4RdpzM
s9IPlWj7z7YMFaz3hMKEOAeBOEd+f65hkN9Z3erh0tRIZ3yQN9LtoIKor7Sy0+2R
aYISUpOEGeECG14asbXuL5Ez4nzdfs5+9EG6EJJSqO5BmsFzwWny9pX3yYHeQRqP
y8J9r115jjU4LDqdmL7YFcM3tfYykQW8Y1Z4CZQHxX8tlhuaSYSZ1kflJY0vPSV/
1h4yhEAx1Zwjwo+GhU05NRhDrh5JGkYtMXHHoJkxWme7vyAY9zh0yuzlLBHafqr2
1x3lSt85AoGBAPeOIqMm6HPR9hDKBT810YDOaACWmWKly5y7hB1AH89vuiEr/j+K
leX19kOg1i4Lp4WbG/S6JEO95KkAQezdUqR4U5bLqTSAD5YVv5clDdudTmm0ar0P
I0elRwG5LIH98iYY/0In4I9TeI8brk21N5BQ2Ldl357UAics+HAyE7xrAoGBAOA6
mbaaPXnvYjIEuuNtoryM9LhVoc05yd1hXCPvwu29yv4V+V4Uup3DEL2gFK5MpwbT
1HQzaNhLzCF76Z5Tj2d5Xf4WWDHvMPUX/lj5nxJEu0OmMnOiimn6yzruHMwr31pz
KolO13k2iuBispFWOhQaUjzJs2vf3eC849fevJXtAoGALVq+H9QWDHowMd1BXtZW
XtnyFgGBcqBbkIiOaOmBqniWylMMDsouMmaAyt0hwGfkNkc3tPuRf1PAG/JvIZ1e
8cEV12/SvUqlEGNFqcRhKJZGcnlagO7QEOVIABnT/LDw92fnBFDYBIpPcTCqG5SS
7H7TZa5lbOz8w/6qOawDD6cCgYBsub2DH3MjxwGUQ1rjoGvChY68OzmDq7vITsdA
3adlZBwj7D/IcMC0cruRVyfwBFcRJnHkDnGOP+KxccYr8VSd1enPpMILJHJWxdWd
Cx0o3I75FINuMqhvDx6wankoPzXERAeiIMq2ic35U6JGessaqFCsDRmk+x45NR49
tFE5WQKBgQCy0BFcMjVw05s3JAAELAczWKnhvWHJdBOp9Y4d5x870JhHC6jlOuAh
ttR4n11uiZS+fvSKFcdpnLF56/R3ytCPGUPhfwCQowxazJ3J4EhzGQlogZouUwy4
KlJIISz2UwoXt9eWciqbSGv9PJpwyhm9oCQTQeLfDPuqsC9sSuGThg==
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert20[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAI9bPddHTR0kwyKStnWEmN0lfV0/MA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODA2
NDZaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDi766mhK1HxzZj7WWZ
N7OXWsUpdUFAARrmpYLunnRWfHz+q0xBGEP9FLVFTxrJ9S88TvewxlRFyTK/KJKX
GIcYb44xJlyHH9pE4vRlRdivpAvqC+R0mxj29a3LFHBYn1mN7SNAyTjkE920qCKe
0O/+DBmXKx94MIArEQYqMPwOL6rW4nLPMY2YpW5SWxvEJtxo8vQhEgei1F5cKT0W
9HX4C46EY5e/jKWOPvW+ZoxOswsKLqS4prIPUryS3pYmCL6M0sHmsMzK/AA3U9/E
5wrZjglHORBn7JNma0ZeSjPl3ryMir5XJ2V/S1ZmWfLdsVUk7A3xpqhqIm0wnIPA
fxSDAgMBAAGjYDBeMB8GA1UdIwQYMBaAFCKlIou2sg5Chv1ozoEAG0aM7iFOMB0G
A1UdDgQWBBS8V7QGcIXIe5pmr+U0js/CSe5kzTAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAAS4HjpWWazcRTPy8t4YsyFTu
jNZeeFFRljP1JKBoL3SCHtR0EPNx0OYixlm5ZwP9VLCksbo1rPAcgBo+UeLzFhfp
h5F2O11+Iuu5s9mpuCkyGFVX3YoXMS45TfT8nyJqhlvNOjJPycNYegwwIk/Ozjws
7nlwYucANMymiA1Xsl3Wo05GgrtN8jShkAmV3HBvOQn9cRD7zQVgqCZlOQD6ReZL
oeHaLciq4LuWcRHxRPTAsaTRRRyARTahdyC1nlCFQ1ONx7/rF2DOrKMMSQ2sw32L
AKlW8ucF68JKicTguoe9lyS7twvvlVEyzL09XdoYLLwDuzvwpjk6XQp1X92Dqg==
-----END CERTIFICATE-----
)KEY";
static const char privkey20[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA4u+upoStR8c2Y+1lmTezl1rFKXVBQAEa5qWC7p50Vnx8/qtM
QRhD/RS1RU8ayfUvPE73sMZURckyvyiSlxiHGG+OMSZchx/aROL0ZUXYr6QL6gvk
dJsY9vWtyxRwWJ9Zje0jQMk45BPdtKgintDv/gwZlysfeDCAKxEGKjD8Di+q1uJy
zzGNmKVuUlsbxCbcaPL0IRIHotReXCk9FvR1+AuOhGOXv4yljj71vmaMTrMLCi6k
uKayD1K8kt6WJgi+jNLB5rDMyvwAN1PfxOcK2Y4JRzkQZ+yTZmtGXkoz5d68jIq+
Vydlf0tWZlny3bFVJOwN8aaoaiJtMJyDwH8UgwIDAQABAoIBAGbXwApuriiNOzTl
7NkWySiaVieHn95loc9Myp1g6dAX8n1nG9OUL5uNuuiCBN3pY1KtZsqR1rcZiwne
VTBsSSzlwUa838nMwZneUGtkz+IpMc0LPqkpcJLam7wgsxk2fds30HYXutSBvEOW
vrnCbkBvM1myjTxmnm7l9ClCsberP6gbSSA4v7yafRmYCcRCWMA2eT/pHnTc25Y9
3fGOpK7+pZ2eOFc1NnaHxctm8vQ00DIaGHM3mvXGmGHlL2J4lK/Fp7oTn+UWMY3X
BYqZEHt+sOcTQ6wH0krK/6YiWrW/gu7fNqZDZYEo3+9z7dprjXyDW7pFsKLw2Lxj
cPgAkCECgYEA/2w9PkSG1okonc6Ly/ICNjqa0ZZVklTXky/dWHtdy5Jn/CemdYfM
14ya9ixsNlqwTlLnQta6dStMVnHBEfIN4NTB95fcppAWkoDyPFQG7EkGUwf6EqTn
yNU+aAuJCHncquUGmTrl0hlBNLTw0Nmr925KFPjTzcFRt85q2Yx1rv0CgYEA43L2
s2zlZa40oxtDKe1GxhXwwcaarwnUhPLpj1BaK/OgJzO2FU+Gl1OKlcxpcDuW2ggc
Om2grXMqKyrkwSD7mO6fz08/f0fpMsn+lpEOOXy2W3xFWJVOGzBZfx6SaPbmuNF1
IkU7pcVIN55GyArEPNruThWZxI2z4zqTvk8N6X8CgYAQUn5naEGlnAY4b2aHleOG
S6BxHEZDn6BXC4v9h4Sq6aQzfOUqRY9VhuqwKI6RuqHhn3Kf1tD7g9TRAVvxJaK1
Z2Mty0qSakrG+e6A5G28073PuIbGgjs9nkLqqzIKlJ8BXLlbOWROVMbR824gKojO
uwGI7Y27bEsoj8xMMqmikQKBgQDN6vYoyS0CGm5zQKpU2gmaKqD2flzqnaa22sjT
Q/bvFbjn5OwzbPU4KZ0tM4w93OUc7fzIdRnkDoCd5k6F/ttpwJgUcvXUwQECR1fj
bt4G876RIvgRU9ML0IEmEkKRHv8RUiiO50kyMmLIX9ZLI1w4kCjxrqKxDW1QCmda
A25f9wKBgH04WxS0r6JAhaoqIbJ3VgkmVd3UYNkqFHiyNvYRv3rT7lsxYnU7GNhf
bddKGpexfxCgk481xA/Xh/GCkzYaM1nfz+jL6fCtYdUh/71pKcjrGRR/t7laKsyO
TIXockyFFa/R1A1xUAJbUi24my6r5tWXaYMNNuZWLYwY236ErGNI
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert21[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAO9uuXzAcFkgxjW+rmjY0PvQtkB5MA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODA3
MjNaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDMplvhUFNjG2bTnhv6
rA4eklwHzKrsTRG3pfT3wMYBo1Hj5Cw3n+QIz5plg/fA0LNAWuIVKX9QnnQ9rjlb
DQGoTGk/huradVt1ZmHYJ5WbUCwntWloyJPXbZXGJlSpim5WbPZZRyk8c58OYWwY
ZA70d1el3TF+PPKJekYTzO9+hd5Poc8fDnsAzOtJqt9w4NibSwJ9B5RhTdx1xzOj
dCmtqMqjVtzopQJQOBSJu7Qhnf/zUQqrVnRppd8nHxyB63IHxRPi+4UhPjFmTHjg
H6MbqMCwtk+NiaWE+yFjH5TQ96GVmD83NpM1CdH7TBO0Nhfv2gxIHlLAEc04o8NV
UDXBAgMBAAGjYDBeMB8GA1UdIwQYMBaAFHGWfb2Sy5mA1qAajZSzNSyZio9TMB0G
A1UdDgQWBBTINVVJjl331EthPj2bXE018flN+DAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEATH7hTj2sT7qYO6HPgu6if66z
uIymD1hp8rSgR8H616KtAYj/yQvOrQmVgmQiiTg9xXlzON616aNDg+TL4nI+hjhg
21HHsCZmCT5i3OAMLmH/oh++fDuJuyTsrA7oHwahzRK90J9H2X7Zz5oiTJB5hdR1
3tQZc28pFhtlQea0uuhZPBYp6Hy0kvn8JdD8BaT4wIF6estwxYnofwkg4j/uM87e
lYg8Z3KJJ3ueAmsEWMk2dJCBrXI/838JXRLnCJXvqtfrrOaUeRPxiyld+lj2w2z1
cYWceXP0EtH2VURdcm/vPAtaDWcSMooNHP+goI+yNLHqlt3O6BsErK7BrJxiBw==
-----END CERTIFICATE-----
)KEY";
static const char privkey21[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAzKZb4VBTYxtm054b+qwOHpJcB8yq7E0Rt6X098DGAaNR4+Qs
N5/kCM+aZYP3wNCzQFriFSl/UJ50Pa45Ww0BqExpP4bq2nVbdWZh2CeVm1AsJ7Vp
aMiT122VxiZUqYpuVmz2WUcpPHOfDmFsGGQO9HdXpd0xfjzyiXpGE8zvfoXeT6HP
Hw57AMzrSarfcODYm0sCfQeUYU3cdcczo3QprajKo1bc6KUCUDgUibu0IZ3/81EK
q1Z0aaXfJx8cgetyB8UT4vuFIT4xZkx44B+jG6jAsLZPjYmlhPshYx+U0PehlZg/
NzaTNQnR+0wTtDYX79oMSB5SwBHNOKPDVVA1wQIDAQABAoIBAFrs9xmulL4N6KvR
LeE8HLHcmWMlvjNrMpoD9Ek3r0H/DF1SfYrhUXHQu2qrLaLPlzTarGR2oYhyPndB
N16K8teRy5lcGnChf9cze/mL7N6qylOCOKlDpNwsZkU5tpf1V3yywOl9woaHYH4F
Lv/PRQ29EIYzNtA9yAzo9MXLYgU2Lb31LWUM6ALzxUbT6dx4oIUKoK7uAAy27bb6
2+d3qA2uXO7OWxzM/Gt4W+mCxxGKFmE37kYVmVBxCJUDpROipE/tVmqOp83OU9yc
dCnEPg+kH7nHlkWfM20YOWorFOr7ZSMydbTlEC9e+wW7iXwcutZfkk2UhZh35ggR
/xD8N6ECgYEA7Xe/9PbM3dhdb+2Dm7ZIYc2A2xjuYUAvESpatfucqUsq7MLDsC5c
O8OggmnAoCIDAkXhmz3/Cbt+tSByBj9obG0bWG+T/enD4DQwqlxEDTG0LVcJxtFA
+xu7/JuNgYNnljnOlSEZy7u67YjV4CdYrTkzcYx9MN52WYc8ylEv9gUCgYEA3J70
uaAjiVwrHV7HzAilHZZkItc64Z98Nk8zAJAXFo4B6hHrugB+rR6HgTprpLV69Es+
365L55yQ+TIojvFLpsnKS0kou+AoB1qVVoFAduE7dfOJZs5YzFhMkrV9grZXOwpG
CwNTiu37nOSglbSDm+KbNExV3TVhTGlTo2kP8Y0CgYBC2TAXdJaNaQg8pv8iiXjs
114Lz4Pjqp7VVTf9/NvIKJnn1pgh85sRU+vOapjIO2rfa+6rthMimT59HKocqiLq
Svikt5xQoUr7xZVhj+G0WSagkNz2tBd32QHdhaibKfOyLYnDH3NUj/96OMgEdu8W
rICX2XWwmR916oUpaxt2RQKBgQCaI4Sf3D53zalhz8KPzsptrZEwtmcxE5hY6WSc
1A8NdxHnU7X6MjZPBbFunD5wyUqqO5gl90SZAU5YjK7Bj9xnHzzy9J49BRNyb9s+
oPdFF3/PnOM2rJxlyJtXNO4H0RWI7liZgS1xN2w/5Yw8i+4Xj28YTuz+rDjahgwl
c1XsUQKBgBfsgewb/toFqdbi/TicgCV3yWMMfsMJFmtgOvgHjCKg6dh1Z5kEFB6K
uBP/kkUG4uFxK9rqEXJGU4OQ1t6weap/M5KQviCPSqPFmpObklA0vSH4QP5uJyMF
8wMOPqx7vSnN6Tjc0iYgUVZYDSQAE5voryoYlnw/lGD/Tmy/bPgN
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert22[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVANBhSrAwpqWi8OFLqmgNYYGhHhqBMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODA4
MDNaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCupQjA9hjWnsCyN3+2
2/qUfe9UrxwEYQfEQqEgErTdwEbGeI26swE2yuO16ECmOwz7vCrbbCU6WX/hhfVp
F/PAX09vUoxZIAZWWelL9DgGI7h3th3TW8/FiheDZW3PS/XpJJbpMTe6AJupReIq
kYk6811FqcOkCgIOvAQiBsz3akErdy7AnzJzG5GkibFDls7pTQeDzJQvxVtPheJV
mnF8SnEmRnSPwNHSntDwbTwCWAS5RowriV0uqaRFUKcd4+ppNW2JxvUjN2asekh0
VbJsGgYocaiyWQj0dozlNfFBwBVJzbT0qGQ9Dc3q4GNU35GizIC6CfInmfXsX1sd
c9HXAgMBAAGjYDBeMB8GA1UdIwQYMBaAFNpAwAJvnSndWLlwWeHLQx+Pyn9RMB0G
A1UdDgQWBBR8CUwGm7t/C4BT75ik6iaSkfIi2zAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAegC7vAS2yHcA3AZZYunC0EPW
GVwDp/VqXnhjAYe8cqievGFtCAHfOg/hL8h6jj7D5WcnoyowbgS6QDtolOT3o3YD
yya/vONOVNVJ9nLi/lYhIpGb6VBtwcwHatCUMta9HQ10muwoDM499uRtTiSobfCs
InERxC1ydP8VyGHTqZXpFDNgd3CbO4qGSL3E+3ENfH7gLONvj4BTlq1VOfwKAazX
XFh0QgEfb0aEVYBvY4oGkt5WRiIhHGepmWGXDTaarcGwFba2VOgyZGng4wUVXFkZ
Uv0FMLCEPo1oHIPsZdzzpt4S+bd5uKsL0DI/9n/k0Non6Mm60WlqZs0gv24gpw==
-----END CERTIFICATE-----
)KEY";
static const char privkey22[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEArqUIwPYY1p7Asjd/ttv6lH3vVK8cBGEHxEKhIBK03cBGxniN
urMBNsrjtehApjsM+7wq22wlOll/4YX1aRfzwF9Pb1KMWSAGVlnpS/Q4BiO4d7Yd
01vPxYoXg2Vtz0v16SSW6TE3ugCbqUXiKpGJOvNdRanDpAoCDrwEIgbM92pBK3cu
wJ8ycxuRpImxQ5bO6U0Hg8yUL8VbT4XiVZpxfEpxJkZ0j8DR0p7Q8G08AlgEuUaM
K4ldLqmkRVCnHePqaTVticb1IzdmrHpIdFWybBoGKHGoslkI9HaM5TXxQcAVSc20
9KhkPQ3N6uBjVN+RosyAugnyJ5n17F9bHXPR1wIDAQABAoIBACfLOPxm1vAISlYR
aUtsQmOVcnQ0Vkj1JwTkPbyGJqokhrvwejLhWNR4VHx9iEWYTPQLQySuNqld/VKQ
6INizsq1rO7ru0kPkAIbCJqi8iUL/JtiZtdjIyuatJLfCK9kWoseKDjIWy9Jti5S
2x7h/IfvLgCSlx6InUFoO/TY4dk5/0tYULcbgzWyaukC+xQ7Vod5PL4Q/RpTvLUk
Ff9kn0pfSqVL6M1vBWh9gjkEE9dLwrL6zi7arDdiSBlG3ddIlfEw1GQRUN7DqEdI
rfKi2sdVF9mL7Gjg9j1hNfTx0M7tb0tiw0ayK8n+psXdSnkWkIuZjlMw9BbsKQA2
oUeAlIkCgYEA4CoVm3De8molX0Du9A8upRc/kIANhSYhBqLfNLLUp6Dd/6MiJ92x
VnA4S0O4YZOINM3gmNeEKxco891VJEj6TQndrKo4p/9oo9M/2ycqvOTEEweQH7sN
sJ64ipKX+bAxNO+5gZPJxi4Egj8KTYFTC5fwxNMIpJhyIX2Zz4f3TiUCgYEAx3KR
SLbLisakEf+GKmdMSuSfazKe3F+87kNGbwmjeSE0KW6YPNUJJEVTtCzdOmCOMB1c
tR8Ic6EjIVGyZKPOl9bInVHoA1HYaXtaQgsrcvrMM17OZ2clyD16EIyH5Av9r7yZ
oeA4FGVLJZHy6hZjx8ivBpUoksTCGmeuQZvMKUsCgYB12cpIUXv06JdDT+kQyNaW
FvXlK11wTg545bIBi68/fdSY7JQtb2End5IxB6ZtLloioMZetJeY6/Es3IVBsVhe
njw75bYppJ7H2m0s85eFfaQT+Rrtg7L700ZkHBPoQphsI0hc9mzTUygtraTmAIK/
ip8ba3OUzUzbjKQLtojvPQKBgEAwWxmv2uoa7Aru1dsny6H+2l/Btubt2PxFopFx
5K7DROpgsZZCaeLGfR3rond7Q8ewa3nPNwUfwxKiHGvPXFn5wi7/gVAwMxofnPjM
Z3tlw/p0dLFKvRoozvQshj+arFa2uCsk4meOAhVhV3CHLC3MJKhVsO0rbdzZqXds
ENTpAoGBAI75Byb9gDG4V9yEtNjTqXhsIwc4rPHaStJhY69ytD3v0JGiMRlRF2VA
z9lJhiUdcOAMOVgGcWO8ejcLSx3Up/i34pwnj8QKxU6abmgA22FST+gK+UTHIyVv
Y4dCZaHHZFzqPRXqM3P5sV4rwqPxNyXGBvPQmqLlhX6n+4oTPikc
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert23[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUP/9NSirVc+xjt7q7dNF0S8MylYgwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDgz
NloXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANg7Vw53SqIP/eOMwTEr
ik8yxhkWq3ALLkVrdmcAS/gmQHZvzVL6nR3WqagaWQL25JKMAPg9GVA3pkh/xEMf
E7IuXXiodIUDTbj3Ce5SpLNfuX+11pyHTjGbIABdxVaMWmldO2Q+QJmHmKTiSGCj
g9LFUMSjkHiRkjpb5AB3sUW5b+X/Qbf2SML1pfuGE9FTWwyINUKHrMh+pNgIqXet
hQH9I9W68cC5kYy6HGDH5Qj+H2YZgoWYYkjpWVxzfLu2urlr7zzRdxCjYCN5ZMdp
k6x3MOWzcNONV0G0D/7Ol+Jd8SJ9xhR9OpSj7Q/FaBWZ2b7pwJ1pqPJN4m4IHXHP
BksCAwEAAaNgMF4wHwYDVR0jBBgwFoAUjd5sUw6781MT5v0F5Tz1r8N5h8IwHQYD
VR0OBBYEFB4DYoDi62nkBgt9B13CiFX1VdgpMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQCLB4y8GsFsu9/8XCt7Ch9iP3bu
8qR1klxQ2MYhgaV6jNaRvHyprhxPNI+vj7VzZ/tFYgW8K+xc3E6dAOHNHTEaEp3V
SK4H7YdrUrWabw8YngNmmPDnP3qlStEbaK73B3oTcu7oMjyd+uAafwPz2f4WcBLo
vTbzEsEBGdPc9AJInZ7ST8EzywbFSGJG7z6NUyBPc3UyOYemTnDomfiUyNP2yqqy
jyqZ3o3PM2xKHrmiHEAPcXaVXQZaC8JQTBMnZ72aeqPc4jQEPmaR533dpOh7styH
yBuSxLk4f7O63NmtmmBvkf9cZaRZR26Qw9tksn4bbgjx4VzHxfRgpIJkiQ5y
-----END CERTIFICATE-----
)KEY";
static const char privkey23[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA2DtXDndKog/944zBMSuKTzLGGRarcAsuRWt2ZwBL+CZAdm/N
UvqdHdapqBpZAvbkkowA+D0ZUDemSH/EQx8Tsi5deKh0hQNNuPcJ7lKks1+5f7XW
nIdOMZsgAF3FVoxaaV07ZD5AmYeYpOJIYKOD0sVQxKOQeJGSOlvkAHexRblv5f9B
t/ZIwvWl+4YT0VNbDIg1QoesyH6k2Aipd62FAf0j1brxwLmRjLocYMflCP4fZhmC
hZhiSOlZXHN8u7a6uWvvPNF3EKNgI3lkx2mTrHcw5bNw041XQbQP/s6X4l3xIn3G
FH06lKPtD8VoFZnZvunAnWmo8k3ibggdcc8GSwIDAQABAoIBAQC2rHsBh5oPi78L
q+/SYFw6SES/ET3ILEns77pZjhCgFYDEDfPCNGLjwGQX1hp4vKehfpGbJdLqx/i8
quPdaLW1BCKHOpklfgL9TgafxkXKHAaUc8RY/J38nnbMmGuMkIoZFWCFcWoVyk4U
je5jZgCJ4ZQ4a44zrZeBofvewT+MDGz4itSq2luidLSs55eUGgINumo7lULZmHnt
GJLLi5NN4E4a0E/vyQCWhhR39A7VMJSYc5utXw2JVb+d/XeSQ5vL5rguKUgQB2E+
VyAwKo8z6pNgsvf2rRQZHYTFZRTd8JObd7gGUhXCJmwcB6SThyEAX7AUzHMX59BG
6TURnpLxAoGBAPgwiyoB9jOH+BxoxBsTj8tF3FR6rb7hhJEIiC5fHqcpTE5ve/YM
c4wL+g4RFiBMV9XKEoRUZM3nx4LaWZyc5os9ZfFmNrlozWMsnPY3cqtEOlU6vnMo
WNIiIOnkJBpuZw9eMd8rOvBtaVTYfZMk3csvW96hAbQmPYxigw7YBGLdAoGBAN8J
VsW0gbjcs2UAQblvFm/+Ui1N3oE9UwqCbjLtb12RIo9hiX8ciQvIwgCnC1rrr3GQ
Ofu8sC1Lj5oauhdijiq7xg24tajQl6m6dq259qIiAOswRKMSd78DGnWluzoWzp7C
QYKVk8wW6lV5aTZw703rBMIIjg6A8RZ5Vjo+0NdHAoGAVqKzYXWP2Msk293iXsVE
U6POVpsAXXphiTMePnmrvJRY86RM9EXCa+eeYRyryZh8rL8+GX1QNuQqq83GrDnE
7GQBWvPgNHaWA4GGRRhz6b8GUcwQwSgbYTbSoHKn+6YAScgKxqlTiKvYsepkY2Hi
HlQo0J0JeDZFE+UrdJoJZe0CgYEAnPdqnSuDXEHe80cx+jpHNTxsQzwZSyWIis3u
lmLpzJT3GKeRZ+cCQEvi1Si64hsa1zakyBkseEREZTVIIGsZGNQb98418wASrL3x
BiTpgYOi1+KZe5bktYaOmZuw35nrgmm+RxPT939tHgibVlg24sRzDceNioGOqr1s
RQOuYMMCgYEA1BnmrRxOdAmrV75tX3eB42l5LrXY6sZXEQd+7aI/EZ9F3tkmcmQY
+/MnN1/UdMmyIx/AkH50pxIVvIOzspTeaOGdTrQMTi5XdEStk4LaW1TPZpZazGaY
MNboIM91TM7o2HyR7Z9fAZAnRPwKcRakUJWVlUV/rQuHu/mlV6YNL3k=
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert24[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVANWRYKjbZOUpR+NpzfHLe3/R6T7FMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODA4
NTdaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDOI9E+DrF6JSHsUC0O
5R+f+W3L1O8IE/2JfaOUE4E4bWYwj8gJTq8lbUf1j4owhdyceJt/ryRZTTV7fMfM
cHdBboIP1WpDRbHZM1lo1OjIXa56Byh5XeQuXqPLqca+RSsfngwypU46+01m8+qu
xQGD/3yPsXCW4i7hdkw01PGsMGM+7QKw8vkEuSu93jS+HR8efdwp/pO7QbHN1fYi
Pfd3GzXPA+Q0Tjr63kVABFXW9PsPwrWMz/61pQCW7NYxVrQeHqAVqLwf9VSeTd4u
F1eNXvXg/mLgBt1/eIlC73IpPW7iX3wgHmMdcVrv8XSDwbbLcspv2rWZg9mcYxyN
jRSnAgMBAAGjYDBeMB8GA1UdIwQYMBaAFHGWfb2Sy5mA1qAajZSzNSyZio9TMB0G
A1UdDgQWBBSW8OS5AMCMNeUC9ql4a03UmralgTAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAKkhsG6JtOufyO9fWWyPljrRx
aXhKz/GMuWAyGB5gFjiE+tCU8/Ahw/E42ZXcvzzEnM07KocSHcKQpFb8TV5Rsj/V
U7DOIebxJ6ubbnnSaiqMNFceCehwzi2eL5jjDER4BhhJHEVY01kw3KYP7eUtCoR+
h1VtinCalPQQfO4LqLKgtTntIaXfMfE1AYt7t99OdLxywQw12kTJ67ffL1ECQfyz
qRnsNhgZ4hlXvcEWqFsGskxeYQ+NS5AQQXnNnxfH8tSXf2DtYJqnBYH0dbhKkCri
P2VYYwUPPhpImWXosm5Cozuq1qS1Q2qzGTesDeBF5P2fStjLACXF4MDDVeMyVQ==
-----END CERTIFICATE-----
)KEY";
static const char privkey24[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAziPRPg6xeiUh7FAtDuUfn/lty9TvCBP9iX2jlBOBOG1mMI/I
CU6vJW1H9Y+KMIXcnHibf68kWU01e3zHzHB3QW6CD9VqQ0Wx2TNZaNToyF2uegco
eV3kLl6jy6nGvkUrH54MMqVOOvtNZvPqrsUBg/98j7FwluIu4XZMNNTxrDBjPu0C
sPL5BLkrvd40vh0fHn3cKf6Tu0GxzdX2Ij33dxs1zwPkNE46+t5FQARV1vT7D8K1
jM/+taUAluzWMVa0Hh6gFai8H/VUnk3eLhdXjV714P5i4Abdf3iJQu9yKT1u4l98
IB5jHXFa7/F0g8G2y3LKb9q1mYPZnGMcjY0UpwIDAQABAoIBADO8SYeJqIE/UIQ+
mxBh8M31i9jcWCFu4vLe1W8UdhnzWvJVI58yYELEaor2u3fyIKwkmOIy0qyW9BVJ
+9sTXtrNIVdX+JNpqnkYqhflV4bx/BgkM8RCDEKW/gTfV0HpmikVQXp0gm3LJGHy
VbQHPZojRN/LaLZ1jc6IIhggWYcdKmHujOKEWOmFacabZUVPV7Cw+hwyNur3TB1D
IdPqDVCP9uqXGbRDS+JsMWn4y7Q7wsZX/IgsiOt9yxbe4idwR5CHbpw4kYFNLy41
v8kHbx4TECxHMpQcdalKm0piEZMshS3e8hhGxUBssKpKIdppxuxhmrcxty3FdFNQ
NOEXoAECgYEA757Ir02RHeRbA58ukyfa1Afg7HEaCPkOl8o3eviLaHC0r77alE4K
ajF8ODsK884k7kHTVFLVXRZPkCWOXQmytBwS1JsDR0WnLQuPd7CFaQMj48d331AB
pAVUVeKsJxfY9D86ReztEMlRySXMSLuYCLRcBkoAzDQoAMe07HIkCcECgYEA3Dsl
ltukqplrzUlwMYOClsrGG1iTj0jzfU4XBkJqUlifL3yE4yWQg3/LVGTwlCKYaGno
XL3ShVt+bSIF7Znn//7TB6/Tj9fV3e7Onkqn9+rRwmvWt+5qrO+tl6gGGG8Ob0Ty
6KPdMFlSmmWegd/jbv2OMFZ3kj6QHC5CewtHKGcCgYB1YHt/2+g1E1I+EPC4pvI7
oOxeBf0x3gZ29lh9BVkMvQDVghECSsQxRrJJvSpdSWKvz1Dc4oKfybkJrEvzaTe/
oGzc9l4BwK3mk94gTDjduusOvfwLSN604ONx+spHkBf3dS1vaWKrMWK9vKU4D6Sm
xuq5LfD8FqRt1WXb2QGsQQKBgQCF8UcsWEvvIj5pbZfZ1WUwNwWEDjw4siK8/CiO
uwgVKyct52qfB/K7t5a/rmCPh/bv0jHrbdvDFeFvmHjIiImMrbxFeY+PdbLyNtLd
fQRpiRyoRZQi69NfeKo4iY2jgK/jxqiCC190j27ozpM35CZcE8RN69jvTjOxoAT4
9uQBUwKBgQDgeBdxE13/PH2nLXBNh/EXQcrGgTslw9KQw1cB1KuxxNe3n9AG3Kyb
3dZ/rfIMLxU7SfosvIk3mcPbluinPeNMqyJ4B2XvRfWWYmatWJ1DI7baEUbyesXN
FwDxCY4QwoU0vjxXt/Vcm4QPe+2J/fkjTaQFTV+Z40E0I3Xr1BI00A==
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert25[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUSUCFPzJgsKs322/DdERH2ZyITIcwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDkx
OFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMis546BIYZylZNGnOP+
sspvLzdhswxcmvMGkfVm30H7vbcXYpr3kdr+NdMecb1oIECu2pv7wkM9sFJm748l
LVcNzJ0/MUuN08mPKufM8ngFXoq1zcZNlM9NjfbaH9OKX6MCFpCF+H+UM+8+K60K
mb/nrZTvnGzv+IWWLOwoZPWsfXVH50laXTuiCMDKTWc6FvHkHXi+BhlXShsvgLpx
2uC8ATI3mw9c0VwCXJ2ejRTgMBw7ikqxy7JXl5IlxUNGYKsjj0vJwvYG0zEGRFBZ
vDgvquMuDN0tmxIGmk2KcQvNdg5FgutX/MCgWeNFRM1J7+lUwOm+Vri6shSGOzf5
dp0CAwEAAaNgMF4wHwYDVR0jBBgwFoAUVJXy3LrJcvhEmeh58v0hrt0rmYwwHQYD
VR0OBBYEFCxQD9GrN3wShw2T/zIvM3WTetytMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQA8Ta8p1r5Wb/0KYulryMYdVMv7
OMfY0OkfksYEfm1oPjUXwvdUD9DmEQiH0o6j7PHsikoUqQBIpvHLfE7ET8SUd9uV
yRE3XzsNxnUd782qZ2sO0sL5Z9yyvjqmPoK2xhy4ETNsDFLdzAp9fyRyF1n2FGc2
BHzfyXrDczgGkkpX5Yzp/E1qr78fgebsKcdJMuXVdPIsezSMiiU0kiLj0SDEymiV
F0fqWQGrPYOY+l6zO3VDYvPUi/Wk5eNvJYrT/zcyjqPY9nnfx0/cm3G9rFcqnPZ0
DhtbToyoNhV72mCcNjZ/7wPDfE41j3wa3SIjHekEvrPZneVHzvqpoy4P4Hqy
-----END CERTIFICATE-----
)KEY";
static const char privkey25[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAyKznjoEhhnKVk0ac4/6yym8vN2GzDFya8waR9WbfQfu9txdi
mveR2v410x5xvWggQK7am/vCQz2wUmbvjyUtVw3MnT8xS43TyY8q58zyeAVeirXN
xk2Uz02N9tof04pfowIWkIX4f5Qz7z4rrQqZv+etlO+cbO/4hZYs7Chk9ax9dUfn
SVpdO6IIwMpNZzoW8eQdeL4GGVdKGy+AunHa4LwBMjebD1zRXAJcnZ6NFOAwHDuK
SrHLsleXkiXFQ0ZgqyOPS8nC9gbTMQZEUFm8OC+q4y4M3S2bEgaaTYpxC812DkWC
61f8wKBZ40VEzUnv6VTA6b5WuLqyFIY7N/l2nQIDAQABAoIBAACuBULIUTLBZJxR
Guryy+w/oCtR22dpUOUN2qS4Kn1wut3J6H2Z5RaW0dvbOItptOJDNV5d5fSQWu6M
hrc3awXmeBD4S9RzsIn4QC701MVvTzitEPtg++YP3zE83+q5h0VoFmd67LKR7/MZ
hNBbPtYVlNLyVNdRoMfjEFfkcyMZ5vDg1hjYCGlQOr5459Bz0r2vx1fXWPr6HLLj
xXKqr+86UMDMox0qKxjfBpUdsjR4YKsIgPtTdUYdbQUO4lNEuEAyA/NtDZk8lVEJ
hyfJ4evHCUGp9+oNpzU+vcCpemfVXOLsU8A6IUHD35NC3ZZ+OeU6mANmPcNqjpkY
2jD+PkkCgYEA8ppCWINuEkVR87hxwvPcq4qXz9YTqve9fKV+YeY6HlXvu4BZSVy0
xPiYnb2LtOsMz5mK3xqAd7Q1k3lMi1bEGxyYxH91mMx6wakTyLujmyMCZKiYy1L7
slzlw/JS70Qg17Nf2PVHrDgLSCq5D/af5RCABmy1ye1JcU8H3i+2vZcCgYEA08Ho
weFy6tnl7QQjKlN+o1HxK3qpaJhE19YDwhJVBnEj7xNvt2dgQDwQbf27/N9aIutt
xMD7l14w2gaCTDLMoMD5U4Os7d06tbdn4di0KGTLN5VsOpiIGBqqELO+WQ9v0xIA
j/PlFv809wsQ/joMw/rchXODv+2q5OsHQYjpm+sCgYANU4vxcMbLGiO2T5twKhUl
FyffstLBwXoXLsASgKMQp4fTtOkyrnT/nOhFQfQ6q+64OKb4Omx0JuIKh888mDeN
hbSelqHPEuxrVu5PSH9kNjgbb2d1Q8mvsoqsiNMlcYicUK4jLNYTD8EO5cnkppeB
7bYapD4YKYCvSLgV7GkNYQKBgCOlSKEaalXOSD4VrDW7yF2Ftas0AsBwoWYTyJ4q
p9Vlxj7AbtXYuQMXGUzqoXJ3xpyKNPgh5DgeyuponhZllryYv1+nkryXuQJN1VbX
O7xw+yRoKsfSVVZ5ZpdZKnmjd5d4OFADJlUwsKHFYx7odq2m1OhCAbOVNmdCiP4n
JevxAoGBAOPbtvjdMVjWgYVEviB/ObfxG3GKEalCzroA7RMq/UW7jeDW+Y/p9++b
5OHBsmZ5/2V/cVEt5gHCcRpwSYqhvC+QpciDGW4MoT8QxvnnZNVAa/qEe8Qnu9aI
KDf8cfFE2+XS/jsjmoTp/r4esDyv3DN5ZnK2nng+NvGZk0jgl+O0
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert26[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUXbYy/BFaaa3+0G9zO0qlMxXwi2swDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDkz
OVoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJCyjmT9gLXHxHOmT/x
ctEb3JG39bB5tbIaV8HrTd/xoqXEzJ6EFytk7eZCv6fGpZtkieyxZmL44dyOX5Ga
+X+9js3kcm9TOuL9BKgeQGUMqsOhJtH9FWfa0S6aEeXJOiqwhT2+UFnFtWaElGi5
JenNURsCGNWbrISTDWBm2xVEQ3uExtFzKhLT5obmGnDLxLnIu+2/wGbsvM0c0rh2
Odb8GUL/gKTpXDUr1ukQ8QQ6VaUf9GjCckj1TPC45W0r284uFkYNV0mDnWsIpESn
b05P+JdajVMGjiqncM04JRbi+H5UGIW0QsV4pHRyS5TXagRVXT90nupoQQt6TTWW
21sCAwEAAaNgMF4wHwYDVR0jBBgwFoAU3eKOHSGG0yupToxwaIhFWug7AKAwHQYD
VR0OBBYEFJSXVDj9M0do3ogjecmJA+RDNPv2MAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQAUFt78FEcKMWIbTHPza3WUufEQ
zJmk/hCDJSPqhuOxH/m4QXravBtP8RsSpm3B5ZsDjX/zCG8DvVkG6yE6EWs76xfv
OwEPMSgaSJamPUmh+lK7+CPnooH1sP71mGS3qD950Wcqs/QaB4rER4Iy5sdUT8qn
gBZT5r/jtYyUMWcZQcEwJJBzYOar3XJITxyJ167muxhyvpQGLOeDu9pOoS2ul/Qm
g4z9yH2D6bgB9KFSo/V9y/fGPTY433JpLBvBJSOAmTRx4YJ0sc75LADMmocz+n3T
LeDfpboTEHpBeO/Ajbg0cRkjXX+LOhlN+HyNQGDX7Si/gBZzC4wDM73zDw6X
-----END CERTIFICATE-----
)KEY";
static const char privkey26[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAskLKOZP2AtcfEc6ZP/Fy0Rvckbf1sHm1shpXwetN3/GipcTM
noQXK2Tt5kK/p8alm2SJ7LFmYvjh3I5fkZr5f72OzeRyb1M64v0EqB5AZQyqw6Em
0f0VZ9rRLpoR5ck6KrCFPb5QWcW1ZoSUaLkl6c1RGwIY1ZushJMNYGbbFURDe4TG
0XMqEtPmhuYacMvEuci77b/AZuy8zRzSuHY51vwZQv+ApOlcNSvW6RDxBDpVpR/0
aMJySPVM8LjlbSvbzi4WRg1XSYOdawikRKdvTk/4l1qNUwaOKqdwzTglFuL4flQY
hbRCxXikdHJLlNdqBFVdP3Se6mhBC3pNNZbbWwIDAQABAoIBAQCIlnStOZo/AuZj
lpIGVNNNNrQDU5mF5zw5g8XKaKn69gNSUUEYFM5MCcHftTV1MNikAJ1OJ3/0Rg+K
F+1SfBTAtFY/nNOySWvNkWGID+X3h4Uw8MH2FJ3aPegadSNRlqZjH254qOKYZs1g
CCZPN869Hv0OXMT6p5XA34xYWXd1CtiZMLDyVFa/24wDvtZ9xiYSXFB8XGKNwleA
Yq2FpdmajHBzfAhx1spsSlMu+eSVGx5svM9r4gPUQXsf6RUkr3M5tbHICxWjovAk
rdbuJHeXoMITb/rjjT32Gi092oEKUOLNJo/qPWuWVTyI3ngZ2p0eszYfvwe00S4d
+rOJNtIBAoGBAOmxX8/uS8Wb311FvAUGA1H8Z+caojDqsea6AC/ObfPffRQwECMW
TgzKq8Eescgvt3QHe0FSBIJ1oodiRDGTd9PoSah8LCD5IzXy0KzqDsKpMrmo4/p2
DmiZEZzbdzOmtxrhw7QrvfUTSgMM9B24KN3nAcI+cDVZR+aQfNbV7ib7AoGBAMNG
2s78P/BU7ggi+EZ036zlFuqj/D7isdELWhFGBf5ETVRwjUKmjPku7ofGTujN/MQh
PHnItxYrzBDgiEDFFYEu5ADTZfmsyi2ZbUsFoFpx2OIq72JFidGeWZK7Gsul1UFr
aZ8d5rHCp5SmCjnPX1my9fD2D5cFb/LP8EgQZ28hAoGAfcJaUUdnt1v+g+83E2z8
9G3kbFXE2pYyTnviBdgNC6RFZ0vGI1upfPDEJRUjjxs+mk48YhaFHr+3SyvyjstR
PNRQbz8DcGsiK7IT4jnC37a1+XuN1AccEeXtuH/SUhVxjCHac2bmvrA2sBWUeTnV
pDTgmWuIbu0w6D7xL5vIoWkCgYAArq2zDXSXvWQ7iQu/C0eAzigfV8VY3ufqKBT/
fHQm6Eww4FfOBND3Se+xb8L000tv0AHrFwV0wWBsKqyvNUEW+3f2fMadcbofIKse
GP3vcvhkCexeNon9voTnm1nDnme5W8u5r5wPwPNe9JmSjCP0J0jut51kVLOIcKuR
ROVnoQKBgDI/HTnXHy6s8iiMl0IXzgmrj0ZhxQrFx6P8xrY74+uJAyYrljTj+p7E
6A8A8H/2Bsqy26fRiTdREuLWOjtbjkoYDGTteKUWnC1sFo8niagRzL/6dhfeiScL
qWcROBRkcXIdscFH4UKuA7EG65jlJg5CxS9WwCYkRNhdHjDge7mg
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert27[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUGQCCr57KJrv8ow1/v8Tm0zmVkwIwDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MDk1
N1oXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALjqzDwe5j4UxUcffVSD
opy0gNVFFWuZk0cpzxtwppLfWsTYDcZDTl2Agy6rPElMPVV+w/OOxjehAWWzc+Ij
BRpn4VrpgVVWHv+Q4MkOB5XchStZ0Lfjrtd7T0iqKN9ScTzjOSFtqDdSQGJtUpiW
J70dfScf46czOAsk6NUs5grANvJybYRlNTbdBNbUXEQLrv+q1VyzSj78qkkyr+cn
GrJXYCGwmYaonASuKSUqKMpUPv7DnKhMDiRYeqxQmylkFhM+UKSmYq9+xdBC9Yt7
JqtQUi0BDNDAXgPgzeqO/fJnMG42GIsBc39fEDK6pryxCdv20aoOdFvRlPvORDZj
aqkCAwEAAaNgMF4wHwYDVR0jBBgwFoAU3eKOHSGG0yupToxwaIhFWug7AKAwHQYD
VR0OBBYEFJse65MUlK6tX4+nyvtTX1tJC/pLMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBVT5hX+RsFvmAVqxT1SV2MyLay
pGG7pnlHBqCpMrT4QtOJHsegRL/yIoaUTzRx4B/GL3TNFikvWWD7+C18FwPxaNwU
bXoFEyB2szLIVYcb1UvB54jWVS3RZ3uPjmBOnLkmENQdVbX/CyTNv3byxF79GL5k
lGTZs4p+exaT6IExZ6RQc7GFOI+1u07eA4DH63tes8RVgxUa4G75QBt93rwKrP/S
y2lr+a57U5VtnBqNGKxiV2Ly0q64+EMCRB5vlck6kxetUU2JBqKT6rkkPrPM2yOJ
1hzFkAxC0mAzfj5sxtPSJluiTHVKl4pxwKVbK6D5tbN9rtByh2i9JpIOmPU7
-----END CERTIFICATE-----
)KEY";
static const char privkey27[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAuOrMPB7mPhTFRx99VIOinLSA1UUVa5mTRynPG3Cmkt9axNgN
xkNOXYCDLqs8SUw9VX7D847GN6EBZbNz4iMFGmfhWumBVVYe/5DgyQ4HldyFK1nQ
t+Ou13tPSKoo31JxPOM5IW2oN1JAYm1SmJYnvR19Jx/jpzM4CyTo1SzmCsA28nJt
hGU1Nt0E1tRcRAuu/6rVXLNKPvyqSTKv5ycasldgIbCZhqicBK4pJSooylQ+/sOc
qEwOJFh6rFCbKWQWEz5QpKZir37F0EL1i3smq1BSLQEM0MBeA+DN6o798mcwbjYY
iwFzf18QMrqmvLEJ2/bRqg50W9GU+85ENmNqqQIDAQABAoIBAGUQ111Imhh2K7iK
qmpFo9u3/+4QWvu7LTao0Cw0LqZ5ctfBKg4hfTQ3Z/ta1ZFOHPucVCbK8uU2l1fp
c7213DUpb47LrsdgUYLmwSTCCT1MEHC4Hw5SX54gJB4X+kzMd12K5w+xKk5KIyxo
icshzkhbVKrMkBUzNj9JBgR1DKn0o1Zt3JNDUgoMpLON2SRlRBHFgxMmUdo02C1S
9FcxGBdAImKTSDpbnThcYxioy9TQz+xCF7zYEojZHJsI1gDDAnTd5gPHU5vmDDfx
xXcWj5mJvrMkXWVC3+1IjA+2ziSgMwarwi3AutTmlhaSefID2Kz/kKXkrmtT36WX
XaTWowECgYEA8Bp5A2GI3zSEUtdqee2G7BlJ+ArhB2Rlhlst9lH5mj+thDR0oZqb
n1XtGlm4bLkogxohSiWx49SGmzvgMXJglL6ItZFT0cR3fKNyj+n9qpjYoA3dSfrT
Y7XGZKskK3IYBLN3uS8nKoittijAoobd5b/U0V6MbtZS+4oGHBBTGyECgYEAxSj4
ZlGaZSpX9gAcLwJ85XD6E6sMo2DK6SphpziOW0Jl3V2GGyyC1J3RhXTuGMus7tY0
PllLLGY4n2S8eoq7AtuQf8iBO8fnN1myk5vYBLy6qXIoyBdl2OQ7CPB+UDNKTmEX
uAHQmo6UK5/7XuVFnD7k8kw6NoWDjW8hVgKDJokCgYBQXJvqPMfh5SLw0zbJDm4d
Ma2nPoTow3qTiIsOqYtATFXYLPJocGXr44eTTTpD8YkUsDc6KPmsRqKy7ExyYFTh
FBCPrM+AIyDW76HVdzy2z6eZkmpha3k7I0j+wqU9gVd2o1bL/XM/cjWqBWPMQhWc
4yLo3JtmMYfetuiqlfAq4QKBgGCjW1Xzq6Too2K93RKhuoxP2xPm5HP0TqEJGAJ5
8zUCD6H4ZHJPV/VVQcyTat0WgDY1IjhpVjJ2DEz8eHHsLAwTiPlJUyb1LfzwZ1p4
qeJwi2HgxeKFrXMpM1KHZrJy4XoXlyOnKBOJzVCwwupZoMwb++ZTvLjaqoeZsBJT
eutZAoGBAOqj4lAuHUgwtt9VYQVfjYboM9BBkTFeAnty2Rz1K0+mhLrfRMpF5bQx
M9A2oPr0JAUrI5rPVE7VaPb0NOwVPNcxau374bEUiP1vbLjnCbZHmyC7m4J6DYRr
ZntF4g5eab8C9bI4+hk66hjW33ey99w+5J3SCIn6klApCyzqYQiX
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert28[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAJmVwdS8ugL07HY4P36iFLYsE1IkMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODEw
MTZaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDOR326ya2JlJr9sUTu
zhX5oL9ffZHcHxnX8M+JT+zagl9YCY/Aw83OpWc35TI8Wf7fs1XavrrHPnJUalkQ
LegoL97nkYUSb0x6iyu9WlQkG7cjvRBtndWiKRS2GXdBSLyXpkTzZxpLRQ2RZ0NY
uah0rQFFW/wKIVM7s2NCaSBfNnyIPMGp3cedhLK/qTfPx7AH7ce/ykK4TKRmf/qy
CINkHjatSOY61p5b2sHvkATm780GAoOa02qiqViyk+d6XKC/MYpQ2duZZ/yMKdlb
9XXI10MeL22BUcbYr8O0yzH52lLEtAOzcRIQnW/w1gEvbVtKZkaSIhtVph2u+Z0V
0ftVAgMBAAGjYDBeMB8GA1UdIwQYMBaAFNpAwAJvnSndWLlwWeHLQx+Pyn9RMB0G
A1UdDgQWBBQeKbzkS8sZslRy5YIuI6EM4oQ2JzAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAVvwzJwyN4/wKCXnayuLdfw9M
PoMvtqIDl2c1IDJL+4V2ot1Do9aTey6SY9OYrWPX4goPVecUsAmRzbNa/Lp1+MvV
0N5qlx5mDOhlAktccGM6kfUZ9rbPsPevE9wTtIhUdpRs+8O6PtNMegZOW1CkxYQP
v2WbiBouHMR2F0xXs9RQ+ANsMBMKZgo0neUsAZiopHKlxBXr21w+8im5EsJ1jsT8
UNXzKcUxgcChGILbiROhchHX4BwlapTGH9jpm56gel7ku/LVPz2/1JjUHnyABWQ1
9ng1NjDxbX3IjDynaRBrRmjrdMTj21/BSDJUoy71s77hpg2MiFa9vt0GFobrpw==
-----END CERTIFICATE-----
)KEY";
static const char privkey28[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAzkd9usmtiZSa/bFE7s4V+aC/X32R3B8Z1/DPiU/s2oJfWAmP
wMPNzqVnN+UyPFn+37NV2r66xz5yVGpZEC3oKC/e55GFEm9MeosrvVpUJBu3I70Q
bZ3VoikUthl3QUi8l6ZE82caS0UNkWdDWLmodK0BRVv8CiFTO7NjQmkgXzZ8iDzB
qd3HnYSyv6k3z8ewB+3Hv8pCuEykZn/6sgiDZB42rUjmOtaeW9rB75AE5u/NBgKD
mtNqoqlYspPnelygvzGKUNnbmWf8jCnZW/V1yNdDHi9tgVHG2K/DtMsx+dpSxLQD
s3ESEJ1v8NYBL21bSmZGkiIbVaYdrvmdFdH7VQIDAQABAoIBAH8cDp3LKORebiNl
2NROqbvpEgQFnJvHjmVCPqCeX5VGQPo+6hYekfOEeNY4pXwK+MNf/seMQAwGruzV
m4wcvbIFzZVkVlvFuZrJp2pG/xJBAdlD55ukZsBGED0l9BaGe6/3BQX3wnwfUP0I
ZLeHsAY1lVLoG6ciYeen4sJJhWklTgqfgdu4mM8HrnZEj3+PpgDtEqd6WdcPN6X9
cLl8zsRfowhZNKZkRypWjVdVVcD0969j7UTgkuhLkuq2uX/L9FBn8Sr24C2zhHeB
RKYGDlqgEC+XXdfLIjBc3oQQtpPKLtsRrH+mx7fzbEIin1p7YjZLdc/8hOu87qpm
I9EiCAECgYEA+DaCNkobCjqinsBisenfhkDF7TIevbzspLahqZI5ngF7CWrA2fUp
m8dn6j59iO7IyUmCgsP5eL/RxOKS1wGPlf7M+go3T/vEqV67hWWYaSegdEwJiu6e
6hpuaFEIi4M8nZj5e6sNIP/eayXK3zPH8OBbRQhUW9OdQQOrZoJg0mkCgYEA1MAy
jdaq4DqDKLN9qZLyEVSgOVuvkXLMOpUpOnZXKSFuL3DOQQQzAny6tIKaHBTjttPb
GvORlByqKzO/HEwjiFci1FCGE0uPiHU0ViXDXFi0Thzz5ezpQ9IkDUkXEiZIFk3N
9Q7CbJxW7bvwJNdecC9U+HBUtyN+WWCGHEKrbA0CgYEA4WLtvkpwX0t7G1hqxk8u
D4SFtasLSxNgdvj3tzj2ZMnHZXewgroh/DiH3aa86ziYzl6XyhIWWFUmL/fliqL6
NSPYETxxmYluvLUu5iSozuOSyce56pRfbQhvNP5QGg8jXZ/o+ykJlOuClh0NKLym
U84GUpsLrw5q/vxMCZteO/ECgYAaiCGF2MiUkBd9Qu0f8ahoiuI0ZP+/Js7bW3C5
zrpN8dJY7jt6ljh6Te3GxybK67v+N+tyL//utahvHNPvhntD7IH2clhWHa03uk4d
lDYJ1TIfL7Mm8GWixJkkYwISjF2fEnLl5cJPYyvgF/j5zzYCzZROK0wVTsLaCJSQ
/6t7HQKBgFZ/cbtZIOH8yDnPtbWrmx5P3wy33DsqjHIl6i0D9inF87USZKhF1tOd
XJGIJxlKKoSqiozuYqQoajZ3ycHxW8bGQiodqfyrtHWl61nCmQaOqJZNyBUksMve
xN8MW8IDYhVsVaOARixlc4yiDRLLpp90Evlhd0cHGwJ11WM7/WSh
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert29[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUEunG77o78Rlf+CedGSTvmbG5Hr4wDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MTAz
OVoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMfE4i0zj44CZnzN88R7
HeObJPk0mYHGB+URzwbHZsLpNkDR+V/KUKzlELjCh/3bPA0rhRiUoui5dc+t/kyw
9Rv9//OV2JwjClhf5AdzPw+bCLBSQeNa9gKVcVzAU/9GkYzGc7sOMNaerMfWvz8f
oxelT4YzbHCrMTP6QzzwaLGzmX5o3MkSmEObfwMje/GY9JtY8WT6BA9jc8ytZBdW
GyFQB9Vos3cFJfLcP2rGBsx7Ou493CjVtAcpZxQUj7BwVw5QTKfK7UbMb4Q9ghDD
Qr859KfRoNGSL3HN2fhCayogTeaeytfn8kwKm4YoqkbU11S8wpOrUgB+dK2hyvUS
8XcCAwEAAaNgMF4wHwYDVR0jBBgwFoAUjd5sUw6781MT5v0F5Tz1r8N5h8IwHQYD
VR0OBBYEFK3c4xeCge+8cvE4YJB7wNIek9lhMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBXPvvJYaw7L41sM//+mqMMidkl
qYZuz3hyjWiUqBfVQDUeoRmumzT5rPE6yUOKa4tPyEwFCuuqmS6JzW4w2AwEx2C8
gcahK4iQyvU7RzRxyAh1UF3/1sRa5Gh8BAZI1LL1t0JpDkSKm0OCizDrYnJcnXLC
1f6WkNthtqVF+mZw/vjUDYJPeHHo8Iuwd94goGzhDSLNcwW+3hzxvVJna97cWmAa
Z7Th7H+qp2VUbyIRvRTywAD8ujU9+gtE+EcqvQ/wsnwwsAKPyRY6Vv99Xj7c86lF
NuuFME8qPgLbutJeSeLspovxGF6iJq+o2KX+mjEaOG5r5adZaHk884c/AY8A
-----END CERTIFICATE-----
)KEY";
static const char privkey29[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAx8TiLTOPjgJmfM3zxHsd45sk+TSZgcYH5RHPBsdmwuk2QNH5
X8pQrOUQuMKH/ds8DSuFGJSi6Ll1z63+TLD1G/3/85XYnCMKWF/kB3M/D5sIsFJB
41r2ApVxXMBT/0aRjMZzuw4w1p6sx9a/Px+jF6VPhjNscKsxM/pDPPBosbOZfmjc
yRKYQ5t/AyN78Zj0m1jxZPoED2NzzK1kF1YbIVAH1WizdwUl8tw/asYGzHs67j3c
KNW0BylnFBSPsHBXDlBMp8rtRsxvhD2CEMNCvzn0p9Gg0ZIvcc3Z+EJrKiBN5p7K
1+fyTAqbhiiqRtTXVLzCk6tSAH50raHK9RLxdwIDAQABAoIBAQCAjeR27h1NZ6II
YoiiQhBD7gznCcBXUNa3xLQTSrKIb4doQzdVO16bRHV0UEsvQ6vrVbw6yzviw6Za
1T82nxG8x8G0QRO5tprrsBmP6cwFAOGghe68nkZ99ymSZHopZoI3s2W6VDF9i3Lo
zvsXZgaQKuN4GxOHlcL+DejDCHp3D1cri0Z3z65HWTSRKlv8YMRwCV6QVv86wkiY
INPFH8PHbxRpTqqEYdGLmzQT47Rq6MToHgpqyjSMM8w4QCAc4DIxN2TRwXf3vdMz
P4djH1hTPtybABkAMPZsH3h+N+R8ckHX9IKV8mqYCjXw25pjoTqDugp8/IkuTz+e
5Ipv2p2RAoGBAO1bR5y/H6T+s4XxdSbRpFr6j5Ur5g37xEylbGfAdoPsmnRP1ILR
bOSCVwgl44zZh74fnqCXoeFfrvuFmrjXYEV+1kciHI2YUBh/P+TvMh/9id0MdK7v
bsGRdEkFw1uNFH5eDhsNBmkDyq/Xe53vkB2qspnz/hHb+CcvKJfSX4NjAoGBANd1
zUOphMcPfhMbhWOK+o5VM/Wj49P58rrZKNoE9EvbTcQ50U4FDQtrUeCCOvbbCjcO
b+tNrMqp2wd14Lme0w7Fk2gOm8bUUNX/0IV0LSorsE/HxIAnAoaM/vwMwNHwdPFh
BcBM5XEuYt9RL+yGsLkydJ1YaF28POQ2qc0L3vfdAoGAPjeuJuVmix6EuniXp+xO
M9b4KjcOn92+EAVQzEWEC4LK3fBLGVuCOsf7vlQIONSQo4KwVxJIScaGHMJh01AJ
mU3tJ0pGx8sEO9enTQByWDZinAH22CBFHRJetAGBuw7ZL2LCOc8JPh/TUpKegcDZ
Ox2SoY52rAHII6VV864egVcCgYEAi/yJxLBbCEpUi22SGL6vU9Kf07ULzSnUPvng
xOX7I131BMzb5Gj7tLOhmtCCJujhlivXI3ZwEaySFiLMQ02hqzNLNoxnQaOocaGn
hU2aGpAQGYWdcfXmei0ZbgGLNduY30s+RVawq/yLWLSTAoXwRCM4CRLrnBs1JMEv
SX2w/DECgYBWN5xFwYwk69bZ9cV9h16sqAlk7lDeLx2sN+hTDGCcfh4ksK3ZowDr
w3q/i+j5NrLy057eaDvvvs3bvIo96S+YnlX9O8rx/ZARUu7BKHHzAZJFqU29SyOX
2AMkvDSHMSEE6bM1zu5XvpR5BxxgIKrYDUvMnCByIwAU/aYEyAQ4lg==
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert30[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUEunG77o78Rlf+CedGSTvmbG5Hr4wDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MTAz
OVoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMfE4i0zj44CZnzN88R7
HeObJPk0mYHGB+URzwbHZsLpNkDR+V/KUKzlELjCh/3bPA0rhRiUoui5dc+t/kyw
9Rv9//OV2JwjClhf5AdzPw+bCLBSQeNa9gKVcVzAU/9GkYzGc7sOMNaerMfWvz8f
oxelT4YzbHCrMTP6QzzwaLGzmX5o3MkSmEObfwMje/GY9JtY8WT6BA9jc8ytZBdW
GyFQB9Vos3cFJfLcP2rGBsx7Ou493CjVtAcpZxQUj7BwVw5QTKfK7UbMb4Q9ghDD
Qr859KfRoNGSL3HN2fhCayogTeaeytfn8kwKm4YoqkbU11S8wpOrUgB+dK2hyvUS
8XcCAwEAAaNgMF4wHwYDVR0jBBgwFoAUjd5sUw6781MT5v0F5Tz1r8N5h8IwHQYD
VR0OBBYEFK3c4xeCge+8cvE4YJB7wNIek9lhMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBXPvvJYaw7L41sM//+mqMMidkl
qYZuz3hyjWiUqBfVQDUeoRmumzT5rPE6yUOKa4tPyEwFCuuqmS6JzW4w2AwEx2C8
gcahK4iQyvU7RzRxyAh1UF3/1sRa5Gh8BAZI1LL1t0JpDkSKm0OCizDrYnJcnXLC
1f6WkNthtqVF+mZw/vjUDYJPeHHo8Iuwd94goGzhDSLNcwW+3hzxvVJna97cWmAa
Z7Th7H+qp2VUbyIRvRTywAD8ujU9+gtE+EcqvQ/wsnwwsAKPyRY6Vv99Xj7c86lF
NuuFME8qPgLbutJeSeLspovxGF6iJq+o2KX+mjEaOG5r5adZaHk884c/AY8A
-----END CERTIFICATE-----
)KEY";
static const char privkey30[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAx8TiLTOPjgJmfM3zxHsd45sk+TSZgcYH5RHPBsdmwuk2QNH5
X8pQrOUQuMKH/ds8DSuFGJSi6Ll1z63+TLD1G/3/85XYnCMKWF/kB3M/D5sIsFJB
41r2ApVxXMBT/0aRjMZzuw4w1p6sx9a/Px+jF6VPhjNscKsxM/pDPPBosbOZfmjc
yRKYQ5t/AyN78Zj0m1jxZPoED2NzzK1kF1YbIVAH1WizdwUl8tw/asYGzHs67j3c
KNW0BylnFBSPsHBXDlBMp8rtRsxvhD2CEMNCvzn0p9Gg0ZIvcc3Z+EJrKiBN5p7K
1+fyTAqbhiiqRtTXVLzCk6tSAH50raHK9RLxdwIDAQABAoIBAQCAjeR27h1NZ6II
YoiiQhBD7gznCcBXUNa3xLQTSrKIb4doQzdVO16bRHV0UEsvQ6vrVbw6yzviw6Za
1T82nxG8x8G0QRO5tprrsBmP6cwFAOGghe68nkZ99ymSZHopZoI3s2W6VDF9i3Lo
zvsXZgaQKuN4GxOHlcL+DejDCHp3D1cri0Z3z65HWTSRKlv8YMRwCV6QVv86wkiY
INPFH8PHbxRpTqqEYdGLmzQT47Rq6MToHgpqyjSMM8w4QCAc4DIxN2TRwXf3vdMz
P4djH1hTPtybABkAMPZsH3h+N+R8ckHX9IKV8mqYCjXw25pjoTqDugp8/IkuTz+e
5Ipv2p2RAoGBAO1bR5y/H6T+s4XxdSbRpFr6j5Ur5g37xEylbGfAdoPsmnRP1ILR
bOSCVwgl44zZh74fnqCXoeFfrvuFmrjXYEV+1kciHI2YUBh/P+TvMh/9id0MdK7v
bsGRdEkFw1uNFH5eDhsNBmkDyq/Xe53vkB2qspnz/hHb+CcvKJfSX4NjAoGBANd1
zUOphMcPfhMbhWOK+o5VM/Wj49P58rrZKNoE9EvbTcQ50U4FDQtrUeCCOvbbCjcO
b+tNrMqp2wd14Lme0w7Fk2gOm8bUUNX/0IV0LSorsE/HxIAnAoaM/vwMwNHwdPFh
BcBM5XEuYt9RL+yGsLkydJ1YaF28POQ2qc0L3vfdAoGAPjeuJuVmix6EuniXp+xO
M9b4KjcOn92+EAVQzEWEC4LK3fBLGVuCOsf7vlQIONSQo4KwVxJIScaGHMJh01AJ
mU3tJ0pGx8sEO9enTQByWDZinAH22CBFHRJetAGBuw7ZL2LCOc8JPh/TUpKegcDZ
Ox2SoY52rAHII6VV864egVcCgYEAi/yJxLBbCEpUi22SGL6vU9Kf07ULzSnUPvng
xOX7I131BMzb5Gj7tLOhmtCCJujhlivXI3ZwEaySFiLMQ02hqzNLNoxnQaOocaGn
hU2aGpAQGYWdcfXmei0ZbgGLNduY30s+RVawq/yLWLSTAoXwRCM4CRLrnBs1JMEv
SX2w/DECgYBWN5xFwYwk69bZ9cV9h16sqAlk7lDeLx2sN+hTDGCcfh4ksK3ZowDr
w3q/i+j5NrLy057eaDvvvs3bvIo96S+YnlX9O8rx/ZARUu7BKHHzAZJFqU29SyOX
2AMkvDSHMSEE6bM1zu5XvpR5BxxgIKrYDUvMnCByIwAU/aYEyAQ4lg==
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert31[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAIry4Ntx57AfVzbFW+ieBgk1I6vQMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODEx
MjVaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDOBoR94iHXhADesG/T
MMHijmZKtxJcBshA9RC2Fsa1CRon1FyclJtSAl7lU2U+OJ6rfJ662t/l8D0wIZL0
pJVnKgriT2HmNN283LE7+5HCjsI09xHZgSkmP0e6OGl1psu2R30iZuPpNQzwf/9k
tdCcnsQ/1Qj5gzwZu4Tspvkt7IRvko8OF1usj7GbXj8rYdUfm0gviSa2x/ujm8LK
LwD0j+bU584594Jm0nYaBevZZ2rDysg0f1KCPLeDiagb1fgeumr7SnrrfgYfL1pw
i/t93uVaZUz3qCexfbo7p+x6o4FTxw6XVR9bPgTZ7VtMW1LWOwfMoIVpVa3xWu6L
WTJpAgMBAAGjYDBeMB8GA1UdIwQYMBaAFJBrLplZvIUEA7y2CtSKPTqC77m5MB0G
A1UdDgQWBBS0MQpBeWJ2ynrBywGR+2vaXs8H0jAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEA29yjQXbBNJE5V2WYDFv4+aM3
tM/Gc/UAxEFPDhp2c6HALSDVwUo1Sur7KPoWRtdTDJcDtCZLvbNffOnW5Mf+B0pf
tjtWhC6raA7lMYIDqXdIRgiPa9vUJvXbgA78u2IeA3G2sxtYWI0R5DazJFfgTVWt
iC1oWjjhihGy6ckIJp2hge2+05d7GVJdcoKtDsTWAeQTMKIMpkHWvqUU8O9V8xAI
yne6viKPzSCrAYLA7/pON7KiYau0lzHznFHTPBA1pNiriAuYXvFd4uIH8/lermNn
oyoGyax0PJXuwTF+L0wb5qgLwela3oE5LUeq0Q3dLe/qluyQTMc1qDOgVSIZbg==
-----END CERTIFICATE-----
)KEY";
static const char privkey31[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAzgaEfeIh14QA3rBv0zDB4o5mSrcSXAbIQPUQthbGtQkaJ9Rc
nJSbUgJe5VNlPjieq3yeutrf5fA9MCGS9KSVZyoK4k9h5jTdvNyxO/uRwo7CNPcR
2YEpJj9HujhpdabLtkd9Imbj6TUM8H//ZLXQnJ7EP9UI+YM8GbuE7Kb5LeyEb5KP
DhdbrI+xm14/K2HVH5tIL4kmtsf7o5vCyi8A9I/m1OfOOfeCZtJ2GgXr2Wdqw8rI
NH9Sgjy3g4moG9X4Hrpq+0p6634GHy9acIv7fd7lWmVM96gnsX26O6fseqOBU8cO
l1UfWz4E2e1bTFtS1jsHzKCFaVWt8Vrui1kyaQIDAQABAoIBAQCYVPdXwIC3QjIG
dbxpEc75vcJikhQwGQ+t/8U4bYTEzJMbSgij+bkSHRCfTVc5d1HSiBUxIa6U7FnB
ocFvw9V37v0XMgy30vT0tNUS/cJbcYSk9t2qzbZoQje9uAI57HcJMriSXDM99my3
0lw60bgKZdXDQLgfDOq+OAljlV3+lmNIKOTfmgX12sQIgy6+O/WebXrtwZIoRnhz
okYFBN7gLYuCzN3pL/2jayjV8L+gwTxiyEkeVEkX/2bGONbYsNFswAzH2w7IxmeM
nD6acq83IkH2EhAXt2KiJ7ToGDDmC/KVt0fZsRhXHOOjtxnFXpu9IHZ9L3Eecjh4
jKjYf2p1AoGBAPVFKYvDttlX9cLe3tUbltr2dnouRDrAJjeW4JkQ7K8oSYlVPHy+
u5dMWymoeedLZv2zkLZl4aqul5oiKPqT3tD9CYo/63mCuaVD5y/+RqSx8wqyYQoZ
glFUiHw36wzoyRlSDUoXiqYTXVNZ7KeVSjYwXuzYsM6LN8QzzCuzklfXAoGBANcJ
2DsqpHDQBQvzNSpJuRGH46Gdm4AkjHiqYjLRy0cH5vn1kDvTcKueyo7RreJi+43o
bRqsjywrqGAnlqzmTm2P4G16TQNtobxb0F3YvTPKycni3/M2Gj6ZjXZE94aN/J2p
u23B1vZyAN+mP30BvZN0RFIJIIMZiz2YEDLaWn+/AoGBANMhoTzl0d3QYQdJ0OoD
4N1GMgDFGQi15XAO2fIJZqT0+jBVBkef7F22Zfc7Rwy6lMKps1W/5/2EY45T/5CM
f87JM1Al6+7igCpqhHWE3ozhfgTkPrJx+QNH0GGVxSmMrae1QSlZkImbu8eHbjdo
RMTW6K/bbH+vyeaEbCnmX4BrAoGAbf0CFZlFDNLJkggu7FdTCs13t+V2cP/AvcNg
gN/f2pAHK1jYElGe8plAv/Ytu3qncjks/RTAEnauI6h8ICmYed+6iiShGZzMuhw4
IphGhiQ2PEYICE1m9RfzTEgQV0luqXH0hTo9akZ7fel51cW7Ip24iEFU2NmK59FM
j8Yxvd8CgYAz6L/EoChRC10Gn2VdFZvPW2y/A6ypk2rL80xXbeQrD468bDM684Oq
FNYihca314rj5A4AyFevWj6IXXydnkBdLt9GVmRg/fhE/kBDoi3L32FGiJj+FTls
ZotyT2qoVZ8nvluX8HL94hGsBNBZL5h/xzH25qwGc39Q54j5Oz+QLg==
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert32[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWTCCAkGgAwIBAgIUDA77qaOy0GWBtU2tXsQozEXxZBswDQYJKoZIhvcNAQEL
BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g
SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTIyMDIxNjE4MTE0
NVoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0
ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAPFGQ5Hsa95RZwNWuIWg
5xyuWcZVFTXcXaWeaZYA9t7e+/f14G/So/r/H7YddmVeFLA60kC02MmBj8RSIvof
z0OAnJz9OG6i/TRvte7IuXWy9ueyehNBveFQF/6IR/qjW5f94v/4zZd7uX71Bt/R
Ojy0MjQLbdvTgGOlRMznxkltm0+r3WXQezL3l0RkG6Bi+7bFpJ7RfUgTqcAdXH/u
LN+6b/2eXRAsLYaaOHh8NvTHn1Ox+zW0jLoKpaxqB9wWSfeHHgI7uJKtV6r41Way
Ybm1M+8T9qW+eCS1e5ONQ21uGnjaWNAA5jeNq7J2W+b0qrFgDpoBPAmACo5XAyYh
QD8CAwEAAaNgMF4wHwYDVR0jBBgwFoAUkGsumVm8hQQDvLYK1Io9OoLvubkwHQYD
VR0OBBYEFPNAx5jVDIJuyNVK+GAsRsZCqPikMAwGA1UdEwEB/wQCMAAwDgYDVR0P
AQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBPaEeFfsnmdtZdu1Gq6jpsRZ4C
s9s68bGmiFopvgk9uIgR1sbF/Wvzjluz3es3LZ/DxeTuWcfUPMrBHVn8m7D/5bQB
pRPHRunlKlwx0IYPPDeIWZ5mnTy28OZYBmtUC4ahcRFHpKBtGu7xO/w0LcjoA5tZ
VQSIvgmrQI6h74vMOR8tUGlcLHeYyeaOoTIVFr9LeygenttcxV3jZgG0SQJCQsGE
m1cDYNLhE0ThgIbmB3ak0V/jddI5i7BUGYhLh1LmS2DaVh59bPPN/kN3fXs1yzYC
nZdRjs3gESUMLZmSaY9xXpg1RPDPVHbJbJ6VnHZmaR78AKJzBk0lbjOK6i+M
-----END CERTIFICATE-----
)KEY";
static const char privkey32[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA8UZDkexr3lFnA1a4haDnHK5ZxlUVNdxdpZ5plgD23t779/Xg
b9Kj+v8fth12ZV4UsDrSQLTYyYGPxFIi+h/PQ4CcnP04bqL9NG+17si5dbL257J6
E0G94VAX/ohH+qNbl/3i//jNl3u5fvUG39E6PLQyNAtt29OAY6VEzOfGSW2bT6vd
ZdB7MveXRGQboGL7tsWkntF9SBOpwB1cf+4s37pv/Z5dECwthpo4eHw29MefU7H7
NbSMugqlrGoH3BZJ94ceAju4kq1XqvjVZrJhubUz7xP2pb54JLV7k41DbW4aeNpY
0ADmN42rsnZb5vSqsWAOmgE8CYAKjlcDJiFAPwIDAQABAoIBAHbKfHw9JgLom0p2
iRUp2oAxi3vt6iqBnA3uB9hWEzY6HV7BTUZhNGFg+TEGIoPYJWF7JIe9Qrs7shgb
xKLXxIlcqj9VIVK/puEJW078vusnOQ9svVP720wyzg2A9cd+ZJtZ4kjbLAY4YWUT
qNXLwtuecNVVBzrjUGQ/O7eTKfp+KuPPzDbchSyING8gKJlMeLKvIAWLOFne15Kh
/ANnjBkj37a5bZ6JdSZLi1L1fN5PDE1ebAFZ5c+l7lwYW/aN1bPzxaIiq40RvtN4
L7W3sencroEHIYxgF1mMHT0RLYjKPJdcilJUo5cBKz3hXsopMPhUzaeqF3SE1XfC
GgoLoAkCgYEA/RudY54AQ6mhhnDPBdVYiTmmr34U+K8AD5KeCkLlIntw8Yd+v5pS
AAJcdofqmOFANE98cY1G/+JkbqrvKp/OfWt0d9AFD1y6h4g01pRb9R8kvpf+s2F9
y41mZZOZZ7zwKLyc9jMSr6GQ3iTgkO/S0Tu/ctLn5ktSLsdMr5YixB0CgYEA9AgI
y35TG8ia8oA5omZ4q+uZvEbhVOYzFH9QcXZ4uopem2QpBZd/uBUXUkJwcI/J8jBz
QHZj5LiE4ojNFOxroEMqVbt1PMQCQMc5rRKO+rOhYNVmZ7XiJy3ZYbn9LvyWPhPa
Cmfw4zZfCY5Xh+2u50q5Icqz8qss5gwNTZ5vrwsCgYAgr0M4zPt9wVcBdGnXeABx
4Ab9XIu6vWaFnEpaSt8p/8TK+9NLTNNP+v4TtGOAEH5pAuawBy1m98Y+YEZ4O6C/
o3Or0SoGHNRdkiI4n3nsgnQNRLV4VpuXAJB7h37k3YopyImAZYIBOhzqAtsY/E8+
A7gqTL/zhkB7l/L4LawZpQKBgB6jM5d40K5YTTvM5W+CNfw/pBmDPJ8QF7GnGk5k
dVvWK3KO7KeLEbnmy/rD5KH9K3mAo7egJYJ9uBFI45aQ6CJCzHnfAyUR5saDPLaB
8fl/u+8QW78BktcgZn5if2r6w1If4+wSiKe+F8h3Wn3yVVcshnm7nUQGQajQMotg
LN1pAoGAaejVdf5jq5NK3B3bLW8NlBW3MHgN8PI7tqYyr5QH7XFzpDzHyAchQacJ
kSWDUN+KYqzGjK6LkH02X1FpzEFWDRnTQS9074jtCeEdeJUIhO3m9eRSCs/5ocEX
Gw5ob2zQTDam42E0qi22tVGJZBf5PzFHVcE4hOOfNiQAn54d9bE=
-----END RSA PRIVATE KEY-----
)KEY";

static const char client_cert33[] PROGMEM = R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAMtdsR+NEqTU7Fh8ew4+1un08rITMA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMjAyMTYxODEy
MDBaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDUaSJiR1sFtLqsFzdc
O9nDp4+E0tjo9vGJIwV5aYVf905Bd4wDBHEzDnceVVUuqrd258JAwI/X4cstY+gr
+MqU4iYtTgfyS39/xzqWU/79GRM2Ocqk8uLfnfJKVlJAz7c/dJGddBCiHSexlYJi
ACvfi/L6vNy9cIzZiSnNQBsPREwtBE9MxW6CC6ZNLvpQaFeOaOsI8a8ca88U5t8t
A04lxPXqrrrYi4TFbdGaYAOMaFWSV23DfrFJLE595oUQzDLWTZMAHJ+4bbqPllnd
Li3r+ZYYk2+ked2V8JrHsc9XlZw7Xst73fHdskgJmYLVDvOUoSVcACUXrBBsz483
Iuf7AgMBAAGjYDBeMB8GA1UdIwQYMBaAFJBrLplZvIUEA7y2CtSKPTqC77m5MB0G
A1UdDgQWBBSpezqEWHAQCSQiuU/ld/sphHrH9DAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAM2I18lfgAN2oxIS1qPaLBKhu
lMnzyPER7Z37UPQDDvkQD5A2TXNtgAS6PZm9ib2WVys5ENrjzKC5FXOTGXj5EqNj
knxzP7YcVcf3GMW/eTbsu/kpOgQmad9gy+d+OjPEEyGBNdlboEMxZoCEQvAa0Lx0
8DJn/sfgHaaFQxbXdr0HqRDmjQSMtYYQ/87DgAbl3z3igI2YNrZTL/0IiqK9uffb
lXucsFaDWc5XBV4LfVHSdYogCWzFNn1O/gMLTs1aJQM9a8Ul5KKfdo7Fhsrlwa4F
hvlnwyGmRI8LtT3A7jcb0t7N513N1hZmj2rAyNXcyaC+eEtpuyoquFB4NKk9KA==
-----END CERTIFICATE-----
)KEY";
static const char privkey33[] PROGMEM = R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA1GkiYkdbBbS6rBc3XDvZw6ePhNLY6PbxiSMFeWmFX/dOQXeM
AwRxMw53HlVVLqq3dufCQMCP1+HLLWPoK/jKlOImLU4H8kt/f8c6llP+/RkTNjnK
pPLi353ySlZSQM+3P3SRnXQQoh0nsZWCYgAr34vy+rzcvXCM2YkpzUAbD0RMLQRP
TMVuggumTS76UGhXjmjrCPGvHGvPFObfLQNOJcT16q662IuExW3RmmADjGhVkldt
w36xSSxOfeaFEMwy1k2TAByfuG26j5ZZ3S4t6/mWGJNvpHndlfCax7HPV5WcO17L
e93x3bJICZmC1Q7zlKElXAAlF6wQbM+PNyLn+wIDAQABAoIBAQCVump/Zpc2BXjU
2CSxAot1jMbxNQOr7G7EYYh5VjWrORPTuCZc9iOpftfe7P33wGVFDwdyR4rd/Y8Q
Nu2xGVYL7wJES030sN12QAT/dMQ4uxU80y4Sahgc0yS42/JtNz6QH2aJ3AfhjrWt
S5u14zMKCIfF0Z20eAMBUNvDkB3C+vPZ7LD3N/QGceczCSCKG9ROgkeSwAIga6Km
K6m5rk/wcleQqInWok6LN+6K5ogVM/HybibKIi91vh4yARBFoyXawGfSuJBTUjJC
dNFh+VftzYOWR/a7Z6weViS+sdyXCDwf4k1GOVM0AdWWc3tOEYyNR/WQMw1y4Rcq
r97XWcsRAoGBAPE7LhxEmuMsw5zy++Ff90FxEnGx4bxa8gNRd5q+q0RtkKfmwqKF
BK8BjCs2SfHKtHf1zYIiYkikn3HgajSHDe8pR0/NgGqlCa8nxaY27xpd4rwFh2jL
ju5DpKtwF/eSS+kQwm+zuVLBi6nZPGIzrMxOYEuZ2KUK/CVrn/5dfH/DAoGBAOFq
QAxAyh7+sMcyzD6IBuuZNA+XfWvGHYTM2+XKqMeOOHBKFoSUB15o5KY0F0tLnQdD
xlJHOmx3gBPyPFNWkiEWI7ODBqz1/Brws1lyPba2UWgU5nutKlqwDTMG269poIz6
ut/mJC5u37pazj3aZ/aG5FSyBQXkuBBPpy0E5GtpAoGAEuegZXa67LLFYbdpJfBv
YCGMYHygRDKcZMYtqWLn6STtATuEZVgnVc+5PGm0wc4w9nW89UXcjfTYygPcTuGG
pgeGHyrrtW/ZonBrfOsxMc/JxRJp2jyZpvKGJSlTt4Mvj8RQeJCNJX8lgtoTcV67
XAeIZGRaJhUGTahxkW2iJCECgYEA1apqBXNQY2rV1CuxcEBuFtpEf9b/uq2zA1NP
yRR9QZLsqnt/trG9/l59BZHw1p1Yvi8fCJSdq3zmeYGubuxrXVNoCdtT85TDICIy
TsG7/7n3linhgy/UKtwf1tNUX4RG+/saMesrNpxAE5vJTNX+n66jNij8G+b+Fe/6
rfCqN6kCgYEAq/BXl4a8NbopFe100H7AHyFB5BOgtZMOxvOnS+SaSo6jFmQAQ5Y2
UBVpQraVLuicXMqJvVLKIt6ogN5O/t0+mB0jxKB1Q0FBj0g9zwaY1swEYnX7PdRW
YfnPjsB82s5Uk0x1zFfACEfud2QOg0viLyG6X79EzQLAApSqfFV9kcE=
-----END RSA PRIVATE KEY-----
)KEY";
// declare global document

DynamicJsonDocument globalDoc(4096);
// variable

const int MQTT_PORT = 8883;
// const char MQTT_SUB_TOPIC[] = "subscribe/espmqtt";

const char MQTT_PUB_TOPIC[] = "iot/gabi_reading";
const char MQTT_SUB_TOPIC[] = "iot/gabi_reading";
const char MQTT_HOST[] = "a3j1bzslspz95c-ats.iot.ap-southeast-1.amazonaws.com";
static const char *const THINGNAME0 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_6c8546e133dc456c8727ef148b144341";  // Dryer A11
static const char *const THINGNAME1 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_4d1e604a207d47239b060d3d20630b41";  // Dryer A11.
static const char *const THINGNAME2 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_0040b47b74bd4cdf9952cf5483c8798f";  // Dryer A5
static const char *const THINGNAME3 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_cabf3d014b3a4789bfaa6b14c1cec65f";  // Dryer A5.
static const char *const THINGNAME4 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_f589f17ac202433bb9209117b66820d5";  // Dryer A13
static const char *const THINGNAME5 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_7e1bcb9a82de4cf19c20c363addabc6d";  // Dryer A13.
static const char *const THINGNAME6 PROGMEM = "fa7797c71d314f6981567b218362b82c_4edbf8304da94aa188e067adb5820ea7";  // Dryer 5  KM
static const char *const THINGNAME7 PROGMEM = "fa7797c71d314f6981567b218362b82c_71fb89f5c8ec408480b167c0be74abb3";  // Dryer 5. KM
static const char *const THINGNAME8 PROGMEM = "fa7797c71d314f6981567b218362b82c_399c9c74b4524b19b398e98ee1c1b074";  // Dryer 6  KM
static const char *const THINGNAME9 PROGMEM = "fa7797c71d314f6981567b218362b82c_b0b2c5cc8d514d499c7f51e851f33fee";  // Dryer 6. KM
static const char *const THINGNAME10 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_03e52f583e794761b70debfb8f715ab8"; // Dryer C10
static const char *const THINGNAME11 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_352cb14a8399460ea4c5258e08225ca3"; // Dryer C10.
static const char *const THINGNAME12 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_6f733be1011a47ed86aa09d7617455e6"; // Dryer A1
static const char *const THINGNAME13 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_1fe46e35a93348dba2fcff59f401ceb5"; // Dryer A1.
static const char *const THINGNAME14 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_0d8f6bfef50d4e03be79bf43d93384ea"; // Dryer A2
static const char *const THINGNAME15 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_8865ee6ee5f94a7baa50c3edd95487df"; // Dryer A2.
static const char *const THINGNAME16 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_1ccfdb19c331483eb50918286995b8c9"; // Dryer A3
static const char *const THINGNAME17 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_2cf2485fc7f34d4cbed0a27c36660c30"; // Dryer A3.
static const char *const THINGNAME18 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_a515cd41ee0d4639b2cc0e6caabe0240"; // Dryer A4
static const char *const THINGNAME19 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_7acc6b23962842818b6b9ba417a56843"; // Dryer A4.
static const char *const THINGNAME20 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_6cb18cf7b75845738e6552c18970fae4"; // Dryer A6
static const char *const THINGNAME21 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_80ef090661f94ab6925dfc7fbda0ca94"; // Dryer A6.
static const char *const THINGNAME22 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_35bbb72511254a5784abd550f8e17c0a"; // Dryer A7
static const char *const THINGNAME23 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_310c989cdb244033ba40e5fb21090235"; // Dryer A7.
static const char *const THINGNAME24 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_14b5d24e082a4a219f6538bc23d635d4"; // Dryer A8
static const char *const THINGNAME25 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_152e45359276432b9c97fb876991ae50"; // Dryer A8.
static const char *const THINGNAME26 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_014edb2df9bb4015acedc2250289bf22"; // Dryer A9
static const char *const THINGNAME27 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_23fea443b76047738ae84144f511abdb"; // Dryer A9.
static const char *const THINGNAME28 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_ef3cada96dc84cf2a438467448d5d579"; // Dryer A10
static const char *const THINGNAME29 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_129e4ea1f46941629c044641d7e2790b"; // Dryer A10.
static const char *const THINGNAME30 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_42dc87af40034fa8b251ebdaa39ebfe0"; // Dryer A12
static const char *const THINGNAME31 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_c37d42415a4d44eba582d7bca18c14a0"; // Dryer A12.
static const char *const THINGNAME32 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_13ea8d1251ad48a580595d3b0f6437e0"; // Dryer A14
static const char *const THINGNAME33 PROGMEM = "af4f32bb53354cbd97c8324243e245e6_61324f80c5c840d988305f9b64711fdf"; // Dryer A14.

const char *const THINGNAME[] PROGMEM = {THINGNAME0, THINGNAME1, THINGNAME2, THINGNAME3, THINGNAME4, THINGNAME5, THINGNAME6, THINGNAME7, THINGNAME8, THINGNAME9, THINGNAME10, THINGNAME11, THINGNAME12, THINGNAME13, THINGNAME14, THINGNAME15, THINGNAME16, THINGNAME17, THINGNAME18, THINGNAME19, THINGNAME20, THINGNAME21, THINGNAME22, THINGNAME23, THINGNAME24, THINGNAME25, THINGNAME26, THINGNAME27, THINGNAME28, THINGNAME29, THINGNAME30, THINGNAME31, THINGNAME32, THINGNAME33};
const char *const privkey[] PROGMEM = {privkey0, privkey1, privkey2, privkey3, privkey4, privkey5, privkey6, privkey7, privkey8, privkey9, privkey10, privkey11, privkey12, privkey13, privkey14, privkey15, privkey16, privkey17, privkey18, privkey19, privkey20, privkey21, privkey22, privkey23, privkey24, privkey25, privkey26, privkey27, privkey28, privkey29, privkey30, privkey31, privkey32, privkey33};
const char *const client_cert[] PROGMEM = {client_cert0, client_cert1, client_cert2, client_cert3, client_cert4, client_cert5, client_cert6, client_cert7, client_cert8, client_cert9, client_cert10, client_cert11, client_cert12, client_cert13, client_cert14, client_cert15, client_cert16, client_cert17, client_cert18, client_cert19, client_cert20, client_cert21, client_cert22, client_cert23, client_cert24, client_cert25, client_cert26, client_cert27, client_cert28, client_cert29, client_cert30, client_cert31, client_cert32, client_cert33};

// nestedSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";

// define UUID
static const char *const DEV_UUID0 PROGMEM = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded"; // Dryer A11

static const char *const DEV_UUID1 PROGMEM = "2b74dc15-d8bf-467d-b15d-e25c3dff260b"; // Dryer A11.

static const char *const DEV_UUID2 PROGMEM = "b1d49ddf-6a7b-424d-b202-9b3715b5c74a"; // Dryer A5

static const char *const DEV_UUID3 PROGMEM = "f827b10c-d463-4ae4-b2f1-ea890480b065"; // Dryer A5.

static const char *const DEV_UUID4 PROGMEM = "517f1b72-0ea3-4a53-a915-ae8dca1568de"; // Dryer A13

static const char *const DEV_UUID5 PROGMEM = "9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f"; // Dryer A13.

static const char *const DEV_UUID6 PROGMEM = "11ecf397-023e-4cfa-a3f1-7a39e463cc69"; // Dryer 5
static const char *const DEV_UUID7 PROGMEM = "9793f7f9-14bf-47cd-b42d-6f4fee6a3642"; // Dryer 5.

static const char *const DEV_UUID8 PROGMEM = "9ff41e9c-9a43-4834-b8c6-3292f497792f"; // Dryer 6
static const char *const DEV_UUID9 PROGMEM = "2d4df282-9828-4c1b-80fc-ea175bb00993"; // Dryer 6.

static const char *const DEV_UUID10 PROGMEM = "84b5de78-e7bd-4312-b6e0-e7c09919b19e"; // Dryer C10

static const char *const DEV_UUID11 PROGMEM = "6ecce4ac-2e5c-4ae9-b0e8-9e5a3f4cd4ea"; // Dryer C10.

static const char *const DEV_UUID12 PROGMEM = "b5153d6f-8bda-4004-a7b0-9e403cf5f333"; // Dryer A1
static const char *const DEV_UUID13 PROGMEM = "107e369a-9f82-43c1-ad12-9b91bdee917a"; // Dryer A1.
static const char *const DEV_UUID14 PROGMEM = "eefd24e6-53ac-404c-a6c7-93ebf3ac34fe"; // Dryer A2
static const char *const DEV_UUID15 PROGMEM = "571ebb72-a9d4-4de6-be6a-2d0deb7a6a61"; // Dryer A2.
static const char *const DEV_UUID16 PROGMEM = "2993dc00-1f84-48a6-b30d-1ce4f3f43445"; // Dryer A3
static const char *const DEV_UUID17 PROGMEM = "c17ac968-106b-493a-b1e9-6796a7a4a0e8"; // Dryer A3.
static const char *const DEV_UUID18 PROGMEM = "116e9fe0-6e05-4e49-a18e-191834dbc56c"; // Dryer A4
static const char *const DEV_UUID19 PROGMEM = "f750df7e-15a6-4244-868e-843454a52a61"; // Dryer A4.
static const char *const DEV_UUID20 PROGMEM = "81ae2ff4-8f6b-4c26-b88b-24163c422917"; // Dryer A6
static const char *const DEV_UUID21 PROGMEM = "01ab3267-b70c-49d3-a7aa-e7c22f044dee"; // Dryer A6.
static const char *const DEV_UUID22 PROGMEM = "0c141b4b-cf35-4f01-aa52-c92364529afd"; // Dryer A7
static const char *const DEV_UUID23 PROGMEM = "035342a4-5414-4ae0-998b-99dd907b070b"; // Dryer A7.
static const char *const DEV_UUID24 PROGMEM = "4ca3edf9-c00f-4490-b2b0-c05a842609ca"; // Dryer A8
static const char *const DEV_UUID25 PROGMEM = "c6b30a05-f792-4795-8b93-4c13ba50f17e"; // Dryer A8.
static const char *const DEV_UUID26 PROGMEM = "0ffae8d7-6242-4585-90f1-73b03d9345f4"; // Dryer A9
static const char *const DEV_UUID27 PROGMEM = "471f067b-aff6-40ed-9629-32a19560c16e"; // Dryer A9.
static const char *const DEV_UUID28 PROGMEM = "fe6f153b-b6f5-4c18-b30e-0934150b2249"; // Dryer A10
static const char *const DEV_UUID29 PROGMEM = "293b0f25-be3c-488b-b93b-8e5b4ba4b9d2"; // Dryer A10.
static const char *const DEV_UUID30 PROGMEM = "79f1fa42-ed76-4330-a026-bebf9171de78"; // Dryer A12
static const char *const DEV_UUID31 PROGMEM = "fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6"; // Dryer A12.
static const char *const DEV_UUID32 PROGMEM = "fd2de1c6-3f89-4965-af03-fed5959ca716"; // Dryer A14
static const char *const DEV_UUID33 PROGMEM = "e722e20e-1b44-4fd4-bd35-57c425d91ce7"; // Dryer A14.
// const char *const DEV_UUID[] PROGMEM = {DEV_UUID0, DEV_UUID1, DEV_UUID2, DEV_UUID3, DEV_UUID4, DEV_UUID5, DEV_UUID6, DEV_UUID7, DEV_UUID8, DEV_UUID9, DEV_UUID10, DEV_UUID11};
const char *const DEV_UUID[] PROGMEM = {DEV_UUID0, DEV_UUID1, DEV_UUID2, DEV_UUID3, DEV_UUID4, DEV_UUID5, DEV_UUID6, DEV_UUID7, DEV_UUID8, DEV_UUID9, DEV_UUID10, DEV_UUID11, DEV_UUID12, DEV_UUID13, DEV_UUID14, DEV_UUID15, DEV_UUID16, DEV_UUID17, DEV_UUID18, DEV_UUID19, DEV_UUID20, DEV_UUID21, DEV_UUID22, DEV_UUID23, DEV_UUID24, DEV_UUID25, DEV_UUID26, DEV_UUID27, DEV_UUID28, DEV_UUID29, DEV_UUID30, DEV_UUID31, DEV_UUID32, DEV_UUID33};
// define sensor input
#define RXD2 16 // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
#define TXD2 17 // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)

// const char ssid[] = "WiFiSSID";
// const char pass[] = "WiFiPASS";
const char ssid[] = "Note";
const char pass[] = "bengguan";
//  const char ssid[] = "+Solar_Project";
//  const char pass[] = "psp1234!@#$";
// const char ssid[] = "gabi";
// const char pass[] = "gabi9999";
// const char ssid[] = "Gabi2";
// const char pass[] = "plusnrg123";

// const char MQTT_HOST[] = "5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/devicePayload";
// const char MQTT_PUB_TOPIC[] = "publish/espmqtt";

// prototype

int ESPMaster = 2;
// 0 - A11
// 1 - A11.
// 2 - A5
// 3 - A5.
// 4 - A13
// 5 - A13.
// 6 - 5
// 7 - 5.
// 8 - 6
// 9 - 6.
// 10 - C10
// 11 - C10.
// 12 - Dryer A1
// 13 - Dryer A1.
// 14 - Dryer A2
// 15 - Dryer A2.
// 16 - Dryer A3
// 17 - Dryer A3.
// 18 - Dryer A4
// 19 - Dryer A4.
// 20 - Dryer A6
// 21 - Dryer A6.
// 22 - Dryer A7
// 23 - Dryer A7.
// 24 - Dryer A8
// 25 - Dryer A8.
// 26 - Dryer A9
// 27 - Dryer A9.
// 28 - Dryer A10
// 29 - Dryer A10.
// 30 - Dryer A12
// 31 - Dryer A12.
// 32 - Dryer A14
// 33 - Dryer A14.
int ESPSwitch = 59;
const char *host = "59";

char convertIntToConstChar[10] = ""; // 4294967296 is the maximum for Uint32, so 10 characters it is

/* ESP Control */
// #ifdef ESP32
// #include <WiFi.h>
// #elif defined(ESP8266)
// #include <ESP8266WiFi.h>
// #else
// #error Platform not supported
// #endif
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson (use v6.xx)
#include <time.h>
#define emptyString String()

/* Web Server */
// #include <WebServer.h>
// #include <ESPmDNS.h>
// #include <HTTPUpdateServer.h>

// Follow instructions from https://github.com/debsahu/ESP-MQTT-AWS-IoT-Core/blob/master/doc/README.md
// Enter values in secrets.h ▼
//  #include "secrets.h"

#if !(ARDUINOJSON_VERSION_MAJOR == 6 and ARDUINOJSON_VERSION_MINOR >= 7)
#error "Install ArduinoJson v6.7.0-beta or higher"
#endif

// static IP configuration
// IPAddress local_IP(192, 168, 68, (ESPSwitch + 100));
// IPAddress gateway(192, 168, 68, 1);
// IPAddress subnet(255, 255, 255, 0);
// IPAddress primaryDNS(192, 168, 0, 197); //optional
// IPAddress secondaryDNS(8, 8, 4, 4);     //optional
// IPAddress local_IP(192, 168, 50, (ESPSwitch + 100));
// IPAddress gateway(192, 168, 50, 254);
// IPAddress subnet(255, 255, 255, 0);
// IPAddress primaryDNS(8, 8, 8, 8);   //optional
// IPAddress secondaryDNS(8, 8, 4, 4); //optional
// const int MQTT_PORT = 8883;
// const char MQTT_SUB_TOPIC[] = "$aws/things/" THINGNAME "/shadow/update";
// const char MQTT_PUB_TOPIC[] = "$aws/things/" THINGNAME "/shadow/update";
// const char MQTT_SUB_TOPIC[] = "subscribe/espmqtt";
// const char MQTT_PUB_TOPIC[] = "publish/espmqtt";

#ifdef USE_SUMMER_TIME_DST
uint8_t DST = 1;
#else
uint8_t DST = 0;
#endif

WiFiClientSecure net;

#ifdef ESP8266
BearSSL::X509List cert(cacert);
BearSSL::X509List client_crt(client_cert[ESPSwitch]);
BearSSL::PrivateKey key(privkey[ESPSwitch]);
#endif

PubSubClient client(net);
// WebServer httpServer(80);
// HTTPUpdateServer httpUpdater;

unsigned long lastMillis = 0;
unsigned long sendDataInterval = 60 * 1000;

unsigned long previousMillis = 0;
unsigned long interval = 30000;
time_t now;
time_t nowish = 1510592825;

void NTPConnect(void)
{
  Serial.print("Setting time using SNTP");
  // configTime(TIME_ZONE * 3600, DST * 3600, "pool.ntp.org", "time.nist.gov");

  // NTP server to request epoch time
  const char *ntpServer = "pool.ntp.org";
  configTime(0, 0, ntpServer);
  now = time(nullptr);
  while (now < nowish)
  {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  Serial.println();
  Serial.println("Done!");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.print(asctime(&timeinfo));
}

/*---------Function for Time----------------*/
unsigned long printLocalTime()
{
  // NTP server to request epoch time
  const char *ntpServer = "pool.ntp.org";
  configTime(0, 0, ntpServer);
  struct tm timeinfo;
  time_t now;
  if (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
    return (0);
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  time(&now);
  Serial.println(now);
  return now;
}
/*-----(END)----Function for Time----------------*/

void getDataFromSerial()
{

  if (Serial2.available())
  {

    String input = Serial2.readString();
    Serial.print("Serial2 received: ");
    Serial.println(input); // local echo.. print out to see if correct

    // Read the JSON document from the "link" serial port
    DeserializationError err = deserializeJson(globalDoc, input);
    serializeJsonPretty(globalDoc, Serial);
    Serial.println("globalDoc");

    // loop add deviceId & timestamp
    int globalDoc_length = globalDoc.size();
    for (int i = 0; i < globalDoc_length; i++)
    {
      globalDoc[i]["deviceId"] = DEV_UUID[ESPMaster];
      globalDoc[i]["timestamp"] = printLocalTime();
    }

    // push to AWS

    // sendData();

    if (err == DeserializationError::Ok)
    {

      // Print the values
      // (we must use as<T>() to resolve the ambiguity)
      // Serial.print("timestamp = ");
      // Serial.println(doc["timestamp"].as<long>());
      // Serial.print("value = ");
      // Serial.println(doc["value"].as<int>());
    }
    else
    {
      // Print error to the "debug" serial port
      Serial.print("deserializeJson() returned ");
      Serial.println(err.c_str());

      // Flush all bytes in the "link" serial port buffer
      while (Serial2.available() > 0)
        Serial2.read();
    }
  }
}

void messageReceived(char *topic, byte *payload, unsigned int length)
{

  // Serial.print("ESP.getFreeHeap(");
  // Serial.print(ESP.getFreeHeap());
  // Serial.println(")");
  // Serial.print("Received [");
  // Serial.print(topic);
  // Serial.println("]");
  // for (int i = 0; i < length; i++)
  // {
  //   Serial.print((char)payload[i]);
  // }
  // Serial.println();
  // Serial.print("ESP.getFreeHeap(");
  // Serial.print(ESP.getFreeHeap());
  // Serial.println(")");

  // // StaticJsonDocument<256> doc;
  // DynamicJsonDocument doc(4096); // temporarily store payload to process
  // DeserializationError error = deserializeJson(doc, payload, length);

  // // Test if parsing succeeds.
  // if (error)
  // {
  //   Serial.print(F("deserializeJson() failed: "));
  //   Serial.println(error.f_str());
  //   return;
  // }

  // const char *companyId = doc["companyId"];
  // const char *locationId = doc["locationId"];
  // const char *lotId = doc["lotId"];
  // const char *smartInverterId = doc["smartInverterId"];
  // const char *operationAction = doc["operationAction"];
  // const char *controlDt = doc["controlDt"];
  // const char *logId = doc["logId"];
  // Serial.println();

  // Serial.print("companyId: ");
  // Serial.println(companyId);
  // Serial.print("locationId: ");
  // Serial.println(locationId);
  // Serial.print("lotId: ");
  // Serial.println(lotId);
  // Serial.print("smartInverterId: ");
  // Serial.println(smartInverterId);
  // Serial.print("operationAction: ");
  // Serial.println(operationAction);
  // Serial.print("controlDt: ");
  // Serial.println(controlDt);
  // Serial.print("logId: ");
  // Serial.println(logId);

  // Serial.print("ESP.getFreeHeap(");
  // Serial.print(ESP.getFreeHeap());
  // Serial.println(")");
  // // if (strcmp_P(smartInverterId, smartInverterIdMem[ESPSwitch]) == 0)
  // // {
  // //   Serial.println("Ok, smartInverterId is the same");
  // //   if (strcmp_P(operationAction, OperationOn) == 0)
  // //   {
  // //     SwitchOn();
  // //     sendData();
  // //     // send feedback
  // //   }
  // //   if (strcmp_P(operationAction, OperationOff) == 0)
  // //   {
  // //     SwitchOff();
  // //     sendData();
  // //     // send feedback
  // //   }
  // // }

  // Serial.println();
}

void pubSubErr(int8_t MQTTErr)
{
  if (MQTTErr == MQTT_CONNECTION_TIMEOUT)
    Serial.print("Connection timeout");
  else if (MQTTErr == MQTT_CONNECTION_LOST)
    Serial.print("Connection lost");
  else if (MQTTErr == MQTT_CONNECT_FAILED)
    Serial.print("Connect failed");
  else if (MQTTErr == MQTT_DISCONNECTED)
    Serial.print("Disconnected");
  else if (MQTTErr == MQTT_CONNECTED)
    Serial.print("Connected");
  else if (MQTTErr == MQTT_CONNECT_BAD_PROTOCOL)
    Serial.print("Connect bad protocol");
  else if (MQTTErr == MQTT_CONNECT_BAD_CLIENT_ID)
    Serial.print("Connect bad Client-ID");
  else if (MQTTErr == MQTT_CONNECT_UNAVAILABLE)
    Serial.print("Connect unavailable");
  else if (MQTTErr == MQTT_CONNECT_BAD_CREDENTIALS)
    Serial.print("Connect bad credentials");
  else if (MQTTErr == MQTT_CONNECT_UNAUTHORIZED)
    Serial.print("Connect unauthorized");
}

void connectToMqtt(bool nonBlocking = false)
{
  Serial.print("MQTT connecting to: ");
  Serial.println(THINGNAME[ESPMaster]);
  Serial.println(DEV_UUID[ESPMaster]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")");
  while (!client.connected())
  {
    if (client.connect(THINGNAME[ESPMaster]))
    {
      Serial.print("ESP.getFreeHeap(");
      Serial.print(ESP.getFreeHeap());
      Serial.println(")client.connect(THINGNAME)");
      Serial.println("MQTT Connected!");
      if (!client.subscribe(MQTT_SUB_TOPIC))
        pubSubErr(client.state());

      Serial.print("ESP.getFreeHeap(");
      Serial.print(ESP.getFreeHeap());
      Serial.println(")client.subscribe(MQTT_SUB_TOPIC)");
    }
    else
    {
      Serial.print("Failed, reason -> ");
      pubSubErr(client.state());
      if (!nonBlocking)
      {
        Serial.println(" < Try again in 5 seconds");
        delay(5000);
      }
      else
      {
        Serial.println(" <");
      }
    }
    if (nonBlocking)
      break;
  }
}

void connectToWiFi(String init_str)
{
  if (init_str != emptyString)
    Serial.print(init_str);
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(1000);
    unsigned long currentMillis = millis();
    // if WiFi is down, try reconnecting every CHECK_WIFI_TIME seconds
    if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >= interval))
    {
      Serial.print(millis());
      Serial.println("Reconnecting to WiFi...");
      WiFi.disconnect();
      WiFi.reconnect();
      previousMillis = currentMillis;
    }
  }
  if (init_str != emptyString)
    Serial.println();
  Serial.println("Connected!");
  Serial.println("");
  Serial.println("WiFi connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("ESP Mac Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Subnet Mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP: ");
  Serial.println(WiFi.gatewayIP());
  Serial.print("DNS: ");
  Serial.println(WiFi.dnsIP());
}

void checkWiFiThenMQTT(void)
{
  connectToWiFi("Checking WiFi");
  connectToMqtt();
}

void checkWiFiThenMQTTNonBlocking(void)
{
  unsigned long previousMillis = 0;
  const long interval = 5000;
  connectToWiFi(emptyString);
  if (millis() - previousMillis >= interval && !client.connected())
  {
    previousMillis = millis();
    connectToMqtt(true);
  }
}

void checkWiFiThenReboot(void)
{
  connectToWiFi("Checking WiFi");
  Serial.print("Rebooting");
  ESP.restart();
}

void sendData(void)
{
  // if (globalDoc != null)
  // DynamicJsonDocument jsonBuffer(JSON_OBJECT_SIZE(3) + 100);
  // {
  //   "data" : [
  //     {
  //       "deviceId" : "6bfaf4e8-50e6-4120-9cd9-d2639be9bded",
  //       "ref" : 1,
  //       "timestamp" : 1639467222,
  //       "temperature" : 0,
  //       "humidity" : 0
  //     },
  //   ]
  // }
  // {
  //       "deviceId" : "6bfaf4e8-50e6-4120-9cd9-d2639be9bded",
  //       "ref" : 1,
  //       "timestamp" : 1639467222,
  //       "temperature" : 0,
  //       "humidity" : 0
  //     }
  DynamicJsonDocument doc(4096);
  // DynamicJsonDocument nestedSensor(4096);

  // nestedSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
  // nestedSensor["ref"] = 1;
  // nestedSensor["timestamp"] = 1639554215;
  // nestedSensor["temperature"] = 40;
  // nestedSensor["humidity"] = 20;
  // JsonArray data = doc.createNestedArray("data"); // create array for loop for each sensor
  doc["data"] = globalDoc;
  // data.add(globalDoc); // append loop

  // JsonObject root = doc.to<JsonObject>();

  // deviceObject["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
  // root["data"] = deviceObject;
  // root["smartInverterId"] = "f4216c8226b0488b9c75c85d7d59221d_f928eb0c081a43f69ed64230476953cf";
  //  root["time"]=asctime(&timeinfo);
  // JsonObject state = root.createNestedObject("state");
  // JsonObject state_reported = state.createNestedObject("reported");
  // state_reported["value"] = random(100);
  Serial.printf("Sending  [%s]: ", MQTT_PUB_TOPIC);
  // serializeJson(root, Serial);
  Serial.println();
  char shadow[measureJson(doc) + 1];
  serializeJson(doc, shadow, sizeof(shadow));
  serializeJsonPretty(doc, Serial);
  Serial.println("doc publish");
  if (!client.publish(MQTT_PUB_TOPIC, shadow, false))
    pubSubErr(client.state());
  else
  {
    globalDoc.clear();
  }
}

////////////////////////////////////////////////////////// MQTT code

void MQTTSetup()
{
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  preferences.putBool("isMesh", false); // set to AP mode after enter MQTT
  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")");
  delay(5000);
  Serial.println();

#ifdef ESP32
  // if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  // {
  //   Serial.println("STA Failed to configure");
  // }

  // WiFi.setHostname(THINGNAME[ESPSwitch]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")WiFi.setHostname");
#else
  WiFi.hostname(THINGNAME[ESPSwitch]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")WiFi.hostname");
#endif
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  connectToWiFi(String("Attempting to connect to SSID: ") + String(ssid));

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")WiFi.hostname");

  NTPConnect();
  client.setBufferSize(4096);

  // sprintf(convertIntToConstChar, "%d", (long)(ESPSwitch + 100)); // where 'myUint32' is your Uint32 variable
  // my_function((const char*)convertIntToConstChar); // where 'my_function' is your function requiring a const char*
  // MDNS.begin((const char *)convertIntToConstChar);
  // if (MDNS.begin("host")) {
  //   Serial.println("mDNS responder started");
  //}
  // httpUpdater.setup(&httpServer);
  // httpServer.begin();
  // MDNS.addService("http", "tcp", 80);
  // Serial.printf("HTTPUpdateServer ready! Open -     http://%s.local/update     - in your browser\n", convertIntToConstChar);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")NTPConnect()");

#ifdef ESP32
  net.setCACert(cacert);
  net.setCertificate(client_cert[ESPMaster]);
  net.setPrivateKey(privkey[ESPMaster]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")net.setCACert");
#else
  net.setTrustAnchors(&cert);
  net.setClientRSACert(&client_crt, &key);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")net.setTrustAnchors");
#endif

  client.setServer(MQTT_HOST, MQTT_PORT);
  client.setCallback(messageReceived);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")client.setServer");

  connectToMqtt();

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")connectToMqtt");
  // ControlMode();
}
void MQTTLoop()
{

  // httpServer.handleClient();
  unsigned long currentMillis = millis();
  // if WiFi is down, try reconnecting every CHECK_WIFI_TIME seconds
  if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >= interval))
  {
    Serial.print(millis());
    Serial.println("Reconnecting to WiFi...");
    WiFi.disconnect();
    WiFi.reconnect();
    previousMillis = currentMillis;
  }
  {
    now = time(nullptr);
    if (!client.connected())
    {
      checkWiFiThenMQTT();

      Serial.println("ESP.getFreeHeap()checkWiFiThenMQTT()");
      Serial.print("ESP.getFreeHeap(");
      Serial.print(ESP.getFreeHeap());
      Serial.println(")");
      // checkWiFiThenMQTTNonBlocking();
      // checkWiFiThenReboot();
    }
    else
    {
      client.loop();
      getDataFromSerial();
      if (millis() - lastMillis > 5000)
      {
        lastMillis = millis();
        unsigned long previousMillis = 0;
        if (!globalDoc.isNull() && millis() - previousMillis > sendDataInterval)

        {
          previousMillis = millis();
          sendData();
        }
      }
    }
  }

  // set reboot ESP after 1 hour using millis

  unsigned long rebootInterval = 60 * 60 * 1000; // hour * minute * second
  unsigned long rebootMillis = millis();
  if (rebootMillis > rebootInterval)
  {
    ESP.restart();
  }
}

void setup()
{
  // 1. read EEPROM for node, ssid, pass
  // 2. read AP or STA mode
  // 3. if STA mode, attempt connect wifi
  // 4. if AP mode, launch AP

  // read EEPROM
  Serial.begin(115200); // Initialising if(DEBUG)Serial Monitor
  Serial.println();

  //---------------------------------------- Read preferences for ssid and pass
  preferences.begin("credentials", false);

  // ref = preferences.getInt("nodeName", 0);

  meshSSID = preferences.getString("meshSSID", "");
  meshPass = preferences.getString("meshPass", "");
  isMesh = preferences.getBool("isMesh", "");

  if (meshSSID == "" || meshPass == "")
  {
    Serial.println("No values saved for ssid or password");
  }
  else
  {
    Serial.println("Print  ssid or password");
    Serial.println(meshSSID);
    Serial.println(meshPass);
    // Serial.println(ref);
  }

  //---------------------------------------- End Read preferences for ssid and pass

  //---------------------------------------- Init wifi
  if (isMesh)
  {
    MQTTSetup();
    // sht.begin(SHT31_ADDRESS);
    // sht.requestData();
    // pinMode(LED, OUTPUT);
    // setupMesh();
    // Serial.print("setupMesh mesh");
  }
  else
  {
    APmode();
  }
}

void loop()
{

  if (isMesh)
  {
    MQTTLoop();
    // mesh.update();
    // digitalWrite(LED, !onFlag);

    // getRootIDLoop();
  }
  else
  {
    checkAPConnection();

    AsyncElegantOTA.loop();
  }
}

void APmode()
{
  //---------------------------------------- Set preferences for AP

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH); // light up when start
  byte mac[6];
  WiFi.macAddress(mac);
  uniq += versionName;
  uniq += "_";
  uniq += ref;
  uniq += "_";
  uniq += meshSSID;
  uniq += "_";
  uniq += String(mac[0], HEX);
  uniq += String(mac[1], HEX);
  uniq += String(mac[2], HEX);
  uniq += String(mac[3], HEX);
  uniq += String(mac[4], HEX);
  uniq += String(mac[5], HEX);
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(ssid, password);
  WiFi.softAP(uniq.c_str(), "");
  Serial.println("");

  //---------------------------------------- End Set preferences for AP

  // Wait for connection
  // while (WiFi.status() != WL_CONNECTED)
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(meshSSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });

  // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
  server.on("/blink", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage1;
              String inputMessage2;
              // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
              if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
              {
                inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
                inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
                digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
              }
              else
              {
                inputMessage1 = "No message sent";
                inputMessage2 = "No message sent";
              }
              Serial.print("GPIO: ");
              Serial.print(inputMessage1);
              Serial.print(" - Set to: ");
              Serial.println(inputMessage2);
              request->send(200, "text/plain", "OK"); });

  // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
  server.on("/setMeshDataNameSSIDPass", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage;
              String inputParam;
              // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
              if (request->hasParam(PARAM_NODENAME))
              {
                inputMessage = request->getParam(PARAM_NODENAME)->value();
                inputParam = PARAM_NODENAME;
              }
              // GET input2 value on <ESP_IP>/get?input2=<inputMessage>
              else if (request->hasParam(PARAM_SSID))
              {
                inputMessage = request->getParam(PARAM_SSID)->value();
                inputParam = PARAM_SSID;
              }
              // GET input3 value on <ESP_IP>/get?input3=<inputMessage>
              else if (request->hasParam(PARAM_PASS))
              {
                inputMessage = request->getParam(PARAM_PASS)->value();
                inputParam = PARAM_PASS;
              }
              else
              {
                inputMessage = "No message sent";
                inputParam = "none";
              }

              if (request->hasParam(PARAM_NODENAME))
              {
                // int refInt = ;
                preferences.putInt("nodeName", request->getParam(PARAM_NODENAME)->value().toInt());
              }
              if (request->hasParam(PARAM_SSID))
                preferences.putString("meshSSID", request->getParam(PARAM_SSID)->value());
              if (request->hasParam(PARAM_PASS))
                preferences.putString("meshPass", request->getParam(PARAM_PASS)->value());

              // preferences.end();
              Serial.println(inputMessage);
              request->send(200, "text/plain", "OK"); });
  // request->send(200, "text/html", "HTTP GET request sent to your ESP on input field (" + inputParam + ") with value: " + inputMessage + "<br><a href=\"/\">Return to Home Page</a>"); });
  server.on("/redirect/internal", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->redirect("/"); });
  server.on("/rebootEsp", HTTP_GET, [](AsyncWebServerRequest *request)
            { ESP.restart(); });

  AsyncElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");
}

void checkAPConnection()
{
  unsigned long previousMillis = 0;
  unsigned long interval = 1000;
  while ((WiFi.softAPgetStationNum() == 0))
  {
    // delay(500);

    // blink without delay code
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis > interval)
    {
      previousMillis = currentMillis;
      Serial.print(".");
      // if (ledState == LOW)
      //   // ledState = HIGH;

      //   Serial.print(".");
      // else
      //   // ledState = LOW;

      //   Serial.print("_");
      // digitalWrite(ledPin, ledState);
    }
    if (currentMillis > intervalOneMinute)
    {
      preferences.putBool("isMesh", true);
      ESP.restart();
    }
  }
}

String processor(const String &var)
{
  // Serial.println(var);
  if (var == "BUTTONPLACEHOLDER")
  {
    String buttons = "";
    buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
    return buttons;
  }
  return String();
}

String outputState(int output)
{
  if (digitalRead(output))
  {
    return "checked";
  }
  else
  {
    return "";
  }
}