//************************************************************
// this is a simple example that uses the painlessMesh library
//
// 1. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 2. prints anything it receives to Serial.print
//
//
//************************************************************
#include "painlessMesh.h"

#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

Scheduler userScheduler; // to control your personal task
painlessMesh  mesh;

SimpleList<uint32_t> nodes;

// User stub
void wifiLoop() ; // use to swwitch OFF and ON wifi
void singleSendLoop() ;  // Send message every interval 
//void startMesh() ;// Prototype so PlatformIO doesn't complain

unsigned long previousMsgTime = 0; 
unsigned long previousWifiTime = 0;
unsigned long previous = 0;
unsigned long onTime = 30000;  //10 sec uptime
unsigned long offTime = 120000; //120 sec downtime
unsigned long timeNow;
unsigned long wifiTimeNow;
unsigned long messageTimeNow;
boolean wifiStatus = true;

boolean singleSendStatus = true;
uint32_t target = 3171370209;


Task taskWifiLoop( TASK_SECOND * 1 , TASK_FOREVER, &wifiLoop );
//Task taskSendSingleLoop( TASK_SECOND * 3 , TASK_FOREVER, &singleSendLoop );



//void sendMessage() {
//  String msg = "Private Message: Hello from node    ";
//  msg += mesh.getNodeId();
//  mesh.sendSingle(  target,  msg );
//  taskSendMessage.setInterval( TASK_SECOND * 5 );
//}

void wifiLoop() {
  if (wifiStatus == true){
    if (singleSendStatus == true){
//      String msg = "PRIVATE MESSAGE from node NINE _ New_Rev2   ";
      DynamicJsonDocument doc(512);
      String msg;
      float temperature;
      float humidity;
      temperature = 30;
      humidity =75.55;
      doc["name"] = "Sensor Node one";
      doc["ref"] = 1;
      doc["temperature"] = temperature;
      doc["humidity"] = humidity;
      serializeJson(doc, msg);
      mesh.sendSingle( target, msg );
      previous = timeNow;
      Serial.println("Sending Message...");
      taskWifiLoop.setInterval( TASK_SECOND * 1);
      nodes = mesh.getNodeList();
      Serial.printf("Num nodes: %d\n", nodes.size());
//      mesh.asNodeTree
//      getRootId(mesh.asNodeTree())
     }
    else {
      Serial.println("Message sent!");
      wifiStatus = false;
      WiFi.mode(WIFI_OFF);
      previous = timeNow;
    }
  }
  else if (wifiStatus == false){
   if (timeNow - previous <= offTime) {
    WiFi.mode(WIFI_OFF);
    taskWifiLoop.setInterval( TASK_SECOND * 10);
    Serial.print("Im Sleeping , Wifi in turned OFF, Wifi status: ");    
    Serial.println(wifiStatus);
    Serial.print("timeNow = ");
    Serial.println(timeNow);
    Serial.print("previous  = ");
    Serial.println(previous);
    Serial.print("Current time - previous time = ");
    Serial.println(timeNow - previous);
   }
   else {
    wifiStatus = true;
    WiFi.mode(WIFI_AP);
    Serial.print("Im awake - Wifi is turned ON, Wifi status: ");
    Serial.println(wifiStatus);
    singleSendStatus = true;
   }
  }
}

//void singleSendLoop (){
//  Serial.print("The Message time now is: ");
//  Serial.println(messageTimeNow);
//  if(singleSendStatus == true){
//    String msg = "PRIVATE MESSAGE from node FOUR    ";
////    msg += mesh.getNodeId();
//    mesh.sendSingle( target, msg );
//    previousMsgTime = messageTimeNow;
////    broadcastLoop = false;
//  }
//  else if (singleSendStatus == false){
////    Serial.println("Reply received. Proceeding to stop for 120 seconds");
//    if (messageTimeNow - previousMsgTime >= offTime) {
//     singleSendStatus = true;
//     previousMsgTime = messageTimeNow;
//    Serial.println("120 seconds has passed, re-sending message");
//    }
//  }
//}



//
//void wifiLoop() {
// Serial.println(" myFreeMemory: " + String(ESP.getFreeHeap()));
// Serial.print("The WiFi time now is: ");
// Serial.println(wifiTimeNow);
// if (wifiStatus == true) {
//  if (wifiTimeNow - previousWifiTime >= onTime) {
//    Serial.println(wifiTimeNow - previousWifiTime);
//    wifiStatus = false;
//    Serial.print("Wifi Status is: ");
//    Serial.println(wifiStatus);
//    previousWifiTime = wifiTimeNow;
//    Serial.print("Previous WiFi time is: ");
//    Serial.println(previousWifiTime);
//    WiFi.mode(WIFI_OFF);
//    Serial.println("WiFi Off");
//    taskWifiLoop.setInterval ( TASK_SECOND*10 );
//    
//  }
// }
// else if (wifiStatus == false) {
//  if (wifiTimeNow - previousWifiTime >= offTime) {
//    Serial.println(wifiTimeNow - previousWifiTime);
//    wifiStatus = true;
//    Serial.print("Wifi Status is: ");
//    Serial.println('wifiStatus');
//    previousWifiTime = wifiTimeNow;
//    Serial.print("Previous WiFi time is: ");
//    Serial.println(previousWifiTime);
//    WiFi.mode(WIFI_AP);
//    Serial.println("WiFi On");
//    taskWifiLoop.setInterval ( TASK_SECOND*30 );
//  }else {
//    WiFi.mode(WIFI_OFF);  
//  }
// }
//}


// Needed for painless library
//void receivedCallback( uint32_t from, String &msg ) {
//  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
//}


void newConnectionCallback(uint32_t nodeId) {
    Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections\n");
}

void nodeTimeAdjustedCallback(int32_t offset) {
    Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(),offset);
}

void receivedCallback( uint32_t from, String &msg  ){
  Serial.printf("echoNode: Received from %u msg=%s\n", from, msg.c_str());
  String msg2 = "Message Received from Node ONE";
//  mesh.sendSingle(from, msg2);
  singleSendStatus = false;
}

void setup() {
  Serial.begin(115200);

//mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);


  userScheduler.addTask( taskWifiLoop );
//  userScheduler.addTask( taskSendSingleLoop);

  
  taskWifiLoop.enable();
//  taskSendSingleLoop.enable();
}

void loop() {
  // it will run the user scheduler as well
  mesh.update();
  wifiTimeNow = millis();
  messageTimeNow = millis();
  timeNow = millis();

//  Serial.println(timeNow);
  
}
