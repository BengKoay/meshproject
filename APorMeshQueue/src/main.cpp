// Code explanation

// 1. read preferences for node, ssid, pass, isMesh
// 2. read AP or STA mode, isMESH true = mesh, isMESH false = AP
// 3. if STA mode, attempt connect Mesh Network
// 3.1. for 60 tries, will attempt connect Mesh Network, start counter "meshNotConnectedCounter"
// 3.1.1. if after 60 tries still unable to connect, will write isMESH false,
// 3.2. if between 60 tries , Mesh Network is connected, counter "meshNotConnectedCounter" resets
// 4. if AP mode, launch AP
// 4.1. if not connected for 60 seconds, will only set isMESH = true after reboot
// 4.2. will only set isMESH = true after reboot

// to work on:
// Done - set AP mode on purpose and not failover, during isMesh
// Done - set timeout after AP?
// progmem
// Done - asynctcp
//

// to do

// Done 1. LED light up when start
// Done 2. name change when updated, version-ing
// Done 3. name of sensor
// Done 4. name of dryer
// Done 5. AP mode for 3 min at start?, after 3 min reboot to Mesh

// naming - version_sensorname_dryername_MAC

// version
// 1.4 - using ref to change to int for backend
// 1.5 - setRoot
// 1.6 - broadcast mesh msg, every 5 sec

// // 3.2.2 - stable code
// // 3.3.0 - reboot mesh if not connected to rootID
// // 3.3.2 - extend time reboot 5 minutes mesh 3.3.0
// // 3.3.3 - bug fix AP
// // 3.3.4 - setRoot code
// // 3.3.5 - reboot mesh if no detect setRoot
// // 3.3.6 - channel 11
// // 3.3.7 - channel 6
#include <Arduino.h>

// #include <ESP8266WiFi.h>
// #include <ESP8266HTTPClient.h>
// #include <ESP8266WebServer.h>

#include <WiFi.h>
// #include <WebServer.h>
// #include <EEPROM.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
// #include <painlessMesh.h>
#include "namedMesh.h"
#include "SHT31.h"

#include <Preferences.h>

// define

// some gpio pin that is connected to an LED...
// on my rig, this is 5, change to the right number of your LED.
#define LED 2              // GPIO number of connected LED, ON ESP-12 IS GPIO2
#define SHT31_ADDRESS 0x44 // define SHT address

#define BLINK_PERIOD 3000  // milliseconds until cycle repeat
#define BLINK_DURATION 100 // milliseconds LED is on for
#define MESH_PORT 5555

// Variables
Preferences preferences;
int i = 0;
int statusCode;
// const char *ssid = "";       // not used?
// const char *passphrase = ""; // not used?
// String st;
// String content;
// String enodeName;
// String esid;
// String epass = "";

// variable for init
bool isMesh = false; // isMESH true = mesh, isMESH false = AP
// bool isMesh;
int meshNotConnectedCounter = 0;
int apNotConnectedCounter = 0;
int APcounter = 0;
// // unsigned long rootID = 3046393153; // A13
// unsigned long rootID = 3046322341; // A13.
// unsigned long rootID = 3046321129; // A11
unsigned long rootID = 3046391301; // A11.
float temperature;
float humidity;

// variable for AP

String versionName = "v3.3.7"; // version-ing
String meshSSID;
String meshPass;
String nodeName = "Slave"; // Name needs to be unique          //--------------------- root will recognize slave by its nodeName
int ref;
String uniq;

const char *PARAM_NODENAME = "param_nodename";
const char *PARAM_SSID = "param_ssid";
const char *PARAM_PASS = "param_pass";

const char *PARAM_INPUT_1 = "output";
const char *PARAM_INPUT_2 = "state";

// variable for mesh
Scheduler userScheduler; // to control your personal task
namedMesh mesh;
bool calc_delay = false;
SimpleList<uint32_t> nodes;

// variable for sensor

SHT31 sht; // declare sht variable

// maybe use blink without delay concept

const int ledPin = 2;
int ledState = LOW;

// declare currentMillis huge value to make sure wifi can connect
// unsigned long currentMillis = 10000;

// constants won't change:
const long interval = 1000; // interval at which to blink (milliseconds)

const long intervalCheckMesh = 1000; // interval at which to blink (milliseconds)
const long intervalCheckAP = 60000;  // interval at which to blink (milliseconds)
const long intervalOneMinute = 60000;

// unsigned long previous = 0;

unsigned long previousMsgTime = 0;
unsigned long previousWifiTime = 0;
unsigned long previous = 0;
unsigned long onTime = 30000;   // 10 sec uptime
unsigned long offTime = 120000; // 120 sec downtime
unsigned long timeNow;
unsigned long wifiTimeNow;
unsigned long messageTimeNow;
boolean wifiStatus = true;

boolean singleSendStatus = true;
uint32_t target = 4146324389;
// Function Declaration

void setupMesh();
// bool testWifi(void);
// void launchWeb(void);
// void setupAP(void);

// void createWebServer();

// prototype

String processor(const String &var);
String outputState(int output);
void APmode();

void wifiLoop();
void receivedCallback(uint32_t from, String &msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback();
void nodeTimeAdjustedCallback(int32_t offset);
void delayReceivedCallback(uint32_t from, int32_t delay);
void requestDataSHT();

void checkAPConnection();

void getRootIDLoop();
size_t getRootId(protocol::NodeTree nodeTree);

Task taskSendMessage(TASK_SECOND * 10, TASK_FOREVER, &requestDataSHT); // start with a one second interval

Task taskWifiLoop(TASK_SECOND * 2, TASK_FOREVER, &wifiLoop);
// Establishing Local server at port 80 whenever required
//  ESP8266WebServer server(80);
//  WebServer server(80);
AsyncWebServer server(80);

// Task to blink the number of nodes
Task blinkNoNodes;
bool onFlag = false;

// HTML

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px}
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.slider {background-color: #b30000}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>ESP Web Server</h2>
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/blink?output="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/blink?output="+element.id+"&state=0", true); }
  xhr.send();
}
</script>
</body>
<br>
<button onclick="location.href = '/update';" id="myButton" class="float-left submit-button" >update</button>
<br>
<button onclick="location.href = '/rebootEsp';" id="myButton" class="float-left submit-button" >rebootEsp</button>
<br>
<form action="/setMeshDataNameSSIDPass">
    param_nodename: <input type="text" name="param_nodename">

  <br>
    param_ssid: <input type="text" name="param_ssid">

  <br>
    param_pass: <input type="text" name="param_pass">
  <br>
    <input type="submit" value="Submit">
  </form>
</html>
)rawliteral";

void setup()
{
  // 1. read EEPROM for node, ssid, pass
  // 2. read AP or STA mode
  // 3. if STA mode, attempt connect wifi
  // 4. if AP mode, launch AP

  // read EEPROM
  Serial.begin(115200); // Initialising if(DEBUG)Serial Monitor
  Serial.println();

  //---------------------------------------- Read preferences for ssid and pass
  preferences.begin("credentials", false);

  ref = preferences.getInt("nodeName", 0);

  meshSSID = preferences.getString("meshSSID", "");
  if (meshSSID == "DryerA1" || meshSSID == "DryerA1.")
    meshSSID = "DryerA1";
  meshPass = preferences.getString("meshPass", "");
  isMesh = preferences.getBool("isMesh", "");

  if (meshSSID == "" || meshPass == "")
  {
    Serial.println("No values saved for ssid or password");
  }
  else
  {
    Serial.println("Print  ssid or password");
    Serial.println(meshSSID);
    Serial.println(meshPass);
    // Serial.println(ref);
  }

  //---------------------------------------- End Read preferences for ssid and pass

  //---------------------------------------- Init wifi
  if (isMesh)
  {
    sht.begin(SHT31_ADDRESS);
    sht.requestData();
    pinMode(LED, OUTPUT);
    setupMesh();
    Serial.print("setupMesh mesh");
  }
  else
  {
    APmode();
  }
}

void loop()
{

  if (isMesh)
  {
    // it will run the user scheduler as well
    mesh.update();
    // wifiTimeNow = millis();
    // messageTimeNow = millis();
    timeNow = millis();

    //  Serial.println(timeNow);
  }
  else
  {
    checkAPConnection();

    AsyncElegantOTA.loop();
  }
}

void setupMesh()
{
  Serial.begin(115200);

  preferences.putBool("isMesh", false); // set to AP mode at start of mesh
  // mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes(ERROR | STARTUP); // set before init() so that you can see startup messages

  mesh.init(meshSSID, meshPass, &userScheduler, MESH_PORT);
  // mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  userScheduler.addTask(taskWifiLoop);
  //  userScheduler.addTask( taskSendSingleLoop);

  taskWifiLoop.enable();
  //  taskSendSingleLoop.enable();
}

void getRootIDLoop()
{
  unsigned long rebootInterval = 3 * 60 * 1000; // minute * second
  unsigned long rebootMillis = millis();
  if (rebootMillis - previous > rebootInterval)
  {
    size_t rootId = getRootId(mesh.asNodeTree());
    if (!rootId)
    {
      // Serial.println("no rootId");
      // Serial.println(rootId);
      preferences.putBool("isMesh", true);
      ESP.restart();
    }
    previous = rebootMillis;
  }
}

// START Identify a root node in a network
size_t getRootId(protocol::NodeTree nodeTree) // https://gitlab.com/painlessMesh/painlessMesh/-/issues/352
{
  {
    if (nodeTree.root)
      return nodeTree.nodeId;
    for (auto &&s : nodeTree.subs)
    {
      auto id = getRootId(s);
      if (id != 0)
        return id;
    }
    return 0;
  }
}
// END Identify a root node in a network

void wifiLoop()
{
  if (wifiStatus == true)
  {
    if (singleSendStatus == true)
    {
      requestDataSHT();
      //      String msg = "PRIVATE MESSAGE from node NINE _ New_Rev2   ";
      DynamicJsonDocument doc(512);
      String msg;
      // float temperature;
      // float humidity;
      // temperature = 30;
      // humidity = 75.55;
      // doc["name"] = "Sensor Node one";
      doc["ref"] = ref;
      doc["temperature"] = temperature;
      doc["humidity"] = humidity;
      serializeJson(doc, msg);
      serializeJsonPretty(doc, Serial);
      Serial.println();
      mesh.sendSingle(target, msg);
      previous = timeNow;
      Serial.println("Sending Message...");
      taskWifiLoop.setInterval(TASK_SECOND * 2);
      nodes = mesh.getNodeList();
      Serial.printf("Num nodes: %d\n", nodes.size());
      //      mesh.asNodeTree
      //      getRootId(mesh.asNodeTree())
    }
    else
    {
      Serial.println("Message sent!");
      wifiStatus = false;
      WiFi.mode(WIFI_OFF);
      previous = timeNow;
    }
  }
  else if (wifiStatus == false)
  {
    if (timeNow - previous <= offTime)
    {
      WiFi.mode(WIFI_OFF);
      taskWifiLoop.setInterval(TASK_SECOND * 10);
      Serial.print("Im Sleeping , Wifi in turned OFF, Wifi status: ");
      Serial.println(wifiStatus);
      Serial.print("timeNow = ");
      Serial.println(timeNow);
      Serial.print("previous  = ");
      Serial.println(previous);
      Serial.print("Current time - previous time = ");
      Serial.println(timeNow - previous);
    }
    else
    {
      wifiStatus = true;
      WiFi.mode(WIFI_AP);
      Serial.print("Im awake - Wifi is turned ON, Wifi status: ");
      Serial.println(wifiStatus);
      singleSendStatus = true;
    }
  }
}

void newConnectionCallback(uint32_t nodeId)
{
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback()
{
  Serial.printf("Changed connections\n");
}

void nodeTimeAdjustedCallback(int32_t offset)
{
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

void receivedCallback(uint32_t from, String &msg)
{
  Serial.printf("echoNode: Received from %u msg=%s\n", from, msg.c_str());
  String msg2 = "Message Received from Node ONE";
  //  mesh.sendSingle(from, msg2);
  singleSendStatus = false;
}

// void receivedCallback(uint32_t from, String &msg)
// {
//   Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
// }

// void newConnectionCallback(uint32_t nodeId)
// {
//   // Reset blink task
//   onFlag = false;
//   blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
//   blinkNoNodes.enableDelayed(BLINK_PERIOD - (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);

//   Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
//   Serial.printf("--> startHere: New Connection, %s\n", mesh.subConnectionJson(true).c_str());
// }

// void changedConnectionCallback()
// {
//   Serial.printf("Changed connections\n");
//   // Reset blink task
//   onFlag = false;
//   blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
//   blinkNoNodes.enableDelayed(BLINK_PERIOD - (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);

//   nodes = mesh.getNodeList();

//   Serial.printf("Num nodes: %d\n", nodes.size());
//   Serial.printf("Connection list:");

//   SimpleList<uint32_t>::iterator node = nodes.begin();
//   while (node != nodes.end())
//   {
//     Serial.printf(" %u", *node);
//     node++;
//   }
//   Serial.println();
//   calc_delay = true;
// }

// void nodeTimeAdjustedCallback(int32_t offset)
// {
//   Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
// }

// void delayReceivedCallback(uint32_t from, int32_t delay)
// {
//   Serial.printf("Delay to node %u is %d us\n", from, delay);
// }
void requestDataSHT()
{
  uint16_t rawTemperature;
  uint16_t rawHumidity;
  String to = "Root";
  if (sht.dataReady())
  {
    bool success = sht.readData();
    sht.requestData(); // request for next sample
    if (success == false)
    {
      Serial.println("Failed read");
    }
    else
    {
      rawTemperature = sht.getRawTemperature();
      rawHumidity = sht.getRawHumidity();
      Serial.print(rawTemperature * (175.0 / 65535) - 45, 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.print("°C\t");
      Serial.print(rawHumidity * (100.0 / 65535), 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.println("%\t");
    }
  }

  DynamicJsonDocument doc(512);
  String msg;

  // if (pm_stage == 0 && nodesize > 0) //send timestamp
  // {

  temperature = (rawTemperature * (175.0 / 65535) - 45);
  humidity = (rawHumidity * (100.0 / 65535));
  // doc["name"] = nodeName; // time stamp info as payload
  doc["ref"] = ref;

  // int moist = random(1400, 1900);
  // doc["Moisture"] = moist;
  doc["temperature"] = temperature; // temperature info as payload
  doc["humidity"] = humidity;       // humidity info as payload
  // doc["Timestamp"] = NTPEpochTime; // time stamp info as payload
  serializeJson(doc, msg);
  // mesh.sendBroadcast(msg);
  mesh.sendSingle(to, msg);
  taskSendMessage.setInterval(TASK_SECOND * 10);
  // mesh.sendSingle(nodeID[node_targetindex], msg);
  // Serial.printf("mesh_tx aws ID: %u msg: %s\n", nodeID[node_targetindex], msg.c_str()); //display tx msg
  // pm_sendstart = millis();
  // pm_stage = 1;
  // }
}

void APmode()
{
  //---------------------------------------- Set preferences for AP

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH); // light up when start
  byte mac[6];
  WiFi.macAddress(mac);
  uniq += versionName;
  uniq += "_";
  uniq += ref;
  uniq += "_";
  uniq += meshSSID;
  uniq += "_";
  uniq += String(mac[0], HEX);
  uniq += String(mac[1], HEX);
  uniq += String(mac[2], HEX);
  uniq += String(mac[3], HEX);
  uniq += String(mac[4], HEX);
  uniq += String(mac[5], HEX);
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(ssid, password);
  WiFi.softAP(uniq.c_str(), "");
  // WiFi.softAP(const char* ssid, const char* password, int channel, int ssid_hidden, int max_connection)

  Serial.println("");

  //---------------------------------------- End Set preferences for AP

  // Wait for connection
  // while (WiFi.status() != WL_CONNECTED)
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(meshSSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });

  // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
  server.on("/blink", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage1;
              String inputMessage2;
              // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
              if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
              {
                inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
                inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
                digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
              }
              else
              {
                inputMessage1 = "No message sent";
                inputMessage2 = "No message sent";
              }
              Serial.print("GPIO: ");
              Serial.print(inputMessage1);
              Serial.print(" - Set to: ");
              Serial.println(inputMessage2);
              request->send(200, "text/plain", "OK"); });

  // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
  server.on("/setMeshDataNameSSIDPass", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage;
              String inputParam;
              // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
              if (request->hasParam(PARAM_NODENAME))
              {
                inputMessage = request->getParam(PARAM_NODENAME)->value();
                inputParam = PARAM_NODENAME;
              }
              // GET input2 value on <ESP_IP>/get?input2=<inputMessage>
              else if (request->hasParam(PARAM_SSID))
              {
                inputMessage = request->getParam(PARAM_SSID)->value();
                inputParam = PARAM_SSID;
              }
              // GET input3 value on <ESP_IP>/get?input3=<inputMessage>
              else if (request->hasParam(PARAM_PASS))
              {
                inputMessage = request->getParam(PARAM_PASS)->value();
                inputParam = PARAM_PASS;
              }
              else
              {
                inputMessage = "No message sent";
                inputParam = "none";
              }

              if (request->hasParam(PARAM_NODENAME))
              {
                // int refInt = ;
                preferences.putInt("nodeName", request->getParam(PARAM_NODENAME)->value().toInt());
              }
              if (request->hasParam(PARAM_SSID))
                preferences.putString("meshSSID", request->getParam(PARAM_SSID)->value());
              if (request->hasParam(PARAM_PASS))
                preferences.putString("meshPass", request->getParam(PARAM_PASS)->value());

              // preferences.end();
              Serial.println(inputMessage);
              request->send(200, "text/plain", "OK"); });
  // request->send(200, "text/html", "HTTP GET request sent to your ESP on input field (" + inputParam + ") with value: " + inputMessage + "<br><a href=\"/\">Return to Home Page</a>"); });
  server.on("/redirect/internal", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->redirect("/"); });
  server.on("/rebootEsp", HTTP_GET, [](AsyncWebServerRequest *request)
            { ESP.restart(); });

  AsyncElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");
}

void checkAPConnection()
{
  unsigned long previousMillis = 0;
  while ((WiFi.softAPgetStationNum() == 0))
  {
    // delay(500);

    // blink without delay code
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis > interval)
    {
      previousMillis = currentMillis;
      Serial.print(".");
      // if (ledState == LOW)
      //   // ledState = HIGH;

      //   Serial.print(".");
      // else
      //   // ledState = LOW;

      //   Serial.print("_");
      // digitalWrite(ledPin, ledState);
    }
    if (currentMillis > intervalOneMinute)
    {
      preferences.putBool("isMesh", true);
      ESP.restart();
    }
  }
}

String processor(const String &var)
{
  // Serial.println(var);
  if (var == "BUTTONPLACEHOLDER")
  {
    String buttons = "";
    buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
    return buttons;
  }
  return String();
}

String outputState(int output)
{
  if (digitalRead(output))
  {
    return "checked";
  }
  else
  {
    return "";
  }
}