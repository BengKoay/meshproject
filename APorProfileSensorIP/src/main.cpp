#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>

#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <Preferences.h>

// AWS sign library

#include <WiFiClientSecure.h>
#include <sha256.h>
#include <HTTPClient.h>

#include "SHT31.h"
// define UUID

#include "dryerUUID.h"

// int DRYERID;
// #define DEV_UUID "160574b8-f610-4acb-96cb-aeb32a869d05"

// wifi at Chantika

// const char *ssid = "gabi";
// const char *password = "gabi9999";
const char *ssid = "Gabi2";
const char *password = "plusnrg123";
// const char *ssid = "gabi1";
// const char *password = "gabi9999";
// const char *ssid = "gabi2";
// const char *password = "gabi9999";
// const char *ssid = "gabi3";
// const char *password = "gabi9999";

// wifi at testing home
// const char *ssid = "koaybb@unifi";
// const char *password = "309678470";

// AWS signature

// AWS sign
// TODO: put in preferences

const char *host = "5l68pc71bd";
const char *service = "execute-api";
const char *region = "ap-southeast-1";
const char *TLD = "amazonaws.com";
const char *path = "/devicePayload";

const char *customFQDN; // reserved for future use

// AWS IAM configuration
// TODO: put in preferences
const char *awsKey = "AKIAVBSYQGXZE6POVWVK";
const char *awsSecret = "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";
const char *apiKey = "eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB";

// declare global document

DynamicJsonDocument globalDoc(4096);

// define sensor input
#define LED 2 // GPIO number of connected LED, ON ESP-12 IS GPIO2
// #define RXD2 16            // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
// #define TXD2 17            // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)
#define SHT31_ADDRESS 0x44 // define SHT address

// IPAddress primaryDNS(192, 168, 1, 1);   // should be constant
// IPAddress secondaryDNS(192, 168, 1, 1); // should be constant

// prototype

SHT31 sht; // declare sht variable

Preferences preferences;
const int ledPin = 2;
int ledState = LOW;
String versionName;
String meshSSID;
String uniq;
int ref;
bool isMesh = false; // isMESH true = mesh, isMESH false = AP

const char *PARAM_NODENAME = "param_nodename";
const char *PARAM_SSID = "param_ssid";
const char *PARAM_PASS = "param_pass";

const char *PARAM_INPUT_1 = "output";
const char *PARAM_INPUT_2 = "state";

AsyncWebServer server(80);
void APmode();
void checkAPConnection();
void ClientMode();
void sendDataAt58second();
void requestDataSHT();
bool sendData(JsonDocument &local_doc);
void connectWifi();
int eachDeviceIP(const String &var, int ref);
String dryerProcessor(const String &var);
String processor(const String &var);
String outputState(int output);

String createRequest(String method, String uri, String payload, String apiKey,
                     String contentType, String queryString);
String hexHash(uint8_t *hash);
String createCanonicalRequest(String method, String uri, String date,
                              String time, String payloadHash, String apiKey,
                              String queryString, String contentType);
String createCanonicalHeaders(String contentType, String date, String time,
                              String payloadHash, String apiKey);
String createRequestHeaders(String contentType, String date, String time,
                            String payload, String payloadHash, String apiKey,
                            String signature);
String FQDN();
String createStringToSign(String canonical_request, String date, String time);
String createSignature(String toSign, String date);

unsigned long printLocalTime();
unsigned long previousMillis = 0;
unsigned long intervalOneSecond = 1000; // every second
const long intervalOneMinute = 60000;
float temperature;
float humidity;

// int restartCounter = 0;
// unsigned long previousMillis = 0;
// unsigned long interval = 1000;           // 1 second
unsigned long reconnectInterval = 5 * 60 * 1000; // 10 second
unsigned long reconnectMillis = 0;
// HTML

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px}
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.slider {background-color: #b30000}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>ESP Web Server</h2>
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/blink?output="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/blink?output="+element.id+"&state=0", true); }
  xhr.send();
}
</script>
</body>
<br>
<button onclick="location.href = '/update';" id="myButton" class="float-left submit-button" >update</button>
<br>
<button onclick="location.href = '/rebootEsp';" id="myButton" class="float-left submit-button" >rebootEsp</button>
<br>
<form action="/setMeshDataNameSSIDPass">
    param_nodename: <input type="text" name="param_nodename">

  <br>
    param_ssid: <input type="text" name="param_ssid">

  <br>
    param_pass: <input type="text" name="param_pass">
  <br>
    <input type="submit" value="Submit">
  </form>
</html>
)rawliteral";

// main code
void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  /////////////////////////////////////////////////////
  preferences.begin("credentials", false);

  ref = preferences.getInt("nodeName", 0);

  meshSSID = preferences.getString("meshSSID", "");
  // meshPass = preferences.getString("meshPass", "");
  isMesh = preferences.getBool("isMesh", "");

  // if (meshSSID == "" || meshPass == "")
  if (meshSSID == "")
  {
    Serial.println("No values saved for ssid or password");
  }
  else
  {
    Serial.println("Print  ssid or password");
    Serial.println(meshSSID);
    // Serial.println(meshPass);
    // Serial.println(ref);
  }

  //////////////////////////////////////////////

  if (!isMesh)
  {
    APmode();
  }
  else
  {
    ClientMode();
  }
}

void loop()
{
  // put your main code here, to run repeatedly:

  if (!isMesh)
  {
    checkAPConnection();

    AsyncElegantOTA.loop();
  }
  else
  {
    connectWifi();
    unsigned long currentMillis = millis();
    if (WiFi.status() == WL_CONNECTED && (currentMillis - previousMillis > intervalOneMinute))
    {
      Serial.print(".");
      requestDataSHT();
      previousMillis = currentMillis;
    }
  }
}

void requestDataSHT()
{
  uint16_t rawTemperature;
  uint16_t rawHumidity;
  if (sht.dataReady())
  {
    bool success = sht.readData();
    sht.requestData(); // request for next sample
    if (success == false)
    {
      Serial.println("Failed read");
    }
    else
    {
      rawTemperature = sht.getRawTemperature();
      rawHumidity = sht.getRawHumidity();
      Serial.print(rawTemperature * (175.0 / 65535) - 45, 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.print("°C\t");
      Serial.print(rawHumidity * (100.0 / 65535), 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.println("%\t");
    }
  }

  temperature = (rawTemperature * (175.0 / 65535) - 45);
  humidity = (rawHumidity * (100.0 / 65535));
  sendDataAt58second();
}

/*---------Function for Time----------------*/
unsigned long printLocalTime()
{
  // NTP server to request epoch time
  const char *ntpServer = "pool.ntp.org";
  configTime(0, 0, ntpServer);
  struct tm timeinfo;
  time_t now;
  if (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
    return (0);
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  time(&now);
  Serial.println(now);
  return now;
}
/*-----(END)----Function for Time----------------*/

// send data

void sendDataAt58second()
{

  DynamicJsonDocument doc(4096); // payload to send to AWS
  // Allocate the JSON document
  // Use arduinojson.org/v6/assistant to compute the capacity.
  DynamicJsonDocument data(4096); //  payload to send to AWS
  Serial.println("ref");
  Serial.println(ref);
  data["deviceId"] = dryerProcessor(meshSSID); // dryerProcessor(dryer name, sensor ref)
  // data["testing"] = globalDoc;
  data["timestamp"] = printLocalTime();
  data["ref"] = ref;
  data["temperature"] = temperature;
  data["humidity"] = humidity;
  serializeJsonPretty(data, Serial);
  sendData(data);

  // int secondsNow = timeinfo.tm_sec;
  // send data at 58th sec
}
/*----(START)-Functions to create payload and send to server---------------*/
bool sendData(JsonDocument &local_doc)
{
  if (WiFi.status() == WL_CONNECTED)
  {
    WiFiClientSecure client;

    char payload[768];
    serializeJson(local_doc, payload);

    HTTPClient http;
    //    http.begin("https://ejfut1hft2.execute-api.ap-southeast-1.amazonaws.com/prod/rawdata");        //test server
    //    http.addHeader("x-api-key", "HXnKw3QwOxkr0t8JOrFC25CfKihOU496sohWpwe0");
    //    http.addHeader("content-type", "application/json");
    http.begin("https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/devicePayload");
    http.addHeader("Authorization", "71b65c7a-6097-405c-9fa7-66ef64d651cc");
    int httpCode = http.POST(payload);
    String response = http.getString();
    Serial.println(httpCode);
    // if (httpCode != 200)
    // {

    //   preferences.putBool("isMesh", true);

    //   ESP.restart();
    // }
    Serial.println(response);
    http.end();
  }
}
/*----(END)-Functions to create payload and send to server---------------*/

///////////////////////////////////////////////////////////////////////////// AP mode

void APmode()
{
  //---------------------------------------- Set preferences for AP

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH); // light up when start
  byte mac[6];
  WiFi.macAddress(mac);
  uniq += versionName;
  uniq += "_";
  uniq += ref;
  uniq += "_";
  uniq += meshSSID;
  uniq += "_";
  uniq += String(mac[0], HEX);
  uniq += String(mac[1], HEX);
  uniq += String(mac[2], HEX);
  uniq += String(mac[3], HEX);
  uniq += String(mac[4], HEX);
  uniq += String(mac[5], HEX);
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(ssid, password);
  WiFi.softAP(uniq.c_str(), "", random(1, 12), 0, 1);
  // WiFi.softAP(const char* ssid, const char* password, int channel, int ssid_hidden, int max_connection)

  Serial.println("");

  //---------------------------------------- End Set preferences for AP

  // Wait for connection
  // while (WiFi.status() != WL_CONNECTED)
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(meshSSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });

  // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
  server.on("/blink", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage1;
              String inputMessage2;
              // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
              if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
              {
                inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
                inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
                digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
              }
              else
              {
                inputMessage1 = "No message sent";
                inputMessage2 = "No message sent";
              }
              Serial.print("GPIO: ");
              Serial.print(inputMessage1);
              Serial.print(" - Set to: ");
              Serial.println(inputMessage2);
              request->send(200, "text/plain", "OK"); });

  // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
  server.on("/setMeshDataNameSSIDPass", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage;
              String inputParam;
              // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
              if (request->hasParam(PARAM_NODENAME))
              {
                inputMessage = request->getParam(PARAM_NODENAME)->value();
                inputParam = PARAM_NODENAME;
              }
              // GET input2 value on <ESP_IP>/get?input2=<inputMessage>
              else if (request->hasParam(PARAM_SSID))
              {
                inputMessage = request->getParam(PARAM_SSID)->value();
                inputParam = PARAM_SSID;
              }
              // GET input3 value on <ESP_IP>/get?input3=<inputMessage>
              else if (request->hasParam(PARAM_PASS))
              {
                inputMessage = request->getParam(PARAM_PASS)->value();
                inputParam = PARAM_PASS;
              }
              else
              {
                inputMessage = "No message sent";
                inputParam = "none";
              }

              if (request->hasParam(PARAM_NODENAME))
              {
                // int refInt = ;
                preferences.putInt("nodeName", request->getParam(PARAM_NODENAME)->value().toInt());
              }
              if (request->hasParam(PARAM_SSID))
                preferences.putString("meshSSID", request->getParam(PARAM_SSID)->value());
              if (request->hasParam(PARAM_PASS))
                preferences.putString("meshPass", request->getParam(PARAM_PASS)->value());

              // preferences.end();
              Serial.println(inputMessage);
              request->send(200, "text/plain", "OK"); });
  // request->send(200, "text/html", "HTTP GET request sent to your ESP on input field (" + inputParam + ") with value: " + inputMessage + "<br><a href=\"/\">Return to Home Page</a>"); });
  server.on("/redirect/internal", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->redirect("/"); });
  server.on("/rebootEsp", HTTP_GET, [](AsyncWebServerRequest *request)
            { ESP.restart(); });

  AsyncElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");
}

void ClientMode()
{
  preferences.putBool("isMesh", false); // set to AP mode at start of mesh
                                        // mesh.setDebugMsgTypes(ERROR | DEBUG); // set before init() so that you can see error messages

  sht.begin(SHT31_ADDRESS);
  sht.requestData();
  IPAddress local_IP(192, 168, 0, eachDeviceIP(meshSSID, ref)); // dynamic
  IPAddress gateway(192, 168, 0, 1);                            // should be constant
  IPAddress subnet(255, 255, 255, 0);                           // should be constant
  IPAddress primaryDNS(192, 168, 1, 1);                         // should be constant
  IPAddress secondaryDNS(192, 168, 0, 1);                       // should be constant
  // Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  // config wifi static ip

  // if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  // // if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  // {
  //   Serial.println("STA Failed to configure");
  // }
  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  {
    Serial.println("STA Failed to configure");
  }
  String hostname = "";
  hostname += meshSSID;
  hostname += "_";
  hostname += ref;
  // WiFi.setHostname(hostname.c_str()); // define hostname
  WiFi.begin(ssid, password);
  connectWifi();
  Serial.println("Connected to the WiFi network");
  printLocalTime();
}

void connectWifi()
{

  // use blink without delay concept
  if (WiFi.status() != WL_CONNECTED)
  {

    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis > intervalOneSecond)
    {
      previousMillis = currentMillis;

      if (ledState == LOW)
        ledState = HIGH;
      else
        ledState = LOW;
      digitalWrite(ledPin, ledState);
      Serial.print(".");
    }
    if ((WiFi.status() != WL_CONNECTED) && (currentMillis - reconnectMillis >= reconnectInterval))
    {
      // Serial.print(millis());
      Serial.println("Reconnecting to WiFi...");
      // WiFi.disconnect();
      // WiFi.reconnect();

      preferences.putBool("isMesh", true);
      ESP.restart();
      reconnectMillis = currentMillis;
    }
  }
}

int eachDeviceIP(const String &var, int ref)
{
  if (var == "DryerA1" || var == "DryerA1.")
    return ref + 0;

  if (var == "DryerA2" || var == "DryerA2.")
    return ref + 17;

  if (var == "DryerA3" || var == "DryerA3.")
    return ref + 34;
  if (var == "DryerA4" || var == "DryerA4.")
    return ref + 51;

  if (var == "DryerA5" || var == "DryerA5.")
    return ref + 68;

  if (var == "DryerA6" || var == "DryerA6.")
    return ref + 85;

  if (var == "DryerA7" || var == "DryerA7.")
    return ref + 102;

  if (var == "DryerA8" || var == "DryerA8.")
    return ref + 119;

  if (var == "DryerA9" || var == "DryerA9.")
    return ref + 136;

  if (var == "DryerA10" || var == "DryerA10.")
    return ref + 153;

  if (var == "DryerA11" || var == "DryerA11.")
    return ref + 170;

  if (var == "DryerA12" || var == "DryerA12.")
    return ref + 187;

  if (var == "DryerA13" || var == "DryerA13.")
    return ref + 204;

  if (var == "DryerA14" || var == "DryerA14.")
    return ref + 221;
}
void checkAPConnection()
{
  unsigned long previousMillis = 0;
  while ((WiFi.softAPgetStationNum() == 0))
  {
    // delay(500);

    // blink without delay code
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis > intervalOneSecond)
    {
      previousMillis = currentMillis;
      Serial.print(".");
      // if (ledState == LOW)
      //   // ledState = HIGH;

      //   Serial.print(".");
      // else
      //   // ledState = LOW;

      //   Serial.print("_");
      // digitalWrite(ledPin, ledState);
    }
    if (currentMillis > intervalOneMinute)
    {
      preferences.putBool("isMesh", true);
      ESP.restart();
    }
  }
}

String dryerProcessor(const String &var)
{
  if (var == "DryerA1")
    return DRYERA1;
  if (var == "DryerA1.")
    return DRYERA1_;

  if (var == "DryerA2")
    return DRYERA2;
  if (var == "DryerA2.")
    return DRYERA2_;

  if (var == "DryerA3")
    return DRYERA3;
  if (var == "DryerA3.")
    return DRYERA3_;

  if (var == "DryerA4")
    return DRYERA4;
  if (var == "DryerA4.")
    return DRYERA4_;

  if (var == "DryerA5")
    return DRYERA5;
  if (var == "DryerA5.")
    return DRYERA5_;

  if (var == "DryerA6")
    return DRYERA6;
  if (var == "DryerA6.")
    return DRYERA6_;

  if (var == "DryerA7")
    return DRYERA7;
  if (var == "DryerA7.")
    return DRYERA7_;

  if (var == "DryerA8")
    return DRYERA8;
  if (var == "DryerA8.")
    return DRYERA8_;

  if (var == "DryerA9")
    return DRYERA9;
  if (var == "DryerA9.")
    return DRYERA9_;

  if (var == "DryerA10")
    return DRYERA10;
  if (var == "DryerA10.")
    return DRYERA10_;

  if (var == "DryerA11")
    return DRYERA11;
  if (var == "DryerA11.")
    return DRYERA11_;

  if (var == "DryerA12")
    return DRYERA12;
  if (var == "DryerA12.")
    return DRYERA12_;

  if (var == "DryerA13")
    return DRYERA13;
  if (var == "DryerA13.")
    return DRYERA13_;

  if (var == "DryerA14")
    return DRYERA14;
  if (var == "DryerA14.")
    return DRYERA14_;
}
String processor(const String &var)
{
  // Serial.println(var);
  if (var == "BUTTONPLACEHOLDER")
  {
    String buttons = "";
    buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
    return buttons;
  }
  return String();
}

String outputState(int output)
{
  if (digitalRead(output))
  {
    return "checked";
  }
  else
  {
    return "";
  }
}
///////////////////////////////////////////////////////////////////////////// AP mode

// for (int i = 1; i <= 17; i++)
// {
//   if (ref == i)
//     return dryerA1[i - 1];
// }
