int NormalOpen = 18;  // NodeMCU at pin 18.
int NormalClose = 19; // NodeMCU at pin 19.

void SwitchOn(void)
{
    digitalWrite(NormalOpen, HIGH); // turn the switch on (HIGH is the voltage level)
    delay(2000);
    digitalWrite(NormalOpen, LOW); // turn the switch on (LOW is the voltage level)
}
void SwitchOff(void)
{
    digitalWrite(NormalClose, HIGH); // turn the switch on (HIGH is the voltage level)
    delay(2000);
    digitalWrite(NormalClose, LOW); // turn the switch on (LOW is the voltage level)
}

void ControlMode(void)
{

    // initialize digital pin as an output.
    pinMode(NormalOpen, OUTPUT);
    pinMode(NormalClose, OUTPUT);
}
