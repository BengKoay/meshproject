// Code explanation

// 1. read preferences for node, ssid, pass, isMesh
// 2. read AP or STA mode, isMESH true = mesh, isMESH false = AP
// 3. if STA mode, attempt connect Mesh Network
// 3.1. for 60 tries, will attempt connect Mesh Network, start counter "meshNotConnectedCounter"
// 3.1.1. if after 60 tries still unable to connect, will write isMESH false,
// 3.2. if between 60 tries , Mesh Network is connected, counter "meshNotConnectedCounter" resets
// 4. if AP mode, launch AP
// 4.1. if not connected for 60 seconds, will only set isMESH = true after reboot
// 4.2. will only set isMESH = true after reboot

// to work on:
// Done - set AP mode on purpose and not failover, during isMesh
// Done - set timeout after AP?
// progmem
// Done - asynctcp
//

// to do

// Done 1. LED light up when start
// Done 2. name change when updated, version-ing
// Done 3. name of sensor
// Done 4. name of dryer
// Done 5. AP mode for 3 min at start?, after 3 min reboot to Mesh

// naming - version_sensorname_dryername_MAC

// version
// 1.4 - using ref to change to int for backend
// 1.5 - setRoot
// 1.6 - broadcast mesh msg, every 5 sec

// // 3.2.2 - stable code
// // 3.3.0 - reboot mesh if not connected to rootID
// // 3.3.2 - extend time reboot 5 minutes mesh 3.3.0
// // 3.3.3 - bug fix AP
// // 3.3.4 - setRoot code
// // 3.3.5 - reboot mesh if no detect setRoot
#include <Arduino.h>
#include <pgmspace.h>

#include "secrets.h"
#include "SwitchControl.h"

// #include <SPI.h>
// #include <Ethernet.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

// #include <ESP8266WiFi.h>
// #include <ESP8266HTTPClient.h>
// #include <ESP8266WebServer.h>

#include <WiFi.h>
// #include <WebServer.h>
// #include <EEPROM.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
// #include <painlessMesh.h>
// #include "namedMesh.h"
// #include "SHT31.h"

#include <Preferences.h>

// define

// some gpio pin that is connected to an LED...
// on my rig, this is 5, change to the right number of your LED.
#define LED 2 // GPIO number of connected LED, ON ESP-12 IS GPIO2
// #define SHT31_ADDRESS 0x44 // define SHT address

#define BLINK_PERIOD 3000  // milliseconds until cycle repeat
#define BLINK_DURATION 100 // milliseconds LED is on for
// #define MESH_PORT 5555

// Variables
Preferences preferences;
int i = 0;
int statusCode;
// const char *ssid = "";       // not used?
// const char *passphrase = ""; // not used?
// String st;
// String content;
// String enodeName;
// String esid;
// String epass = "";

// variable for init
bool isMesh = false; // isMESH true = mesh, isMESH false = AP
// bool isMesh;
int meshNotConnectedCounter = 0;
int apNotConnectedCounter = 0;
int APcounter = 0;
// // unsigned long rootID = 3046393153; // A13
// unsigned long rootID = 3046322341; // A13.
// unsigned long rootID = 3046321129; // A11
// unsigned long rootID = 3046391301; // A11.

// variable for AP

String versionName = "S"; // version-ing
String meshSSID;
String meshPass;
String nodeName = "Switch"; // Name needs to be unique          //--------------------- root will recognize slave by its nodeName
int ref;
String uniq;

const char *PARAM_NODENAME = "param_nodename";
const char *PARAM_SSID = "param_ssid";
const char *PARAM_PASS = "param_pass";

const char *PARAM_INPUT_1 = "output";
const char *PARAM_INPUT_2 = "state";

// variable for mesh
// Scheduler userScheduler; // to control your personal task
// namedMesh mesh;
bool calc_delay = false;
// SimpleList<uint32_t> nodes;

// variable for sensor

// SHT31 sht; // declare sht variable

// maybe use blink without delay concept

const int ledPin = 2;
int ledState = LOW;

// declare currentMillis huge value to make sure wifi can connect
// unsigned long currentMillis = 10000;

// constants won't change:
// const long interval = 1000; // interval at which to blink (milliseconds)

const long intervalCheckMesh = 1000; // interval at which to blink (milliseconds)
const long intervalCheckAP = 60000;  // interval at which to blink (milliseconds)
const long intervalOneMinute = 60000;

unsigned long previous = 0;

// Function Declaration

void setupMesh();
// bool testWifi(void);
// void launchWeb(void);
// void setupAP(void);

// void createWebServer();

// prototype

String processor(const String &var);
String outputState(int output);
void APmode();

void checkAPConnection();

// Establishing Local server at port 80 whenever required
//  ESP8266WebServer server(80);
//  WebServer server(80);
AsyncWebServer server(80);

// Task to blink the number of nodes
bool onFlag = false;

// HTML

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px}
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.slider {background-color: #b30000}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>ESP Web Server</h2>
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/blink?output="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/blink?output="+element.id+"&state=0", true); }
  xhr.send();
}
</script>
</body>
<br>
<button onclick="location.href = '/update';" id="myButton" class="float-left submit-button" >update</button>
<br>
<button onclick="location.href = '/rebootEsp';" id="myButton" class="float-left submit-button" >rebootEsp</button>
<br>
<form action="/setMeshDataNameSSIDPass">
    param_nodename: <input type="text" name="param_nodename">

  <br>
    param_ssid: <input type="text" name="param_ssid">

  <br>
    param_pass: <input type="text" name="param_pass">
  <br>
    <input type="submit" value="Submit">
  </form>
</html>
)rawliteral";
////////////////////////////////////////////////////////// MQTT code
// define sensor input
#define RXD2 16 // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
#define TXD2 17 // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)

// // variable

const int MQTT_PORT = 8883;
const char MQTT_SUB_TOPIC[] = "iot/gabi_write/af4f32bb-5335-4cbd-97c8-324243e245e6";

// const char ssid[] = "WiFiSSID";
// const char pass[] = "WiFiPASS";
//  const char ssid[] = "+Solar_Project";
//  const char pass[] = "psp1234!@#$";
// const char ssid[] = "gabi";
// const char pass[] = "gabi9999";
const char ssid[] = "gabidecoA1";
const char pass[] = "plusnrg123";
// const char ssid[] = "gabidecoA2";
// const char pass[] = "plusnrg123";
// const char ssid[] = "Gabi2";
// const char pass[] = "plusnrg123";

// const char MQTT_HOST[] = "5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/devicePayload";
// const char MQTT_PUB_TOPIC[] = "publish/espmqtt";

// prototype

int ESPMaster = 2;
// 0 - A11
// 1 - A11.
// 2 - A5
// 3 - A5.
// 4 - A13
// 5 - A13.
// 6 - 5
// 7 - 5.
// 8 - 6
// 9 - 6.
// 10 - C10
// 11 - C10.
// 12 - Dryer A1
// 13 - Dryer A1.
// 14 - Dryer A2
// 15 - Dryer A2.
// 16 - Dryer A3
// 17 - Dryer A3.
// 18 - Dryer A4
// 19 - Dryer A4.
// 20 - Dryer A6
// 21 - Dryer A6.
// 22 - Dryer A7
// 23 - Dryer A7.
// 24 - Dryer A8
// 25 - Dryer A8.
// 26 - Dryer A9
// 27 - Dryer A9.
// 28 - Dryer A10
// 29 - Dryer A10.
// 30 - Dryer A12
// 31 - Dryer A12.
// 32 - Dryer A14
// 33 - Dryer A14.
int ESPSwitch = 14;
const char *host = "59";

char convertIntToConstChar[10] = ""; // 4294967296 is the maximum for Uint32, so 10 characters it is

/* ESP Control */
// #ifdef ESP32
// #include <WiFi.h>
// #elif defined(ESP8266)
// #include <ESP8266WiFi.h>
// #else
// #error Platform not supported
// #endif
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson (use v6.xx)
#include <time.h>
#define emptyString String()

/* Web Server */
// #include <WebServer.h>
// #include <ESPmDNS.h>
// #include <HTTPUpdateServer.h>

// Follow instructions from https://github.com/debsahu/ESP-MQTT-AWS-IoT-Core/blob/master/doc/README.md
// Enter values in secrets.h ▼
//  #include "secrets.h"

#if !(ARDUINOJSON_VERSION_MAJOR == 6 and ARDUINOJSON_VERSION_MINOR >= 7)
#error "Install ArduinoJson v6.7.0-beta or higher"
#endif

// static IP configuration
// IPAddress local_IP(192, 168, 68, (ESPSwitch + 100));
// IPAddress gateway(192, 168, 68, 1);
// IPAddress subnet(255, 255, 255, 0);
// IPAddress primaryDNS(192, 168, 0, 197); //optional
// IPAddress secondaryDNS(8, 8, 4, 4);     //optional
// IPAddress local_IP(192, 168, 50, (ESPSwitch + 100));
// IPAddress gateway(192, 168, 50, 254);
// IPAddress subnet(255, 255, 255, 0);
// IPAddress primaryDNS(8, 8, 8, 8);   //optional
// IPAddress secondaryDNS(8, 8, 4, 4); //optional
// const int MQTT_PORT = 8883;
// const char MQTT_SUB_TOPIC[] = "$aws/things/" THINGNAME "/shadow/update";
// const char MQTT_PUB_TOPIC[] = "$aws/things/" THINGNAME "/shadow/update";
// const char MQTT_SUB_TOPIC[] = "subscribe/espmqtt";
// const char MQTT_PUB_TOPIC[] = "publish/espmqtt";

#ifdef USE_SUMMER_TIME_DST
uint8_t DST = 1;
#else
uint8_t DST = 0;
#endif

WiFiClientSecure net;

#ifdef ESP8266
BearSSL::X509List cert(cacert);
BearSSL::X509List client_crt(client_cert[ESPSwitch]);
BearSSL::PrivateKey key(privkey[ESPSwitch]);
#endif

PubSubClient client(net);
// WebServer httpServer(80);
// HTTPUpdateServer httpUpdater;

unsigned long lastMillis = 0;
unsigned long sendDataInterval = 60 * 1000;

unsigned long previousMillis = 0;
unsigned long interval = 30000;
time_t now;
time_t nowish = 1510592825;

void NTPConnect(void)
{
  Serial.print("Setting time using SNTP");
  // configTime(TIME_ZONE * 3600, DST * 3600, "pool.ntp.org", "time.nist.gov");

  // NTP server to request epoch time
  const char *ntpServer = "pool.ntp.org";
  configTime(0, 0, ntpServer);
  now = time(nullptr);
  while (now < nowish)
  {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  Serial.println();
  Serial.println("Done!");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.print(asctime(&timeinfo));
}

/*---------Function for Time----------------*/
unsigned long printLocalTime()
{
  // NTP server to request epoch time
  const char *ntpServer = "pool.ntp.org";
  configTime(0, 0, ntpServer);
  struct tm timeinfo;
  time_t now;
  if (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
    return (0);
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  time(&now);
  Serial.println(now);
  return now;
}
/*-----(END)----Function for Time----------------*/

void messageReceived(char *topic, byte *payload, unsigned int length)
{

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")");
  Serial.print("Received [");
  Serial.print(topic);
  Serial.println("]");
  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")");

  StaticJsonDocument<256> doc;
  DeserializationError error = deserializeJson(doc, payload, length);

  // Test if parsing succeeds.
  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }

  // const char *companyId = doc["companyId"];
  // const char *locationId = doc["locationId"];
  // const char *lotId = doc["lotId"];
  // const char *smartInverterId = doc["smartInverterId"];
  const char *operationAction = doc["operationAction"];
  // const char *controlDt = doc["controlDt"];
  // const char *logId = doc["logId"];
  const char *smartInverterId = doc["switchId"];
  Serial.println();

  // Serial.print("companyId: ");
  // Serial.println(companyId);
  // Serial.print("locationId: ");
  // Serial.println(locationId);
  // Serial.print("lotId: ");
  // Serial.println(lotId);
  Serial.print("smartInverterId: ");
  Serial.println(smartInverterId);
  Serial.print("operationAction: ");
  Serial.println(operationAction);
  // Serial.print("controlDt: ");
  // Serial.println(controlDt);
  // Serial.print("logId: ");
  // Serial.println(logId);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")");
  if (strcmp_P(smartInverterId, smartInverterIdMem[ESPSwitch - 1]) == 0)
  {
    Serial.println("Ok, smartInverterId is the same");
    if (strcmp_P(operationAction, OperationOn) == 0)
    {
      SwitchOn();
      // sendData();
      // send feedback
    }
    if (strcmp_P(operationAction, OperationOff) == 0)
    {
      SwitchOff();
      // sendData();
      // send feedback
    }
  }

  Serial.println();
}

void pubSubErr(int8_t MQTTErr)
{
  if (MQTTErr == MQTT_CONNECTION_TIMEOUT)
    Serial.print("Connection timeout");
  else if (MQTTErr == MQTT_CONNECTION_LOST)
    Serial.print("Connection lost");
  else if (MQTTErr == MQTT_CONNECT_FAILED)
    Serial.print("Connect failed");
  else if (MQTTErr == MQTT_DISCONNECTED)
    Serial.print("Disconnected");
  else if (MQTTErr == MQTT_CONNECTED)
    Serial.print("Connected");
  else if (MQTTErr == MQTT_CONNECT_BAD_PROTOCOL)
    Serial.print("Connect bad protocol");
  else if (MQTTErr == MQTT_CONNECT_BAD_CLIENT_ID)
    Serial.print("Connect bad Client-ID");
  else if (MQTTErr == MQTT_CONNECT_UNAVAILABLE)
    Serial.print("Connect unavailable");
  else if (MQTTErr == MQTT_CONNECT_BAD_CREDENTIALS)
    Serial.print("Connect bad credentials");
  else if (MQTTErr == MQTT_CONNECT_UNAUTHORIZED)
    Serial.print("Connect unauthorized");
}

void connectToMqtt(bool nonBlocking = false)
{
  Serial.print("MQTT connecting to: ");
  Serial.println(THINGNAME[ESPSwitch - 1]);
  // Serial.println(DEV_UUID[ESPMaster]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")");
  while (!client.connected())
  {
    if (client.connect(THINGNAME[ESPSwitch - 1]))
    {
      Serial.print("ESP.getFreeHeap(");
      Serial.print(ESP.getFreeHeap());
      Serial.println(")client.connect(THINGNAME)");
      Serial.println("MQTT Connected!");
      if (!client.subscribe(MQTT_SUB_TOPIC))
        pubSubErr(client.state());

      Serial.print("ESP.getFreeHeap(");
      Serial.print(ESP.getFreeHeap());
      Serial.println(")client.subscribe(MQTT_SUB_TOPIC)");
    }
    else
    {
      Serial.print("Failed, reason -> ");
      pubSubErr(client.state());
      if (!nonBlocking)
      {
        Serial.println(" < Try again in 5 seconds");
        delay(5000);
      }
      else
      {
        Serial.println(" <");
      }
    }
    if (nonBlocking)
      break;
  }
}

void connectToWiFi(String init_str)
{
  if (init_str != emptyString)
    Serial.print(init_str);
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(1000);
    unsigned long currentMillis = millis();
    // if WiFi is down, try reconnecting every CHECK_WIFI_TIME seconds
    if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >= interval))
    {
      Serial.print(millis());
      Serial.println("Reconnecting to WiFi...");
      WiFi.disconnect();
      WiFi.reconnect();
      previousMillis = currentMillis;
    }
  }
  if (init_str != emptyString)
    Serial.println();
  Serial.println("Connected!");
  Serial.println("");
  Serial.println("WiFi connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("ESP Mac Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Subnet Mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP: ");
  Serial.println(WiFi.gatewayIP());
  Serial.print("DNS: ");
  Serial.println(WiFi.dnsIP());
}

void checkWiFiThenMQTT(void)
{
  connectToWiFi("Checking WiFi");
  connectToMqtt();
}

void checkWiFiThenMQTTNonBlocking(void)
{
  unsigned long previousMillis = 0;
  const long interval = 5000;
  connectToWiFi(emptyString);
  if (millis() - previousMillis >= interval && !client.connected())
  {
    previousMillis = millis();
    connectToMqtt(true);
  }
}

void checkWiFiThenReboot(void)
{
  connectToWiFi("Checking WiFi");
  Serial.print("Rebooting");
  ESP.restart();
}

////////////////////////////////////////////////////////// MQTT code

void MQTTSetup()
{
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  preferences.putBool("isMesh", false); // set to AP mode after enter MQTT
  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")");
  delay(5000);
  Serial.println();

#ifdef ESP32
  // if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  // {
  //   Serial.println("STA Failed to configure");
  // }

  // WiFi.setHostname(THINGNAME[ESPSwitch]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")WiFi.setHostname");
#else
  WiFi.hostname(THINGNAME[ESPSwitch]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")WiFi.hostname");
#endif
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  connectToWiFi(String("Attempting to connect to SSID: ") + String(ssid));

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")WiFi.hostname");

  NTPConnect();
  client.setBufferSize(4096);

  // sprintf(convertIntToConstChar, "%d", (long)(ESPSwitch + 100)); // where 'myUint32' is your Uint32 variable
  // my_function((const char*)convertIntToConstChar); // where 'my_function' is your function requiring a const char*
  // MDNS.begin((const char *)convertIntToConstChar);
  // if (MDNS.begin("host")) {
  //   Serial.println("mDNS responder started");
  //}
  // httpUpdater.setup(&httpServer);
  // httpServer.begin();
  // MDNS.addService("http", "tcp", 80);
  // Serial.printf("HTTPUpdateServer ready! Open -     http://%s.local/update     - in your browser\n", convertIntToConstChar);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")NTPConnect()");

#ifdef ESP32
  net.setCACert(cacert);
  net.setCertificate(client_cert[ESPSwitch - 1]);
  net.setPrivateKey(privkey[ESPSwitch - 1]);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")net.setCACert");
#else
  net.setTrustAnchors(&cert);
  net.setClientRSACert(&client_crt, &key);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")net.setTrustAnchors");
#endif

  client.setServer(MQTT_HOST, MQTT_PORT);
  client.setCallback(messageReceived);

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")client.setServer");

  connectToMqtt();

  Serial.print("ESP.getFreeHeap(");
  Serial.print(ESP.getFreeHeap());
  Serial.println(")connectToMqtt");
  ControlMode();
}
void MQTTLoop()
{

  // httpServer.handleClient();
  unsigned long currentMillis = millis();
  // if WiFi is down, try reconnecting every CHECK_WIFI_TIME seconds
  if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >= interval))
  {
    Serial.print(millis());
    Serial.println("Reconnecting to WiFi...");
    // WiFi.disconnect();
    // WiFi.reconnect();
    preferences.putBool("isMesh", true);
    ESP.restart();
    previousMillis = currentMillis;
  }
  {
    now = time(nullptr);
    if (!client.connected())
    {
      checkWiFiThenMQTT();

      Serial.println("ESP.getFreeHeap()checkWiFiThenMQTT()");
      Serial.print("ESP.getFreeHeap(");
      Serial.print(ESP.getFreeHeap());
      Serial.println(")");
      // checkWiFiThenMQTTNonBlocking();
      // checkWiFiThenReboot();
    }
    else
    {
      client.loop();
    }
  }

  // set reboot ESP after 1 hour using millis

  unsigned long rebootInterval = 60 * 60 * 1000; // hour * minute * second
  unsigned long rebootMillis = millis();
  if (rebootMillis > rebootInterval)
  {
    ESP.restart();
  }
}

void setup()
{
  // 1. read EEPROM for node, ssid, pass
  // 2. read AP or STA mode
  // 3. if STA mode, attempt connect wifi
  // 4. if AP mode, launch AP

  // read EEPROM
  Serial.begin(115200); // Initialising if(DEBUG)Serial Monitor
  Serial.println();

  //---------------------------------------- Read preferences for ssid and pass
  preferences.begin("credentials", false);

  ref = preferences.getInt("nodeName", 0);
  ESPSwitch = ref;

  meshSSID = preferences.getString("meshSSID", "");
  meshPass = preferences.getString("meshPass", "");
  isMesh = preferences.getBool("isMesh", "");

  if (meshSSID == "" || meshPass == "")
  {
    Serial.println("No values saved for ssid or password");
  }
  else
  {
    Serial.println("Print  ssid or password");
    Serial.println(meshSSID);
    Serial.println(meshPass);
    // Serial.println(ref);
  }

  //---------------------------------------- End Read preferences for ssid and pass

  //---------------------------------------- Init wifi
  if (isMesh)
  {
    MQTTSetup();
    // sht.begin(SHT31_ADDRESS);
    // sht.requestData();
    // pinMode(LED, OUTPUT);
    // setupMesh();
    // Serial.print("setupMesh mesh");
  }
  else
  {
    APmode();
  }
}

void loop()
{

  if (isMesh)
  {
    MQTTLoop();
    // mesh.update();
    // digitalWrite(LED, !onFlag);

    // getRootIDLoop();
  }
  else
  {
    checkAPConnection();

    AsyncElegantOTA.loop();
  }
}

void APmode()
{
  //---------------------------------------- Set preferences for AP

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH); // light up when start
  byte mac[6];
  WiFi.macAddress(mac);
  uniq += versionName;
  uniq += "_";
  uniq += ref;
  uniq += "_";
  uniq += meshSSID;
  uniq += "_";
  uniq += String(mac[0], HEX);
  uniq += String(mac[1], HEX);
  uniq += String(mac[2], HEX);
  uniq += String(mac[3], HEX);
  uniq += String(mac[4], HEX);
  uniq += String(mac[5], HEX);
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(ssid, password);
  WiFi.softAP(uniq.c_str(), "");
  Serial.println("");

  //---------------------------------------- End Set preferences for AP

  // Wait for connection
  // while (WiFi.status() != WL_CONNECTED)
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(meshSSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });

  // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
  server.on("/blink", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage1;
              String inputMessage2;
              // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
              if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
              {
                inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
                inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
                digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
              }
              else
              {
                inputMessage1 = "No message sent";
                inputMessage2 = "No message sent";
              }
              Serial.print("GPIO: ");
              Serial.print(inputMessage1);
              Serial.print(" - Set to: ");
              Serial.println(inputMessage2);
              request->send(200, "text/plain", "OK"); });

  // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
  server.on("/setMeshDataNameSSIDPass", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage;
              String inputParam;
              // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
              if (request->hasParam(PARAM_NODENAME))
              {
                inputMessage = request->getParam(PARAM_NODENAME)->value();
                inputParam = PARAM_NODENAME;
              }
              // GET input2 value on <ESP_IP>/get?input2=<inputMessage>
              else if (request->hasParam(PARAM_SSID))
              {
                inputMessage = request->getParam(PARAM_SSID)->value();
                inputParam = PARAM_SSID;
              }
              // GET input3 value on <ESP_IP>/get?input3=<inputMessage>
              else if (request->hasParam(PARAM_PASS))
              {
                inputMessage = request->getParam(PARAM_PASS)->value();
                inputParam = PARAM_PASS;
              }
              else
              {
                inputMessage = "No message sent";
                inputParam = "none";
              }

              if (request->hasParam(PARAM_NODENAME))
              {
                // int refInt = ;
                preferences.putInt("nodeName", request->getParam(PARAM_NODENAME)->value().toInt());
              }
              if (request->hasParam(PARAM_SSID))
                preferences.putString("meshSSID", request->getParam(PARAM_SSID)->value());
              if (request->hasParam(PARAM_PASS))
                preferences.putString("meshPass", request->getParam(PARAM_PASS)->value());

              // preferences.end();
              Serial.println(inputMessage);
              request->send(200, "text/plain", "OK"); });
  // request->send(200, "text/html", "HTTP GET request sent to your ESP on input field (" + inputParam + ") with value: " + inputMessage + "<br><a href=\"/\">Return to Home Page</a>"); });
  server.on("/redirect/internal", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->redirect("/"); });
  server.on("/rebootEsp", HTTP_GET, [](AsyncWebServerRequest *request)
            { ESP.restart(); });

  AsyncElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");
}

void checkAPConnection()
{
  unsigned long previousMillis = 0;
  unsigned long interval = 1000;
  while ((WiFi.softAPgetStationNum() == 0))
  {
    // delay(500);

    // blink without delay code
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis > interval)
    {
      previousMillis = currentMillis;
      Serial.print(".");
      // if (ledState == LOW)
      //   // ledState = HIGH;

      //   Serial.print(".");
      // else
      //   // ledState = LOW;

      //   Serial.print("_");
      // digitalWrite(ledPin, ledState);
    }
    if (currentMillis > intervalOneMinute)
    {
      preferences.putBool("isMesh", true);
      ESP.restart();
    }
  }
}

String processor(const String &var)
{
  // Serial.println(var);
  if (var == "BUTTONPLACEHOLDER")
  {
    String buttons = "";
    buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
    return buttons;
  }
  return String();
}

String outputState(int output)
{
  if (digitalRead(output))
  {
    return "checked";
  }
  else
  {
    return "";
  }
}