#include <Arduino.h>
#include <pgmspace.h>

// // UUID
static const char *const DRYERA1 PROGMEM = "b5153d6f-8bda-4004-a7b0-9e403cf5f333";   // Dryer A1
static const char *const DRYERA1_ PROGMEM = "107e369a-9f82-43c1-ad12-9b91bdee917a";  // Dryer A1.
static const char *const DRYERA2 PROGMEM = "eefd24e6-53ac-404c-a6c7-93ebf3ac34fe";   // Dryer A2
static const char *const DRYERA2_ PROGMEM = "571ebb72-a9d4-4de6-be6a-2d0deb7a6a61";  // Dryer A2.
static const char *const DRYERA3 PROGMEM = "2993dc00-1f84-48a6-b30d-1ce4f3f43445";   // Dryer A3
static const char *const DRYERA3_ PROGMEM = "c17ac968-106b-493a-b1e9-6796a7a4a0e8";  // Dryer A3.
static const char *const DRYERA4 PROGMEM = "116e9fe0-6e05-4e49-a18e-191834dbc56c";   // Dryer A4
static const char *const DRYERA4_ PROGMEM = "f750df7e-15a6-4244-868e-843454a52a61";  // Dryer A4.
static const char *const DRYERA5 PROGMEM = "b1d49ddf-6a7b-424d-b202-9b3715b5c74a";   // Dryer A5
static const char *const DRYERA5_ PROGMEM = "f827b10c-d463-4ae4-b2f1-ea890480b065";  // Dryer A5.
static const char *const DRYERA6 PROGMEM = "81ae2ff4-8f6b-4c26-b88b-24163c422917";   // Dryer A6
static const char *const DRYERA6_ PROGMEM = "01ab3267-b70c-49d3-a7aa-e7c22f044dee";  // Dryer A6.
static const char *const DRYERA7 PROGMEM = "0c141b4b-cf35-4f01-aa52-c92364529afd";   // Dryer A7
static const char *const DRYERA7_ PROGMEM = "035342a4-5414-4ae0-998b-99dd907b070b";  // Dryer A7.
static const char *const DRYERA8 PROGMEM = "4ca3edf9-c00f-4490-b2b0-c05a842609ca";   // Dryer A8
static const char *const DRYERA8_ PROGMEM = "c6b30a05-f792-4795-8b93-4c13ba50f17e";  // Dryer A8.
static const char *const DRYERA9 PROGMEM = "0ffae8d7-6242-4585-90f1-73b03d9345f4";   // Dryer A9
static const char *const DRYERA9_ PROGMEM = "471f067b-aff6-40ed-9629-32a19560c16e";  // Dryer A9.
static const char *const DRYERA10 PROGMEM = "fe6f153b-b6f5-4c18-b30e-0934150b2249";  // Dryer A10
static const char *const DRYERA10_ PROGMEM = "293b0f25-be3c-488b-b93b-8e5b4ba4b9d2"; // Dryer A10.
static const char *const DRYERA11 PROGMEM = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";  // Dryer A11
static const char *const DRYERA11_ PROGMEM = "2b74dc15-d8bf-467d-b15d-e25c3dff260b"; // Dryer A11.
static const char *const DRYERA12 PROGMEM = "79f1fa42-ed76-4330-a026-bebf9171de78";  // Dryer A12
static const char *const DRYERA12_ PROGMEM = "fc474bb1-15ab-4bbe-9f4c-cd785cbda1f6"; // Dryer A12.
static const char *const DRYERA13 PROGMEM = "517f1b72-0ea3-4a53-a915-ae8dca1568de";  // Dryer A13
static const char *const DRYERA13_ PROGMEM = "9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f"; // Dryer A13.
static const char *const DRYERA14 PROGMEM = "fd2de1c6-3f89-4965-af03-fed5959ca716";  // Dryer A14
static const char *const DRYERA14_ PROGMEM = "e722e20e-1b44-4fd4-bd35-57c425d91ce7"; // Dryer A14.

// // // DRYER A1 UUID 1 - 17
// static const char *const DRYERA1ID1 PROGMEM = "4a00c883-a252-4760-86c6-03b62048e667"; // Single Device A1 S1
// static const char *const DRYERA1ID2 PROGMEM = "cc6ebb2f-4693-4bf7-b5f3-59c422656a15"; // Single Device A1 S2
// static const char *const DRYERA1ID3 PROGMEM = "";                                     // Single Device A1 S3
// static const char *const DRYERA1ID4 PROGMEM = "";                                     // Single Device A1 S4
// static const char *const DRYERA1ID5 PROGMEM = "";                                     // Single Device A1 S5
// static const char *const DRYERA1ID6 PROGMEM = "";                                     // Single Device A1 S6
// static const char *const DRYERA1ID7 PROGMEM = "";                                     // Single Device A1 S7
// static const char *const DRYERA1ID8 PROGMEM = "";                                     // Single Device A1 S8
// static const char *const DRYERA1ID9 PROGMEM = "";                                     // Single Device A1 S9
// static const char *const DRYERA1ID10 PROGMEM = "";                                    // Single Device A1 S10
// static const char *const DRYERA1ID11 PROGMEM = "";                                    // Single Device A1 S11
// static const char *const DRYERA1ID12 PROGMEM = "";                                    // Single Device A1 S12
// static const char *const DRYERA1ID13 PROGMEM = "";                                    // Single Device A1 S13
// static const char *const DRYERA1ID14 PROGMEM = "";                                    // Single Device A1 S14
// static const char *const DRYERA1ID15 PROGMEM = "";                                    // Single Device A1 S15
// static const char *const DRYERA1ID16 PROGMEM = "";                                    // Single Device A1 S16
// static const char *const DRYERA1ID17 PROGMEM = "";                                    // Single Device A1 S17
// // // DRYER A2 UUID 1 - 17
// static const char *const DRYERA2ID1 PROGMEM = "";  // Single Device A2 S1
// static const char *const DRYERA2ID2 PROGMEM = "";  // Single Device A2 S2
// static const char *const DRYERA2ID3 PROGMEM = "";  // Single Device A2 S3
// static const char *const DRYERA2ID4 PROGMEM = "";  // Single Device A2 S4
// static const char *const DRYERA2ID5 PROGMEM = "";  // Single Device A2 S5
// static const char *const DRYERA2ID6 PROGMEM = "";  // Single Device A2 S6
// static const char *const DRYERA2ID7 PROGMEM = "";  // Single Device A2 S7
// static const char *const DRYERA2ID8 PROGMEM = "";  // Single Device A2 S8
// static const char *const DRYERA2ID9 PROGMEM = "";  // Single Device A2 S9
// static const char *const DRYERA2ID10 PROGMEM = ""; // Single Device A2 S10
// static const char *const DRYERA2ID11 PROGMEM = ""; // Single Device A2 S11
// static const char *const DRYERA2ID12 PROGMEM = ""; // Single Device A2 S12
// static const char *const DRYERA2ID13 PROGMEM = ""; // Single Device A2 S13
// static const char *const DRYERA2ID14 PROGMEM = ""; // Single Device A2 S14
// static const char *const DRYERA2ID15 PROGMEM = ""; // Single Device A2 S15
// static const char *const DRYERA2ID16 PROGMEM = ""; // Single Device A2 S16
// static const char *const DRYERA2ID17 PROGMEM = ""; // Single Device A2 S17
// // // DRYER A3 UUID 1 - 17
// // static const char *const DRYERA3ID1 PROGMEM = "";  // Single Device A3 S1
// // static const char *const DRYERA3ID2 PROGMEM = "";  // Single Device A3 S2
// // static const char *const DRYERA3ID3 PROGMEM = "";  // Single Device A3 S3
// // static const char *const DRYERA3ID4 PROGMEM = "";  // Single Device A3 S4
// // static const char *const DRYERA3ID5 PROGMEM = "";  // Single Device A3 S5
// // static const char *const DRYERA3ID6 PROGMEM = "";  // Single Device A3 S6
// // static const char *const DRYERA3ID7 PROGMEM = "";  // Single Device A3 S7
// // static const char *const DRYERA3ID8 PROGMEM = "";  // Single Device A3 S8
// // static const char *const DRYERA3ID9 PROGMEM = "";  // Single Device A3 S9
// // static const char *const DRYERA3ID10 PROGMEM = ""; // Single Device A3 S10
// // static const char *const DRYERA3ID11 PROGMEM = ""; // Single Device A3 S11
// // static const char *const DRYERA3ID12 PROGMEM = ""; // Single Device A3 S12
// // static const char *const DRYERA3ID13 PROGMEM = ""; // Single Device A3 S13
// // static const char *const DRYERA3ID14 PROGMEM = ""; // Single Device A3 S14
// // static const char *const DRYERA3ID15 PROGMEM = ""; // Single Device A3 S15
// // static const char *const DRYERA3ID16 PROGMEM = ""; // Single Device A3 S16
// // static const char *const DRYERA3ID17 PROGMEM = ""; // Single Device A3 S17
// // // DRYER A4 UUID 1 - 17
// // static const char *const DRYERA4ID1 PROGMEM = "";  // Single Device A4 S1
// // static const char *const DRYERA4ID2 PROGMEM = "";  // Single Device A4 S2
// // static const char *const DRYERA4ID3 PROGMEM = "";  // Single Device A4 S3
// // static const char *const DRYERA4ID4 PROGMEM = "";  // Single Device A4 S4
// // static const char *const DRYERA4ID5 PROGMEM = "";  // Single Device A4 S5
// // static const char *const DRYERA4ID6 PROGMEM = "";  // Single Device A4 S6
// // static const char *const DRYERA4ID7 PROGMEM = "";  // Single Device A4 S7
// // static const char *const DRYERA4ID8 PROGMEM = "";  // Single Device A4 S8
// // static const char *const DRYERA4ID9 PROGMEM = "";  // Single Device A4 S9
// // static const char *const DRYERA4ID10 PROGMEM = ""; // Single Device A4 S10
// // static const char *const DRYERA4ID11 PROGMEM = ""; // Single Device A4 S11
// // static const char *const DRYERA4ID12 PROGMEM = ""; // Single Device A4 S12
// // static const char *const DRYERA4ID13 PROGMEM = ""; // Single Device A4 S13
// // static const char *const DRYERA4ID14 PROGMEM = ""; // Single Device A4 S14
// // static const char *const DRYERA4ID15 PROGMEM = ""; // Single Device A4 S15
// // static const char *const DRYERA4ID16 PROGMEM = ""; // Single Device A4 S16
// // static const char *const DRYERA4ID17 PROGMEM = ""; // Single Device A4 S17

// // // DRYER A5 UUID 1 - 17
// static const char *const DRYERA5ID1 PROGMEM = "160574b8-f610-4acb-96cb-aeb32a869d05";  // Single Device A5 S1
// static const char *const DRYERA5ID2 PROGMEM = "f58443b0-e5fa-418a-89cc-fb45fb423e55";  // Single Device A5 S2
// static const char *const DRYERA5ID3 PROGMEM = "10c8c8ae-7857-4c89-a223-d0b0704a718c";  // Single Device A5 S3
// static const char *const DRYERA5ID4 PROGMEM = "86a09ccb-00eb-474b-949c-6d1932355f32";  // Single Device A5 S4
// static const char *const DRYERA5ID5 PROGMEM = "0f2b0d33-3cce-42d5-8b55-da93b8f9bccb";  // Single Device A5 S5
// static const char *const DRYERA5ID6 PROGMEM = "e00fa00c-a2d8-4ce0-a573-1619701d66c4";  // Single Device A5 S6
// static const char *const DRYERA5ID7 PROGMEM = "6e54a473-d6ae-4f2a-a0b0-bba46ce55fc4";  // Single Device A5 S7
// static const char *const DRYERA5ID8 PROGMEM = "b5661925-eeb6-455d-8497-5aaa112eb32b";  // Single Device A5 S8
// static const char *const DRYERA5ID9 PROGMEM = "d32569a7-70e2-4713-a2b2-56d287eb6ef1";  // Single Device A5 S9
// static const char *const DRYERA5ID10 PROGMEM = "9fbb3768-2efa-4b94-8328-716658e04275"; // Single Device A5 S10
// static const char *const DRYERA5ID11 PROGMEM = "de3ea239-a465-4286-bab2-f67468ffa013"; // Single Device A5 S11
// static const char *const DRYERA5ID12 PROGMEM = "b998220f-1878-4ec3-87c0-a43fe0aa5d26"; // Single Device A5 S12
// static const char *const DRYERA5ID13 PROGMEM = "d200f967-9eed-45c0-9f0d-7d39ac4796d8"; // Single Device A5 S13
// static const char *const DRYERA5ID14 PROGMEM = "c5a1cfdc-7119-409e-9e98-fea679d195fc"; // Single Device A5 S14
// static const char *const DRYERA5ID15 PROGMEM = "86f3d686-57f2-4d5e-84dd-9974b7391384"; // Single Device A5 S15
// static const char *const DRYERA5ID16 PROGMEM = "396e34b3-be2f-45f5-8706-ceb99b094449"; // Single Device A5 S16
// static const char *const DRYERA5ID17 PROGMEM = "aef53e6b-9bc5-439a-8279-a25d47519e4f"; // Single Device A5 S17

// // // DRYER A6 UUID 1 - 17
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3
// // static const char *const DRYERA6ID1 PROGMEM = ""; // Single Device A6 S1
// // static const char *const DRYERA6ID2 PROGMEM = ""; // Single Device A6 S2
// // static const char *const DRYERA6ID3 PROGMEM = ""; // Single Device A6 S3

// const char *const dryerA1[] PROGMEM = {DRYERA1ID1, DRYERA1ID2, DRYERA1ID3, DRYERA1ID4, DRYERA1ID5, DRYERA1ID6, DRYERA1ID7, DRYERA1ID8, DRYERA1ID9, DRYERA1ID10, DRYERA1ID11, DRYERA1ID12, DRYERA1ID13, DRYERA1ID14, DRYERA1ID15, DRYERA1ID16, DRYERA1ID17};
// const char *const dryerA2[] PROGMEM = {DRYERA2ID1, DRYERA2ID2, DRYERA2ID3, DRYERA2ID4, DRYERA2ID5, DRYERA2ID6, DRYERA2ID7, DRYERA2ID8, DRYERA2ID9, DRYERA2ID10, DRYERA2ID11, DRYERA2ID12, DRYERA2ID13, DRYERA2ID14, DRYERA2ID15, DRYERA2ID16, DRYERA2ID17};
// const char *const dryerA3[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA4[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA5[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA6[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA7[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA8[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA9[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA10[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA11[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA12[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA13[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
// const char *const dryerA14[] PROGMEM = {DRYERA5ID1, DRYERA5ID2, DRYERA5ID3, DRYERA5ID4, DRYERA5ID5, DRYERA5ID6, DRYERA5ID7, DRYERA5ID8, DRYERA5ID9, DRYERA5ID10, DRYERA5ID11, DRYERA5ID12, DRYERA5ID13, DRYERA5ID14, DRYERA5ID15, DRYERA5ID16, DRYERA5ID17};
