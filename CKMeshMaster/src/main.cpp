#include <WiFi.h>
// #include <ESP32Ping.h>
#include <HTTPClient.h>
#include "time.h"
#include <ArduinoJson.h>
#include <sha256.h>
#include <ESPRandom.h>

// const char *ssid = "25.house@MaxisFIbre";
// const char *password = "80118050";

//const char* ssid = "Minimalist-2.4Ghz";
//const char* password = "lan10052019";

const char *ssid = "Gabi2.4G";
const char *password = "plusnrg123";

//const char* ssid = "gabi";
//const char* password = "gabi9999";

const char *NTP_SERVER = "ch.pool.ntp.org";
const char *TZ_INFO = "SGT-8"; // enter your time zone (https://remotemonitoringsystems.ca/time-zone-abbreviations.php)

tm timeinfo;
time_t now;
long unsigned lastNTPtime;
unsigned long lastEntryTime;

// AWS database path
//const char* serverName = "https://50ioojd4va.execute-api.ap-southeast-1.amazonaws.com/prod/reading";
//const char* serverName = "https://50ioojd4va.execute-api.amoip-southeast-1.amazonaws.com/prod/SmartSensorReading";
const char *serverName = "https://50ioojd4va.execute-api.ap-southeast-1.amazonaws.com/prod/SmartSensorReading";

int httpResponseCode;
HTTPClient http;

int counter = 0;
int count = 0;
int internetCounter = 0;

float mois;
float id;
long timestamp;

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTime = 0;
unsigned long retryTime = 0;
// Timer set to 10 minutes (600000)
//unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
unsigned long timerDelay = 10000; // push interval
unsigned long retryDelay = 1000;  //

IPAddress ip(192, 168, 50, 99); // Sensor static IP
IPAddress dns(8, 8, 8, 8);
IPAddress gateway(192, 168, 50, 254);
IPAddress subnet(255, 255, 255, 0);

#define RXD2 16
#define TXD2 17

int moist_buf[18] = {0};
int temp_buf[18] = {0};
int humi_buf[18] = {0};
long timestamp_buf[16] = {0}; //might not need
int slaveId_buf[16] = {0};    //might not needed
DynamicJsonDocument newdoc(1024);

bool aws_pushflag = false;
int aws_pushctr = 0;
int aws_pushmaxretry = 5;

// AWS signature

// AWS sign
// TODO: put in preferences
const char *host = "9ry30zin92";
const char *service = "execute-api";
const char *region = "ap-southeast-1";
const char *TLD = "amazonaws.com";
const char *path = "/public/rawtmpdata";

const char *customFQDN; // reserved for future use

// AWS IAM configuration
// TODO: put in preferences
const char *awsKey = "AKIAVBSYQGXZE6POVWVK";
const char *awsSecret = "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";
const char *apiKey = "eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB";

// protoype

bool sendData(JsonDocument &local_doc);
String createRequest(String method, String uri, String payload, String apiKey,
                     String contentType, String queryString);
String hexHash(uint8_t *hash);
String createCanonicalRequest(String method, String uri, String date,
                              String time, String payloadHash, String apiKey,
                              String queryString, String contentType);
String createCanonicalHeaders(String contentType, String date, String time,
                              String payloadHash, String apiKey);
String createRequestHeaders(String contentType, String date, String time,
                            String payload, String payloadHash, String apiKey,
                            String signature);
String FQDN();
String createStringToSign(String canonical_request, String date, String time);
String createSignature(String toSign, String date);

void connectWifi();

bool getNTPtime(int sec);
void showTime(tm localTime);
float calculate_humidex(float t, float h);
void showTime(tm localTime);
void GetDataFromSerial2();
void prepareData();
void AWS_Push_handle();

void setup()
{
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);

  connectWifi();

  // NTP server retrieve time
  configTime(0, 0, NTP_SERVER);
  // See https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv for Timezone codes for your region
  setenv("TZ", TZ_INFO, 1);

  if (getNTPtime(10))
  { // wait up to 10sec to sync
  }
  else
  {
    Serial.println("Time not set");
    ESP.restart();
  }
  showTime(timeinfo);
  lastNTPtime = time(&now);
  lastEntryTime = millis();

  Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");
}

void loop()
{

  GetDataFromSerial2(); //Part A

  unsigned long current_t = millis();
  if ((current_t - lastTime) > timerDelay)
  { //handle PartB
    prepareData();
    lastTime = current_t;
  }

  if ((current_t - retryTime) > retryDelay)
  { //handle PartC
    // AWS_Push_handle();

    DynamicJsonDocument doc(4096); // payload to send to AWS
    uint8_t uuid_array[16];
    ESPRandom::uuid4(uuid_array);
    doc["id"] = ESPRandom::uuidToString(uuid_array); // assign UUID to doc JSON object ["id"]

    String dataString;
    serializeJson(newdoc, dataString);
    doc["data"] = dataString;
    sendData(doc);
    retryTime = current_t;
  }
}

void connectWifi()
{

  //    if (WiFi.config(ip, gateway, subnet, dns, dns) == false) {
  //      Serial.println("Configuration failed.");
  //    }
  WiFi.begin(ssid, password);
  //    delay(500);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {

    delay(500);
    Serial.print(".");
    internetCounter++;
    if (internetCounter > 60)
    {
      ESP.restart();
    }
  }

  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}

float calculate_humidex(float t, float h)
{

  float e = (6.112 * pow(10, (7.5 * t / (237.7 + t))) * h / 100); //vapor pressure
  float humidex = t + 0.55555555 * (e - 10.0);                    //humidex
  return humidex;
}

// retrieve unix timestamp from NTP server
bool getNTPtime(int sec)
{

  {
    uint32_t start = millis();
    do
    {
      time(&now);
      localtime_r(&now, &timeinfo);
      Serial.print(".");
      delay(10);
    } while (((millis() - start) <= (1000 * sec)) && (timeinfo.tm_year < (2016 - 1900)));
    if (timeinfo.tm_year <= (2016 - 1900))
      return false; // the NTP call was not successful
    Serial.print("now ");
    Serial.println(now);
    timestamp = now;
    char time_output[30];
    strftime(time_output, 30, "%a  %d-%m-%y %T", localtime(&now));
    Serial.println(time_output);
    Serial.println();
  }
  return true;
}

// convert to human date
void showTime(tm localTime)
{
  Serial.print(localTime.tm_mday);
  Serial.print('/');
  Serial.print(localTime.tm_mon + 1);
  Serial.print('/');
  Serial.print(localTime.tm_year - 100);
  Serial.print('-');
  Serial.print(localTime.tm_hour);
  Serial.print(':');
  Serial.print(localTime.tm_min);
  Serial.print(':');
  Serial.print(localTime.tm_sec);
  Serial.print(" Day of Week ");
  if (localTime.tm_wday == 0)
    Serial.println(7);
  else
    Serial.println(localTime.tm_wday);
}

/*****************************************************
   PART A : Get data from Serial2 and deserialize them into buffer array
 *****************************************************/
void GetDataFromSerial2()
{

  if (Serial2.available())
  {

    String input = Serial2.readString();
    Serial.print("Serial2 received: ");
    Serial.println(input); // local echo.. print out to see if correct

    //Deserializing
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, input);

    if (error)
    {
      Serial.print("DeserializeJson() failed: ");
      Serial.println(error.c_str());
    }
    else //save data to respective array based on arrayctr value
    {
      for (int a = 1; a < 16; a++) //start from 1, 0 is not used
      {
        String moisture_tag = "Moisture" + String(a);
        String temp_tag = "temperature" + String(a);
        String humi_tag = "humidity" + String(a);
        //  String timestp_tag = "Timestamp" + String(a);
        //  String slvid_tag = "SlaveId" + String(a);

        moist_buf[a] = doc[moisture_tag];
        temp_buf[a] = doc[temp_tag];
        humi_buf[a] = doc[humi_tag];
        //  timestamp_buf[a] = newdoc[timestp_tag];  //-----------------------------------might not need , not important anyway
        //  slaveId_buf[a] = newdoc[slvid_tag];   //-----------------------------------might not need , not important anyway
      }
    }
  }
}

/*****************************************************
   PART B : prepare json  buffer array value, + timestamp + UUID + Datatype to AWS

 *****************************************************/

void prepareData()
{
  getNTPtime(10); //update latest time
  showTime(timeinfo);

  //------Form New Json from the array and also add in required data
  newdoc["deviceID"] = "0d9e94e3-6cf0-4807-bcb4-05805493975f";
  // newdoc["DataType"] = "moisture";
  newdoc["timestamp"] = String(timestamp);

  for (int a = 1; a < 16; a++) //start from 1, 0 is not used
  {
    String moisture_tag = "Moisture" + String(a);
    String temp_tag = "temperature" + String(a);
    String humi_tag = "humidity" + String(a);
    //  String timestp_tag = "Timestamp" + String(a);
    //  String slvid_tag = "SlaveId" + String(a);

    newdoc[moisture_tag] = moist_buf[a];
    newdoc[temp_tag] = temp_buf[a];
    newdoc[humi_tag] = humi_buf[a];
    //  newdoc[timestp_tag] = timestamp_buf[a];  //-----------------------------------might not need , not important anyway
    //   newdoc[slvid_tag] =  slaveId_buf[a];   //-----------------------------------might not need , not important anyway
  }

  aws_pushflag = true;
  aws_pushctr = 0;
}

/*****************************************************
   PART C : json AWS push and retry

 *****************************************************/
bool internetconnected = false;

void AWS_Push_handle()
{

  //Check WiFi connection status
  if (WiFi.status() == WL_CONNECTED)
  {
    internetconnected = true;
  }
  else
  { //if no WiFi connection
    internetconnected = false;
    ESP.restart(); // hard reset for ESP32
  }

  if (aws_pushflag)
  {

    if ((aws_pushctr < aws_pushmaxretry) && internetconnected) // not yet exceed retry  and must have wifi to push
    {
      Serial.print(F("aws_push..."));
      Serial.println(aws_pushctr);

      String aws_jsonstr;
      serializeJson(newdoc, aws_jsonstr);

      Serial.print(F("aws_push: "));
      Serial.println(aws_jsonstr);

      http.begin(serverName);
      http.addHeader("x-api-key", "isCRANTHrOSIMerMoDet");
      http.addHeader("Content-Type", "application/json");

      //------------datastr to be POST---------------
      httpResponseCode = http.POST(aws_jsonstr);

      //------------evaluate response--------------
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      String response = http.getString(); //Get the response to the request
      Serial.print("Response: ");
      Serial.println(response); //Print request answer
      DynamicJsonDocument doc(1024);
      deserializeJson(doc, response);
      int val = doc["c"];
      Serial.println("ori val: ");
      Serial.print(val);
      Serial.print(F("[HTTP] POST\n"));

      if (val != 0 || httpResponseCode != 200) //fail
      {
      }
      else //success
      {
        aws_pushflag = false; //completed .. flag down.. no retry
        Serial.println(F("aws_push OK"));
      }
      http.end();

      aws_pushctr++;
    }
    else //exceed retry already  OR no internet  save to log file as pending data to be handle later
    {
      Serial.println("Sensor failed to send! Let's try on the next PUSH!");
      ESP.restart(); // hard reset for ESP32
    }
  }
}

/*----(START)-Functions to create payload and send to server---------------*/
bool sendData(JsonDocument &local_doc)
{
  // HTTPS wrapper
  WiFiClientSecure client;
  if (WiFi.status() == WL_CONNECTED)
  {
    char payload[2560];
    serializeJson(local_doc, payload);
    Serial.println(payload);

    static uint32_t requests = 0;

    String url = String(host) + "." + String(service) + "." + String(region) +
                 "." + String(TLD);

    String request = createRequest("POST", String(path), payload,
                                   String(apiKey), "application/json", "");
    Serial.println(request);

    Serial.println("\nStarting connection to server...");
    client.setInsecure(); // skip verification
    client.setTimeout(5000);

    if (!client.connect(url.c_str(), 443))
    {
      Serial.println("Connection failed!");
      client.print(request);
      // return false;
    }
    else
    {
      Serial.println("Connected to server!");
      // Make a HTTP request:
      client.print(request);

      // Check HTTP status
      char status[32] = {0};
      client.readBytesUntil('\r', status, sizeof(status));
      // It should be "HTTP/1.0 200 OK" or "HTTP/1.1 200 OK"
      if (strcmp(status + 9, "200 OK") != 0)
      {
        Serial.print(F("Unexpected response: "));
        Serial.println(status);
        // if there are incoming bytes available
        // from the server, read them and print them:
        while (client.available())
        {
          char c = client.read();
          Serial.write(c);
        }
        client.stop();
        // return false;
      }
      // if there are incoming bytes available
      // from the server, read them and print them:
      while (client.available())
      {
        char c = client.read();
        Serial.write(c);
      }
      client.stop();
      // return true;
    }
    Serial.println();
    // HTTPClient http;
    // http.begin("https://ejfut1hft2.execute-api.ap-southeast-1.amazonaws.com/prod/rawdata"); //test server
    // http.addHeader("x-api-key", "HXnKw3QwOxkr0t8JOrFC25CfKihOU496sohWpwe0");
    // http.addHeader("content-type", "application/json");
    // int httpCode = http.POST(payload);
    // String response = http.getString();
    // Serial.println(httpCode);
    // Serial.println(response);
    // http.end();
  }
}
/*----(END)-Functions to create payload and send to server---------------*/

/*-------AWS API signing requests functions----------*/
String createRequest(String method, String uri, String payload, String apiKey,
                     String contentType, String queryString)
{
  char dateBuf[9], timeBuf[7];

  struct tm timeinfo;
  if (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
  }
  snprintf(dateBuf, sizeof(dateBuf), "%4d%02d%02d", (timeinfo.tm_year + 1900),
           (timeinfo.tm_mon + 1), (timeinfo.tm_mday));
  snprintf(timeBuf, sizeof(timeBuf), "%02d%02d%02d", (timeinfo.tm_hour),
           (timeinfo.tm_min), (timeinfo.tm_sec));
  String date(dateBuf);
  String time(timeBuf);

  Sha256.init();
  Sha256.print(payload);
  String payloadHash = hexHash(Sha256.result());

  String canonical_request = createCanonicalRequest(
      method, uri, date, time, payloadHash, apiKey, queryString, contentType);
  String string_to_sign = createStringToSign(canonical_request, date, time);
  String signature = createSignature(string_to_sign, date);
  String headers = createRequestHeaders(contentType, date, time, payload,
                                        payloadHash, apiKey, signature);

  String retval;
  retval += method + " " + "https://" + FQDN() + uri + " " + "HTTP/1.1\r\n";
  retval += headers + "\r\n";
  retval += payload + "\r\n\r\n";
  return retval;
}

String hexHash(uint8_t *hash)
{
  char hashStr[(HASH_LENGTH * 2) + 1];
  for (int i = 0; i < HASH_LENGTH; ++i)
  {
    sprintf(hashStr + 2 * i, "%02lx", 0xff & (unsigned long)hash[i]);
  }
  return String(hashStr);
}

String createCanonicalRequest(String method, String uri, String date,
                              String time, String payloadHash, String apiKey,
                              String queryString, String contentType)
{
  String retval;
  String _signedHeaders = "content-type;host;x-amz-content-sha256;x-amz-date";
  retval += method + "\n";
  retval += uri + "\n";
  retval += queryString + "\n";
  String headers =
      createCanonicalHeaders(contentType, date, time, payloadHash, apiKey);
  retval += headers + _signedHeaders + "\n";
  retval += payloadHash;
  return retval;
}

String createCanonicalHeaders(String contentType, String date, String time,
                              String payloadHash, String apiKey)
{
  String retval;
  retval += "content-type:" + contentType + "\n";
  retval += "host:" + FQDN() + "\n";
  retval += "x-amz-content-sha256:" + payloadHash + "\n";
  retval += "x-amz-date:" + date + "T" + time + "Z\n\n";
  return retval;
}

String createRequestHeaders(String contentType, String date, String time,
                            String payload, String payloadHash, String apiKey,
                            String signature)
{
  String retval;
  String _signedHeaders = "content-type;host;x-amz-content-sha256;x-amz-date";
  retval += "Content-Type: " + contentType + "\r\n";
  retval += "Connection: close\r\n";
  retval += "Content-Length: " + String(payload.length()) + "\r\n";
  retval += "x-api-key: " + apiKey + "\r\n";
  retval += "Host: " + FQDN() + "\r\n";
  retval += "x-amz-content-sha256: " + payloadHash + "\r\n";
  retval += "x-amz-date: " + date + "T" + time + "Z\r\n";
  retval += "Authorization: AWS4-HMAC-SHA256 Credential=" + String(awsKey) +
            "/" + String(date) + "/" + String(region) + "/" + String(service) +
            "/aws4_request,SignedHeaders=" + _signedHeaders +
            ",Signature=" + signature + "\r\n";
  return retval;
}

String FQDN()
{
  String retval;
  if (((String)customFQDN).length() > 0)
  {
    retval = String(customFQDN);
  }
  else
  {
    retval = String(host) + "." + String(service) + "." + String(region) + "." +
             String(TLD);
  }
  return retval;
}

String createStringToSign(String canonical_request, String date, String time)
{
  Sha256.init();
  Sha256.print(canonical_request);
  String hash = hexHash(Sha256.result());

  String retval;
  retval += "AWS4-HMAC-SHA256\n";
  retval += date + "T" + time + "Z\n";
  retval +=
      date + "/" + String(region) + "/" + String(service) + "/aws4_request\n";
  retval += hash;
  return retval;
}

String createSignature(String toSign, String date)
{
  String key = "AWS4" + String(awsSecret);

  Sha256.initHmac((uint8_t *)key.c_str(), key.length());
  Sha256.print(date);
  uint8_t *hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print(String(region));
  hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print(String(service));
  hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print("aws4_request");
  hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print(toSign);
  hash = Sha256.resultHmac();

  return hexHash(hash);
}
/*---(END)----AWS API signing requests functions----------*/
/*
  // Test (False UUID)
  httpResponseCode = http.POST("{\"UUID\":\"0d9e94e3-6cf0-4807-bcb4-05805493975f\" , \"DataType\":\"moisture\" , \"Timestamp\":" + String(timestamp) + " , \"reading\": [{ \"id\": " + String(id) + " , \"MoistureContent\": " + String(mois) + " }] }");



  // Sensor 1 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"0c2dd338-959f-48e3-bd36-0cba26848967\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 2 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"0755775e-f8b5-49fb-8150-9d2fe68d4f74\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 3 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"f65f1191-045d-449b-9907-f797b1161409\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 4 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"baee2412-4e38-4942-b36e-dc6254aade2f\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 5 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"618f2919-66f0-4b0d-b7ca-9cf7462d6fc8\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 6 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"ed039615-85bb-4d6d-bef6-b1689a174078\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 7 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"afefe9eb-3b78-4297-8ad0-062216b0c49b\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 8 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"e36ad6d7-9976-4a1e-bf86-149bc42e4e83\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 9 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"3b98ede8-8924-4198-88c5-4a7e9189ddf6\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 10 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"a74f9993-94f2-427a-9da8-a0165e9bec34\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 11 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"b2070202-d434-4d15-b1e8-cb7a6e76bfb7\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 12 (Dryer 1)
  //        httpResponseCode = http.POST("{\"UUID\":\"f0e33d01-6070-480c-8efa-6b542a0c69f5\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 13 (Dryer 1 inside)
  //        httpResponseCode = http.POST("{\"UUID\":\"a47c8b45-21ef-494b-8236-f542122390a7\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 14 (Dryer 1 outside)
  //        httpResponseCode = http.POST("{\"UUID\":\"b877ca8d-cdbb-4c1c-a86b-b73e067efd2a\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");




  // Sensor 1 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"c6eccf76-89f7-4469-aabb-9e4a35518a33\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 2 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"3f376227-cd90-420e-a30b-6dab8ce21f06\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 3 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"34e7f6e7-abae-4a76-8a2f-ede68fccea0c\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 4 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"9868f96c-9af6-4479-97c0-10890f4acbfa\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 5 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"fd99fb84-5a51-4bd0-b486-5251a8fd9be2\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 6 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"e06c8345-a1c7-4498-a230-a39ad417dd18\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 7 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"d5c63892-6664-4c4d-994f-2f4653697fc4\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 8 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"d1309b48-9c75-4b49-aadc-fe80cabf5379\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 9 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"3632fadd-dae0-48f2-8005-eaba21f893c7\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 10 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"3bd314ae-4938-4e78-813f-4d578a35b06d\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 11 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"20599fd2-b159-4505-811e-a22a72f5bc85\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 12 (Dryer 2)
  //        httpResponseCode = http.POST("{\"UUID\":\"10fd3062-17f6-4f37-9f96-a88280e9e3d8\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 13 (Dryer 2 inside)
  //        httpResponseCode = http.POST("{\"UUID\":\"9aeca1f1-c351-46ac-9aab-7660e5c07bd5\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 14 (Dryer 2 outside)
  //        httpResponseCode = http.POST("{\"UUID\":\"0a41b8d7-c553-4bea-bd55-ef26a388a9c8\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");



  // Sensor 1 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"be89825f-966b-44d9-a03a-2b27bb219b4e\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 2 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"41c37ab9-40ce-4ef6-b71f-3390ebf6e6fa\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 3 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"6d8f6b3a-92d6-4904-a1a7-b1df17f33d2e\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 4 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"e369afd4-0f51-4481-8576-e0463671bc46\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 5 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"71cdb188-a91e-4234-85b3-b729968bea07\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 6 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"2ee69f03-2ca0-412a-8332-930e23c535af\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 7 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"80612719-2a24-4ce1-8594-e926cc40b4fa\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 8 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"e4825e7f-cbd3-4d19-91b1-6004726ef945\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 9 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"ca475100-7379-4ca7-8d7e-9729e39254d0\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 10 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"ca7620f8-00f3-4d5d-b698-ac59e01ad661\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 11 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"06786ef3-594a-42a0-b484-6ac9244ab8f1\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 12 (Dryer 3)
  //        httpResponseCode = http.POST("{\"UUID\":\"33e57be7-961b-4ba1-8e49-1a3dc002de74\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 13 (Dryer 3 inside)
  //        httpResponseCode = http.POST("{\"UUID\":\"142985f5-bd3e-45c1-a2ef-49e0a0e31048\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 14 (Dryer 3 outside)
  //        httpResponseCode = http.POST("{\"UUID\":\"f8728684-732c-48eb-a500-ff34732cc971\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");



  // Sensor 1 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"8ad97e73-1a3f-4c3b-b28e-39d2366a8929\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 2 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"5b4987d1-20af-4ca4-a564-355c1aece135\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 3 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"ecf26d10-3ead-4a6a-b6fc-63bf76c22830\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 4 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"de9a4c28-bee8-4bdd-a020-158c3ab111fa\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 5 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"9bfd95b3-17ac-4f03-bbdd-6376c0c04099\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 6 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"4c4f17f1-0a5a-4a2e-beef-88420c28e91d\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 7 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"01ef52b8-9d97-4bca-bbdf-425b572cb7db\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 8 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"396c1e67-3025-4626-9701-095480126d5f\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 9 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"2c01b794-540e-4546-9426-844c7a119dde\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 10 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"68e0138c-c85c-48da-bef6-cfe07293b094\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 11 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"ae369856-d1ea-4033-9f8f-062efa91ff0c\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 12 (Dryer 4)
  //        httpResponseCode = http.POST("{\"UUID\":\"c12c0494-92c3-404e-9526-aae304b2c56f\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 13 (Dryer 4 inside)
  //        httpResponseCode = http.POST("{\"UUID\":\"0e6b16be-e9d7-4809-aee8-55eba6a33aba\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
  // Sensor 14 (Dryer 4 outside)
  //        httpResponseCode = http.POST("{\"UUID\":\"7372a490-5a26-4c6e-b1e8-28f44ee08a2a\" , \"DataType\":\"temperature\" , \"Temperature\": " + String(t) + " , \"Humidity\": " + String(h) + " , \"Humidex\":" + String(humidex) + " , \"Timestamp\":" + String(timestamp) + "}");
*/
