#include <Arduino.h>

//************************************************************
// this is a simple example that uses the painlessMesh library
//
// This example shows how to build a mesh with named nodes
//
//************************************************************
#include "namedMesh.h"
#include "SHT31.h"

#define MESH_SSID "CKMeshingNetwork"
#define MESH_PASSWORD "somethingSneaky"
#define MESH_PORT 5555

#define SHT31_ADDRESS 0x44 // define SHT address

Scheduler userScheduler; // to control your personal task
namedMesh mesh;
SHT31 sht; // declare sht variable

String nodeName = "Slave1"; // Name needs to be unique          //--------------------- root will recognize slave by its nodeName

void sendMessage();

Task taskSendMessage(TASK_SECOND * 1, TASK_FOREVER, &sendMessage);

void sendMessage()
{
  uint16_t rawTemperature = 0;
  uint16_t rawHumidity = 0;
  if (sht.dataReady())
  {
    bool success = sht.readData();
    sht.requestData(); // request for next sample
    if (success == false)
    {
      Serial.println("Failed read");
    }
    else
    {
      rawTemperature = sht.getRawTemperature();
      rawHumidity = sht.getRawHumidity();
      Serial.print(rawTemperature * (175.0 / 65535) - 45, 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.print("°C\t");
      Serial.print(rawHumidity * (100.0 / 65535), 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.println("%\t");
    }
  }

  String tobi = "Root";

  //Serializing in JSON Format
  DynamicJsonDocument doc(1024);

  float temperature;
  float humidity;
  temperature = (rawTemperature * (175.0 / 65535) - 45);
  humidity = (rawHumidity * (100.0 / 65535));
  doc["temperature"] = temperature; // temperature info as payload
  doc["humidity"] = humidity;       // humidity info as payload

  // int moist = random(1400, 1900);
  // doc["Moisture"] = moist;
  //  doc["Timestamp"] = 1599061556;                        //---------------------slave does not has internet thus does not provide timestamp
  //  doc["SlaveId"] = 1;                                   //------------------- this might not be needed, root will recognize slave by nodeName
  String msg;
  serializeJson(doc, msg);
  mesh.sendSingle(tobi, msg);

  taskSendMessage.setInterval((TASK_SECOND * 10));
}

void setup()
{
  Serial.begin(115200);
  sht.begin(SHT31_ADDRESS);
  sht.requestData();

  mesh.setDebugMsgTypes(ERROR | DEBUG | CONNECTION); // set before init() so that you can see startup messages

  mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);

  mesh.setName(nodeName); // This needs to be an unique name!

  mesh.onReceive([](uint32_t from, String &msg)
                 { Serial.printf("Received message by id from: %u, %s  \n", from, msg.c_str()); });

  mesh.onReceive([](String &from, String &msg)
                 { Serial.printf("Received message by name from: %s, %s  \n", from.c_str(), msg.c_str()); });

  mesh.onChangedConnections([]()
                            { Serial.printf("Changed connection\n"); });

  userScheduler.addTask(taskSendMessage);
  taskSendMessage.enable();
}

void loop()
{
  // it will run the user scheduler as well
  mesh.update();
}
