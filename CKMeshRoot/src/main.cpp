#include <Arduino.h>

//************************************************************
// this is a simple example that uses the painlessMesh library
//
// This example shows how to build a mesh with named nodes
//
//************************************************************
#include "namedMesh.h"

#define RXD2 16 // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
#define TXD2 17 // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)

#define MESH_SSID "CKMeshingNetwork"
#define MESH_PASSWORD "somethingSneaky"
#define MESH_PORT 5555

Scheduler userScheduler; // to control your personal task
namedMesh mesh;

String nodeName = "Root"; // Name needs to be unique

int sensor_pin = 34; // assign GPIO 34 as the signal pin to communicate with moisture sensor SEN-0193

int output_value; // analogue output read from moisture sensor

int moist;
long timestamp;
int slaveId;

/*****************************************************
   PART B : sending buffer array value to Serial2

 *****************************************************/
// int moist_buf[18] = {0};
int temp_buf[18] = {0};
int humi_buf[18] = {0};
long timestamp_buf[18] = {0}; //might not need
int slaveId_buf[18] = {0};    //might not needed

void t1Callback();
Task t1(10000, TASK_FOREVER, &t1Callback);
Scheduler runner;

void t1Callback()
{

  //------Form New Json
  DynamicJsonDocument newdoc(1024);

  for (int a = 1; a < 18; a++) //start from 1, 0 is not used
  {
    // String moisture_tag = "Moisture" + String(a);
    String temp_tag = "Temperature" + String(a);
    String humi_tag = "Humidity" + String(a);
    //  String timestp_tag = "Timestamp" + String(a);
    //  String slvid_tag = "SlaveId" + String(a);

    // newdoc[moisture_tag] = moist_buf[a];
    newdoc[temp_tag] = temp_buf[a];
    newdoc[humi_tag] = humi_buf[a];
    //  newdoc[timestp_tag] = timestamp_buf[a];  //-----------------------------------might not need , not important anyway
    //   newdoc[slvid_tag] =  slaveId_buf[a];   //-----------------------------------might not need , not important anyway
  }

  Serial.print("Serial2 sent: ");
  serializeJson(newdoc, Serial);
  Serial.println();
  Serial.println();

  serializeJson(newdoc, Serial2);
}

/*****************************************************
   PART C : check sensor timeout,
   if there is no data coming from the slave for x duration,
   then set its data buffer value to -1 to indicate connection dropped

 *****************************************************/
unsigned long lastactive_time[18] = {0};
unsigned long timeoutduration = 20000; //20 seconds

void checklastactive()
{
  unsigned long currenttime = millis();

  for (int a = 1; a < 18; a++) //start from 1, 0 not used
  {
    if (currenttime - lastactive_time[a] > timeoutduration)
    {
      // moist_buf[a] = -1;
      temp_buf[a] = -1;
      humi_buf[a] = -1;
      //    timestamp_buf[a] = -1; //-----------------------------------might not need , not important anyway
      //    slaveId_buf[a] = -1;     //-----------------------------------might not need , not important anyway
    }
  }
}

void setup()
{
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);

  mesh.setDebugMsgTypes(ERROR | DEBUG | CONNECTION); // set before init() so that you can see startup messages

  // mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);
  mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, 6);

  mesh.setName(nodeName); // This needs to be an unique name!

  /*****************************************************
    PART A : receive data from any Slave,
    identify which slave they come from,
    deserialize json and put the value to respective buffer array
    this buffer array hold the value of all sensors. PartB will send this array to serial 2
  *****************************************************/
  mesh.onReceive([](String &from, String &msg)
                 {
                   //data classification, which slave the data comes from then assign the respective arrayctr number.
                   String slave_nodeName = from.c_str();
                   int arrayctr = 0;
                   if (slave_nodeName == "Slave1")
                   {
                     arrayctr = 1;
                   }
                   else if (slave_nodeName == "Slave2")
                   {
                     arrayctr = 2;
                   }
                   else if (slave_nodeName == "Slave3")
                   {
                     arrayctr = 3;
                   }
                   else if (slave_nodeName == "Slave4")
                   {
                     arrayctr = 4;
                   }
                   else if (slave_nodeName == "Slave5")
                   {
                     arrayctr = 5;
                   }
                   else if (slave_nodeName == "Slave6")
                   {
                     arrayctr = 6;
                   }
                   else if (slave_nodeName == "Slave7")
                   {
                     arrayctr = 7;
                   }
                   else if (slave_nodeName == "Slave8")
                   {
                     arrayctr = 8;
                   }
                   else if (slave_nodeName == "Slave9")
                   {
                     arrayctr = 9;
                   }
                   else if (slave_nodeName == "Slave10")
                   {
                     arrayctr = 10;
                   }
                   else if (slave_nodeName == "Slave11")
                   {
                     arrayctr = 11;
                   }
                   else if (slave_nodeName == "Slave12")
                   {
                     arrayctr = 12;
                   }
                   else if (slave_nodeName == "Slave13")
                   {
                     arrayctr = 13;
                   }
                   else if (slave_nodeName == "Slave14")
                   {
                     arrayctr = 14;
                   }
                   else if (slave_nodeName == "Slave15")
                   {
                     arrayctr = 15;
                   }
                   else if (slave_nodeName == "Slave16")
                   {
                     arrayctr = 16;
                   }
                   else if (slave_nodeName == "Slave17")
                   {
                     arrayctr = 17;
                   }
                   else
                   {
                     arrayctr = 0; //0 is used to store unclassified/ unidentified Slave nodeName// not goin to read them anyway
                   }

                   //Deserializing
                   String json;
                   DynamicJsonDocument doc(1024);
                   json = msg.c_str();
                   DeserializationError error = deserializeJson(doc, json);

                   if (error)
                   {
                     Serial.print("DeserializeJson() failed: ");
                     Serial.println(error.c_str());
                   }
                   else //save data to respective array based on arrayctr value
                   {
                     //  moist_buf[arrayctr] = doc["Moisture"];
                     temp_buf[arrayctr] = doc["temperature"];
                     humi_buf[arrayctr] = doc["humidity"];
                     //    timestamp_buf[arrayctr] = doc["Timestamp"]; //-----------------------------------might not need , not important anyway
                     //    slaveId_buf[arrayctr] = doc["SlaveId"];     //-----------------------------------might not need , not important anyway

                     //record last activetime for respective sensor.
                     lastactive_time[arrayctr] = millis();

                     //  Serial.printf("from: %s , %s  \n", from.c_str(), msg.c_str()); //just printout..
                     //  Serial.println();
                   }
                 });

  mesh.onChangedConnections([]()
                            { Serial.printf("Changed connection\n"); });
  // Setup over the air update support
  mesh.initOTAReceive("bridge");
  // Bridge node, should (in most cases) be a root node. See [the
  // wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation)
  // for some background
  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root,
  // so call this on all nodes
  mesh.setContainsRoot(true);

  //------handle PartB stuff-------
  runner.init();
  runner.addTask(t1);
  t1.enable();
}

void loop()
{
  // it will run the user scheduler as well
  mesh.update();     //handle part A stuff
  runner.execute();  //handle part B stuff
  checklastactive(); // handle part C stuff
  // read moisture
  //  readMoisture();
}

void readMoisture()
{

  output_value = analogRead(sensor_pin);

  Serial.print("Analogue read: ");
  Serial.println(output_value);

  // set benchmark for moisture level (benchmark value set according to dfRobot hardware specifications)
  //////////// 2900 = max dryness (dry air) ////////////////
  //////////// 1440 = max wetness (water) ////////////////
  //   output_value = map(output_value,2900,1440,0,100); // default calibration
  output_value = map(output_value, 2900, 1400, 0, 100); // default calibration
  //   output_value = map(output_value,2900,2767,0,100); // calibration based on paddy moisture level

  Serial.print("Moisture : ");
  Serial.print(output_value);

  Serial.println("%");

  delay(10000);
}
