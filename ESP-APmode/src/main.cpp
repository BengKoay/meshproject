#include <WiFi.h>
#include <HTTPClient.h>
#include <WebServer.h>
#include <EEPROM.h>
#include "namedMesh.h"
#include "SHT31.h"

#define LED 2              // GPIO number of connected LED, ON ESP-12 IS GPIO2
#define BLINK_PERIOD 3000  // milliseconds until cycle repeat
#define BLINK_DURATION 100 // milliseconds LED is on for
#define SHT31_ADDRESS 0x44 // define SHT address

//Variables
int i = 0;
int statusCode;
//const char *ssid = "Default SSID";
//const char *passphrase = "Default passord";

String st;
String content;
namedMesh mesh;
Scheduler userScheduler; // to control your personal task

// declare SHT
SHT31 sht; // declare sht variable

//Node setting//
TSTRING enodeName;
TSTRING esid;
TSTRING epass = "";
#define eport 5555

//Function Decalration//
bool testWifi(void);
void launchWeb(void);
void setupAP(void);
void createWebServer(void);

//Mesh Function Declaration//
void sendMessage();
void receivedCallback(uint32_t from, String &msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback();
void nodeTimeAdjustedCallback(int32_t offset);
void delayReceivedCallback(uint32_t from, int32_t delay);
void requestDataSHT();

//Establishing Local server at port 80//
WebServer server(80);

//Mesh//
bool calc_delay = false;
SimpleList<uint32_t> nodes;

void sendMessage(); // Prototype
// Task taskSendMessage(TASK_SECOND * 30, TASK_FOREVER, []()
//                      {
//                        String msg = String("This is a message from: ") + enodeName + String(" for logNode");
//                        String to = "logNode";
//                        mesh.sendSingle(to, msg);
//                      }); // start with a one second interval

Task taskSendMessage(TASK_SECOND * 1, TASK_FOREVER, &requestDataSHT); // start with a one second interval

// Task to blink the number of nodes
Task blinkNoNodes;
bool onFlag = false;

void setup()
{

  Serial.begin(115200); //Initialising if(DEBUG)Serial Monitor
  sht.begin(SHT31_ADDRESS);
  sht.requestData();
  Serial.println();
  Serial.println("Disconnecting current wifi connection");
  WiFi.disconnect();
  EEPROM.begin(512); //Initialasing EEPROM
  delay(10);
  pinMode(32, INPUT_PULLUP);
  pinMode(LED, OUTPUT);
  Serial.println();
  Serial.println();
  Serial.println("Startup");

  //---------------------------------------- Read eeprom for ssid and pass
  Serial.println("Reading EEPROM ssid");

  for (int i = 0; i < 32; ++i)
  {
    enodeName += char(EEPROM.read(i));
  }
  Serial.println();
  Serial.print("NODE NAME: ");
  Serial.println(enodeName);

  for (int i = 32; i < 64; ++i)
  {
    esid += char(EEPROM.read(i));
  }
  Serial.println();
  Serial.print("SSID: ");
  Serial.println(esid);

  for (int i = 64; i < 96; ++i)
  {
    epass += char(EEPROM.read(i));
  }
  Serial.println();
  Serial.print("PASS: ");
  Serial.println(epass);

  // for (int i = 96; i < 128; ++i)
  // {
  //   eport += char(EEPROM.read(i));
  // }
  // Serial.println();
  // Serial.print("PORT: ");
  // Serial.println(eport);

  WiFi.begin(esid.c_str(), epass.c_str());
  mesh.setDebugMsgTypes(ERROR | DEBUG); // set before init() so that you can see error messages
  mesh.init(esid, epass, &userScheduler, eport);
  mesh.setName(enodeName); // This needs to be an unique name!

  //Mesh Communication//
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  mesh.onNodeDelayReceived(&delayReceivedCallback);

  userScheduler.addTask(taskSendMessage);
  taskSendMessage.enable();

  blinkNoNodes.set(BLINK_PERIOD, (mesh.getNodeList().size() + 1) * 2, []()
                   {
                     // If on, switch off, else switch on
                     if (onFlag)
                       onFlag = false;
                     else
                       onFlag = true;
                     blinkNoNodes.delay(BLINK_DURATION);

                     if (blinkNoNodes.isLastIteration())
                     {
                       // Finished blinking. Reset task for next run
                       // blink number of nodes (including this node) times
                       blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
                       // Calculate delay based on current mesh time and BLINK_PERIOD
                       // This results in blinks between nodes being synced
                       blinkNoNodes.enableDelayed(BLINK_PERIOD -
                                                  (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);
                     }
                   });
  userScheduler.addTask(blinkNoNodes);
  blinkNoNodes.enable();

  randomSeed(analogRead(A0));
}

void loop()
{

  // Serial.print("Connected to ");
  // Serial.print(esid);
  // Serial.println(" Successfully");

  if ((WiFi.status() == WL_CONNECTED))
  {
    //Mesh//
    mesh.update();
    digitalWrite(LED, !onFlag);
    delay(1000);
  }
  else
  {
  }

  if (testWifi() && (digitalRead(32) == 1))
  {
    // Serial.println("Connection Status Positive");
    return;
  }
  else
  {
    Serial.println("Connection Status Negative / D32 HIGH");
    Serial.println("Turning the HotSpot On");
    launchWeb();
    setupAP(); // Setup HotSpot
  }

  Serial.println();
  Serial.println("Waiting.");

  while ((WiFi.status() != WL_CONNECTED))
  {
    Serial.print(".");
    digitalWrite(LED, HIGH);
    delay(100);
    digitalWrite(LED, LOW);
    delay(100);
    server.handleClient();
  }
  delay(1000);
}

//----------------------------------------------- Fuctions used for WiFi credentials saving and connecting to it which you do not need to change
bool testWifi(void)
{
  int c = 0;
  //Serial.println("Waiting for Wifi to connect");
  while (c < 20)
  {
    if (WiFi.status() == WL_CONNECTED)
    {
      return true;
    }
    delay(500);
    Serial.print("*");
    c++;
  }
  Serial.println("");
  Serial.println("Connect timed out, opening AP");
  return false;
}

void launchWeb()
{
  Serial.println("");
  if (WiFi.status() == WL_CONNECTED)
    Serial.println("WiFi connected");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("SoftAP IP: ");
  Serial.println(WiFi.softAPIP());
  createWebServer();
  // Start the server
  server.begin();
  Serial.println("Server started");
}

void setupAP(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      //Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
  st = "<ol>";
  for (int i = 0; i < n; ++i)
  {
    // Print SSID and RSSI for each network found
    st += "<li>";
    st += WiFi.SSID(i);
    st += " (";
    st += WiFi.RSSI(i);

    st += ")";
    //st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
    st += "</li>";
  }
  st += "</ol>";
  delay(100);
  WiFi.softAP("Profile_Sensor_AP", "");
  Serial.println("Initializing_softap_for_wifi credentials_modification");
  launchWeb();
  Serial.println("over");
}

void createWebServer()
{
  {
    server.on("/", []()
              {
                IPAddress ip = WiFi.softAPIP();
                String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
                content = "<!DOCTYPE HTML>\r\n<html>Welcome to Wifi Credentials Update page";
                content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"Scan\"></form>";
                content += ipStr;
                content += "<p>";
                content += st;
                content += "<p>";
                content += "</p><form method='get' action='setting'><label>NODE_NAME: </label><input name='nodeName' length=32>";
                content += "<p>";
                // content += "<label>MESH_PORT: </label><input name='port' length=32>";
                // content += "<p>";
                content += "<label>MESH_SSID: </label><input name='ssid' length=32>";
                content += "<p>";
                content += "<label>MESH_PASSWORD: </label><input name='pass' length=64>";
                content += "<p>";
                content += "<input type='submit'></form>";

                // content = "<!DOCTYPE HTML>\r\n<html>Welcome to Wifi Credentials Update page";
                // content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"scan\"></form>";
                // content += ipStr;
                // content += "<p>";
                // content += st;
                // content += "</p><form method='get' action='setting'><label>SSID: </label><input name='ssid' length=32><input name='pass' length=64><input type='submit'></form>";
                content += "</html>";
                server.send(200, "text/html", content);
              });
    server.on("/scan", []()
              {
                //setupAP();
                IPAddress ip = WiFi.softAPIP();
                String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);

                content = "<!DOCTYPE HTML>\r\n<html>go back";
                server.send(200, "text/html", content);
              });

    server.on("/setting", []()
              {
                String qnodeName = server.arg("nodeName");
                String qsid = server.arg("ssid");
                String qpass = server.arg("pass");
                // String qport = server.arg("port");

                if (qnodeName.length() > 0 && qsid.length() > 0 && qpass.length() > 0) // && qport.length() > 0)
                {
                  Serial.println("clearing eeprom");
                  for (int i = 0; i < 160; ++i)
                  {
                    EEPROM.write(i, 0);
                  }
                  Serial.println(qnodeName);
                  Serial.println("");
                  Serial.println(qsid);
                  Serial.println("");
                  Serial.println(qpass);
                  Serial.println("");
                  // Serial.println(qport);
                  // Serial.println("");

                  Serial.println("writing eeprom nodeName:");
                  for (int i = 0; i < qnodeName.length(); ++i)
                  {
                    EEPROM.write(i, qnodeName[i]);
                    Serial.print("Wrote: ");
                    Serial.println(qnodeName[i]);
                  }
                  Serial.println("writing eeprom ssid:");
                  for (int i = 0; i < qsid.length(); ++i)
                  {
                    EEPROM.write(32 + i, qsid[i]);
                    Serial.print("Wrote: ");
                    Serial.println(qsid[i]);
                  }
                  Serial.println("writing eeprom pass:");
                  for (int i = 0; i < qpass.length(); ++i)
                  {
                    EEPROM.write(64 + i, qpass[i]);
                    Serial.print("Wrote: ");
                    Serial.println(qpass[i]);
                  }
                  // Serial.println("writing eeprom port:");
                  // for (int i = 0; i < qport.length(); ++i)
                  // {
                  //   EEPROM.write(96 + i, qport[i]);
                  //   Serial.print("Wrote: ");
                  //   Serial.println(qport[i]);
                  // }
                  EEPROM.commit();

                  content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
                  statusCode = 200;
                  ESP.restart();
                }
                else
                {
                  content = "{\"Error\":\"404 not found\"}";
                  statusCode = 404;
                  Serial.println("Sending 404");
                }
                server.sendHeader("Access-Control-Allow-Origin", "*");
                server.send(statusCode, "application/json", content);
              });
  }
}

//Mesh//
// void sendMessage()
// {
//   String msg = "Hello from node ";
//   msg += mesh.getNodeId();
//   msg += " myFreeMemory: " + String(ESP.getFreeHeap());
//   mesh.sendBroadcast(msg);

//   if (calc_delay)
//   {
//     SimpleList<uint32_t>::iterator node = nodes.begin();
//     while (node != nodes.end())
//     {
//       mesh.startDelayMeas(*node);
//       node++;
//     }
//     calc_delay = false;
//   }

//   Serial.printf("Sending message: %s\n", msg.c_str());

//   taskSendMessage.setInterval(random(TASK_SECOND * 1, TASK_SECOND * 5)); // between 1 and 5 seconds
// }

void receivedCallback(uint32_t from, String &msg)
{
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
}

void newConnectionCallback(uint32_t nodeId)
{
  // Reset blink task
  onFlag = false;
  blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
  blinkNoNodes.enableDelayed(BLINK_PERIOD - (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);

  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
  Serial.printf("--> startHere: New Connection, %s\n", mesh.subConnectionJson(true).c_str());
}

void changedConnectionCallback()
{
  Serial.printf("Changed connections\n");
  // Reset blink task
  onFlag = false;
  blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
  blinkNoNodes.enableDelayed(BLINK_PERIOD - (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);

  nodes = mesh.getNodeList();

  Serial.printf("Num nodes: %d\n", nodes.size());
  Serial.printf("Connection list:");
  //////ERROR HERE////////////////////////////////////////////////////////////////////////////////////////
  SimpleList<uint32_t>::iterator node = nodes.begin();
  while (node != nodes.end())
  {
    Serial.printf(" %u", *node);
    node++;
  }
  Serial.println();
  calc_delay = true;
}

void nodeTimeAdjustedCallback(int32_t offset)
{
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

void delayReceivedCallback(uint32_t from, int32_t delay)
{
  Serial.printf("Delay to node %u is %d us\n", from, delay);
}

// SHT function

void requestDataSHT()
{
  uint16_t rawTemperature;
  uint16_t rawHumidity;
  if (sht.dataReady())
  {
    bool success = sht.readData();
    sht.requestData(); // request for next sample
    if (success == false)
    {
      Serial.println("Failed read");
    }
    else
    {
      rawTemperature = sht.getRawTemperature();
      rawHumidity = sht.getRawHumidity();
      Serial.print(rawTemperature * (175.0 / 65535) - 45, 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.print("°C\t");
      Serial.print(rawHumidity * (100.0 / 65535), 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.println("%\t");
    }
  }

  DynamicJsonDocument doc(512);
  String msg;

  // if (pm_stage == 0 && nodesize > 0) //send timestamp
  // {

  float temperature;
  float humidity;
  temperature = (rawTemperature * (175.0 / 65535) - 45);
  humidity = (rawHumidity * (100.0 / 65535));
  doc["name"] = enodeName; // time stamp info as payload

  doc["temperature"] = temperature; // temperature info as payload
  doc["humidity"] = humidity;       // humidity info as payload
  // doc["Timestamp"] = NTPEpochTime; // time stamp info as payload
  serializeJson(doc, msg);
  mesh.sendBroadcast(msg);
  taskSendMessage.setInterval(TASK_SECOND * 1);
  // mesh.sendSingle(nodeID[node_targetindex], msg);
  // Serial.printf("mesh_tx aws ID: %u msg: %s\n", nodeID[node_targetindex], msg.c_str()); //display tx msg
  // pm_sendstart = millis();
  // pm_stage = 1;
  // }
}