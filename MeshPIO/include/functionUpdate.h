void functionUpdate(const char *payload);

void sendDataAt58second();
bool sendData(JsonDocument &local_doc);

String createRequest(String method, String uri, String payload, String apiKey,
                     String contentType, String queryString);
String hexHash(uint8_t *hash);
String createCanonicalRequest(String method, String uri, String date,
                              String time, String payloadHash, String apiKey,
                              String queryString, String contentType);
String createCanonicalHeaders(String contentType, String date, String time,
                              String payloadHash, String apiKey);
String createRequestHeaders(String contentType, String date, String time,
                            String payload, String payloadHash, String apiKey,
                            String signature);
String FQDN();
String createStringToSign(String canonical_request, String date, String time);
String createSignature(String toSign, String date);

unsigned long printLocalTime();