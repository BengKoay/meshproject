//************************************************************
// this is a simple example that uses the painlessMesh library to
// connect to a node on another network. Please see the WIKI on gitlab
// for more details
// https://gitlab.com/painlessMesh/painlessMesh/wikis/bridge-between-mesh-and-another-network
//************************************************************

// Mesh Bridge

// start by Station mode for node for 60 seconds
// during 1st 60 seconds, takes 10 seconds to initialize then start receiving from Mesh Node, Mesh Node sending every 5 seconds
// after 1st 60 seconds, start connecting to AP Wifi for internet to push
// after 1st push, may notice instability, which can be avoided by reboot ESP
// after 3 * 60 seconds, reboot ESP, or else it will not be responsive while connecting to both Wifi & node Station
// #include "painlessMesh.h"
#include "namedMesh.h"
#include "functionUpdate.h"
// #include "WiFi.h"

#define MESH_PREFIX "MeshingNetwork"
#define MESH_PASSWORD "somethingSneaky"
#define MESH_PORT 5555
String nodeName = "Root"; // Name needs to be unique

#define STATION_SSID "Gabi2"
#define STATION_PASSWORD "plusnrg123"
// #define STATION_PORT 5555
// uint8_t station_ip[4] = {0, 0, 0, 0}; // IP of the server

// prototypes
Scheduler userScheduler; // to control your personal task
void receivedCallback(uint32_t from, String &msg);
void callFunctionToSendData();
void callFunctionToConnectWifi();
void firstMinuteDoNothingThenAdd();
int minuteMultiply = 1;
Task runFirstMinuteDoNothing(TASK_SECOND * 1, 40, &firstMinuteDoNothingThenAdd);
Task taskSendMessage(TASK_SECOND *minuteMultiply, 1, &callFunctionToSendData);    // start with a one second interval
Task taskConnectWifi(TASK_MINUTE *minuteMultiply, 2, &callFunctionToConnectWifi); // start with a one second interval

namedMesh mesh;

void setup()
{
  Serial.begin(115200);

  // mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE | DEBUG);
  // mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | MSG_TYPES | REMOTE);
  mesh.setDebugMsgTypes(
      ERROR | STARTUP |
      CONNECTION); // set before init() so that you can see startup messages

  // Channel set to 6. Make sure to use the same channel for your mesh and for
  // you other network (STATION_SSID)
  // mesh.init(MESH_PREFIX, MESH_PASSWORD, MESH_PORT, WIFI_AP, 6);
  mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, 6);
  // Setup over the air update support
  mesh.initOTAReceive("bridge");
  mesh.setName(nodeName); // This needs to be an unique name!

  // mesh.stationManual(STATION_SSID, STATION_PASSWORD, STATION_PORT,
  // station_ip);
  // mesh.stationManual(STATION_SSID, STATION_PASSWORD);
  // Bridge node, should (in most cases) be a root node. See [the
  // wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation)
  // for some background
  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root,
  // so call this on all nodes
  mesh.setContainsRoot(true);

  mesh.onReceive(&receivedCallback);
  userScheduler.addTask(runFirstMinuteDoNothing);
  runFirstMinuteDoNothing.enable();
}

void loop()
{
  mesh.update();
}

void receivedCallback(uint32_t from, String &msg)
{
  Serial.printf("bridge: Received from %u msg=%s\n", from, msg.c_str());
  functionUpdate(msg.c_str());
}

void callFunctionToConnectWifi()
{
  // Serial.println("callFunctionToSendData");
  mesh.stationManual(STATION_SSID, STATION_PASSWORD);
  if (taskConnectWifi.isLastIteration())
  {
    userScheduler.addTask(taskSendMessage);
    taskSendMessage.enable();
  }
}

void callFunctionToSendData()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    sendDataAt58second();
  }
  // taskSendMessage.setInterval(TASK_MINUTE * minuteMultiply); // between 1 and 5 seconds
  if (taskSendMessage.isLastIteration())
  {
    // Serial.println("callFunctionToSendData isLastIteration");
    ESP.restart();
  }
}

void firstMinuteDoNothingThenAdd()
{
  // Serial.println("firstMinuteDoNothingThenAdd");
  if (runFirstMinuteDoNothing.isLastIteration())
  {
    // Serial.println("firstMinuteDoNothingThenAdd isLastIteration");
    userScheduler.addTask(taskConnectWifi);
    taskConnectWifi.enable();
  }
}