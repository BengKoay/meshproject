#include <Arduino.h>

//************************************************************
// this is a simple example that uses the painlessMesh library
//
// 1. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 2. prints anything it receives to Serial.print
//
//
//************************************************************
#include "painlessMesh.h"

// define sensor input
#define RXD2 16 // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
#define TXD2 17 // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)

// #define MESH_PREFIX "whateverYouLike"
// #define MESH_PASSWORD "somethingSneaky"
#define MESH_PREFIX "DryerA1."
#define MESH_PASSWORD "Password"
#define MESH_PORT 5555

unsigned long myTime;

Scheduler userScheduler; // to control your personal task
painlessMesh mesh;

// User stub
void sendMessage(); // Prototype so PlatformIO doesn't complain

Task taskSendMessage(TASK_SECOND * 1, TASK_FOREVER, &sendMessage);

void sendMessage()
{
  String msg = "Hello from root ";
  msg += mesh.getNodeId();
  // mesh.sendBroadcast( msg );
  taskSendMessage.setInterval(TASK_SECOND * 10);
  // Serial.println(" myFreeMemory: " + String(ESP.getFreeHeap()));

  if (ESP.getFreeHeap() > 230000)
  {

    // Serial.println( "Memory Okay");
  }
  else
  {

    Serial.println("-----------------------------------------------------------> Memory restart");
    ESP.restart();
  };
}

// Needed for painless library
void receivedCallback(uint32_t from, String &msg)
{
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
  String msg2 = "This Message is from ROOT ------> establish connection with Root";
  mesh.sendSingle(from, msg2);
  // receive msg from node, send Serial to Master

  // send Serial

  // send via serial
  // serializeJson(msg.c_str(), Serial2);

  DynamicJsonDocument tempStore(4096);

  DynamicJsonDocument globalDoc(4096);
  deserializeJson(tempStore, msg);
  globalDoc.to<JsonArray>(); // empty array []
  globalDoc.add(tempStore);  // append array [{data}]
  serializeJson(globalDoc, Serial2);
}

void newConnectionCallback(uint32_t nodeId)
{
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback()
{
  Serial.printf("Changed connections\n");
}

void nodeTimeAdjustedCallback(int32_t offset)
{
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

void setup()
{
  Serial.begin(115200);

  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  // mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes(ERROR | STARTUP); // set before init() so that you can see startup messages

  mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  userScheduler.addTask(taskSendMessage);
  taskSendMessage.enable();

  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
  mesh.setContainsRoot(true);

  Serial.println("mesh.getNodeId()");
  Serial.println(mesh.getNodeId());
}

void loop()
{
  // it will run the user scheduler as well
  mesh.update();
  // Serial.print("Time: ");
  // myTime = millis();
  //
  // Serial.println(myTime);
}