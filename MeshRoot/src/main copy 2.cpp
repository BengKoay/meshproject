// //************************************************************
// // this is a simple example that uses the painlessMesh library to
// // connect to a node on another network. Please see the WIKI on gitlab
// // for more details
// // https://gitlab.com/painlessMesh/painlessMesh/wikis/bridge-between-mesh-and-another-network
// //************************************************************

// // Mesh Bridge

// // start by Station mode for node for 60 seconds
// // during 1st 60 seconds, takes 10 seconds to initialize then start receiving from Mesh Node, Mesh Node sending every 5 seconds
// // after 1st 60 seconds, start connecting to AP Wifi for internet to push
// // after 1st push, may notice instability, which can be avoided by reboot ESP
// // after 3 * 60 seconds, reboot ESP, or else it will not be responsive while connecting to both Wifi & node Station
// // #include "painlessMesh.h"
// #include "namedMesh.h"
// // #include "functionUpdate.h"
// // #include "WiFi.h"

// // define sensor input
// #define RXD2 16 // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
// #define TXD2 17 // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)

// #define MESH_PREFIX "DryerA5."
// #define MESH_PASSWORD "Password"
// #define MESH_PORT 5555
// String nodeName = "Root"; // Name needs to be unique

// // #define STATION_SSID "Gabi2"
// // #define STATION_PASSWORD "plusnrg123"
// // #define STATION_PORT 5555
// // uint8_t station_ip[4] = {0, 0, 0, 0}; // IP of the server

// // prototypes
// Scheduler userScheduler; // to control your personal task
// void receivedCallback(uint32_t from, String &msg);
// // void callFunctionToSendData();
// // void callFunctionToConnectWifi();
// // void firstMinuteDoNothingThenAdd();
// void sendEveryMinute();
// void restartAfterTwoHours();
// void functionUpdate(const char *payload);
// int minuteMultiply = 1;
// DynamicJsonDocument globalDoc(4096);
// // Task runFirstMinuteDoNothing(TASK_SECOND * 1, 40, &firstMinuteDoNothingThenAdd);
// // Task taskSendMessage(TASK_SECOND *minuteMultiply, 1, &callFunctionToSendData);    // start with a one second interval
// // Task taskConnectWifi(TASK_MINUTE *minuteMultiply, 2, &callFunctionToConnectWifi); // start with a one second interval
// Task taskEveryMinute(TASK_MINUTE * 1, TASK_FOREVER, &sendEveryMinute);
// Task taskRestart(TASK_MINUTE * 1, 120, &restartAfterTwoHours);

// namedMesh mesh;

// void setup()
// {
//   Serial.begin(115200);
//   Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);

//   // mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE | DEBUG);
//   // mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | MSG_TYPES | REMOTE);
//   mesh.setDebugMsgTypes(
//       ERROR | STARTUP |
//       CONNECTION); // set before init() so that you can see startup messages

//   // Channel set to 6. Make sure to use the same channel for your mesh and for
//   // you other network (STATION_SSID)
//   // mesh.init(MESH_PREFIX, MESH_PASSWORD, MESH_PORT, WIFI_AP, 6);
//   // mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, 6);
//   mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT);
//   // Setup over the air update support
//   // mesh.initOTAReceive("bridge");
//   mesh.setName(nodeName); // This needs to be an unique name!

//   // mesh.stationManual(STATION_SSID, STATION_PASSWORD, STATION_PORT,
//   // station_ip);
//   // mesh.stationManual(STATION_SSID, STATION_PASSWORD);
//   // Bridge node, should (in most cases) be a root node. See [the
//   // wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation)
//   // for some background
//   // mesh.setRoot(true);
//   // This node and all other nodes should ideally know the mesh contains a root,
//   // so call this on all nodes
//   // mesh.setContainsRoot(true);

//   mesh.onReceive(&receivedCallback);
//   userScheduler.addTask(taskEveryMinute);
//   taskEveryMinute.enable();
//   userScheduler.addTask(taskRestart);
//   taskRestart.enable();
//   // userScheduler.addTask(runFirstMinuteDoNothing);
//   // runFirstMinuteDoNothing.enable();
// }

// void loop()
// {
//   mesh.update();
// }

// void receivedCallback(uint32_t from, String &msg)
// {
//   Serial.printf("bridge: Received from %u msg=%s\n", from, msg.c_str());
//   functionUpdate(msg.c_str());
// }

// // void callFunctionToConnectWifi()
// // {
// //   // Serial.println("callFunctionToSendData");
// //   // mesh.stationManual(STATION_SSID, STATION_PASSWORD);
// //   if (taskConnectWifi.isLastIteration())
// //   {
// //     userScheduler.addTask(taskSendMessage);
// //     taskSendMessage.enable();
// //   }
// // }

// void restartAfterTwoHours()
// {
//   if (taskRestart.isLastIteration())
//   {
//     // Serial.println("callFunctionToSendData isLastIteration");
//     ESP.restart();
//   }
// }
// // void callFunctionToSendData()
// // {
// //   if (WiFi.status() == WL_CONNECTED)
// //   {
// //     // sendDataAt58second();
// //   }
// //   // taskSendMessage.setInterval(TASK_MINUTE * minuteMultiply); // between 1 and 5 seconds
// //   if (taskSendMessage.isLastIteration())
// //   {
// //     // Serial.println("callFunctionToSendData isLastIteration");
// //     ESP.restart();
// //   }
// // }

// // void firstMinuteDoNothingThenAdd()
// // {
// //   // Serial.println("firstMinuteDoNothingThenAdd");
// //   if (runFirstMinuteDoNothing.isLastIteration())
// //   {
// //     // Serial.println("firstMinuteDoNothingThenAdd isLastIteration");
// //     userScheduler.addTask(taskConnectWifi);
// //     taskConnectWifi.enable();
// //   }
// // }

// void sendEveryMinute()
// {
//   serializeJsonPretty(globalDoc, Serial);
//   Serial.println("globalDoc");

//   // send via serial
//   serializeJson(globalDoc, Serial2);
//   Serial.println("Serial2");

//   globalDoc.clear(); // clears array after sent
// }

// void functionUpdate(const char *payload)
// {
//   DynamicJsonDocument tempStore(4096); // temporarily store payload to process

//   // 1. copy incoming payload to temp payload
//   // incoming payload = payload
//   // 2. loop collecting payload
//   // 3. in loop, check each JSON object if equal to temp payload
//   // 4. if not equal, add.
//   // 5. if equal, replace
//   deserializeJson(tempStore, payload);
//   // serializeJsonPretty(tempStore, Serial);
//   // Serial.println("tempStore");
//   if (globalDoc.isNull())
//   {
//     globalDoc.to<JsonArray>(); // declare globalDoc a JSON array
//   }
//   if (!globalDoc.isNull())
//   {
//     // int globalDoc_length = globalDoc.size();
//     // for (int i = 0; i < globalDoc_length; i++)
//     // {
//     // serializeJsonPretty(globalDoc[i], Serial);
//     // Serial.println("globalDoc Serial");
//     // serializeJsonPretty(tempStore, Serial);
//     // Serial.println("tempStore Serial");
//     // Serial.println("strcmp");
//     // Serial.println(strcmp(globalDoc[i]["name"], tempStore["name"]));

//     // DynamicJsonDocument loopPayload(768);
//     // DeserializationError error = deserializeJson(loopPayload, globalDoc[i]);
//     // if (error)
//     // {
//     //     Serial.print(F("deserializeJson() failed: "));
//     //     Serial.println(error.f_str());
//     //     return;
//     // }
//     // Serial.println(i);
//     // deserializeJson(globalDoc[i]["name"], loopPayload);
//     // Serial.println(i);
//     // deserializeJson(tempStore["name"], loopPayload);
//     // Serial.println(i);
//     // deserializeJson(tempStore, loopPayload);
//     // Serial.println(i);

//     // const char *name = globalDoc[i]["name"];
//     // Serial.println(name.c_str());
//     if ((strcmp(tempStore["name"], "Dryer4S1") == 0) && globalDoc[0].isNull())
//     {
//       globalDoc[0] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S2") == 0) && globalDoc[1].isNull())

//     {
//       globalDoc[1] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S3") == 0) && globalDoc[2].isNull())
//     {
//       globalDoc[2] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S4") == 0) && globalDoc[3].isNull())
//     {
//       globalDoc[3] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S5") == 0) && globalDoc[4].isNull())
//     {
//       globalDoc[4] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S6") == 0) && globalDoc[5].isNull())
//     {
//       globalDoc[5] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S7") == 0) && globalDoc[6].isNull())
//     {
//       globalDoc[6] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S8") == 0) && globalDoc[7].isNull())
//     {
//       globalDoc[7] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S9") == 0) && globalDoc[8].isNull())
//     {
//       globalDoc[8] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S10") == 0) && globalDoc[9].isNull())
//     {
//       globalDoc[9] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S11") == 0) && globalDoc[10].isNull())
//     {
//       globalDoc[10] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S12") == 0) && globalDoc[11].isNull())
//     {
//       globalDoc[11] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S13") == 0) && globalDoc[12].isNull())
//     {
//       globalDoc[12] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S14") == 0) && globalDoc[13].isNull())
//     {
//       globalDoc[13] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S15") == 0) && globalDoc[14].isNull())
//     {
//       globalDoc[14] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S16") == 0) && globalDoc[15].isNull())
//     {
//       globalDoc[15] = tempStore.as<JsonObject>();
//     }
//     if ((strcmp(tempStore["name"], "Dryer4S17") == 0) && globalDoc[16].isNull())
//     {
//       globalDoc[16] = tempStore.as<JsonObject>();
//     }

//     // if (strcmp(globalDoc[i]["name"], tempStore["name"]) == 0) // if strcmp equal return 0, !strcmp equal return 1
//     // {

//     //     // Serial.println("strcmp equal, if equal, replace object");
//     //     // globalDoc[i] = tempStore; // if equal, replace object
//     //     break;
//     // }
//     // strcmp(globalDoc[i]["name"], tempStore["name"]) != 0 means not same
//     // else if (strcmp(globalDoc[i]["name"], tempStore["name"]) != 0 && ((i + 1) == globalDoc_length))
//     // {
//     //     // Serial.println("add");
//     //     globalDoc.add(tempStore);
//     //     break;
//     // }
//     // else
//     // {
//     //     // Serial.println("strcmp not equal, not last, skip");
//     //     // globalDoc.add(tempStore); // if not equal, add to global
//     // }
//     // }
//   }
//   // Serial.print("globalDoc size: ");
//   // Serial.println(globalDoc.size());
// }
