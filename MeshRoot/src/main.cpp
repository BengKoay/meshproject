//************************************************************
// this is a simple example that uses the painlessMesh library to
// connect to a node on another network. Please see the WIKI on gitlab
// for more details
// https://gitlab.com/painlessMesh/painlessMesh/wikis/bridge-between-mesh-and-another-network
//************************************************************

// Mesh Bridge

// start by Station mode for node for 60 seconds
// during 1st 60 seconds, takes 10 seconds to initialize then start receiving from Mesh Node, Mesh Node sending every 5 seconds
// after 1st 60 seconds, start connecting to AP Wifi for internet to push
// after 1st push, may notice instability, which can be avoided by reboot ESP
// after 3 * 60 seconds, reboot ESP, or else it will not be responsive while connecting to both Wifi & node Station
// #include "painlessMesh.h"
#include "namedMesh.h"
// #include "functionUpdate.h"
// #include "WiFi.h"

// define sensor input
#define RXD2 16 // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
#define TXD2 17 // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)

// #define MESH_PREFIX "Dryer4"
// #define MESH_PASSWORD "passwordPassword"
#define MESH_PREFIX "DryerA14."
#define MESH_PASSWORD "Password"
// #define MESH_PREFIX "testmesh"
// #define MESH_PASSWORD "testmesh"
#define MESH_PORT 5555
String nodeName = "Root"; // Name needs to be unique

// #define STATION_SSID "Gabi2"
// #define STATION_PASSWORD "plusnrg123"
// #define STATION_PORT 5555
// uint8_t station_ip[4] = {0, 0, 0, 0}; // IP of the server

// prototypes
Scheduler userScheduler; // to control your personal task
void receivedCallback(uint32_t from, String &msg);
// void callFunctionToSendData();
// void callFunctionToConnectWifi();
// void firstMinuteDoNothingThenAdd();
void sendEveryMinute();
void restartAfterOneHours();
void functionUpdate(const char *payload);
int minuteMultiply = 1;
DynamicJsonDocument globalDoc(4096);
// Task runFirstMinuteDoNothing(TASK_SECOND * 1, 40, &firstMinuteDoNothingThenAdd);
// Task taskSendMessage(TASK_SECOND *minuteMultiply, 1, &callFunctionToSendData);    // start with a one second interval
// Task taskConnectWifi(TASK_MINUTE *minuteMultiply, 2, &callFunctionToConnectWifi); // start with a one second interval
Task taskEveryMinute(TASK_MINUTE * 1, TASK_FOREVER, &sendEveryMinute);
Task taskRestart(TASK_MINUTE * 1, 60, &restartAfterOneHours);

namedMesh mesh;

void setup()
{
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);

  mesh.setDebugMsgTypes(ERROR | DEBUG | CONNECTION); // set before init() so that you can see startup messages
  // mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE | DEBUG);
  // mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | MSG_TYPES | REMOTE);
  // mesh.setDebugMsgTypes(
  //     ERROR | STARTUP |
  //     CONNECTION); // set before init() so that you can see startup messages

  // Channel set to 6. Make sure to use the same channel for your mesh and for
  // you other network (STATION_SSID)
  // mesh.init(MESH_PREFIX, MESH_PASSWORD, MESH_PORT, WIFI_AP, 6);
  // mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, 6);
  mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT);
  // Setup over the air update support
  // mesh.initOTAReceive("bridge");
  mesh.setName(nodeName); // This needs to be an unique name!

  // mesh.stationManual(STATION_SSID, STATION_PASSWORD, STATION_PORT,
  // station_ip);
  // mesh.stationManual(STATION_SSID, STATION_PASSWORD);
  // Bridge node, should (in most cases) be a root node. See [the
  // wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation)
  // for some background
  // mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root,
  // so call this on all nodes
  // mesh.setContainsRoot(true);

  mesh.onReceive(&receivedCallback);
  userScheduler.addTask(taskEveryMinute);
  taskEveryMinute.enable();
  userScheduler.addTask(taskRestart);
  taskRestart.enable();
  // userScheduler.addTask(runFirstMinuteDoNothing);
  // runFirstMinuteDoNothing.enable();
}

void loop()
{
  mesh.update();
}

void receivedCallback(uint32_t from, String &msg)
{
  Serial.printf("bridge: Received from %u msg=%s\n", from, msg.c_str());
  functionUpdate(msg.c_str());
}

// void callFunctionToConnectWifi()
// {
//   // Serial.println("callFunctionToSendData");
//   // mesh.stationManual(STATION_SSID, STATION_PASSWORD);
//   if (taskConnectWifi.isLastIteration())
//   {
//     userScheduler.addTask(taskSendMessage);
//     taskSendMessage.enable();
//   }
// }

void restartAfterOneHours()
{
  if (taskRestart.isLastIteration())
  {
    // Serial.println("callFunctionToSendData isLastIteration");
    ESP.restart();
  }
}
// void callFunctionToSendData()
// {
//   if (WiFi.status() == WL_CONNECTED)
//   {
//     // sendDataAt58second();
//   }
//   // taskSendMessage.setInterval(TASK_MINUTE * minuteMultiply); // between 1 and 5 seconds
//   if (taskSendMessage.isLastIteration())
//   {
//     // Serial.println("callFunctionToSendData isLastIteration");
//     ESP.restart();
//   }
// }

// void firstMinuteDoNothingThenAdd()
// {
//   // Serial.println("firstMinuteDoNothingThenAdd");
//   if (runFirstMinuteDoNothing.isLastIteration())
//   {
//     // Serial.println("firstMinuteDoNothingThenAdd isLastIteration");
//     userScheduler.addTask(taskConnectWifi);
//     taskConnectWifi.enable();
//   }
// }

void sendEveryMinute()
{
  serializeJsonPretty(globalDoc, Serial);
  Serial.println("globalDoc");

  // send via serial
  serializeJson(globalDoc, Serial2);
  Serial.println("Serial2");

  globalDoc.clear(); // clears array after sent
}

void functionUpdate(const char *payload)
{
  DynamicJsonDocument tempStore(4096); // temporarily store payload to process

  // 1. copy incoming payload to temp payload
  // incoming payload = payload
  // 2. loop collecting payload
  // 3. in loop, check each JSON object if equal to temp payload
  // 4. if not equal, add.
  // 5. if equal, replace
  deserializeJson(tempStore, payload);
  serializeJsonPretty(tempStore, Serial);
  Serial.println("tempStore");
  // Serial.println(globalDoc.isNull());
  // Serial.println("globalDoc.isNull()");
  if (globalDoc.isNull())
  {
    globalDoc.to<JsonArray>(); // declare globalDoc a JSON array
  }
  if (!globalDoc.isNull())
  {
    // globalDoc.add(tempStore); // if not equal, add to global

    int globalDoc_length = globalDoc.size();
    if (globalDoc.size() == 0)
      globalDoc.add(tempStore); // if not equal, add to global

    for (int i = 0; i < globalDoc_length; i++)
    {
      // serializeJsonPretty(globalDoc[i], Serial);
      // Serial.println("globalDoc Serial");
      // serializeJsonPretty(tempStore, Serial);
      // Serial.println("tempStore Serial");
      // Serial.println("strcmp");
      // Serial.println(strcmp(globalDoc[i]["name"], tempStore["name"]));

      // DynamicJsonDocument loopPayload(768);
      // DeserializationError error = deserializeJson(loopPayload, globalDoc[i]);
      // if (error)
      // {
      //     Serial.print(F("deserializeJson() failed: "));
      //     Serial.println(error.f_str());
      //     return;
      // }
      // Serial.println(i);
      // deserializeJson(globalDoc[i]["name"], loopPayload);
      // Serial.println(i);
      // deserializeJson(tempStore["name"], loopPayload);
      // Serial.println(i);
      // deserializeJson(tempStore, loopPayload);
      // Serial.println(i);

      // const char *name = globalDoc[i]["name"];
      // Serial.println(name.c_str());

      // if (strcmp(globalDoc[i]["name"], tempStore["name"]) == 0) // if strcmp equal return 0, !strcmp equal return 1
      // {

      //     // Serial.println("strcmp equal, if equal, replace object");
      //     // globalDoc[i] = tempStore; // if equal, replace object
      //     break;
      // }
      // // strcmp(globalDoc[i]["name"], tempStore["name"]) != 0 means not same
      // else if (strcmp(globalDoc[i]["name"], tempStore["name"]) != 0 && ((i + 1) == globalDoc_length))
      // {
      //     // Serial.println("add");
      //     globalDoc.add(tempStore);
      //     break;
      // }
      // else
      // {
      //     // Serial.println("strcmp not equal, not last, skip");
      //     // globalDoc.add(tempStore); // if not equal, add to global
      // }
      // code to add sensor, loop?
      // declare JSON
      // DynamicJsonDocument eachSensor(1024);
      // if ((strcmp(tempStore["name"], "Sensor1") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 1;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[0]["deviceId"];
      //   // globalDoc[0] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor2") == 0))

      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 2;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[1] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor3") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 3;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[2] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor4") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 4;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[3] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor5") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 5;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[4] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor6") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 6;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[5] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor7") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 7;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[6] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor8") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 8;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[7] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor9") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 9;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[8] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor10") == 0))
      // {
      //   eachSensor["deviceId"] = "6bfaf4e8-50e6-4120-9cd9-d2639be9bded";
      //   eachSensor["ref"] = 10;
      //   eachSensor["temperature"] = tempStore["temperature"];
      //   eachSensor["humidity"] = tempStore["humidity"];
      //   // globalDoc.add(eachSensor);
      //   // globalDoc[9] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor11") == 0) && globalDoc[10].isNull())
      // {
      //   globalDoc[10] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor12") == 0) && globalDoc[11].isNull())
      // {
      //   globalDoc[11] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor13") == 0) && globalDoc[12].isNull())
      // {
      //   globalDoc[12] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor14") == 0) && globalDoc[13].isNull())
      // {
      //   globalDoc[13] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor15") == 0) && globalDoc[14].isNull())
      // {
      //   globalDoc[14] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor16") == 0) && globalDoc[15].isNull())
      // {
      //   globalDoc[15] = tempStore.as<JsonObject>();
      // }
      // if ((strcmp(tempStore["name"], "Sensor17") == 0) && globalDoc[16].isNull())
      // {
      //   globalDoc[16] = tempStore.as<JsonObject>();
      // }
      // Serial.println(globalDoc[i]["ref"]);

      // for (int i = 0; i < 10; i++)
      // {

      // if (tempStore["ref"] == i + 1 && globalDoc[i].isNull())
      // {
      //   globalDoc[i] = tempStore.as<JsonObject>();
      // }
      // int globalDocRef = globalDoc[i]["ref"];
      // Serial.println("globalDocRef");
      // Serial.println(globalDocRef);
      // if (globalDocRef == i + 1)
      // {
      //   globalDoc[i] = tempStore.as<JsonObject>();
      // }

      // globalDoc.add(tempStore);
      // serializeJsonPretty(globalDoc, Serial);
      // Serial.println("globalDoc");
      // append/push incoming payload from "tempStore" to "globalDoc"
      if ((globalDoc[i]["ref"] == tempStore["ref"]))
      // if (strcmp(globalDoc[i]["name"], tempStore["name"]) == 0) // if strcmp equal return 0, !strcmp equal return 1
      {

        Serial.println("strcmp equal, if equal, replace object");
        // globalDoc[i] = tempStore; // if equal, replace object
        break;
      }
      // strcmp(globalDoc[i]["name"], tempStore["name"]) != 0 means not same
      else if (globalDoc[i]["ref"] != tempStore["ref"] && ((i + 1) == globalDoc_length))
      // else if (strcmp(globalDoc[i]["name"], tempStore["name"]) != 0 && ((i + 1) == globalDoc_length))
      {
        Serial.println("add");
        // globalDoc[i] = tempStore.as<JsonObject>();
        globalDoc.add(tempStore);
        break;
      }
      else
      {
        Serial.println("strcmp not equal, not last, skip");
        // globalDoc.add(tempStore); // if not equal, add to global
      }
      // }
      // globalDoc.add(eachSensor);
      // eachSensor.clear();
    }
  }
  // Serial.print("globalDoc size: ");
  // Serial.println(globalDoc.size());
}
