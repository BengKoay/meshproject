

/*
  Rui Santos
  Complete project details
   - Arduino IDE: https://RandomNerdTutorials.com/esp32-ota-over-the-air-arduino/
   - VS Code: https://RandomNerdTutorials.com/esp32-ota-over-the-air-vs-code/
  
  This sketch shows a Basic example from the AsyncElegantOTA library: ESP32_Async_Demo
  https://github.com/ayushsharma82/AsyncElegantOTA
*/

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>

// prototype

String processor(const String &var);

String outputState(int output);

const char *ssid = "testAP";
const char *password = "REPLACE_WITH_YOUR_PASSWORD";

const char *PARAM_INPUT_1 = "output";
const char *PARAM_INPUT_2 = "state";
AsyncWebServer server(80);

const int ledPin = 2;
int ledState = LOW;
unsigned long previousMillis = 0;
unsigned long interval = 1000;

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px}
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.slider {background-color: #b30000}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>ESP Web Server</h2>
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/blink?output="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/blink?output="+element.id+"&state=0", true); }
  xhr.send();
}
</script>
</body>
<button onclick="location.href = '/update';" id="myButton" class="float-left submit-button" >update</button>

</html>
)rawliteral";

// HTML web page to handle 3 input fields (input1, input2, input3)
const char inputssid_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>ESP Input Form</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
  <form action="/get">
    input1: <input type="text" name="input1">
    <input type="submit" value="Submit">
  </form><br>
  <form action="/get">
    input2: <input type="text" name="input2">
    <input type="submit" value="Submit">
  </form><br>
  <form action="/get">
    input3: <input type="text" name="input3">
    <input type="submit" value="Submit">
  </form>
</body></html>)rawliteral";

// Replaces placeholder with button section in your web page
String processor(const String &var)
{
  //Serial.println(var);
  if (var == "BUTTONPLACEHOLDER")
  {
    String buttons = "";
    buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
    return buttons;
  }
  return String();
}

String outputState(int output)
{
  if (digitalRead(output))
  {
    return "checked";
  }
  else
  {
    return "";
  }
}

void setup(void)
{
  Serial.begin(115200);
  byte mac[6];
  WiFi.macAddress(mac);
  String uniq = "testAP_" + String(mac[0], HEX) + String(mac[1], HEX) + String(mac[2], HEX) + String(mac[3], HEX); // +String(mac[4], HEX) + String(mac[5], HEX);
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(ssid, password);
  WiFi.softAP(uniq.c_str(), "");
  Serial.println("");

  // Wait for connection
  // while (WiFi.status() != WL_CONNECTED)
  while ((WiFi.softAPgetStationNum() == 0))
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });

  // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
  server.on("/blink", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String inputMessage1;
              String inputMessage2;
              // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
              if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
              {
                inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
                inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
                digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
              }
              else
              {
                inputMessage1 = "No message sent";
                inputMessage2 = "No message sent";
              }
              Serial.print("GPIO: ");
              Serial.print(inputMessage1);
              Serial.print(" - Set to: ");
              Serial.println(inputMessage2);
              request->send(200, "text/plain", "OK");
            });
  server.on("/redirect/internal", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->redirect("/"); });

  AsyncElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");
  pinMode(ledPin, OUTPUT);
}

void loop(void)
{
  AsyncElegantOTA.loop();

  // blink without delay code
  // unsigned long currentMillis = millis();
  // if (currentMillis - previousMillis > interval)
  // {
  //   previousMillis = currentMillis;
  //   if (ledState == LOW)
  //     ledState = HIGH;
  //   else
  //     ledState = LOW;
  //   digitalWrite(ledPin, ledState);
  // }
}

// #include <Arduino.h>

// #include <WiFi.h>
// #include <WiFiClient.h>
// #include <WebServer.h>
// #include <ESPmDNS.h>
// #include <Update.h>

// const char *host = "esp32";
// const char *ssid = "testAP";
// const char *password = "309678470";

// WebServer server(80);

// /*
//  * Login page
//  */

// const char *loginIndex =
//     "<form name='loginForm'>"
//     "<table width='20%' bgcolor='A09F9F' align='center'>"
//     "<tr>"
//     "<td colspan=2>"
//     "<center><font size=4><b>ESP32 Login Page</b></font></center>"
//     "<br>"
//     "</td>"
//     "<br>"
//     "<br>"
//     "</tr>"
//     "<tr>"
//     "<td>Username:</td>"
//     "<td><input type='text' size=25 name='userid'><br></td>"
//     "</tr>"
//     "<br>"
//     "<br>"
//     "<tr>"
//     "<td>Password:</td>"
//     "<td><input type='Password' size=25 name='pwd'><br></td>"
//     "<br>"
//     "<br>"
//     "</tr>"
//     "<tr>"
//     "<td><input type='submit' onclick='check(this.form)' value='Login'></td>"
//     "</tr>"
//     "</table>"
//     "</form>"
//     "<script>"
//     "function check(form)"
//     "{"
//     "if(form.userid.value=='admin' && form.pwd.value=='admin')"
//     "{"
//     "window.open('/serverIndex')"
//     "}"
//     "else"
//     "{"
//     " alert('Error Password or Username')/*displays error message*/"
//     "}"
//     "}"
//     "</script>";

// /*
//  * Server Index Page
//  */

// const char *serverIndex =
//     "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
//     "<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
//     "<input type='file' name='update'>"
//     "<input type='submit' value='Update'>"
//     "</form>"
//     "<div id='prg'>progress: 0%</div>"
//     "<script>"
//     "$('form').submit(function(e){"
//     "e.preventDefault();"
//     "var form = $('#upload_form')[0];"
//     "var data = new FormData(form);"
//     " $.ajax({"
//     "url: '/update',"
//     "type: 'POST',"
//     "data: data,"
//     "contentType: false,"
//     "processData:false,"
//     "xhr: function() {"
//     "var xhr = new window.XMLHttpRequest();"
//     "xhr.upload.addEventListener('progress', function(evt) {"
//     "if (evt.lengthComputable) {"
//     "var per = evt.loaded / evt.total;"
//     "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
//     "}"
//     "}, false);"
//     "return xhr;"
//     "},"
//     "success:function(d, s) {"
//     "console.log('success!')"
//     "},"
//     "error: function (a, b, c) {"
//     "}"
//     "});"
//     "});"
//     "</script>";
// const int ledPin = 13;
// int ledState = LOW;
// unsigned long previousMillis = 0;
// unsigned long interval = 1000;
// /*
//  * setup function
//  */
// void setup(void)
// {
//   Serial.begin(115200);
//   // Connect to Wi-Fi network with SSID and password
//   Serial.print("Setting AP (Access Point)…");
//   // Remove the password parameter, if you want the AP (Access Point) to be open
//   // WiFi.softAP(ssid, password);
//   WiFi.softAP(ssid, "");
//   // Connect to WiFi network
//   // WiFi.begin(ssid, password);
//   Serial.println("");

//   // Wait for connection
//   while ((WiFi.softAPgetStationNum() == 0))
//   {
//     delay(500);
//     Serial.print(".");
//   }
//   Serial.println("");
//   Serial.print("Connected to ");
//   Serial.println(ssid);
//   Serial.print("IP address: ");
//   Serial.println(WiFi.localIP());

//   /*use mdns for host name resolution*/
//   if (!MDNS.begin(host))
//   { //http://esp32.local
//     Serial.println("Error setting up MDNS responder!");
//     while (1)
//     {
//       delay(1000);
//     }
//   }
//   Serial.println("mDNS responder started");
//   /*return index page which is stored in serverIndex */
//   server.on("/", HTTP_GET, []()
//             {
//               server.sendHeader("Connection", "close");
//               server.send(200, "text/html", loginIndex);
//             });
//   server.on("/serverIndex", HTTP_GET, []()
//             {
//               server.sendHeader("Connection", "close");
//               server.send(200, "text/html", serverIndex);
//             });
//   /*handling uploading firmware file */
//   server.on(
//       "/update", HTTP_POST, []()
//       {
//         server.sendHeader("Connection", "close");
//         server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
//         ESP.restart();
//       },
//       []()
//       {
//         HTTPUpload &upload = server.upload();
//         if (upload.status == UPLOAD_FILE_START)
//         {
//           Serial.printf("Update: %s\n", upload.filename.c_str());
//           if (!Update.begin(UPDATE_SIZE_UNKNOWN))
//           { //start with max available size
//             Update.printError(Serial);
//           }
//         }
//         else if (upload.status == UPLOAD_FILE_WRITE)
//         {
//           /* flashing firmware to ESP*/
//           if (Update.write(upload.buf, upload.currentSize) != upload.currentSize)
//           {
//             Update.printError(Serial);
//           }
//         }
//         else if (upload.status == UPLOAD_FILE_END)
//         {
//           if (Update.end(true))
//           { //true to set the size to the current progress
//             Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
//           }
//           else
//           {
//             Update.printError(Serial);
//           }
//         }
//       });
//   server.begin();

//   pinMode(ledPin, OUTPUT);
// }

// void loop(void)
// {
//   server.handleClient();
//   delay(1);

//   unsigned long currentMillis = millis();
//   if (currentMillis - previousMillis > interval)
//   {
//     previousMillis = currentMillis;
//     if (ledState == LOW)
//       ledState = HIGH;
//     else
//       ledState = LOW;
//     digitalWrite(ledPin, ledState);
//   }
// }

// /*********
//   Rui Santos
//   Complete project details at https://RandomNerdTutorials.com/esp32-async-web-server-espasyncwebserver-library/
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
// *********/

// // Import required libraries
// #include <WiFi.h>
// #include <AsyncTCP.h>
// #include <ESPAsyncWebServer.h>

// // Replace with your network credentials
// const char* ssid = "REPLACE_WITH_YOUR_SSID";
// const char* password = "REPLACE_WITH_YOUR_PASSWORD";

// const char* PARAM_INPUT_1 = "output";
// const char* PARAM_INPUT_2 = "state";

// // Create AsyncWebServer object on port 80
// AsyncWebServer server(80);

// const char index_html[] PROGMEM = R"rawliteral(
// <!DOCTYPE HTML><html>
// <head>
//   <title>ESP Web Server</title>
//   <meta name="viewport" content="width=device-width, initial-scale=1">
//   <link rel="icon" href="data:,">
//   <style>
//     html {font-family: Arial; display: inline-block; text-align: center;}
//     h2 {font-size: 3.0rem;}
//     p {font-size: 3.0rem;}
//     body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
//     .switch {position: relative; display: inline-block; width: 120px; height: 68px}
//     .switch input {display: none}
//     .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
//     .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
//     input:checked+.slider {background-color: #b30000}
//     input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
//   </style>
// </head>
// <body>
//   <h2>ESP Web Server</h2>
//   %BUTTONPLACEHOLDER%
// <script>function toggleCheckbox(element) {
//   var xhr = new XMLHttpRequest();
//   if(element.checked){ xhr.open("GET", "/update?output="+element.id+"&state=1", true); }
//   else { xhr.open("GET", "/update?output="+element.id+"&state=0", true); }
//   xhr.send();
// }
// </script>
// </body>
// </html>
// )rawliteral";

// // Replaces placeholder with button section in your web page
// String processor(const String& var){
//   //Serial.println(var);
//   if(var == "BUTTONPLACEHOLDER"){
//     String buttons = "";
//     buttons += "<h4>Output - GPIO 2</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"2\" " + outputState(2) + "><span class=\"slider\"></span></label>";
//     buttons += "<h4>Output - GPIO 4</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"4\" " + outputState(4) + "><span class=\"slider\"></span></label>";
//     buttons += "<h4>Output - GPIO 33</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"33\" " + outputState(33) + "><span class=\"slider\"></span></label>";
//     return buttons;
//   }
//   return String();
// }

// String outputState(int output){
//   if(digitalRead(output)){
//     return "checked";
//   }
//   else {
//     return "";
//   }
// }

// void setup(){
//   // Serial port for debugging purposes
//   Serial.begin(115200);

//   pinMode(2, OUTPUT);
//   digitalWrite(2, LOW);
//   pinMode(4, OUTPUT);
//   digitalWrite(4, LOW);
//   pinMode(33, OUTPUT);
//   digitalWrite(33, LOW);

//   // Connect to Wi-Fi
//   WiFi.begin(ssid, password);
//   while (WiFi.status() != WL_CONNECTED) {
//     delay(1000);
//     Serial.println("Connecting to WiFi..");
//   }

//   // Print ESP Local IP Address
//   Serial.println(WiFi.localIP());

//   // Route for root / web page
//   server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
//     request->send_P(200, "text/html", index_html, processor);
//   });

//   // Send a GET request to <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
//   server.on("/update", HTTP_GET, [] (AsyncWebServerRequest *request) {
//     String inputMessage1;
//     String inputMessage2;
//     // GET input1 value on <ESP_IP>/update?output=<inputMessage1>&state=<inputMessage2>
//     if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2)) {
//       inputMessage1 = request->getParam(PARAM_INPUT_1)->value();
//       inputMessage2 = request->getParam(PARAM_INPUT_2)->value();
//       digitalWrite(inputMessage1.toInt(), inputMessage2.toInt());
//     }
//     else {
//       inputMessage1 = "No message sent";
//       inputMessage2 = "No message sent";
//     }
//     Serial.print("GPIO: ");
//     Serial.print(inputMessage1);
//     Serial.print(" - Set to: ");
//     Serial.println(inputMessage2);
//     request->send(200, "text/plain", "OK");
//   });

//   // Start server
//   server.begin();
// }

// void loop() {

// }