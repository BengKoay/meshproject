#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>

// AWS sign library

#include <WiFiClientSecure.h>
#include <sha256.h>
#include <ESPRandom.h>

// Task Scheduler
#include <TaskScheduler.h>

// define UUID
// #define DEV_UUID "6ae1f782-9942-4cf5-9dd1-214a365ec8db"
// #define DEV_UUID "cb174863-f941-480d-8ea5-80f63fc05935"
// #define DEV_UUID "0d9e94e3-6cf0-4807-bcb4-05805493975f"
// #define DEV_UUID "b8a84ba8-f61b-403b-b5f5-d6ba89509347" // Dryer 5
// #define DEV_UUID "b8a84ba8-f61b-403b-b5f3-d6ba89509347" // Dryer 3
#define DEV_UUID "b8a84ba8-f61b-403b-b5f4-d6ba89509347" // Dryer 4

// wifi at Chantika

const char *ssid = "gabi";
const char *password = "gabi9999";

// wifi at testing home
// const char *ssid = "koaybb@unifi";
// const char *password = "309678470";

// AWS signature

// AWS sign
// TODO: put in preferences
const char *host = "9ry30zin92";
const char *service = "execute-api";
const char *region = "ap-southeast-1";
const char *TLD = "amazonaws.com";
const char *path = "/public/rawtmpdata";

const char *customFQDN; // reserved for future use

// AWS IAM configuration
// TODO: put in preferences
const char *awsKey = "AKIAVBSYQGXZE6POVWVK";
const char *awsSecret = "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";
const char *apiKey = "eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB";

// declare global document

DynamicJsonDocument globalDoc(4096);

// define sensor input
#define RXD2 16 // assign GPIO 16 as receiver pin (receive signal from transmitter pin from moisture sensor)
#define TXD2 17 // assign GPIO 17 as transmitter pin (transmit signal to receiver pin at moisture sensor)

// IPAddress local_IP(192, 168, 68, 10);
// IPAddress gateway(192, 168, 68, 1);
// IPAddress subnet(255, 255, 255, 0);
// IPAddress primaryDNS(192, 168, 0, 197);  //optional
// IPAddress secondaryDNS(192, 168, 68, 1); //optional

// prototype

Scheduler userScheduler; // to control your personal task
void fiveMinuteCheckWifi();
Task taskCheckWifi(TASK_MINUTE * 1, 2, &fiveMinuteCheckWifi);

void getDataFromSerial();

void sendDataAt58second();
bool sendData(JsonDocument &local_doc);

String createRequest(String method, String uri, String payload, String apiKey,
                     String contentType, String queryString);
String hexHash(uint8_t *hash);
String createCanonicalRequest(String method, String uri, String date,
                              String time, String payloadHash, String apiKey,
                              String queryString, String contentType);
String createCanonicalHeaders(String contentType, String date, String time,
                              String payloadHash, String apiKey);
String createRequestHeaders(String contentType, String date, String time,
                            String payload, String payloadHash, String apiKey,
                            String signature);
String FQDN();
String createStringToSign(String canonical_request, String date, String time);
String createSignature(String toSign, String date);

unsigned long printLocalTime();

// main code
void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  // config wifi static ip

  // if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  // // if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  // {
  //   Serial.println("STA Failed to configure");
  // }
  WiFi.begin(ssid, password);
  int restartCounter = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting to WiFi..");
    restartCounter++;
    if (restartCounter > 20)
      ESP.restart();
  }

  userScheduler.addTask(taskCheckWifi);
  taskCheckWifi.enable();
  Serial.println("Connected to the WiFi network");
  if (WiFi.status() == WL_CONNECTED)
    printLocalTime();
}

void loop()
{
  // put your main code here, to run repeatedly:
  getDataFromSerial();
}

void fiveMinuteCheckWifi()
{
  Serial.printf("fiveMinuteCheckWifi");
  if (WiFi.status() != WL_CONNECTED)
  {
  }
  if (taskCheckWifi.isLastIteration())
  {
    Serial.printf("fiveMinuteCheckWifi isLastIteration");
    ESP.restart();
  }
}

void getDataFromSerial()
{

  if (Serial2.available())
  {

    String input = Serial2.readString();
    Serial.print("Serial2 received: ");
    Serial.println(input); // local echo.. print out to see if correct

    // Read the JSON document from the "link" serial port
    DeserializationError err = deserializeJson(globalDoc, input);
    serializeJsonPretty(globalDoc, Serial);
    Serial.println("globalDoc");

    // push to AWS

    sendDataAt58second();

    if (err == DeserializationError::Ok)
    {

      // Print the values
      // (we must use as<T>() to resolve the ambiguity)
      // Serial.print("timestamp = ");
      // Serial.println(doc["timestamp"].as<long>());
      // Serial.print("value = ");
      // Serial.println(doc["value"].as<int>());
    }
    else
    {
      // Print error to the "debug" serial port
      Serial.print("deserializeJson() returned ");
      Serial.println(err.c_str());

      // Flush all bytes in the "link" serial port buffer
      while (Serial2.available() > 0)
        Serial2.read();
    }
  }
}

/*---------Function for Time----------------*/
unsigned long printLocalTime()
{
  // NTP server to request epoch time
  const char *ntpServer = "pool.ntp.org";
  configTime(0, 0, ntpServer);
  struct tm timeinfo;
  time_t now;
  if (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
    return (0);
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  time(&now);
  Serial.println(now);
  return now;
}
/*-----(END)----Function for Time----------------*/

// send data

void sendDataAt58second()
{

  DynamicJsonDocument doc(4096); // payload to send to AWS
  //declare UUID
  // Use an old-school array to create a v4 UUID
  uint8_t uuid_array[16];
  ESPRandom::uuid4(uuid_array);
  // Serial.println("UUID V4:");
  // Serial.println(ESPRandom::uuidToString(uuid_array));
  //  delay(500);
  // Allocate the JSON document
  // Use arduinojson.org/v6/assistant to compute the capacity.
  DynamicJsonDocument data(4096); //  payload to send to AWS
  // DynamicJsonDocument payloadStore(4096);
  // DynamicJsonDocument tempStore(4096);
  // deserializeJson(payloadStore, payload);
  // String topic = payloadStore["topic"];
  // String name = payloadStore["name"];

  doc["id"] = ESPRandom::uuidToString(uuid_array); // assign UUID to doc JSON object ["id"]

  data["deviceID"] = DEV_UUID;
  data["testing"] = globalDoc;
  data["timestamp"] = printLocalTime();
  String dataString;
  serializeJson(data, dataString);
  doc["data"] = dataString;

  // int secondsNow = timeinfo.tm_sec;
  // send data at 58th sec
  if (!globalDoc.isNull())
  {
    // Serial.println(secondsNow);
    // Serial.println("At 58th second send data");
    sendData(doc);
    globalDoc.clear(); // clears array after sent
    doc.clear();
  }
}
/*----(START)-Functions to create payload and send to server---------------*/
bool sendData(JsonDocument &local_doc)
{
  // HTTPS wrapper
  WiFiClientSecure client;
  if (WiFi.status() == WL_CONNECTED)
  {
    char payload[2560];
    serializeJson(local_doc, payload);
    Serial.println(payload);

    static uint32_t requests = 0;

    String url = String(host) + "." + String(service) + "." + String(region) +
                 "." + String(TLD);

    String request = createRequest("POST", String(path), payload,
                                   String(apiKey), "application/json", "");
    Serial.println(request);

    Serial.println("\nStarting connection to server...");
    client.setInsecure(); // skip verification
    client.setTimeout(5000);

    if (!client.connect(url.c_str(), 443))
    {
      Serial.println("Connection failed!");
      client.print(request);
      // return false;
    }
    else
    {
      Serial.println("Connected to server!");
      // Make a HTTP request:
      client.print(request);

      // Check HTTP status
      char status[32] = {0};
      client.readBytesUntil('\r', status, sizeof(status));
      // It should be "HTTP/1.0 200 OK" or "HTTP/1.1 200 OK"
      if (strcmp(status + 9, "200 OK") != 0)
      {
        Serial.print(F("Unexpected response: "));
        Serial.println(status);
        // if there are incoming bytes available
        // from the server, read them and print them:
        while (client.available())
        {
          char c = client.read();
          Serial.write(c);
        }
        client.stop();
        // return false;
      }
      // if there are incoming bytes available
      // from the server, read them and print them:
      while (client.available())
      {
        char c = client.read();
        Serial.write(c);
      }
      client.stop();
      // return true;
    }
    Serial.println();
    // HTTPClient http;
    // http.begin("https://ejfut1hft2.execute-api.ap-southeast-1.amazonaws.com/prod/rawdata"); //test server
    // http.addHeader("x-api-key", "HXnKw3QwOxkr0t8JOrFC25CfKihOU496sohWpwe0");
    // http.addHeader("content-type", "application/json");
    // int httpCode = http.POST(payload);
    // String response = http.getString();
    // Serial.println(httpCode);
    // Serial.println(response);
    // http.end();
  }
}
/*----(END)-Functions to create payload and send to server---------------*/

/*-------AWS API signing requests functions----------*/
String createRequest(String method, String uri, String payload, String apiKey,
                     String contentType, String queryString)
{
  char dateBuf[9], timeBuf[7];

  struct tm timeinfo;
  if (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
  }
  snprintf(dateBuf, sizeof(dateBuf), "%4d%02d%02d", (timeinfo.tm_year + 1900),
           (timeinfo.tm_mon + 1), (timeinfo.tm_mday));
  snprintf(timeBuf, sizeof(timeBuf), "%02d%02d%02d", (timeinfo.tm_hour),
           (timeinfo.tm_min), (timeinfo.tm_sec));
  String date(dateBuf);
  String time(timeBuf);

  Sha256.init();
  Sha256.print(payload);
  String payloadHash = hexHash(Sha256.result());

  String canonical_request = createCanonicalRequest(
      method, uri, date, time, payloadHash, apiKey, queryString, contentType);
  String string_to_sign = createStringToSign(canonical_request, date, time);
  String signature = createSignature(string_to_sign, date);
  String headers = createRequestHeaders(contentType, date, time, payload,
                                        payloadHash, apiKey, signature);

  String retval;
  retval += method + " " + "https://" + FQDN() + uri + " " + "HTTP/1.1\r\n";
  retval += headers + "\r\n";
  retval += payload + "\r\n\r\n";
  return retval;
}

String hexHash(uint8_t *hash)
{
  char hashStr[(HASH_LENGTH * 2) + 1];
  for (int i = 0; i < HASH_LENGTH; ++i)
  {
    sprintf(hashStr + 2 * i, "%02lx", 0xff & (unsigned long)hash[i]);
  }
  return String(hashStr);
}

String createCanonicalRequest(String method, String uri, String date,
                              String time, String payloadHash, String apiKey,
                              String queryString, String contentType)
{
  String retval;
  String _signedHeaders = "content-type;host;x-amz-content-sha256;x-amz-date";
  retval += method + "\n";
  retval += uri + "\n";
  retval += queryString + "\n";
  String headers =
      createCanonicalHeaders(contentType, date, time, payloadHash, apiKey);
  retval += headers + _signedHeaders + "\n";
  retval += payloadHash;
  return retval;
}

String createCanonicalHeaders(String contentType, String date, String time,
                              String payloadHash, String apiKey)
{
  String retval;
  retval += "content-type:" + contentType + "\n";
  retval += "host:" + FQDN() + "\n";
  retval += "x-amz-content-sha256:" + payloadHash + "\n";
  retval += "x-amz-date:" + date + "T" + time + "Z\n\n";
  return retval;
}

String createRequestHeaders(String contentType, String date, String time,
                            String payload, String payloadHash, String apiKey,
                            String signature)
{
  String retval;
  String _signedHeaders = "content-type;host;x-amz-content-sha256;x-amz-date";
  retval += "Content-Type: " + contentType + "\r\n";
  retval += "Connection: close\r\n";
  retval += "Content-Length: " + String(payload.length()) + "\r\n";
  retval += "x-api-key: " + apiKey + "\r\n";
  retval += "Host: " + FQDN() + "\r\n";
  retval += "x-amz-content-sha256: " + payloadHash + "\r\n";
  retval += "x-amz-date: " + date + "T" + time + "Z\r\n";
  retval += "Authorization: AWS4-HMAC-SHA256 Credential=" + String(awsKey) +
            "/" + String(date) + "/" + String(region) + "/" + String(service) +
            "/aws4_request,SignedHeaders=" + _signedHeaders +
            ",Signature=" + signature + "\r\n";
  return retval;
}

String FQDN()
{
  String retval;
  if (((String)customFQDN).length() > 0)
  {
    retval = String(customFQDN);
  }
  else
  {
    retval = String(host) + "." + String(service) + "." + String(region) + "." +
             String(TLD);
  }
  return retval;
}

String createStringToSign(String canonical_request, String date, String time)
{
  Sha256.init();
  Sha256.print(canonical_request);
  String hash = hexHash(Sha256.result());

  String retval;
  retval += "AWS4-HMAC-SHA256\n";
  retval += date + "T" + time + "Z\n";
  retval +=
      date + "/" + String(region) + "/" + String(service) + "/aws4_request\n";
  retval += hash;
  return retval;
}

String createSignature(String toSign, String date)
{
  String key = "AWS4" + String(awsSecret);

  Sha256.initHmac((uint8_t *)key.c_str(), key.length());
  Sha256.print(date);
  uint8_t *hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print(String(region));
  hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print(String(service));
  hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print("aws4_request");
  hash = Sha256.resultHmac();

  Sha256.initHmac(hash, HASH_LENGTH);
  Sha256.print(toSign);
  hash = Sha256.resultHmac();

  return hexHash(hash);
}
/*---(END)----AWS API signing requests functions----------*/