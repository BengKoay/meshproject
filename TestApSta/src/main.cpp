// Code explanation

// 1. read EEPROM for node, ssid, pass
// 2. read AP or STA mode, isMESH true = mesh, isMESH false = AP
// 3. if STA mode, attempt connect Mesh Network
// 3.1. for 60 tries, will attempt connect Mesh Network, start counter "meshNotConnectedCounter"
// 3.1.1. if after 60 tries still unable to connect, will write isMESH false,
// 3.2. if between 60 tries , Mesh Network is connected, counter "meshNotConnectedCounter" resets
// 4. if AP mode, launch AP
// 4.1. will only set isMESH = true after typing name ssid pass

// to work on:
// set AP mode on purpose and not failover, during isMesh
// set timeout after AP?
// progmem
// asynctcp
//

#include <Arduino.h>

// #include <ESP8266WiFi.h>
// #include <ESP8266HTTPClient.h>
// #include <ESP8266WebServer.h>

#include <WiFi.h>
#include <WebServer.h>
#include <EEPROM.h>
// #include <painlessMesh.h>
#include "namedMesh.h"
#include "SHT31.h"

// define

// some gpio pin that is connected to an LED...
// on my rig, this is 5, change to the right number of your LED.
#define LED 2              // GPIO number of connected LED, ON ESP-12 IS GPIO2
#define SHT31_ADDRESS 0x44 // define SHT address

#define BLINK_PERIOD 3000  // milliseconds until cycle repeat
#define BLINK_DURATION 100 // milliseconds LED is on for
#define MESH_PORT 5555

//Variables
int i = 0;
int statusCode;
// const char *ssid = "";       // not used?
// const char *passphrase = ""; // not used?
String st;
String content;
bool isMESH = false; // isMESH true = mesh, isMESH false = AP
String enodeName;
String esid;
String epass = "";

Scheduler userScheduler; // to control your personal task

// painlessMesh mesh;
namedMesh mesh;
SHT31 sht; // declare sht variable
bool calc_delay = false;
SimpleList<uint32_t> nodes;
int meshNotConnectedCounter = 0;
int APcounter = 0;

// maybe use blink without delay concept

// declare currentMillis huge value to make sure wifi can connect
unsigned long currentMillis = 10000;

// constants won't change:
const long interval = 1000; // interval at which to blink (milliseconds)

const long intervalCheckMesh = 1000; // interval at which to blink (milliseconds)
const long intervalCheckAP = 60000;  // interval at which to blink (milliseconds)

//Function Declaration

void setupMesh();
bool testWifi(void);
void launchWeb(void);
void setupAP(void);

void createWebServer();

// prototype

void receivedCallback(uint32_t from, String &msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback();
void nodeTimeAdjustedCallback(int32_t offset);
void delayReceivedCallback(uint32_t from, int32_t delay);
void requestDataSHT();
void checkMeshConnected();

Task taskSendMessage(TASK_SECOND * 5, TASK_FOREVER, &requestDataSHT); // start with a one second interval

//Establishing Local server at port 80 whenever required
// ESP8266WebServer server(80);
WebServer server(80);

// Task to blink the number of nodes
Task blinkNoNodes;
bool onFlag = false;

void setup()
{
  // 1. read EEPROM for node, ssid, pass
  // 2. read AP or STA mode
  // 3. if STA mode, attempt connect wifi
  // 4. if AP mode, launch AP

  // read EEPROM
  Serial.begin(115200); //Initialising if(DEBUG)Serial Monitor
  Serial.println();
  Serial.println("Disconnecting previously connected WiFi");
  WiFi.disconnect();
  EEPROM.begin(512); //Initialasing EEPROM
  delay(10);
  // Serial.println("clearing eeprom");
  // for (int i = 0; i < 512; ++i)
  // {
  //   EEPROM.write(i, 0);
  // }
  // EEPROM.commit();
  // delay(10);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.println();
  Serial.println();
  Serial.println("Startup");

  //---------------------------------------- Read EEPROM for Node name, SSID and pass

  for (int i = 0; i < 32; ++i)
  {
    enodeName += char(EEPROM.read(i));
  }
  Serial.println();
  Serial.print("NODE NAME: ");
  Serial.println(enodeName);

  Serial.println("Reading EEPROM ssid");

  //---------------------------------------- Start Read EEPROM for SSID
  for (int i = 32; i < 64; ++i)
  {
    esid += char(EEPROM.read(i));
  }
  Serial.println();
  Serial.print("SSID: ");
  Serial.println(esid);

  //---------------------------------------- Done Read EEPROM for SSID
  //---------------------------------------- Start Read EEPROM for pass
  Serial.println("Reading EEPROM pass");

  for (int i = 64; i < 96; ++i)
  {
    epass += char(EEPROM.read(i));
  }
  Serial.print("PASS: ");
  Serial.println(epass);

  //---------------------------------------- Done Read EEPROM for pass

  //---------------------------------------- Done EEPROM for SSID and pass

  // read EEPROM for isMESH
  isMESH = bool(EEPROM.read(96)); // position right after epass
  Serial.print("isMESH");
  Serial.print(isMESH);

  //---------------------------------------- Init wifi
  if (isMESH)
  {
    sht.begin(SHT31_ADDRESS);
    sht.requestData();
    pinMode(LED, OUTPUT);
    setupMesh();
    Serial.print("setupMesh mesh");
  }
  else
  {
    WiFi.begin(esid.c_str(), epass.c_str());
    if (testWifi())
    {
      Serial.println("Succesfully Connected!!!");
      return;
    }
    else
    {
      Serial.println("Turning the HotSpot On");
      launchWeb();
      setupAP(); // Setup HotSpot
    }

    Serial.println();
    Serial.println("Waiting.");

    while ((WiFi.status() != WL_CONNECTED))
    {
      Serial.print(".");
      delay(100);
      server.handleClient();
      APcounter++;
      currentMillis = millis();
      // if (APcounter > 100)
      unsigned long previousMillis = 0; // will store last time LED was updated
      // Serial.println("currentMillis");
      // Serial.println(currentMillis);
      // Serial.println("previousMillis");
      // Serial.println(previousMillis);
      // Serial.println("intervalCheckAP");
      // Serial.println(intervalCheckAP);

      if (currentMillis - previousMillis >= intervalCheckAP)
      {
        Serial.println("Failed AP setting timeout");
        EEPROM.write(96, (bool)true); // set to isMesh true
        EEPROM.commit();
        ESP.restart();
      }
    }
  }
}
void loop()
{

  // Serial.print("loop mesh");
  // mesh.update();
  // digitalWrite(LED, !onFlag);
  if (isMESH)
  {
    // Serial.print("loop mesh");
    mesh.update();
    digitalWrite(LED, !onFlag);
  }
  else
  { // probably not used
    if ((WiFi.status() == WL_CONNECTED))
    {

      for (int i = 0; i < 10; i++)
      {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(1000);
        digitalWrite(LED_BUILTIN, LOW);
        delay(1000);
      }
    }
  }
}

//-------- Functions used for WiFi credentials saving and connecting to it which you do not need to change
bool testWifi(void)
{
  // int c = 0;
  Serial.println("Waiting for Wifi to connect");
  // Generally, you should use "unsigned long" for variables that hold time
  // The value will quickly become too large for an int to store
  unsigned long previousMillis = 0; // will store last time LED was updated

  Serial.print("currentMillis");
  Serial.print(currentMillis);
  Serial.print("previousMillis");
  Serial.print(previousMillis);
  Serial.print("interval");
  Serial.print(interval);
  while (currentMillis - previousMillis >= interval)
  {
    currentMillis = millis();
    if (WiFi.status() == WL_CONNECTED)
    {
      previousMillis = currentMillis;
      return true;
    }
    // save the last time you blinked the LED
    // delay(500);
    // Serial.print("*");
    // c++;
    // if the LED is off turn it on and vice-versa:
    // if (ledState == LOW)
    // {
    //   ledState = HIGH;
    // }
    // else
    // {
    //   ledState = LOW;
    // }

    // set the LED with the ledState of the variable:
    // digitalWrite(ledPin, ledState);
  }
  // while (c < 20)
  // {
  //   if (WiFi.status() == WL_CONNECTED)
  //   {
  //     return true;
  //   }
  //   delay(500);
  //   Serial.print("*");
  //   c++;
  // }
  Serial.println("");
  Serial.println("Connect timed out, opening AP");
  return false;
}

void launchWeb()
{
  Serial.println("");
  if (WiFi.status() == WL_CONNECTED)
    Serial.println("WiFi connected");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("SoftAP IP: ");
  Serial.println(WiFi.softAPIP());
  createWebServer();
  // Start the server
  server.begin();
  Serial.println("Server started");
}

void setupAP(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      // Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
  st = "<ol>";
  for (int i = 0; i < n; ++i)
  {
    // Print SSID and RSSI for each network found
    st += "<li>";
    st += WiFi.SSID(i);
    st += " (";
    st += WiFi.RSSI(i);

    st += ")";
    // st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
    st += (WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? " " : "*";
    st += "</li>";
  }
  st += "</ol>";
  delay(100);
  WiFi.softAP("APtest", ""); // set as AP with APSSID and empty Pass
  Serial.println("softap");
  launchWeb();
  Serial.println("over");
}

void createWebServer()
{
  {
    server.on("/", []()
              {
                IPAddress ip = WiFi.softAPIP();
                String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
                content = "<!DOCTYPE HTML>\r\n<html>Hello from ESP8266 at ";
                content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"scan\"></form>";
                content += ipStr;
                content += "<p>";
                content += st;
                content += "</p><form method='get' action='setting'><label>SSID: </label><input name='nodeName' length=32><input name='ssid' length=64><input name='pass' length=96><input type='submit'></form>";
                content += "</html>";
                server.send(200, "text/html", content);
              });
    server.on("/scan", []()
              {
                //setupAP();
                IPAddress ip = WiFi.softAPIP();
                String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);

                content = "<!DOCTYPE HTML>\r\n<html>go back";
                server.send(200, "text/html", content);
              });

    server.on("/setting", []()
              {
                String qnodename = server.arg("nodeName");
                String qsid = server.arg("ssid");
                String qpass = server.arg("pass");
                if (qnodename.length() > 0 && qsid.length() > 0 && qpass.length() > 0)
                {
                  Serial.println("clearing eeprom");
                  for (int i = 0; i < 96; ++i)
                  {
                    EEPROM.write(i, 0);
                  }
                  Serial.println(qnodename);
                  Serial.println("");
                  Serial.println(qsid);
                  Serial.println("");
                  Serial.println(qpass);
                  Serial.println("");

                  Serial.println("writing eeprom node name:");
                  for (int i = 0; i < qnodename.length(); ++i)
                  {
                    EEPROM.write(i, qnodename[i]);
                    Serial.print("Wrote: ");
                    Serial.println(qnodename[i]);
                  }

                  Serial.println("writing eeprom ssid:");
                  for (int i = 0; i < qsid.length(); ++i)
                  {
                    EEPROM.write(32 + i, qsid[i]);
                    Serial.print("Wrote: ");
                    Serial.println(qsid[i]);
                  }
                  Serial.println("writing eeprom pass:");
                  for (int i = 0; i < qpass.length(); ++i)
                  {
                    EEPROM.write(64 + i, qpass[i]);
                    Serial.print("Wrote: ");
                    Serial.println(qpass[i]);
                  }
                  EEPROM.write(96, (bool)true); // write isMesh = true
                  EEPROM.commit();

                  content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
                  statusCode = 200;
                  // ESP.reset();
                  // write isMESH to true

                  ESP.restart();
                }
                else
                {
                  content = "{\"Error\":\"404 not found\"}";
                  statusCode = 404;
                  Serial.println("Sending 404");
                }
                server.sendHeader("Access-Control-Allow-Origin", "*");
                server.send(statusCode, "application/json", content);
              });
  }
}

void setupMesh()
{

  // mesh.setDebugMsgTypes(ERROR | DEBUG); // set before init() so that you can see error messages
  mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE | DEBUG);
  // mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);

  mesh.init(esid, epass, &userScheduler, MESH_PORT);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  mesh.onNodeDelayReceived(&delayReceivedCallback);
  // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
  // mesh.setContainsRoot(true);
  mesh.setName(enodeName); // This needs to be an unique name!

  userScheduler.addTask(taskSendMessage);
  taskSendMessage.enable();

  blinkNoNodes.set(BLINK_PERIOD, (mesh.getNodeList().size() + 1) * 2, []()
                   {
                     // If on, switch off, else switch on
                     if (onFlag)
                       onFlag = false;
                     else
                       onFlag = true;
                     blinkNoNodes.delay(BLINK_DURATION);

                     if (blinkNoNodes.isLastIteration())
                     {
                       // Finished blinking. Reset task for next run
                       // blink number of nodes (including this node) times
                       blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
                       // Calculate delay based on current mesh time and BLINK_PERIOD
                       // This results in blinks between nodes being synced
                       blinkNoNodes.enableDelayed(BLINK_PERIOD -
                                                  (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);
                     }
                     Serial.println(mesh.getNodeList().size());
                     Serial.println("mesh.getNodeList().size()");
                     checkMeshConnected();
                   });
  userScheduler.addTask(blinkNoNodes);
  blinkNoNodes.enable();

  randomSeed(analogRead(A0));
}

void receivedCallback(uint32_t from, String &msg)
{
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
}

void newConnectionCallback(uint32_t nodeId)
{
  // Reset blink task
  onFlag = false;
  blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
  blinkNoNodes.enableDelayed(BLINK_PERIOD - (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);

  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
  Serial.printf("--> startHere: New Connection, %s\n", mesh.subConnectionJson(true).c_str());
}

void changedConnectionCallback()
{
  Serial.printf("Changed connections\n");
  // Reset blink task
  onFlag = false;
  blinkNoNodes.setIterations((mesh.getNodeList().size() + 1) * 2);
  blinkNoNodes.enableDelayed(BLINK_PERIOD - (mesh.getNodeTime() % (BLINK_PERIOD * 1000)) / 1000);

  nodes = mesh.getNodeList();

  Serial.printf("Num nodes: %d\n", nodes.size());
  Serial.printf("Connection list:");

  SimpleList<uint32_t>::iterator node = nodes.begin();
  while (node != nodes.end())
  {
    Serial.printf(" %u", *node);
    node++;
  }
  Serial.println();
  calc_delay = true;
}

void nodeTimeAdjustedCallback(int32_t offset)
{
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

void delayReceivedCallback(uint32_t from, int32_t delay)
{
  Serial.printf("Delay to node %u is %d us\n", from, delay);
}
void requestDataSHT()
{
  uint16_t rawTemperature;
  uint16_t rawHumidity;
  String to = "Root";
  if (sht.dataReady())
  {
    bool success = sht.readData();
    sht.requestData(); // request for next sample
    if (success == false)
    {
      Serial.println("Failed read");
    }
    else
    {
      rawTemperature = sht.getRawTemperature();
      rawHumidity = sht.getRawHumidity();
      Serial.print(rawTemperature * (175.0 / 65535) - 45, 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.print("°C\t");
      Serial.print(rawHumidity * (100.0 / 65535), 1); // This formula comes from page 14 of the SHT31 datasheet
      Serial.println("%\t");
    }
  }

  DynamicJsonDocument doc(512);
  String msg;

  // if (pm_stage == 0 && nodesize > 0) //send timestamp
  // {

  float temperature;
  float humidity;
  temperature = (rawTemperature * (175.0 / 65535) - 45);
  humidity = (rawHumidity * (100.0 / 65535));
  // doc["name"] = nodeName; // time stamp info as payload
  doc["name"] = enodeName;

  doc["temperature"] = temperature; // temperature info as payload
  doc["humidity"] = humidity;       // humidity info as payload
  // doc["Timestamp"] = NTPEpochTime; // time stamp info as payload
  serializeJson(doc, msg);
  // mesh.sendBroadcast(msg);
  mesh.sendSingle(to, msg);
  taskSendMessage.setInterval(TASK_SECOND * 1);
  // mesh.sendSingle(nodeID[node_targetindex], msg);
  // Serial.printf("mesh_tx aws ID: %u msg: %s\n", nodeID[node_targetindex], msg.c_str()); //display tx msg
  // pm_sendstart = millis();
  // pm_stage = 1;
  // }
}

void checkMeshConnected()
{

  if ((mesh.getNodeList().size() == 0)) // mesh == 0 means not connected
  {
    meshNotConnectedCounter++;
    if (meshNotConnectedCounter > 20)
    {
      Serial.println("Failed connection timeout");
      EEPROM.write(96, (bool)false);
      EEPROM.commit();
      ESP.restart();
    }

    // Serial.println("Failed connection");
    // // insert a timing code
    // // check to see if it's time to blink the LED; that is, if the difference
    // // between the current time and last time you blinked the LED is bigger than
    // // the interval at which you want to blink the LED.
    // // unsigned long currentMillis = millis();
    // currentMillis = millis();
    // unsigned long previousMillis = currentMillis; // will store last time LED was updated

    // Serial.print("currentMillis");
    // Serial.print(currentMillis);
    // Serial.print("previousMillis");
    // Serial.print(previousMillis);
    // Serial.print("intervalCheckMesh");
    // Serial.print(intervalCheckMesh);
    // if (currentMillis - previousMillis >= intervalCheckMesh)
    // {
    //   // time taken for check mesh, after previousMillis = 1000
    //   // set to isMesh false
    //   Serial.println("Failed connection timeout");
    //   EEPROM.write(96, (bool)false);
    //   EEPROM.commit();
    //   ESP.restart();
    //   // save the last time you blinked the LED
    //   // previousMillis = currentMillis;

    //   // // if the LED is off turn it on and vice-versa:
    //   // if (ledState == LOW)
    //   // {
    //   //   ledState = HIGH;
    //   // }
    //   // else
    //   // {
    //   //   ledState = LOW;
    //   // }

    //   // set the LED with the ledState of the variable:
    //   // digitalWrite(ledPin, ledState);
    // }
  }
  else
  {
    // refresh counter
    meshNotConnectedCounter = 0;
  }
}